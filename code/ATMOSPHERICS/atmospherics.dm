/*
Quick overview:

Pipes combine to form pipelines
Pipelines and other atmospheric objects combine to form pipe_networks
	Note: A single pipe_network represents a completely open space

Pipes -> Pipelines
Pipelines + Other Objects -> Pipe network

*/

obj/machinery/atmospherics
	anchored = 1
	use_power = 0
	idle_power_usage = 0
	active_power_usage = 0
	power_channel = ENVIRON
	var/image/vis
	var/nodealert = 0



obj/machinery/atmospherics/var/initialize_directions = 0
obj/machinery/atmospherics/var/_color

obj/machinery/atmospherics/process()
	build_network()

obj/machinery/atmospherics/proc/network_expand(datum/pipe_network/new_network, obj/machinery/atmospherics/pipe/reference)
	// Check to see if should be added to network. Add self if so and adjust variables appropriately.
	// Note don't forget to have neighbors look as well!

	return null

obj/machinery/atmospherics/proc/build_network()
	// Called to build a network from this node

	return null

obj/machinery/atmospherics/proc/return_network(obj/machinery/atmospherics/reference)
	// Returns pipe_network associated with connection to reference
	// Notes: should create network if necessary
	// Should never return null

	return null

obj/machinery/atmospherics/proc/reassign_network(datum/pipe_network/old_network, datum/pipe_network/new_network)
	// Used when two pipe_networks are combining

obj/machinery/atmospherics/proc/return_network_air(datum/network/reference)
	// Return a list of gas_mixture(s) in the object
	//		associated with reference pipe_network for use in rebuilding the networks gases list
	// Is permitted to return null

obj/machinery/atmospherics/proc/disconnect(obj/machinery/atmospherics/reference)

obj/machinery/atmospherics/update_icon()
	return null

obj/machinery/atmospherics/Del()
	if(vis) del(vis)
	..()

obj/machinery/atmospherics/proc/mingle_with_turf(turf/simulated/target, var/datum/gas_mixture/air_sample)
	if(istype(target) && target.parent && target.parent.group_processing)
		//Have to consider preservation of group statuses
		var/datum/gas_mixture/turf_copy = new

		turf_copy.copy_from(target.parent.air)
		turf_copy.volume = target.parent.air.volume //Copy a good representation of the turf from parent group

		equalize_gases(list(air_sample, turf_copy))

		if(target.parent.air.compare(turf_copy))
			//The new turf would be an acceptable group member so permit the integration
			turf_copy.subtract(target.parent.air)
			target.parent.air.merge(turf_copy)
		else
			//Comparison failure so dissemble group and copy turf
			target.parent.suspend_group_processing()
			target.air.copy_from(turf_copy)

	else if(!istype(target,/turf/space))
		var/datum/gas_mixture/turf_air = target.return_air()

		equalize_gases(list(air_sample, turf_air))
		//turf_air already modified by equalize_gases()

	if(istype(target) && !target.processing)
		if(target.air)
			if(target.air.check_tile_graphic())
				target.update_visuals(target.air)

obj/machinery/atmospherics/proc/aircontent_leak(var/direction, var/datum/gas_mixture/air_contents)
	if(!LeakSmokeEffectIcon)
		LeakSmokeEffectIcon = new/icon('icons/effects/effects.dmi',"extinguish")
		LeakSmokeEffectIcon.GrayScale()

	if(direction)
		var/obj/effect/steam/SE = new(src.loc)
		SE.invisibility = 101
		if(loc.density)
			var/turf/testT = get_step(loc,direction)
			if(!testT.density)
				SE.loc = testT
		else
			step(SE,direction)
		var/turf/T = SE.loc

		var/datum/gas_mixture/locair = T.return_air()
		if(abs(air_contents.return_pressure()-locair.return_pressure())>1)
			mingle_with_turf(T, air_contents)
			if(air_contents.return_pressure()-locair.return_pressure()>15)
				SE.icon = LeakSmokeEffectIcon
				SE.icon_state=""
				SE.opacity = 0
				SE.invisibility = 0
				spawn(5)
					del(SE)
			else
				del(SE)
		else
			del(SE)
	if(!(src in machines))
		machines += src
