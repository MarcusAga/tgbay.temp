/*
What are the archived variables for?
	Calculations are done using the archived variables with the results merged into the regular variables.
	This prevents race conditions that arise based on the order of tile processing.
*/

#define SHARE_PART 5

#define SPECIFIC_HEAT_TOXIN		200
#define SPECIFIC_HEAT_AIR		20
#define SPECIFIC_HEAT_CDO		30
#define SPECIFIC_HEAT_N2O		40
#define SPECIFIC_HEAT_VFUEL		30
#define HEAT_CAPACITY_CALCULATION(oxygen,carbon_dioxide,nitrogen,toxins,sleeping_agent,volatile_fuel) \
	(carbon_dioxide*SPECIFIC_HEAT_CDO + (oxygen+nitrogen)*SPECIFIC_HEAT_AIR + toxins*SPECIFIC_HEAT_TOXIN + sleeping_agent*SPECIFIC_HEAT_N2O + volatile_fuel*SPECIFIC_HEAT_VFUEL)

#define MINIMUM_HEAT_CAPACITY	0.0003
#define QUANTIZE(variable)		(round(variable,0.001))

/datum/gas_mixture
	var/oxygen = 0
	var/carbon_dioxide = 0
	var/nitrogen = 0
	var/toxins = 0
	var/sleeping_agent = 0
	var/volatile_fuel = 0

	var/volume = CELL_VOLUME

	var/temperature = 0 //in Kelvin, use calculate_temperature() to modify

	var/group_multiplier = 1
		//Size of the group this gas_mixture is representing.
		//=1 for singletons

	var/graphic

	var/tmp/oxygen_archived
	var/tmp/carbon_dioxide_archived
	var/tmp/nitrogen_archived
	var/tmp/toxins_archived
	var/tmp/sleeping_agent_archived
	var/tmp/volatile_fuel_archived

	var/tmp/temperature_archived

	var/tmp/graphic_archived
	var/tmp/fuel_burnt = 0

	var/tmp/total_moles_archived

	proc/update_values()

	proc/adjust(o2 = 0, co2 = 0, n2 = 0, tx = 0, n2o = 0, fuel = 0)
		//Purpose: Adjusting the gases within a airmix
		//Called by: Nothing, yet!
		//Inputs: The values of the gases to adjust
		//Outputs: null

		oxygen = max(0, oxygen + o2)
		carbon_dioxide = max(0, carbon_dioxide + co2)
		nitrogen = max(0, nitrogen + n2)
		toxins = max(0, toxins + tx)
		sleeping_agent = max(0, sleeping_agent + n2o)
		volatile_fuel = max(0, volatile_fuel + fuel)

		//handle trace gasses
		update_values()
		return

	//PV=nRT - related procedures
	proc/heat_capacity()
		return HEAT_CAPACITY_CALCULATION(oxygen,carbon_dioxide,nitrogen,toxins,sleeping_agent,volatile_fuel)


	proc/heat_capacity_archived()
		return HEAT_CAPACITY_CALCULATION(oxygen_archived,carbon_dioxide_archived,nitrogen_archived,toxins_archived,sleeping_agent_archived,volatile_fuel_archived)


	proc/total_moles()
		var/moles = oxygen + carbon_dioxide + nitrogen + toxins + sleeping_agent + volatile_fuel
		return moles

	proc/total_moles_archived()
		return total_moles_archived


	proc/return_pressure()
		if(temperature<0.001)
			temperature = 0.001
		if(volume>0)
			return total_moles()*R_IDEAL_GAS_EQUATION*temperature/volume
		return 0


	proc/return_temperature()
		if(temperature<0)
			temperature = 0
		return temperature


	proc/return_volume()
		return max(0, volume)


	proc/thermal_energy()
		return temperature*heat_capacity()


	//Procedures used for very specific events
	proc/check_tile_graphic()
		//returns 1 if graphic changed
		graphic = null
		if(toxins > MOLES_PLASMA_VISIBLE)
			graphic = "plasma"
		else
			if(sleeping_agent > 1)
				graphic = "sleeping_agent"
			else
				graphic = null

		return graphic != graphic_archived

	proc/react(atom/dump_location)
		var/reacting = 0 //set to 1 if a notable reaction occured (used by pipe_network)

		fuel_burnt = 0
		if(temperature > FIRE_MINIMUM_TEMPERATURE_TO_EXIST)
			//world << "pre [temperature], [oxygen], [toxins]"
			var/obj/effect/decal/cleanable/liquid_fuel/liquid
			if(dump_location)
				liquid = locate() in dump_location
//				if(liquid)
//					world<<"location \ref[dump_location], liquid \ref[liquid]"

			if(fire(liquid) > 0)
				reacting = 1
			//world << "post [temperature], [oxygen], [toxins]"

		return reacting

	proc/fire(var/obj/effect/decal/cleanable/liquid_fuel/liquid)
		var/energy_released = 0
		var/old_heat_capacity = heat_capacity()

		if(volatile_fuel) //General volatile gas burn
			var/burned_fuel = 0

			if(oxygen < volatile_fuel)
				burned_fuel = oxygen
				volatile_fuel -= burned_fuel
				oxygen = 0
			else
				burned_fuel = volatile_fuel
				oxygen -= volatile_fuel

			energy_released += FIRE_CARBON_ENERGY_RELEASED * burned_fuel
			carbon_dioxide += burned_fuel
			fuel_burnt += burned_fuel

		if(liquid) //General volatile gas burn
			var/to_burn = min(min(liquid.amount,oxygen),1.0)

			liquid.amount = liquid.amount - to_burn
			oxygen -= to_burn

			if(liquid.amount <= 0)
				del(liquid)

			energy_released += FIRE_CARBON_ENERGY_RELEASED * to_burn * 0.1
			carbon_dioxide += to_burn
			fuel_burnt += to_burn

		//Handle plasma burning
		if(toxins > MINIMUM_HEAT_CAPACITY)
			var/plasma_burn_rate = 0
			var/oxygen_burn_rate = 0
			//more plasma released at higher temperatures
			var/temperature_scale
			if(temperature > PLASMA_UPPER_TEMPERATURE)
				temperature_scale = 1
			else
				temperature_scale = (temperature-PLASMA_MINIMUM_BURN_TEMPERATURE)/(PLASMA_UPPER_TEMPERATURE-PLASMA_MINIMUM_BURN_TEMPERATURE)
			if(temperature_scale > 0)
				oxygen_burn_rate = 1.4 - temperature_scale
				if(oxygen > toxins*PLASMA_OXYGEN_FULLBURN)
					plasma_burn_rate = (toxins*temperature_scale)/4
				else
					plasma_burn_rate = (temperature_scale*(oxygen/PLASMA_OXYGEN_FULLBURN))/4
				if(plasma_burn_rate > MINIMUM_HEAT_CAPACITY)
					toxins -= plasma_burn_rate
					oxygen -= plasma_burn_rate*oxygen_burn_rate
					carbon_dioxide += plasma_burn_rate

					energy_released += FIRE_PLASMA_ENERGY_RELEASED * (plasma_burn_rate)

					fuel_burnt += (plasma_burn_rate)*(1+oxygen_burn_rate)

		if(energy_released > 0)
			var/new_heat_capacity = heat_capacity()
			if(new_heat_capacity > MINIMUM_HEAT_CAPACITY)
				temperature = (temperature*old_heat_capacity + energy_released)/new_heat_capacity

		return fuel_burnt

	proc/archive()
		//Update archived versions of variables
		//Returns: 1 in all cases

	proc/merge(datum/gas_mixture/giver)
			//Merges all air from giver into self. Deletes giver.
			//Returns: 1 on success (no failure cases yet)

	proc/check_then_merge(datum/gas_mixture/giver)
			//Similar to merge(...) but first checks to see if the amount of air assumed is small enough
			//	that group processing is still accurate for source (aborts if not)
			//Returns: 1 on successful merge, 0 if the check failed

	proc/remove(amount)
			//Proportionally removes amount of gas from the gas_mixture
			//Returns: gas_mixture with the gases removed

	proc/remove_ratio(ratio)
			//Proportionally removes amount of gas from the gas_mixture
			//Returns: gas_mixture with the gases removed

	proc/subtract(datum/gas_mixture/right_side)
			//Subtracts right_side from air_mixture. Used to help turfs mingle

	proc/check_then_remove(amount)
			//Similar to remove(...) but first checks to see if the amount of air removed is small enough
			//	that group processing is still accurate for source (aborts if not)
			//Returns: gas_mixture with the gases removed or null

	proc/copy_from(datum/gas_mixture/sample)
			//Copies variables from sample

	proc/share(datum/gas_mixture/sharer)
			//Performs air sharing calculations between two gas_mixtures assuming only 1 boundary length
			//Return: amount of gas exchanged (+ if sharer received)

	proc/mimic(turf/model)
			//Similar to share(...), except the model is not modified
			//Return: amount of gas exchanged

	proc/check_gas_mixture(datum/gas_mixture/sharer)
			//Returns: 0 if the self-check failed then -1 if sharer-check failed then 1 if both checks pass

	proc/check_turf(turf/model)
			//Returns: 0 if self-check failed or 1 if check passes

	//	check_me_then_share(datum/gas_mixture/sharer)
			//Similar to share(...) but first checks to see if amount of air moved is small enough
			//	that group processing is still accurate for source (aborts if not)
			//Returns: 1 on successful share, 0 if the check failed

	//	check_me_then_mimic(turf/model)
			//Similar to mimic(...) but first checks to see if amount of air moved is small enough
			//	that group processing is still accurate (aborts if not)
			//Returns: 1 on successful mimic, 0 if the check failed

	//	check_both_then_share(datum/gas_mixture/sharer)
			//Similar to check_me_then_share(...) but also checks to see if amount of air moved is small enough
			//	that group processing is still accurate for the sharer (aborts if not)
			//Returns: 0 if the self-check failed then -1 if sharer-check failed then 1 if successful share


	proc/temperature_mimic(turf/model, conduction_coefficient)

	proc/temperature_share(datum/gas_mixture/sharer, conduction_coefficient)

	proc/temperature_turf_share(turf/simulated/sharer, conduction_coefficient)


	proc/check_me_then_temperature_mimic(turf/model, conduction_coefficient)

	proc/check_me_then_temperature_share(datum/gas_mixture/sharer, conduction_coefficient)

	proc/check_both_then_temperature_share(datum/gas_mixture/sharer, conduction_coefficient)

	proc/check_me_then_temperature_turf_share(turf/simulated/sharer, conduction_coefficient)

	proc/compare(datum/gas_mixture/sample)
			//Compares sample to self to see if within acceptable ranges that group processing may be enabled

	archive()
		oxygen_archived = oxygen
		carbon_dioxide_archived = carbon_dioxide
		nitrogen_archived = nitrogen
		toxins_archived = toxins
		sleeping_agent_archived = sleeping_agent
		volatile_fuel_archived = volatile_fuel

		temperature_archived = temperature

		graphic_archived = graphic

		total_moles_archived = total_moles()

		return 1

	check_then_merge(datum/gas_mixture/giver)
		if(!giver)
			return 0
		if(((giver.oxygen > MINIMUM_AIR_TO_SUSPEND) && (giver.oxygen >= oxygen*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((giver.carbon_dioxide > MINIMUM_AIR_TO_SUSPEND) && (giver.carbon_dioxide >= carbon_dioxide*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((giver.nitrogen > MINIMUM_AIR_TO_SUSPEND) && (giver.nitrogen >= nitrogen*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((giver.toxins > MINIMUM_AIR_TO_SUSPEND) && (giver.toxins >= toxins*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((giver.sleeping_agent > MINIMUM_AIR_TO_SUSPEND) && (giver.sleeping_agent >= sleeping_agent*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((giver.volatile_fuel > MINIMUM_AIR_TO_SUSPEND) && (giver.volatile_fuel >= volatile_fuel*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			)
			return 0
		if(abs(giver.temperature - temperature) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND)
			return 0

		return merge(giver)

	merge(datum/gas_mixture/giver)
		if(!giver)
			return 0

		if(abs(temperature-giver.temperature)>MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/self_heat_capacity = heat_capacity()*group_multiplier
			var/giver_heat_capacity = giver.heat_capacity()*giver.group_multiplier
			var/combined_heat_capacity = giver_heat_capacity + self_heat_capacity
			if(combined_heat_capacity != 0)
				temperature = (giver.temperature*giver_heat_capacity + temperature*self_heat_capacity)/combined_heat_capacity

		if((group_multiplier>1)||(giver.group_multiplier>1))
			oxygen += giver.oxygen*giver.group_multiplier/group_multiplier
			carbon_dioxide += giver.carbon_dioxide*giver.group_multiplier/group_multiplier
			nitrogen += giver.nitrogen*giver.group_multiplier/group_multiplier
			toxins += giver.toxins*giver.group_multiplier/group_multiplier
			sleeping_agent += giver.sleeping_agent*giver.group_multiplier/group_multiplier
			volatile_fuel += giver.volatile_fuel*giver.group_multiplier/group_multiplier
		else
			oxygen += giver.oxygen
			carbon_dioxide += giver.carbon_dioxide
			nitrogen += giver.nitrogen
			toxins += giver.toxins
			sleeping_agent += giver.sleeping_agent
			volatile_fuel += giver.volatile_fuel

		return 1

	remove(amount)
		var/sum = total_moles()
		amount = min(amount,sum) //Can not take more air than tile has!

		var/datum/gas_mixture/removed = new
		removed.temperature = temperature
		if(amount <= 0)
			return removed

		removed.oxygen = QUANTIZE((oxygen/sum)*amount)
		removed.nitrogen = QUANTIZE((nitrogen/sum)*amount)
		removed.carbon_dioxide = QUANTIZE((carbon_dioxide/sum)*amount)
		removed.toxins = QUANTIZE((toxins/sum)*amount)
		removed.sleeping_agent = QUANTIZE((sleeping_agent/sum)*amount)
		removed.volatile_fuel = QUANTIZE((volatile_fuel/sum)*amount)

		oxygen -= removed.oxygen/group_multiplier
		nitrogen -= removed.nitrogen/group_multiplier
		carbon_dioxide -= removed.carbon_dioxide/group_multiplier
		toxins -= removed.toxins/group_multiplier
		sleeping_agent -= removed.sleeping_agent/group_multiplier
		volatile_fuel -= removed.volatile_fuel/group_multiplier

		return removed

	remove_ratio(ratio)

		if(ratio <= 0)
			return null

		ratio = min(ratio, 1)

		var/datum/gas_mixture/removed = new

		removed.oxygen = QUANTIZE(oxygen*ratio)
		removed.nitrogen = QUANTIZE(nitrogen*ratio)
		removed.carbon_dioxide = QUANTIZE(carbon_dioxide*ratio)
		removed.toxins = QUANTIZE(toxins*ratio)
		removed.sleeping_agent = QUANTIZE(sleeping_agent*ratio)
		removed.volatile_fuel = QUANTIZE(volatile_fuel*ratio)

		oxygen -= removed.oxygen/group_multiplier
		nitrogen -= removed.nitrogen/group_multiplier
		carbon_dioxide -= removed.carbon_dioxide/group_multiplier
		toxins -= removed.toxins/group_multiplier
		sleeping_agent -= removed.sleeping_agent/group_multiplier
		volatile_fuel -= removed.volatile_fuel/group_multiplier

		removed.temperature = temperature

		return removed

	check_then_remove(amount)

		//Since it is all proportional, the check may be done on the gas as a whole
		var/sum = total_moles()
		amount = min(amount,sum) //Can not take more air than tile has!

		if((amount > MINIMUM_AIR_RATIO_TO_SUSPEND) && (amount > sum*MINIMUM_AIR_RATIO_TO_SUSPEND))
			return 0

		return remove(amount)

	copy_from(datum/gas_mixture/sample)
		oxygen = sample.oxygen
		carbon_dioxide = sample.carbon_dioxide
		nitrogen = sample.nitrogen
		toxins = sample.toxins
		sleeping_agent = sample.sleeping_agent
		volatile_fuel = sample.volatile_fuel

		temperature = sample.temperature

		return 1

	subtract(datum/gas_mixture/right_side)
		oxygen -= right_side.oxygen
		carbon_dioxide -= right_side.carbon_dioxide
		nitrogen -= right_side.nitrogen
		toxins -= right_side.toxins
		sleeping_agent -= right_side.sleeping_agent
		volatile_fuel -= right_side.volatile_fuel

		return 1

	check_gas_mixture(datum/gas_mixture/sharer)
		if(!sharer)	return 0
		var/delta_oxygen = (oxygen_archived - sharer.oxygen_archived)/SHARE_PART
		var/delta_carbon_dioxide = (carbon_dioxide_archived - sharer.carbon_dioxide_archived)/SHARE_PART
		var/delta_nitrogen = (nitrogen_archived - sharer.nitrogen_archived)/SHARE_PART
		var/delta_toxins = (toxins_archived - sharer.toxins_archived)/SHARE_PART
		var/delta_sleeping_agent = (sleeping_agent_archived - sharer.sleeping_agent_archived)/SHARE_PART
		var/delta_volatile_fuel = (volatile_fuel_archived - sharer.volatile_fuel_archived)/SHARE_PART

		var/delta_temperature = (temperature_archived - sharer.temperature_archived)

		if(((abs(delta_oxygen) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_oxygen) >= oxygen_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_carbon_dioxide) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_carbon_dioxide) >= carbon_dioxide_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_nitrogen) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_nitrogen) >= nitrogen_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_toxins) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_toxins) >= toxins_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_sleeping_agent) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_sleeping_agent) >= sleeping_agent_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_volatile_fuel) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_volatile_fuel) >= volatile_fuel_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			)
			return 0

		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND)
			return 0

		if(((abs(delta_oxygen) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_oxygen) >= sharer.oxygen_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_carbon_dioxide) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_carbon_dioxide) >= sharer.carbon_dioxide_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_nitrogen) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_nitrogen) >= sharer.nitrogen_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_toxins) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_toxins) >= sharer.toxins_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_sleeping_agent) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_sleeping_agent) >= sharer.sleeping_agent_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_volatile_fuel) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_volatile_fuel) >= sharer.volatile_fuel_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			)
			return -1

		return 1

	check_turf(turf/model)
		var/delta_oxygen = (oxygen_archived - model.oxygen)/SHARE_PART
		var/delta_carbon_dioxide = (carbon_dioxide_archived - model.carbon_dioxide)/SHARE_PART
		var/delta_nitrogen = (nitrogen_archived - model.nitrogen)/SHARE_PART
		var/delta_toxins = (toxins_archived - model.toxins)/SHARE_PART
		var/delta_sleeping_agent = (sleeping_agent_archived - model.sleeping_agent)/SHARE_PART
		var/delta_volatile_fuel = (volatile_fuel_archived - model.volatile_fuel)/SHARE_PART

		var/delta_temperature = (temperature_archived - model.temperature)

		if(((abs(delta_oxygen) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_oxygen) >= oxygen_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_carbon_dioxide) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_carbon_dioxide) >= carbon_dioxide_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_nitrogen) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_nitrogen) >= nitrogen_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_toxins) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_toxins) >= toxins_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_sleeping_agent) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_sleeping_agent) >= sleeping_agent_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			|| ((abs(delta_volatile_fuel) > MINIMUM_AIR_TO_SUSPEND) && (abs(delta_volatile_fuel) >= volatile_fuel_archived*MINIMUM_AIR_RATIO_TO_SUSPEND)) \
			)
			return 0
		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND)
			return 0

		return 1

	share(datum/gas_mixture/sharer)
		if(!sharer)	return 0
		var/s_group_multiplier = sharer.group_multiplier

		var/delta_oxygen = QUANTIZE(oxygen_archived - sharer.oxygen_archived)/SHARE_PART
		var/delta_carbon_dioxide = QUANTIZE(carbon_dioxide_archived - sharer.carbon_dioxide_archived)/SHARE_PART
		var/delta_nitrogen = QUANTIZE(nitrogen_archived - sharer.nitrogen_archived)/SHARE_PART
		var/delta_toxins = QUANTIZE(toxins_archived - sharer.toxins_archived)/SHARE_PART
		var/delta_sleeping_agent = QUANTIZE(sleeping_agent_archived - sharer.sleeping_agent_archived)/SHARE_PART
		var/delta_volatile_fuel = QUANTIZE(volatile_fuel_archived - sharer.volatile_fuel_archived)/SHARE_PART

		var/delta_temperature = (temperature_archived - sharer.temperature_archived)

		var/old_self_heat_capacity = 0
		var/old_sharer_heat_capacity = 0

		var/heat_capacity_self_to_sharer = 0
		var/heat_capacity_sharer_to_self = 0

		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/delta_air = delta_oxygen+delta_nitrogen
			if(delta_air)
				var/air_heat_capacity = SPECIFIC_HEAT_AIR*delta_air
				if(delta_air > 0)
					heat_capacity_self_to_sharer += air_heat_capacity
				else
					heat_capacity_sharer_to_self -= air_heat_capacity

			if(delta_carbon_dioxide)
				var/carbon_dioxide_heat_capacity = SPECIFIC_HEAT_CDO*delta_carbon_dioxide
				if(delta_carbon_dioxide > 0)
					heat_capacity_self_to_sharer += carbon_dioxide_heat_capacity
				else
					heat_capacity_sharer_to_self -= carbon_dioxide_heat_capacity

			if(delta_toxins)
				var/toxins_heat_capacity = SPECIFIC_HEAT_TOXIN*delta_toxins
				if(delta_toxins > 0)
					heat_capacity_self_to_sharer += toxins_heat_capacity
				else
					heat_capacity_sharer_to_self -= toxins_heat_capacity

			if(delta_sleeping_agent)
				var/sleeping_agent_heat_capacity = SPECIFIC_HEAT_N2O*delta_sleeping_agent
				if(delta_sleeping_agent > 0)
					heat_capacity_self_to_sharer += sleeping_agent_heat_capacity
				else
					heat_capacity_sharer_to_self -= sleeping_agent_heat_capacity

			if(delta_volatile_fuel)
				var/volatile_fuel_heat_capacity = SPECIFIC_HEAT_VFUEL*delta_volatile_fuel
				if(delta_volatile_fuel > 0)
					heat_capacity_self_to_sharer += volatile_fuel_heat_capacity
				else
					heat_capacity_sharer_to_self -= volatile_fuel_heat_capacity

			old_self_heat_capacity = heat_capacity()*group_multiplier
			old_sharer_heat_capacity = sharer.heat_capacity()*s_group_multiplier

		oxygen -= delta_oxygen/group_multiplier
		sharer.oxygen += delta_oxygen/s_group_multiplier

		carbon_dioxide -= delta_carbon_dioxide/group_multiplier
		sharer.carbon_dioxide += delta_carbon_dioxide/s_group_multiplier

		nitrogen -= delta_nitrogen/group_multiplier
		sharer.nitrogen += delta_nitrogen/s_group_multiplier

		toxins -= delta_toxins/group_multiplier
		sharer.toxins += delta_toxins/s_group_multiplier

		sleeping_agent -= delta_sleeping_agent/group_multiplier
		sharer.sleeping_agent += delta_sleeping_agent/s_group_multiplier

		volatile_fuel -= delta_volatile_fuel/group_multiplier
		sharer.volatile_fuel += delta_volatile_fuel/s_group_multiplier

		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/new_self_heat_capacity = old_self_heat_capacity + heat_capacity_sharer_to_self - heat_capacity_self_to_sharer
			var/new_sharer_heat_capacity = old_sharer_heat_capacity + heat_capacity_self_to_sharer - heat_capacity_sharer_to_self

			if(new_self_heat_capacity > MINIMUM_HEAT_CAPACITY)
				temperature = (old_self_heat_capacity*temperature - heat_capacity_self_to_sharer*temperature_archived + heat_capacity_sharer_to_self*sharer.temperature_archived)/new_self_heat_capacity

			if(new_sharer_heat_capacity > MINIMUM_HEAT_CAPACITY)
				sharer.temperature = (old_sharer_heat_capacity*sharer.temperature-heat_capacity_sharer_to_self*sharer.temperature_archived + heat_capacity_self_to_sharer*temperature_archived)/new_sharer_heat_capacity

				if(abs(old_sharer_heat_capacity) > MINIMUM_HEAT_CAPACITY)
					if(abs(new_sharer_heat_capacity/old_sharer_heat_capacity - 1) < 0.10) // <10% change in sharer heat capacity
						temperature_share(sharer, OPEN_HEAT_TRANSFER_COEFFICIENT)

		var/delta_pressure = (temperature_archived*total_moles_archived() - sharer.temperature_archived*sharer.total_moles_archived())*R_IDEAL_GAS_EQUATION/volume
		if(delta_pressure > 0.1)
			return delta_pressure
		else
			return 0

	mimic(turf/model, border_multiplier)
		var/delta_oxygen = QUANTIZE(oxygen_archived - model.oxygen)/SHARE_PART
		var/delta_carbon_dioxide = QUANTIZE(carbon_dioxide_archived - model.carbon_dioxide)/SHARE_PART
		var/delta_nitrogen = QUANTIZE(nitrogen_archived - model.nitrogen)/SHARE_PART
		var/delta_toxins = QUANTIZE(toxins_archived - model.toxins)/SHARE_PART
		var/delta_sleeping_agent = QUANTIZE(sleeping_agent_archived - model.sleeping_agent)/SHARE_PART
		var/delta_volatile_fuel = QUANTIZE(volatile_fuel_archived - model.volatile_fuel)/SHARE_PART

		var/delta_temperature = (temperature_archived - model.temperature)

		var/heat_transferred = 0
		var/old_self_heat_capacity = 0
		var/heat_capacity_transferred = 0

		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)

			var/delta_air = delta_oxygen+delta_nitrogen
			if(delta_air)
				var/air_heat_capacity = SPECIFIC_HEAT_AIR*delta_air
				heat_transferred -= air_heat_capacity*model.temperature
				heat_capacity_transferred -= air_heat_capacity

			if(delta_carbon_dioxide)
				var/carbon_dioxide_heat_capacity = SPECIFIC_HEAT_CDO*delta_carbon_dioxide
				heat_transferred -= carbon_dioxide_heat_capacity*model.temperature
				heat_capacity_transferred -= carbon_dioxide_heat_capacity

			if(delta_toxins)
				var/toxins_heat_capacity = SPECIFIC_HEAT_TOXIN*delta_toxins
				heat_transferred -= toxins_heat_capacity*model.temperature
				heat_capacity_transferred -= toxins_heat_capacity

			if(delta_sleeping_agent)
				var/sleeping_agent_heat_capacity = SPECIFIC_HEAT_N2O*delta_sleeping_agent
				heat_transferred -= sleeping_agent_heat_capacity*model.temperature
				heat_capacity_transferred -= sleeping_agent_heat_capacity

			if(delta_volatile_fuel)
				var/volatile_fuel_heat_capacity = SPECIFIC_HEAT_VFUEL*delta_volatile_fuel
				heat_transferred -= volatile_fuel_heat_capacity*model.temperature
				heat_capacity_transferred -= volatile_fuel_heat_capacity

			old_self_heat_capacity = heat_capacity()*group_multiplier

		if(border_multiplier)
			oxygen -= delta_oxygen*border_multiplier/group_multiplier
			carbon_dioxide -= delta_carbon_dioxide*border_multiplier/group_multiplier
			nitrogen -= delta_nitrogen*border_multiplier/group_multiplier
			toxins -= delta_toxins*border_multiplier/group_multiplier
			sleeping_agent -= delta_sleeping_agent*border_multiplier/group_multiplier
			volatile_fuel -= delta_volatile_fuel*border_multiplier/group_multiplier
		else
			oxygen -= delta_oxygen/group_multiplier
			carbon_dioxide -= delta_carbon_dioxide/group_multiplier
			nitrogen -= delta_nitrogen/group_multiplier
			toxins -= delta_toxins/group_multiplier
			sleeping_agent -= delta_sleeping_agent/group_multiplier
			volatile_fuel -= delta_volatile_fuel/group_multiplier

//		var/moved_moles = (delta_oxygen + delta_carbon_dioxide + delta_nitrogen + delta_toxins)

		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/new_self_heat_capacity = old_self_heat_capacity - heat_capacity_transferred
			if(new_self_heat_capacity > MINIMUM_HEAT_CAPACITY)
				if(border_multiplier)
					temperature = (old_self_heat_capacity*temperature - heat_capacity_transferred*border_multiplier*temperature_archived)/new_self_heat_capacity
				else
					temperature = (old_self_heat_capacity*temperature - heat_capacity_transferred*border_multiplier*temperature_archived)/new_self_heat_capacity

			temperature_mimic(model, model.thermal_conductivity, border_multiplier)

		var/total_moles = model.oxygen+model.carbon_dioxide+model.nitrogen+model.sleeping_agent+model.volatile_fuel
		var/delta_pressure = (temperature_archived*total_moles_archived() - model.temperature*total_moles)*R_IDEAL_GAS_EQUATION/volume
		if(delta_pressure > 0.1)
			return delta_pressure
		else
			return 0

	check_both_then_temperature_share(datum/gas_mixture/sharer, conduction_coefficient)
		var/delta_temperature = (temperature_archived - sharer.temperature_archived)

		var/self_heat_capacity = heat_capacity_archived()
		var/sharer_heat_capacity = sharer.heat_capacity_archived()

		var/self_temperature_delta = 0
		var/sharer_temperature_delta = 0

		if((sharer_heat_capacity > MINIMUM_HEAT_CAPACITY) && (self_heat_capacity > MINIMUM_HEAT_CAPACITY))
			var/heat = conduction_coefficient*delta_temperature* \
				(self_heat_capacity*sharer_heat_capacity/(self_heat_capacity+sharer_heat_capacity))

			self_temperature_delta = -heat/(self_heat_capacity*group_multiplier)
			sharer_temperature_delta = heat/(sharer_heat_capacity*sharer.group_multiplier)
		else
			return 1

		if((abs(self_temperature_delta) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND) \
			&& (abs(self_temperature_delta) > MINIMUM_TEMPERATURE_RATIO_TO_SUSPEND*temperature_archived))
			return 0

		if((abs(sharer_temperature_delta) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND) \
			&& (abs(sharer_temperature_delta) > MINIMUM_TEMPERATURE_RATIO_TO_SUSPEND*sharer.temperature_archived))
			return -1

		temperature += self_temperature_delta
		sharer.temperature += sharer_temperature_delta

		return 1
		//Logic integrated from: temperature_share(sharer, conduction_coefficient) for efficiency

	check_me_then_temperature_share(datum/gas_mixture/sharer, conduction_coefficient)
		var/delta_temperature = (temperature_archived - sharer.temperature_archived)

		var/self_heat_capacity = heat_capacity_archived()
		var/sharer_heat_capacity = sharer.heat_capacity_archived()

		var/self_temperature_delta = 0
		var/sharer_temperature_delta = 0

		if((sharer_heat_capacity > MINIMUM_HEAT_CAPACITY) && (self_heat_capacity > MINIMUM_HEAT_CAPACITY))
			var/heat = conduction_coefficient*delta_temperature* \
				(self_heat_capacity*sharer_heat_capacity/(self_heat_capacity+sharer_heat_capacity))

			self_temperature_delta = -heat/(self_heat_capacity*group_multiplier)
			sharer_temperature_delta = heat/(sharer_heat_capacity*sharer.group_multiplier)
		else
			return 1

		if((abs(self_temperature_delta) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND) \
			&& (abs(self_temperature_delta) > MINIMUM_TEMPERATURE_RATIO_TO_SUSPEND*temperature_archived))
			return 0

		temperature += self_temperature_delta
		sharer.temperature += sharer_temperature_delta

		return 1
		//Logic integrated from: temperature_share(sharer, conduction_coefficient) for efficiency

	check_me_then_temperature_turf_share(turf/simulated/sharer, conduction_coefficient)
		var/delta_temperature = (temperature_archived - sharer.temperature)

		var/self_temperature_delta = 0
		var/sharer_temperature_delta = 0

		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/self_heat_capacity = heat_capacity_archived()

			if((sharer.heat_capacity > MINIMUM_HEAT_CAPACITY) && (self_heat_capacity > MINIMUM_HEAT_CAPACITY))
				var/heat = conduction_coefficient*delta_temperature* \
					(self_heat_capacity*sharer.heat_capacity/(self_heat_capacity+sharer.heat_capacity))

				self_temperature_delta = -heat/(self_heat_capacity*group_multiplier)
				sharer_temperature_delta = heat/sharer.heat_capacity
		else
			return 1

		if((abs(self_temperature_delta) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND) \
			&& (abs(self_temperature_delta) > MINIMUM_TEMPERATURE_RATIO_TO_SUSPEND*temperature_archived))
			return 0

		temperature += self_temperature_delta
		sharer.temperature += sharer_temperature_delta

		return 1
		//Logic integrated from: temperature_turf_share(sharer, conduction_coefficient) for efficiency

	check_me_then_temperature_mimic(turf/model, conduction_coefficient)
		var/delta_temperature = (temperature_archived - model.temperature)
		var/self_temperature_delta = 0

		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/self_heat_capacity = heat_capacity_archived()

			if((model.heat_capacity > MINIMUM_HEAT_CAPACITY) && (self_heat_capacity > MINIMUM_HEAT_CAPACITY))
				var/heat = conduction_coefficient*delta_temperature* \
					(self_heat_capacity*model.heat_capacity/(self_heat_capacity+model.heat_capacity))

				self_temperature_delta = -heat/(self_heat_capacity*group_multiplier)

		if((abs(self_temperature_delta) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND) \
			&& (abs(self_temperature_delta) > MINIMUM_TEMPERATURE_RATIO_TO_SUSPEND*temperature_archived))
			return 0

		temperature += self_temperature_delta

		return 1
		//Logic integrated from: temperature_mimic(model, conduction_coefficient) for efficiency

	temperature_share(datum/gas_mixture/sharer, conduction_coefficient)

		var/delta_temperature = (temperature_archived - sharer.temperature_archived)
		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/self_heat_capacity = heat_capacity_archived()
			var/sharer_heat_capacity = sharer.heat_capacity_archived()

			if((sharer_heat_capacity > MINIMUM_HEAT_CAPACITY) && (self_heat_capacity > MINIMUM_HEAT_CAPACITY))
				var/heat = conduction_coefficient*delta_temperature* \
					(self_heat_capacity*sharer_heat_capacity/(self_heat_capacity+sharer_heat_capacity))

				temperature -= heat/(self_heat_capacity*group_multiplier)
				sharer.temperature += heat/(sharer_heat_capacity*sharer.group_multiplier)

	temperature_mimic(turf/model, conduction_coefficient, border_multiplier)
		var/delta_temperature = (temperature - model.temperature)
		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/self_heat_capacity = heat_capacity()//_archived()

			if((model.heat_capacity > MINIMUM_HEAT_CAPACITY) && (self_heat_capacity > MINIMUM_HEAT_CAPACITY))
				var/heat = conduction_coefficient*delta_temperature* \
					(self_heat_capacity*model.heat_capacity/(self_heat_capacity+model.heat_capacity))

				if(border_multiplier)
					temperature -= heat*border_multiplier/(self_heat_capacity*group_multiplier)
				else
					temperature -= heat/(self_heat_capacity*group_multiplier)

	temperature_turf_share(turf/simulated/sharer, conduction_coefficient)
		var/delta_temperature = (temperature_archived - sharer.temperature)
		if(abs(delta_temperature) > MINIMUM_TEMPERATURE_DELTA_TO_CONSIDER)
			var/self_heat_capacity = heat_capacity()

			if((sharer.heat_capacity > MINIMUM_HEAT_CAPACITY) && (self_heat_capacity > MINIMUM_HEAT_CAPACITY))
				var/heat = conduction_coefficient*delta_temperature* \
					(self_heat_capacity*sharer.heat_capacity/(self_heat_capacity+sharer.heat_capacity))

				temperature -= heat/(self_heat_capacity*group_multiplier)
				sharer.temperature += heat/sharer.heat_capacity

	compare(datum/gas_mixture/sample)
		if((abs(oxygen-sample.oxygen) > MINIMUM_AIR_TO_SUSPEND) && \
			((oxygen < (1-MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.oxygen) || (oxygen > (1+MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.oxygen)))
			return 0
		if((abs(nitrogen-sample.nitrogen) > MINIMUM_AIR_TO_SUSPEND) && \
			((nitrogen < (1-MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.nitrogen) || (nitrogen > (1+MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.nitrogen)))
			return 0
		if((abs(carbon_dioxide-sample.carbon_dioxide) > MINIMUM_AIR_TO_SUSPEND) && \
			((carbon_dioxide < (1-MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.carbon_dioxide) || (oxygen > (1+MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.carbon_dioxide)))
			return 0
		if((abs(toxins-sample.toxins) > MINIMUM_AIR_TO_SUSPEND) && \
			((toxins < (1-MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.toxins) || (toxins > (1+MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.toxins)))
			return 0
		if((abs(sleeping_agent-sample.sleeping_agent) > MINIMUM_AIR_TO_SUSPEND) && \
			((sleeping_agent < (1-MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.sleeping_agent) || (sleeping_agent > (1+MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.sleeping_agent)))
			return 0
		if((abs(volatile_fuel-sample.volatile_fuel) > MINIMUM_AIR_TO_SUSPEND) && \
			((volatile_fuel < (1-MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.volatile_fuel) || (volatile_fuel > (1+MINIMUM_AIR_RATIO_TO_SUSPEND)*sample.volatile_fuel)))
			return 0

		if(total_moles() > MINIMUM_AIR_TO_SUSPEND)
			if((abs(temperature-sample.temperature) > MINIMUM_TEMPERATURE_DELTA_TO_SUSPEND) && \
				((temperature < (1-MINIMUM_TEMPERATURE_RATIO_TO_SUSPEND)*sample.temperature) || (temperature > (1+MINIMUM_TEMPERATURE_RATIO_TO_SUSPEND)*sample.temperature)))
				//world << "temp fail [temperature] & [sample.temperature]"
				return 0

		return 1
