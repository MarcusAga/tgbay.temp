
//check if mob is lying down on something we can operate him on.
/proc/can_operate(mob/living/carbon/M)
	return (M.buckled || M.lying || M.weakened || M.stunned || M.paralysis || M.sleeping || M.stat)
//	return (locate(/obj/machinery/optable, M.loc) && M.resting) || \
//	(locate(/obj/structure/stool/bed/roller, M.loc) && (M.buckled || M.lying || M.weakened || M.stunned || M.paralysis || M.sleeping || M.stat)) || \
//	(locate(/obj/structure/table/, M.loc) && (M.lying || M.weakened || M.stunned || M.paralysis || M.sleeping || M.stat))

/datum/surgery_status/
	var/eyes	=	0
	var/face	=	0
	var/appendix	=	0
	var/stomach	=	0
	var/brain	=	0
	var/ribcage	=	0

/mob/living/carbon/var/datum/surgery_status/op_stage = new/datum/surgery_status

/* SURGERY STEPS */

/datum/surgery_step
	// type path referencing the required tool for this step
	var/required_tool = null
	var/name = "unnamed step"

	// type path referencing tools that can be used as substitude for this step
	var/list/allowed_tools = null

	// When multiple steps can be applied with the current tool etc., choose the one with higher priority

	// checks whether this step can be applied with the given user and target
	proc/can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		return 0

	// does stuff to begin the step, usually just printing messages
	proc/begin_step(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		return

	// does stuff to end the step, which is normally print a message + do whatever this step changes
	proc/end_step(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		return

	// stuff that happens when the step fails
	proc/fail_step(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		return null

	proc/do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)

	// duration of the step
	var/min_duration = 0
	var/max_duration = 0

	// evil infection stuff that will make everyone hate me
	var/can_infect = 0

	proc/getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		return name

	proc/success_chance(mob/living/carbon/M, obj/tool, mob/living/user)
		var/mod = 0.5
		var/newb = 1
		if(ishuman(user))
			var/skill = user:getSkill("anatomy")
			newb = 0
			if(skill>=3)
				mod = 1.8
			else if(skill>=2)
				mod = 1.25
			else if(skill>=1)
				mod = 0.8
			else
				mod = 0.4
				newb = 1
		else if(issilicon(user))
			newb = 0
			mod = 2
		if(istype(tool, required_tool)) mod*=1.0
		else if(allowed_tools && (tool.type in allowed_tools)) mod*=0.6
		else mod = 0.0
		if(locate(/obj/machinery/optable, M.loc) && M.resting) return 100*mod
		if(locate(/obj/structure/stool/bed/roller, M.loc) && (M.buckled || M.lying || M.weakened || M.stunned || M.paralysis || M.sleeping || M.stat)) return 85*mod
		if(locate(/obj/structure/table/, M.loc) && (M.lying || M.weakened || M.stunned || M.paralysis || M.sleeping || M.stat)) return 75*mod
		return newb?4:(60*mod)

// Build this list by iterating over all typesof(/datum/surgery_step) and sorting the results by priority
var/global/list/surgery_steps = null

proc/build_surgery_steps_list()
	surgery_steps = list()
	var/list/scalpel_substitutes = list(/obj/item/weapon/knife,/obj/item/weapon/kitchen/utensil/knife,/obj/item/weapon/shard,/obj/item/weapon/butch)
	var/list/hemostat_substitutes = list(/obj/item/weapon/mousetrap, /obj/item/weapon/wirecutters)
	var/list/cautery_substitutes = list(/obj/item/weapon/weldingtool, /obj/item/clothing/mask/cigarette, /obj/item/weapon/lighter)
	var/list/retractor_substitutes = list(/obj/item/weapon/kitchen/utensil/fork, /obj/item/weapon/screwdriver)
	var/list/bonesetter_substitutes = list(/obj/item/weapon/wrench)
	var/list/bonegel_substitutes = list(/obj/item/stack/medical/ointment)
	var/list/saw_substitutes = list(/obj/item/weapon/hatchet)
	for(var/T in typesof(/datum/surgery_step)-/datum/surgery_step)
		var/datum/surgery_step/S = new T
		surgery_steps += S
		if(S.allowed_tools) continue
		if(S.required_tool == /obj/item/weapon/scalpel)
			S.allowed_tools = scalpel_substitutes
		else if(S.required_tool == /obj/item/weapon/hemostat)
			S.allowed_tools = hemostat_substitutes
		else if(S.required_tool == /obj/item/weapon/cautery)
			S.allowed_tools = cautery_substitutes
		else if(S.required_tool == /obj/item/weapon/retractor)
			S.allowed_tools = retractor_substitutes
		else if(S.required_tool == /obj/item/weapon/bonesetter)
			S.allowed_tools = bonesetter_substitutes
		else if(S.required_tool == /obj/item/weapon/bonegel)
			S.allowed_tools = bonegel_substitutes
		else if(S.required_tool == /obj/item/weapon/circular_saw)
			S.allowed_tools = saw_substitutes

proc/spread_germs_to_organ(datum/organ/external/E, mob/living/carbon/human/user)
	if(!istype(user) || !istype(E)) return

	var/germ_level = user.germ_level
	if(user.gloves)
		germ_level = user.gloves.germ_level

	E.germ_level = germ_level


//////////////////////////////////////////////////////////////////
//						COMMON STEPS							//
//////////////////////////////////////////////////////////////////
/datum/surgery_step/generic/
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected	//affected organ
		if (target_zone == "eyes")	//there are specific steps for eye surgery
			return 0
		if (!hasorgans(target))
			return 0
		affected = target.get_organ(target_zone)
		if (affected == null || affected.status&ORGAN_DESTROYED>0)
			return 0
		if (affected.status & ORGAN_DESTROYED)
			return 0
		if (affected.status & ORGAN_ROBOT)
			return 0
		if(target.checkcoverage(affected,target_zone))
			return -1
		return 1

/datum/surgery_step/generic/cut_open
	required_tool = /obj/item/weapon/scalpel
	name = "cut open"

	min_duration = 90
	max_duration = 110

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		return "cut [affected.display_name] open"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.open == 0 && target_zone != "mouth")
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts the incision on %knownface:2%'s [affected.display_name] with \the [tool].", \
		"You start the incision on %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("You feel a horrible pain as if from a sharp knife in your [affected.display_name]!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% has made an incision on %knownface:2%'s [affected.display_name] with \the [tool].", \
		"\blue You have made an incision on %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))
		affected.open = 1
		affected.createwound(CUT, 1)
		spread_germs_to_organ(affected, user)
		if (target_zone == "head")
			target.op_stage.brain = 1
		target.UpdateDamageIcon()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, slicing open %knownface:2%'s [affected.display_name] in a wrong spot with \the [tool]!", \
		"\red Your hand slips, slicing open %knownface:2%'s [affected.display_name] in a wrong spot with \the [tool]!",actors=list(user,target))
		affected.createwound(CUT, 10)
		if (ishuman(user))
			user:bloody_hands(target, 0)

/datum/surgery_step/generic/clamp_bleeders
	required_tool = /obj/item/weapon/hemostat
	allowed_tools = list(/obj/item/weapon/cable_coil, /obj/item/weapon/mousetrap, /obj/item/weapon/wirecutters)
	name = "clamp bleeders"

	min_duration = 40
	max_duration = 60

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		return "clamp bleeders into [affected.display_name]"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.open && (affected.status & ORGAN_BLEEDING))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts clamping bleeders in %knownface:2%'s [affected.display_name] with \the [tool].", \
		"You start clamping bleeders in %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("The pain in your [affected.display_name] is maddening!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% clamps bleeders in %knownface:2%'s [affected.display_name] with \the [tool].",	\
		"\blue You clamp bleeders in %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))
		affected.clamp()
		affected.status &= ~ORGAN_BLEEDING
		spread_germs_to_organ(affected, user)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, tearing blood vessals and causing massive bleeding in %knownface:2%'s [affected.display_name] with the \[tool]!",	\
		"\red Your hand slips, tearing blood vessels and causing massive bleeding in %knownface:2%'s [affected.display_name] with \the [tool]!",actors=list(user,target))
		affected.createwound(CUT, 10)
		if (ishuman(user))
			user:bloody_hands(target, 0)

/datum/surgery_step/generic/retract_skin
	required_tool = /obj/item/weapon/retractor
	name = "retract skin"

	min_duration = 30
	max_duration = 40

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		return "retract skin on [affected.display_name]"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.open < 2 && !(affected.status & ORGAN_BLEEDING))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		var/msg = "%knownface:1% starts to pry open the incision on %knownface:2%'s [affected.display_name] with \the [tool]."
		var/self_msg = "You start to pry open the incision on %knownface:2%'s [affected.display_name] with \the [tool]."
		if (target_zone == "chest")
			msg = "%knownface:1% starts to separate the ribcage and rearrange the organs in %knownface:2%'s torso with \the [tool]."
			self_msg = "You start to separate the ribcage and rearrange the organs in %knownface:2%'s torso with \the [tool]."
		if (target_zone == "groin")
			msg = "%knownface:1% starts to pry open the incision and rearrange the organs in %knownface:2%'s lower abdomen with \the [tool]."
			self_msg = "You start to pry open the incision and rearrange the organs in %knownface:2%'s lower abdomen with \the [tool]."
		user.visible_message(msg, self_msg, actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("It feels like the skin on your [affected.display_name] is on fire!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		var/msg = "\blue %knownface:1% keeps the incision open on %knownface:2%'s [affected.display_name] with \the [tool]."
		var/self_msg = "\blue You keep the incision open on %knownface:2%'s [affected.display_name] with \the [tool]."
		if (target_zone == "chest")
			msg = "\blue %knownface:1% keeps the ribcage open on %knownface:2%'s torso with \the [tool]."
			self_msg = "\blue You keep the ribcage open on %knownface:2%'s torso with \the [tool]."
		if (target_zone == "groin")
			msg = "\blue %knownface:1% keeps the incision open on %knownface:2%'s lower abdomen with \the [tool]."
			self_msg = "\blue You keep the incision open on %knownface:2%'s lower abdomen with \the [tool]."
		user.visible_message(msg, self_msg, actors=list(user,target))
		affected.open = 2
		spread_germs_to_organ(affected, user)
		target.UpdateDamageIcon()
		if (prob(40)) user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		var/msg = "\red %knownface:1%'s hand slips, tearing the edges of incision on %knownface:2%'s [affected.display_name] with \the [tool]!"
		var/self_msg = "\red Your hand slips, tearing the edges of incision on %knownface:2%'s [affected.display_name] with \the [tool]!"
		if (target_zone == "chest")
			msg = "\red %knownface:1%'s hand slips, damaging several organs %knownface:2%'s torso with \the [tool]!"
			self_msg = "\red Your hand slips, damaging several organs %knownface:2%'s torso with \the [tool]!"
		if (target_zone == "groin")
			msg = "\red %knownface:1%'s hand slips, damaging several organs %knownface:2%'s lower abdomen with \the [tool]"
			self_msg = "\red Your hand slips, damaging several organs %knownface:2%'s lower abdomen with \the [tool]!"
		user.visible_message(msg, self_msg, actors=list(user,target))
		target.apply_damage(12, BRUTE, affected)

/datum/surgery_step/generic/cauterize
	required_tool = /obj/item/weapon/cautery
	name = "cauterize"

	min_duration = 70
	max_duration = 100

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		return "cauterize [affected.display_name]"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if((tool.type in allowed_tools) && !is_hot(tool)) return 0
		if(!hasorgans(target)) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.open && target_zone != "mouth" && (target_zone != "chest" || !target.op_stage.stomach))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% is beginning to cauterize the incision on %knownface:2%'s [affected.display_name] with \the [tool]." , \
		"You are beginning to cauterize the incision on %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("Your [affected.display_name] is being burned!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% cauterizes the incision on %knownface:2%'s [affected.display_name] with \the [tool].", \
		"\blue You cauterize the incision on %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))
		affected.open = 0
		affected.germ_level = 0
		affected.status &= ~ORGAN_BLEEDING
		target.UpdateDamageIcon()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, leaving a small burn on %knownface:2%'s [affected.display_name] with \the [tool]!", \
		"\red Your hand slips, leaving a small burn on %knownface:2%'s [affected.display_name] with \the [tool]!",actors=list(user,target))
		target.apply_damage(3, BURN, affected)

//////////////////////////////////////////////////////////////////
//						APPENDECTOMY							//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/appendectomy/
	var/datum/organ/external/groin
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		if (target_zone != "groin")
			return 0
		groin = target.get_organ("groin")
		if (!groin)
			return 0
		if (groin.open < 2)
			return 0
		if(target.checkcoverage(groin,target_zone))
			return -1
		return 1

/datum/surgery_step/appendectomy/cut_appendix
	required_tool = /obj/item/weapon/scalpel
	name = "cut appendix"

	min_duration = 70
	max_duration = 90

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.appendix == 0)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/groin/groin = target.get_organ("groin")
		if(!groin || groin.status&(ORGAN_DESTROYED|ORGAN_ROBOT) || groin.appendicitis<0)
			user.show_message("You fail to find appendix in %knownface:1%!", 1, actors=list(target))
			return -1

		user.visible_message("%knownface:1% starts to separate %knownface:2%'s appendix from the abdominal wall with \the [tool].", \
		"You start to separate %knownface:2%'s appendix from the abdominal wall with \the [tool]." ,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("The pain in your abdomen is living hell!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% has separated %knownface:2%'s appendix with \the [tool]." , \
		"\blue You have separated %knownface:2%'s appendix with \the [tool].",actors=list(user,target))
		target.op_stage.appendix = 1
		if (ishuman(user) && prob(40)) user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/groin = target.get_organ("groin")
		user.visible_message("\red %knownface:1%'s hand slips, slicing an artery inside %knownface:2%'s abdomen with \the [tool]!", \
		"\red Your hand slips, slicing an artery inside %knownface:2%'s abdomen with \the [tool]!",actors=list(user,target))
		groin.createwound(CUT, 50, 1)
		if (ishuman(user))
			user:bloody_body(target)

/datum/surgery_step/appendectomy/remove_appendix
	required_tool = /obj/item/weapon/hemostat
	name = "remove appendix"

	min_duration = 60
	max_duration = 80

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.appendix == 1)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts removing %knownface:2%'s appendix with \the [tool].", \
		"You start removing %knownface:2%'s appendix with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("Someone's ripping out your bowels!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% has removed %knownface:2%'s appendix with \the [tool].", \
		"\blue You have removed %knownface:2%'s appendix with \the [tool].",actors=list(user,target))
		var/datum/organ/external/groin/Groin = target.get_organ("groin")
		if(!Groin || Groin.status&ORGAN_DESTROYED)
			user.visible_message("\red %knownface:1% fails to find any groin on %knownface:2%!", \
			"\red You fail to find groin on %knownface:2%",actors=list(user,target))
		if(Groin.status&(ORGAN_DESTROYED|ORGAN_ROBOT))
			Groin.appendicitis = -1
		if(Groin.appendicitis>0)
			new /obj/item/weapon/reagent_containers/food/snacks/appendix/inflamed(get_turf(target))
		else if(Groin.appendicitis == 0)
			new /obj/item/weapon/reagent_containers/food/snacks/appendix(get_turf(target))
		Groin.appendicitis = -1
		target.op_stage.appendix = 0
		if (ishuman(user) && prob(40)) user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, nicking internal organs in %knownface:2%'s abdomen with \the [tool]!", \
		"\red Your hand slips, nicking internal organs in %knownface:2%'s abdomen with \the [tool]!",actors=list(user,target))
		affected.createwound(BRUISE, 20)


//////////////////////////////////////////////////////////////////
//					INTERNAL WOUND PATCHING						//
//////////////////////////////////////////////////////////////////


/datum/surgery_step/fix_vein
	required_tool = /obj/item/weapon/FixOVein
	allowed_tools = list(/obj/item/weapon/cable_coil)
	name = "fix damaged veins"

	min_duration = 70
	max_duration = 90

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		return "fix damaged veins in [affected.display_name]"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)

		var/internal_bleeding = 0
		for(var/datum/wound/W in affected.wounds) if(W.internal)
			internal_bleeding = 1
			break

		if(target.checkcoverage(affected,target_zone))
			return -1

		return affected.open == 2 && internal_bleeding

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts patching the damaged vein in %knownface:2%'s [affected.display_name] with \the [tool]." , \
		"You start patching the damaged vein in %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("The pain in [affected.display_name] is unbearable!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% has patched the damaged vein in %knownface:2%'s [affected.display_name] with \the [tool].", \
			"\blue You have patched the damaged vein in %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

		for(var/datum/wound/W in affected.wounds) if(W.internal)
			affected.wounds -= W
			affected.update_damages()
		if (ishuman(user) && prob(40)) user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, smearing [tool] in the incision in %knownface:2%'s [affected.display_name]!" , \
		"\red Your hand slips, smearing [tool] in the incision in %knownface:2%'s [affected.display_name]!",actors=list(user,target))
		affected.take_damage(5, 0)


//////////////////////////////////////////////////////////////////
//						BONE SURGERY							//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/glue_bone
	required_tool = /obj/item/weapon/bonegel
	name = "apply bone glue"

	min_duration = 50
	max_duration = 60

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		return "apply bone glue on [affected.display_name] bones"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(target.checkcoverage(affected,target_zone))
			return -1
		return affected.open == 2 && affected.stage == 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected.stage == 0)
			user.visible_message("%knownface:1% starts applying medication to the damaged bones in %knownface:2%'s [affected.display_name] with \the [tool]." , \
			"You start applying medication to the damaged bones in %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("Something in your [affected.display_name] is causing you a lot of pain!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% applies some [tool] to %knownface:2%'s bone in [affected.display_name]", \
			"\blue You apply some [tool] to %knownface:2%'s bone in [affected.display_name] with \the [tool].",actors=list(user,target))
		affected.stage = 1
		spread_germs_to_organ(affected, user)
		if (ishuman(user) && prob(80)) user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, smearing [tool] in the incision in %knownface:2%'s [affected.display_name]!" , \
		"\red Your hand slips, smearing [tool] in the incision in %knownface:2%'s [affected.display_name]!",actors=list(user,target))

/datum/surgery_step/set_bone
	required_tool = /obj/item/weapon/bonesetter
	name = "fix bones"

	min_duration = 60
	max_duration = 70

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.status & ORGAN_BROKEN)
			return "set bones in [affected.display_name]"
		else
			return "break bones in [affected.display_name]"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(target.checkcoverage(affected,target_zone))
			return -1
		return affected.name != "head" && affected.open == 2 && affected.stage == 1

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% is beginning to set the bone in %knownface:2%'s [affected.display_name] in place with \the [tool]." , \
			"You are beginning to set the bone in %knownface:2%'s [affected.display_name] in place with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("The pain in your [affected.display_name] is going to make you pass out!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected.status & ORGAN_BROKEN)
			user.visible_message("\blue %knownface:1% sets the bone in %knownface:2%'s [affected.display_name] in place with \the [tool].", \
				"\blue You set the bone in %knownface:2%'s [affected.display_name] in place with \the [tool].",actors=list(user,target))
			affected.stage = 2
			spread_germs_to_organ(affected, user)
		else
			user.visible_message("\blue %knownface:1% sets the bone in %knownface:2%'s [affected.display_name]\red in the WRONG place with \the [tool].", \
				"\blue You set the bone in %knownface:2%'s [affected.display_name]\red in the WRONG place with \the [tool].",actors=list(user,target))
			affected.fracture()
			spread_germs_to_organ(affected, user)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, damaging the bone in %knownface:2%'s [affected.display_name] with \the [tool]!" , \
			"\red Your hand slips, damaging the bone in %knownface:2%'s [affected.display_name] with \the [tool]!",actors=list(user,target))
		affected.createwound(BRUISE, 5)

/datum/surgery_step/mend_skull
	required_tool = /obj/item/weapon/bonesetter
	name = "mend skull"

	min_duration = 60
	max_duration = 70

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(target.checkcoverage(affected,target_zone))
			return -1
		return affected.name == "head" && affected.open == 2 && affected.stage == 1

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% is beginning piece together %knownface:2%'s skull with \the [tool]."  , \
			"You are beginning piece together %knownface:2%'s skull with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		target.custom_pain("The pain in your [affected.display_name] is going to make you pass out!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% sets %knownface:2%'s skull with \the [tool]." , \
			"\blue You set %knownface:2%'s skull with \the [tool].",actors=list(user,target))
		affected.stage = 2
		spread_germs_to_organ(affected, user)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, damaging %knownface:2%'s face with \the [tool]!"  , \
			"\red Your hand slips, damaging %knownface:2%'s face with \the [tool]!",actors=list(user,target))
		var/datum/organ/external/head/h = affected
		h.createwound(BRUISE, 10)
		h.disfigured = 1

/datum/surgery_step/finish_bone
	required_tool = /obj/item/weapon/bonegel
	name = "finish bone setting"

	min_duration = 50
	max_duration = 60

	getName(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		return "finish bone setting in [affected.display_name]"

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(target.checkcoverage(affected,target_zone))
			return -1
		return affected.open == 2 && affected.stage == 2

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts to finish mending the damaged bones in %knownface:2%'s [affected.display_name] with \the [tool].", \
		"You start to finish mending the damaged bones in %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% has mended the damaged bones in %knownface:2%'s [affected.display_name] with \the [tool]."  , \
			"\blue You have mended the damaged bones in %knownface:2%'s [affected.display_name] with \the [tool]." ,actors=list(user,target))
		affected.status &= ~ORGAN_BROKEN
		affected.status &= ~ORGAN_SPLINTED
		affected.stage = 0
		affected.perma_injury = 0
		spread_germs_to_organ(affected, user)
		if (ishuman(user) && prob(80)) user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, smearing [tool] in the incision in %knownface:2%'s [affected.display_name]!" , \
		"\red Your hand slips, smearing [tool] in the incision in %knownface:2%'s [affected.display_name]!",actors=list(user,target))

//////////////////////////////////////////////////////////////////
//						EYE SURGERY							//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/eye
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (!affected)
			return 0
		if(target.checkcoverage(affected,target_zone))
			return -1
		return target_zone == "eyes"

/datum/surgery_step/eye/cut_open
	required_tool = /obj/item/weapon/scalpel
	name = "cut eyes open"

	min_duration = 90
	max_duration = 110

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts to separate the corneas on %knownface:2%'s eyes with \the [tool].", \
		"You start to separate the corneas on %knownface:2%'s eyes with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% has separated the corneas on %knownface:2%'s eyes with \the [tool]." , \
		"\blue You have separated the corneas on %knownface:2%'s eyes with \the [tool].",actors=list(user,target))
		target.op_stage.eyes = 1

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, slicing %knownface:2%'s eyes wth \the [tool]!" , \
		"\red Your hand slips, slicing %knownface:2%'s eyes wth \the [tool]!" ,actors=list(user,target))
		affected.createwound(CUT, 10)

/datum/surgery_step/eye/lift_eyes
	required_tool = /obj/item/weapon/retractor
	name = "lift corneas"

	min_duration = 30
	max_duration = 40

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.eyes == 1)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts lifting corneas from %knownface:2%'s eyes with \the [tool].", \
		"You start lifting corneas from %knownface:2%'s eyes with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% has lifted the corneas from %knownface:2%'s eyes from with \the [tool]." , \
		"\blue You has lifted the corneas from %knownface:2%'s eyes from with \the [tool]." ,actors=list(user,target))
		target.op_stage.eyes = 2

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, damaging %knownface:2%'s eyes with \the [tool]!", \
		"\red Your hand slips, damaging %knownface:2%'s eyes with \the [tool]!",actors=list(user,target))
		target.apply_damage(10, BRUTE, affected)

/datum/surgery_step/eye/mend_eyes
	required_tool = /obj/item/weapon/hemostat
	name = "fix eyes"

	min_duration = 80
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.eyes == 2)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts mending the nerves and lenses in %knownface:2%'s eyes with \the [tool].", \
		"You start mending the nerves and lenses in %knownface:2%'s eyes with the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% mends the nerves and lenses in %knownface:2%'s with \the [tool]." ,	\
		"\blue You mend the nerves and lenses in %knownface:2%'s with \the [tool].",actors=list(user,target))
		target.op_stage.eyes = 3

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, stabbing \the [tool] into %knownface:2%'s eye!", \
		"\red Your hand slips, stabbing \the [tool] into %knownface:2%'s eye!",actors=list(user,target))
		target.apply_damage(10, BRUTE, affected)

/datum/surgery_step/eye/cauterize
	required_tool = /obj/item/weapon/cautery
	name = "cauterize eyes"

	min_duration = 70
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if((tool.type in allowed_tools) && !is_hot(tool)) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% is beginning to cauterize the incision around %knownface:2%'s eyes with \the [tool]." , \
		"You are beginning to cauterize the incision around %knownface:2%'s eyes with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% cauterizes the incision around %knownface:2%'s eyes with \the [tool].", \
		"\blue You cauterize the incision around %knownface:2%'s eyes with \the [tool].",actors=list(user,target))
		if (target.op_stage.eyes == 3)
			target.sdisabilities &= ~BLIND
			target.eye_stat = 0
		target.op_stage.eyes = 0

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips,  searing %knownface:2%'s eyes with \the [tool]!", \
		"\red Your hand slips, searing %knownface:2%'s eyes with \the [tool]!",actors=list(user,target))
		target.apply_damage(5, BURN, affected)
		target.eye_stat += 5

//////////////////////////////////////////////////////////////////
//						FACE SURGERY							//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/face
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (!affected)
			return 0
		if(target.checkcoverage(affected,target_zone))
			return -1
		return target_zone == "mouth" && affected.open == 2 && !(affected.status & ORGAN_BLEEDING)

/datum/surgery_step/generic/cut_face
	required_tool = /obj/item/weapon/scalpel
	name = "cut face"

	min_duration = 90
	max_duration = 110

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target_zone == "mouth" && target.op_stage.face == 0)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts to cut open %knownface:2%'s face and neck with \the [tool].", \
		"You start to cut open %knownface:2%'s face and neck with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% has cut open %knownface:2%'s face and neck with \the [tool]." , \
		"\blue You have cut open %knownface:2%'s face and neck with \the [tool].",actors=list(user,target))
		target.op_stage.face = 1

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, slicing %knownface:2%'s throat wth \the [tool]!" , \
		"\red Your hand slips, slicing %knownface:2%'s throat wth \the [tool]!" ,actors=list(user,target))
		affected.createwound(CUT, 60)
		target.losebreath += 10

/datum/surgery_step/face/mend_vocal
	required_tool = /obj/item/weapon/hemostat
	name = "mend vocal cords"

	min_duration = 70
	max_duration = 90

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.face == 1)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts mending %knownface:2%'s vocal cords with \the [tool].", \
		"You start mending %knownface:2%'s vocal cords with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% mends %knownface:2%'s vocal cords with \the [tool].", \
		"\blue You mend %knownface:2%'s vocal cords with \the [tool].",actors=list(user,target))
		target.op_stage.face = 2

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, clamping %knownface:2%'s trachea shut for a moment with \the [tool]!", \
		"\red Your hand slips, clamping %knownface:1%'s trachea shut for a moment with \the [tool]!",actors=list(user,target))
		target.losebreath += 10
		if (ishuman(user))
			user:bloody_body(target)
			user:bloody_hands(target, 0)

/datum/surgery_step/face/fix_face
	required_tool = /obj/item/weapon/retractor
	name = "put face back"

	min_duration = 80
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.face == 2)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts pulling skin on %knownface:2%'s face back in place with \the [tool].", \
		"You start pulling skin on %knownface:2%'s face back in place with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% pulls skin on %knownface:2%'s face back in place with \the [tool].",	\
		"\blue You pull skin on %knownface:2%'s face back in place with \the [tool].",actors=list(user,target))
		target.op_stage.face = 3

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, tearing skin on %knownface:2%'s face with \the [tool]!", \
		"\red Your hand slips, tearing skin on %knownface:2%'s face with \the [tool]!",actors=list(user,target))
		target.apply_damage(10, BRUTE, affected)

/datum/surgery_step/face/cauterize
	required_tool = /obj/item/weapon/cautery
	name = "cauterize face"

	min_duration = 70
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if((tool.type in allowed_tools) && !is_hot(tool)) return 0
		if(target.op_stage.face <= 0) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% is beginning to cauterize the incision on %knownface:2%'s face and neck with \the [tool]." , \
		"You are beginning to cauterize the incision on %knownface:2%'s face and neck with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% cauterizes the incision on %knownface:2%'s face and neck with \the [tool].", \
		"\blue You cauterize the incision on %knownface:2%'s face and neck with \the [tool].",actors=list(user,target))
		affected.open = 0
		affected.status &= ~ORGAN_BLEEDING
		if (target.op_stage.face == 3)
			var/datum/organ/external/head/h = affected
			h.disfigured = 0
		target.op_stage.face = 0
		target.UpdateDamageIcon()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, leaving a small burn on %knownface:2%'s face with \the [tool]!", \
		"\red Your hand slips, leaving a small burn on %knownface:2%'s face with \the [tool]!",actors=list(user,target))
		target.apply_damage(4, BURN, affected)

//////////////////////////////////////////////////////////////////
//						BRAIN SURGERY							//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/brain/
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(ishuman(target) && target.checkcoverage(target_zone)) return -1
		if(target_zone != "head" || !hasorgans(target)) return 0
		var/datum/organ/external/head/affected = target.get_organ(target_zone)
		if((affected.status & ORGAN_ROBOT) || istype(affected.brain,/obj/item/device/mmi)) return 0
		return 1

/datum/surgery_step/brain/saw_skull
	required_tool = /obj/item/weapon/circular_saw
	name = "saw skull"

	min_duration = 50
	max_duration = 70

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target_zone == "head" && target.op_stage.brain == 1)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% begins to cut through %knownface:2%'s skull with \the [tool].", \
		"You begin to cut through %knownface:2%'s skull with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("You feel a horrible pain from a [tool] in your head!",1)
		if(target.analgesic || target.reagents.has_reagent("tramadol") || target.reagents.has_reagent("oxycodone"))
			return
		target.emote("scream")

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% has cut through %knownface:2%'s skull open with \the [tool].",		\
		"\blue You have cut through %knownface:2%'s skull open with \the [tool].",actors=list(user,target))
		target.op_stage.brain = 2

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.pain("head", 100, 1)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, cracking %knownface:2%'s skull with \the [tool]!" , \
		"\red Your hand slips, cracking %knownface:2%'s skull with \the [tool]!" ,actors=list(user,target))
		target.apply_damage(10, BRUTE, "head")

/datum/surgery_step/brain/cut_brain
	required_tool = /obj/item/weapon/scalpel
	name = "separate brain"

	min_duration = 80
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.brain != 2) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts separating connections to %knownface:2%'s brain with \the [tool].", \
		"You start separating connections to %knownface:2%'s brain with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.pain("head", 100, 1)
		if(target.analgesic || target.reagents.has_reagent("tramadol") || target.reagents.has_reagent("oxycodone"))
			return
		target.emote("scream")

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% separates connections to %knownface:2%'s brain with \the [tool].",	\
		"\blue You separate connections to %knownface:2%'s brain with \the [tool].",actors=list(user,target))
		target.op_stage.brain = 3

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.pain("head", 100, 1)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, cutting a vein in %knownface:2%'s brain with \the [tool]!", \
		"\red Your hand slips, cutting a vein in %knownface:2%'s brain with \the [tool]!",actors=list(user,target))
		target.apply_damage(50, BRUTE, "head", 1)
		if (ishuman(user))
			user:bloody_body(target)
			user:bloody_hands(target, 0)

/datum/surgery_step/brain/saw_spine
	required_tool = /obj/item/weapon/circular_saw
	name = "saw spine"

	min_duration = 50
	max_duration = 70

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.brain != 3) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/his = target.gender==MALE?"his":(target.gender==FEMALE?"her":"its")
		user.visible_message("%knownface:1% starts separating %knownface:2%'s brain from [his] spine with \the [tool].", \
		"You start separating %knownface:2%'s brain from spine with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/his = target.gender==MALE?"his":(target.gender==FEMALE?"her":"its")
		user.visible_message("\blue %knownface:1% separates %knownface:2%'s brain from [his] spine with \the [tool].",	\
		"\blue You separate %knownface:2%'s brain from spine with \the [tool].",actors=list(user,target))

		user.attack_log += "\[[time_stamp()]\]<font color='red'> Debrained [target.name] ([target.ckey]) with [tool.name] (INTENT: [uppertext(user.a_intent)])</font>"
		target.attack_log += "\[[time_stamp()]\]<font color='orange'> Debrained by [user.name] ([user.ckey]) with [tool.name] (INTENT: [uppertext(user.a_intent)])</font>"

		log_admin("ATTACK: [user] ([user.ckey]) debrained [target] ([target.ckey]) with [tool].")
		message_admins("ATTACK: [user] ([user.ckey]) debrained [target] ([target.ckey]) with [tool].")
		log_attack("<font color='red'>[user] ([user.ckey]) debrained [target.name] ([target.ckey]) with [tool.name] (INTENT: [uppertext(user.a_intent)])</font>")

		var/datum/organ/external/affected = target.get_organ(target_zone)
		affected.add_autopsy_data("Debraining", 999)
		var/obj/item/brain/B = new(target.loc)
		B.transfer_identity(target)

		target:op_stage.brain = 4.0
		target.death()//You want them to die after the brain was transferred, so not to trigger client death() twice.

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, cutting a vein in %knownface:2%'s brain with \the [tool]!", \
		"\red Your hand slips, cutting a vein in %knownface:2%'s brain with \the [tool]!",actors=list(user,target))
		target.apply_damage(30, BRUTE, "head", 1)
		if (ishuman(user))
			user:bloody_body(target)
			user:bloody_hands(target, 0)

/datum/surgery_step/brain/remove_mmi
	required_tool = /obj/item/weapon/screwdriver
	name = "detach mmi"

	min_duration = 50
	max_duration = 70

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(ishuman(target) && target.checkcoverage(target_zone)) return -1
		if(target_zone != "head" || !hasorgans(target)) return 0
		var/datum/organ/external/head/affected = target.get_organ(target_zone)
		if(!(affected.status & ORGAN_ROBOT) || (affected.status & ORGAN_ROBOT_MASKED)) return 0
		if(target.stat==0) return 0
		if(istype(affected.brain,/obj/item/device/mmi)) return 1
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts detaching %knownface:2%'s mmi from [target.gender!=FEMALE?"his":"her"] head with \the [tool].", \
		"You start separating %knownface:2%'s mmi from [target.gender!=FEMALE?"his":"her"] head with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% detaches %knownface:2%'s mmi from [target.gender!=FEMALE?"his":"her"] head \the [tool].",	\
		"\blue You separate %knownface:2%'s mmi from [target.gender!=FEMALE?"his":"her"] head with \the [tool].",actors=list(user,target))

		user.attack_log += "\[[time_stamp()]\]<font color='red'> Debrained [target.name] ([target.ckey]) with [tool.name] (INTENT: [uppertext(user.a_intent)])</font>"
		target.attack_log += "\[[time_stamp()]\]<font color='orange'> Debrained by [user.name] ([user.ckey]) with [tool.name] (INTENT: [uppertext(user.a_intent)])</font>"

		log_admin("ATTACK: %knownface:1% ([user.ckey]) debrained %knownface:2% ([target.ckey]) with [tool].")
		message_admins("ATTACK: %knownface:1% ([user.ckey]) debrained %knownface:2% ([target.ckey]) with [tool].")
		log_attack("<font color='red'>[user.name] ([user.ckey]) debrained [target.name] ([target.ckey]) with [tool.name] (INTENT: [uppertext(user.a_intent)])</font>")

		var/datum/organ/external/head/affected = target.get_organ(target_zone)
		affected.add_autopsy_data("MMI detachment", 999)

		var/obj/item/device/mmi/MMI = affected.brain
		MMI.loc = target.loc
		add_to_mob_list(MMI.brainmob)
		if(target.mind)	target.mind.transfer_to(MMI.brainmob, target)
		affected.brain = null

		target:op_stage.brain = 4.0
		target.death()//You want them to die after the brain was transferred, so not to trigger client death() twice.

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, cutting a vein in %knownface:2%'s brain with \the [tool]!", \
		"\red Your hand slips, cutting a vein in %knownface:2%'s brain with \the [tool]!",actors=list(user,target))
		target.apply_damage(30, BRUTE, "head", 1)
		if (ishuman(user))
			user:bloody_body(target)
			user:bloody_hands(target, 0)


//////////////////////////////////////////////////////////////////
//				slime CORE EXTRACTION							//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/slime/
	can_use(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		return istype(target, /mob/living/carbon/slime/) && target.stat == 2

/datum/surgery_step/slime/cut_flesh
	required_tool = /obj/item/weapon/scalpel
	name = "cut flesh"

	min_duration = 30
	max_duration = 50

	can_use(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		if(target.op_stage.brain != 0) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts cutting %knownface:2%'s flesh with \the [tool].", \
		"You start cutting %knownface:2%'s flesh with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% cuts %knownface:2%'s flesh with \the [tool].",	\
		"\blue You cut %knownface:2%'s flesh with \the [tool], exposing the cores",actors=list(user,target))
		target.op_stage.brain = 1

	fail_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, tearing %knownface:2%'s flesh with \the [tool]!", \
		"\red Your hand slips, tearing %knownface:2%'s flesh with \the [tool]!",actors=list(user,target))

/datum/surgery_step/slime/cut_innards
	required_tool = /obj/item/weapon/scalpel
	name = "cut innards"

	min_duration = 30
	max_duration = 50

	can_use(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		if(target.op_stage.brain != 1) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts cutting %knownface:2%'s silky innards apart with \the [tool].", \
		"You start cutting %knownface:2%'s silky innards apart with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% cuts %knownface:2%'s innards apart with \the [tool], exposing the cores",	\
		"\blue You cut %knownface:2%'s innards apart with \the [tool], exposing the cores",actors=list(user,target))
		target.op_stage.brain = 2

	fail_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, tearing %knownface:2%'s innards with \the [tool]!", \
		"\red Your hand slips, tearing %knownface:2%'s innards with \the [tool]!",actors=list(user,target))

/datum/surgery_step/slime/saw_core
	required_tool = /obj/item/weapon/circular_saw
	name = "saw core"

	min_duration = 50
	max_duration = 70

	can_use(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		if(target.op_stage.brain == 2 && target.cores > 0)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts cutting out one of %knownface:2%'s cores with \the [tool].", \
		"You start cutting out one of %knownface:2%'s cores with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		target.cores--
		user.visible_message("\blue %knownface:1% cuts out one of %knownface:2%'s cores with \the [tool].",,	\
		"\blue You cut out one of %knownface:2%'s cores with \the [tool]. [target.cores] cores left.",actors=list(user,target))
		if(target.cores >= 0)
			new target.coretype(target.loc)
		if(target.cores <= 0)
			target.icon_state = "[target.colour] baby slime dead-nocore"

	fail_step(mob/user, mob/living/carbon/slime/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, failing to cut core out!", \
		"\red Your hand slips, failing to cut core out!",actors=list(user,target))

//////////////////////////////////////////////////////////////////
//						LIMB SURGERY							//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/limb/
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (!affected)
			return 0
		if (!(affected.status & ORGAN_DESTROYED))
			return 0
		if (affected.body_part==HEAD && affected:headless)
			return 0
		if (affected.parent)
			if (affected.parent.status & ORGAN_DESTROYED)
				return 0
		if(target.checkcoverage(affected,target_zone))
			return -1
		return target_zone in list("r_leg","l_leg","l_arm","r_arm","r_foot","l_foot","l_hand","r_hand","head","groin")

	proc/is_missing(mob/living/carbon/human/target, target_zone)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected == null)
			return 0
		if(affected.status&ORGAN_DESTROYED>0) return 1
		else return 0

/datum/surgery_step/generic/cut_away
	required_tool = /obj/item/weapon/circular_saw
	name = "cut away"

	min_duration = 90
	max_duration = 110

	proc/is_missing(mob/living/carbon/human/target, target_zone)
		if(!hasorgans(target)) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected == null)
			return 0
		if(affected.status&ORGAN_DESTROYED>0) return 1
		else return 0

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		if(is_missing(target, target_zone)) return 0
		if(!(target_zone in list("r_leg","l_leg","l_arm","r_arm","r_foot","l_foot","l_hand","r_hand","head","groin"))) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected && affected.open) return 0
		if(target_zone == "head" && target.stat != DEAD) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		var/msg = "%knownface:1% is beginning to [istype(tool,/obj/item/weapon/hatchet)?"hack":"saw"] away %knownface:2%'s [affected.display_name] with [tool]."
		var/self_msg = "You begin to [istype(tool,/obj/item/weapon/hatchet)?"hack":"saw"] away %knownface:2%'s [affected.display_name] with [tool]!"
		user.visible_message(msg, self_msg,actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		var/msg = "\blue %knownface:1% finishes [istype(tool,/obj/item/weapon/hatchet)?"hacking":"sawing"] %knownface:2%'s [affected.display_name] with [tool]!"
		var/self_msg = "\blue You finish [istype(tool,/obj/item/weapon/hatchet)?"hacking":"sawing"] %knownface:2%'s [affected.display_name] with [tool]!"
		user.visible_message(msg, self_msg,actors=list(user,target))
		affected.status |= ORGAN_DESTROYED
		affected.status |= ORGAN_BLEEDING
		affected.amputated=1
		affected.setAmputatedTree()
		affected.droplimb()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, damaging %knownface:2%'s [affected.display_name] in a horrible way with \the [tool]!", \
		"\red Your hand slips, damaging %knownface:2%'s [affected.display_name] in a horrible way with \the [tool]!",actors=list(user,target))
		affected.createwound(CUT, 20)

/datum/surgery_step/limb/cut
	required_tool = /obj/item/weapon/scalpel
	name = "cut off leftovers"

	min_duration = 80
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(!hasorgans(target)) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.parent && !(affected.parent.status & ORGAN_ROBOT))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts cutting away flesh where %knownface:2%'s [affected.display_name] used to be with \the [tool].", \
		"You start cutting away flesh where %knownface:2%'s [affected.display_name] used to be with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% cuts away flesh where %knownface:2%'s [affected.display_name] used to be with \the [tool].",	\
		"\blue You cut away flesh where %knownface:2%'s [affected.display_name] used to be with \the [tool].",actors=list(user,target))
		affected.status |= ORGAN_DESTROYED
		affected.status |= ORGAN_CUT_AWAY
		affected.open = 1
		affected.createwound(CUT, 1)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected.parent)
			affected = affected.parent
			user.visible_message("\red %knownface:1%'s hand slips, cutting %knownface:2%'s [affected.display_name] open!", \
			"\red Your hand slips,  cutting %knownface:2%'s [affected.display_name] open!",actors=list(user,target))
			affected.createwound(CUT, 10)

/datum/surgery_step/limb/clamp_bleeders
	required_tool = /obj/item/weapon/hemostat
	name = "clamp bleeders"

	min_duration = 40
	max_duration = 60

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(!hasorgans(target)) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(is_missing(target, target_zone) && affected.open && affected.parent && !(affected.parent.status & ORGAN_ROBOT) && ((affected.status & ORGAN_BLEEDING) || !affected.amputated))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		var/msg = "\red %knownface:1% is beginning to clamp bleeders in the stump where %knownface:2%'s [affected.display_name] used to be with [tool]."
		var/self_msg = "\red %knownface:1% begins to clamp bleeders in the stump where %knownface:2%'s [affected.display_name] used to be with [tool]!"
		user.visible_message(msg, self_msg,actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		var/msg = "\blue %knownface:1% finishes clamping bleeders in the stump where %knownface:2%'s [affected.display_name] used to be with [tool]!"
		var/self_msg = "\blue You finish clamping bleeders in the stump where %knownface:2%'s [affected.display_name] used to be with [tool]!"
		user.visible_message(msg, self_msg,actors=list(user,target))
		affected.bandage()
		affected.status &= ~ORGAN_BLEEDING
		affected.status |= ORGAN_GAUZED
		affected.amputated=1
		affected.setAmputatedTree()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, tearing blood vessals and causing massive bleeding in %knownface:2%'s [affected.display_name] with the \[tool]!",	\
		"\red Your hand slips, tearing blood vessels and causing massive bleeding in %knownface:2%'s [affected.display_name] with \the [tool]!",actors=list(user,target))
		affected.createwound(CUT, 10)

/datum/surgery_step/limb/mend
	required_tool = /obj/item/weapon/retractor
	name = "prepare attachment point"

	min_duration = 80
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if((affected.status & ORGAN_CUT_AWAY) && affected.parent && !(affected.parent.status & ORGAN_ROBOT))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% is beginning reposition flesh and nerve endings where %knownface:2%'s [affected.display_name] used to be with [tool].", \
		"You start repositioning flesh and nerve endings where %knownface:2%'s [affected.display_name] used to be with [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% has finished repositioning flesh and nerve endings where %knownface:2%'s [affected.display_name] used to be with [tool].",	\
		"\blue You have finished repositioning flesh and nerve endings where %knownface:2%'s [affected.display_name] used to be with [tool].",actors=list(user,target))
		affected.open = 3

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected.parent)
			affected = affected.parent
			user.visible_message("\red %knownface:1%'s hand slips, tearing flesh on %knownface:2%'s [affected.display_name]!", \
			"\red Your hand slips, tearing flesh on %knownface:2%'s [affected.display_name]!",actors=list(user,target))
			target.apply_damage(10, BRUTE, affected)


/datum/surgery_step/limb/prepare
	required_tool = /obj/item/weapon/cautery
	name = "finish attachment point"

	min_duration = 60
	max_duration = 70

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(!hasorgans(target)) return 0
		if((tool.type in allowed_tools) && !is_hot(tool)) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.open == 3 && affected.parent && !(affected.parent.status & (ORGAN_ROBOT|ORGAN_ATTACHABLE)))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts adjusting area around %knownface:2%'s [affected.display_name] with \the [tool].", \
		"You start adjusting area around %knownface:2%'s [affected.display_name] with \the [tool]..",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% has finished adjusting the area around %knownface:2%'s [affected.display_name] with \the [tool].",	\
		"\blue You have finished adjusting the area around %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))
		affected.status |= ORGAN_ATTACHABLE
		affected.status &= ~ORGAN_BLEEDING
		affected.amputated = 1
		affected.setAmputatedTree()
		affected.open = 0
		target.UpdateDamageIcon()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected.parent)
			affected = affected.parent
			user.visible_message("\red %knownface:1%'s hand slips, searing %knownface:2%'s [affected.display_name]!", \
			"\red Your hand slips, searing %knownface:2%'s [affected.display_name]!",actors=list(user,target))
			target.apply_damage(10, BURN, affected)

/datum/surgery_step/limb/prepare2
	required_tool = /obj/item/weapon/screwdriver
	name = "fix damaged attachpoints"

	min_duration = 60
	max_duration = 70

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(!hasorgans(target)) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.parent && (affected.parent.status&ORGAN_ROBOT))
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts fixing %knownface:2%'s [affected.display_name] damaged attachpoint with \the [tool].", \
		"You start fixing %knownface:2%'s [affected.display_name] damaged attachpoint with \the [tool]..",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% has finished fixing %knownface:2%'s [affected.display_name] attachpoint with \the [tool].",	\
		"\blue You have finished fixing %knownface:2%'s [affected.display_name] attachpoint with \the [tool].",actors=list(user,target))
		affected.status |= ORGAN_ATTACHABLE
		affected.status &= ~ORGAN_BLEEDING
		affected.amputated = 1
		affected.setAmputatedTree()
		affected.open = 0
		target.UpdateDamageIcon()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if (affected.parent)
			affected = affected.parent
			user.visible_message("\red %knownface:1%'s hand slips, damaging %knownface:2%'s [affected.display_name]!", \
			"\red Your hand slips, damaging %knownface:2%'s [affected.display_name]!",actors=list(user,target))
			target.apply_damage(10, BRUTE, affected)

/datum/surgery_step/limb/attach
	required_tool = /obj/item/robot_parts
	name = "attach"

	min_duration = 80
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/obj/item/robot_parts/p = tool
		if (p.part)
			if (!(target_zone in p.part))
				return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.status & ORGAN_ATTACHABLE)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts attaching [tool] where %knownface:2%'s [affected.display_name] used to be.", \
		"You start attaching [tool] where %knownface:2%'s [affected.display_name] used to be.",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% has attached [tool] where %knownface:2%'s [affected.display_name] used to be.",	\
		"\blue You have attached [tool] where %knownface:2%'s [affected.display_name] used to be.",actors=list(user,target))
		affected.robotize()
		target.update_body()
		target.updatehealth()
		target.UpdateDamageIcon()
		user.u_equip(tool)
		del(tool)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, damaging connectors on %knownface:2%'s [affected.display_name]!", \
		"\red Your hand slips, damaging connectors on %knownface:2%'s [affected.display_name]!",actors=list(user,target))
		target.apply_damage(10, BRUTE, affected)

/datum/surgery_step/limb/insert_mmi
	required_tool = /obj/item/device/mmi
	name = "insert mmi"

	min_duration = 10
	max_duration = 20

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		if (!(target_zone=="head"))
			return 0
		var/datum/organ/external/head/affected = target.get_organ(target_zone)
		if(target.checkcoverage(affected,target_zone))
			return -1
		if(!(affected.status & ORGAN_ROBOT) || (affected.status & ORGAN_DESTROYED)) return 0
		if(!istype(tool,/obj/item/device/mmi/advanced))
			user << "\red This MMI's neural interfaces are not good enough to make brain work at full capacity."
			return 0
		if(affected.brain)
			user << "\red There is already something here."
			return 0
		var/obj/item/device/mmi/MMI = tool
		if(!MMI.brainmob)
			user << "\red Sticking an empty MMI would sort of defeat the purpose."
			return 0
		if(!MMI.brainmob.key)
			var/ghost_can_reenter = 0
			if(MMI.brainmob.mind)
				for(var/mob/dead/observer/G in player_list)
					if(G.can_reenter_corpse && G.mind == MMI.brainmob.mind)
						ghost_can_reenter = 1
						break
			if(!ghost_can_reenter)
				user << "<span class='notice'>The mmi indicates that their mind is completely unresponsive; there's no point.</span>"
				return 0

		if(MMI.brainmob.stat == DEAD)
			user << "\red Sticking a dead brain would sort of defeat the purpose."
			return 0
		return 1

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts inserting [tool] into %knownface:2%'s [affected.display_name].", \
		"You start inserting [tool] into %knownface:2%'s [affected.display_name].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/head/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% inserted [tool] into %knownface:2%'s [affected.display_name].",	\
		"\blue You have inserted [tool] into %knownface:2%'s [affected.display_name].",actors=list(user,target))
		var/obj/item/device/mmi/MMI = tool
		user.u_equip(tool)
		affected.brain = MMI
		tool.loc = null
		target.disabilities &= ~(NERVOUS|TOURETTES)
		target.sdisabilities &= ~(BLIND|DEAF)
		MMI.brainmob.mind.transfer_to(target, MMI.brainmob)
		if(target.stat == DEAD && target.health>config.health_threshold_dead)
			if(target.health>config.health_threshold_crit)
				target.stat = CONSCIOUS
			else
				target.stat = UNCONSCIOUS

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% fails to insert [tool] into %knownface:2%'s [affected.display_name].", \
		"You fail to insert [tool] into %knownface:2%'s [affected.display_name].",actors=list(user,target))


//////////////////////////////////////////////////////////////////
//			RIBCAGE SURGERY(LUNGS AND ALIENS)					//
//////////////////////////////////////////////////////////////////

/datum/surgery_step/ribcage
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(ishuman(target) && target.checkcoverage(target_zone)) return -1
		if(target_zone != "chest" || !hasorgans(target)) return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(affected.open<2) return 0
		return 1

/datum/surgery_step/ribcage/saw_ribcage
	required_tool = /obj/item/weapon/circular_saw
	name = "saw ribcage"

	min_duration = 50
	max_duration = 70

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.ribcage == 0)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% begins to [istype(tool,/obj/item/weapon/hatchet)?"hack":"saw"] through %knownface:2%'s ribcage with \the [tool].", \
		"You begin to [istype(tool,/obj/item/weapon/hatchet)?"hack":"saw"] through %knownface:2%'s ribcage with \the [tool].",actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("Something hurts horribly in your chest!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\blue %knownface:1% has [istype(tool,/obj/item/weapon/hatchet)?"hacked":"sawed"] through %knownface:2%'s ribcage open with \the [tool].",		\
		"\blue You have [istype(tool,/obj/item/weapon/hatchet)?"hacked":"sawed"] through %knownface:2%'s ribcage open with \the [tool].",actors=list(user,target))
		target.op_stage.ribcage = 1

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1%'s hand slips, cracking %knownface:2%'s ribcage with \the [tool]!" , \
		"\red Your hand slips, cracking %knownface:2%'s ribcage with \the [tool]!" ,actors=list(user,target))


/datum/surgery_step/ribcage/retract_ribcage
	required_tool = /obj/item/weapon/retractor
	allowed_tools = list(/obj/item/weapon/crowbar)
	name = "open ribcage"

	min_duration = 30
	max_duration = 40

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.ribcage == 1)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "%knownface:1% starts to force open the ribcage in %knownface:2%'s torso with \the [tool]."
		var/self_msg = "You start to force open the ribcage in %knownface:2%'s torso with \the [tool]."
		user.visible_message(msg, self_msg,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("Something hurts horribly in your chest!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "\blue %knownface:1% forces open %knownface:2%'s ribcage with \the [tool]."
		var/self_msg = "\blue You force open %knownface:2%'s ribcage with \the [tool]."
		user.visible_message(msg, self_msg,actors=list(user,target))
		target.op_stage.ribcage = 2

		// Whoops!
		if(prob(10))
			var/datum/organ/external/affected = target.get_organ(target_zone)
			affected.fracture()

		if (ishuman(user))
			user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "\red %knownface:1%'s hand slips, breaking %knownface:2%'s ribcage!"
		var/self_msg = "\red Your hand slips, breaking %knownface:2%'s ribcage!"
		user.visible_message(msg, self_msg,actors=list(user,target))
		var/datum/organ/external/affected = target.get_organ(target_zone)
		affected.fracture()

/datum/surgery_step/ribcage/close_ribcage
	required_tool = /obj/item/weapon/retractor
	allowed_tools = list(/obj/item/weapon/crowbar)
	name = "close ribcage"

	min_duration = 20
	max_duration = 40

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.ribcage == 2)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "%knownface:1% starts bending %knownface:2%'s ribcage back into place with \the [tool]."
		var/self_msg = "You start bending %knownface:2%'s ribcage back into place with \the [tool]."
		user.visible_message(msg, self_msg,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("Something hurts horribly in your chest!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "\blue %knownface:1% bends %knownface:2%'s ribcage back into place with \the [tool]."
		var/self_msg = "\blue You bend %knownface:2%'s ribcage back into place with \the [tool]."
		user.visible_message(msg, self_msg,actors=list(user,target))

		target.op_stage.ribcage = 1

/datum/surgery_step/ribcage/mend_ribcage
	required_tool = /obj/item/weapon/bonegel
	name = "mend ribcage"

	min_duration = 20
	max_duration = 40

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.ribcage == 1)
			return ..()
		return 0

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "%knownface:1% starts applying \the [tool] to %knownface:2%'s ribcage."
		var/self_msg = "You start applying \the [tool] to %knownface:2%'s ribcage."
		user.visible_message(msg, self_msg,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("Something hurts horribly in your chest!",1)


	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "\blue %knownface:1% applied \the [tool] to %knownface:2%'s ribcage."
		var/self_msg = "\blue You applied \the [tool] to %knownface:2%'s ribcage."
		user.visible_message(msg, self_msg,actors=list(user,target))

		target.op_stage.ribcage = 0


/datum/surgery_step/ribcage/remove_embryo
	required_tool = /obj/item/weapon/hemostat
	name = "remove foreign body"

	min_duration = 80
	max_duration = 100

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(target.op_stage.ribcage != 2) return 0
		var/datum/embryo/alien/Parasite = locate() in target.parasites
		if(!Parasite) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/msg = "%knownface:1% starts to pull something out from %knownface:2%'s ribcage with \the [tool]."
		var/self_msg = "You start to pull something out from %knownface:2%'s ribcage with \the [tool]."
		user.visible_message(msg, self_msg,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("Something hurts horribly in your chest!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("\red %knownface:1% rips the larva out of %knownface:2%'s ribcage!",
							 "You rip the larva out of %knownface:2%'s ribcage!",actors=list(user,target))

		var/mob/living/carbon/alien/larva/stupid = new(target.loc)
		stupid.death(0)

		var/datum/embryo/alien/Parasite = locate() in target.parasites
		if(Parasite)
			del(Parasite)

		if (ishuman(user)) user:bloody_hands(target, 0)

/datum/surgery_step/ribcage/fix_lungs
	required_tool = /obj/item/weapon/scalpel
	name = "fix lungs"

	min_duration = 70
	max_duration = 90

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(!ishuman(target))
			return 0
		if(!target.is_lung_ruptured() || target.op_stage.ribcage != 2) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts mending the rupture in %knownface:2%'s lungs with \the [tool].", \
		"You start mending the rupture in %knownface:2%'s lungs with \the [tool]." ,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("The pain in your chest is living hell!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/chest/affected = target.get_organ("chest")
		user.visible_message("\blue %knownface:1% mends the rupture in %knownface:2%'s lungs with \the [tool].", \
		"\blue You mend the rupture in %knownface:2%'s lungs with \the [tool]." ,actors=list(user,target))
		affected.ruptured_lungs = 0
		if (ishuman(user) && prob(80)) user:bloody_hands(target, 0)

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/chest/affected = target.get_organ("chest")
		user.visible_message("\red %knownface:1%'s hand slips, slicing an artery inside %knownface:2%'s chest with \the [tool]!", \
		"\red Your hand slips, slicing an artery inside %knownface:2%'s chest with \the [tool]!",actors=list(user,target))
		affected.createwound(CUT, 20)
		if (ishuman(user))
			user:bloody_hands(target, 0)
			user:bloody_body(target)

/datum/surgery_step/stomach
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if(ishuman(target) && target.checkcoverage(target_zone)) return -1
		return target_zone == "chest" && hasorgans(target)// && target.op_stage.ribcage == 0

/datum/surgery_step/stomach/cut_open
	required_tool = /obj/item/weapon/scalpel
	name = "cut stomach open"

	min_duration = 70
	max_duration = 90

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(!affected || !affected.open || target.op_stage.stomach != 0) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts cutting %knownface:2%'s stomach with \the [tool].", \
		"You start cutting %knownface:2%'s stomach with \the [tool]." ,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("The pain in your stomach is living hell!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% cuts %knownface:2%'s stomach open with \the [tool].", \
		"You cut %knownface:2%'s stomach open with \the [tool]." ,actors=list(user,target))
		target.op_stage.stomach = 1
		for(var/mob/M in target.stomach_contents)
			target.stomach_contents.Remove(M)
			M.loc = target.loc
			user.visible_message("\red <B>[M] bursts out of %knownface:1%!</B>",actors=list(target))
		for(var/obj/O in target.stomach_contents)
			target.stomach_contents.Remove(O)
			O.loc = target.loc
			user.visible_message("\red <B>[O] falls out of %knownface:%!</B>",actors=list(target))

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/chest/affected = target.get_organ("chest")
		user.visible_message("\red %knownface:1%'s hand slips, slicing an artery inside %knownface:2%'s stomach with \the [tool]!", \
		"\red Your hand slips, slicing an artery inside %knownface:2%'s stomach with \the [tool]!",actors=list(user,target))
		affected.createwound(CUT, 15)

/datum/surgery_step/stomach/mend_closed
	required_tool = /obj/item/weapon/cautery
	name = "mend stomach"

	min_duration = 70
	max_duration = 90

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if((tool.type in allowed_tools) && !is_hot(tool)) return 0
		if(target.op_stage.stomach != 1) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% starts mending %knownface:2%'s stomach with \the [tool].", \
		"You start mending %knownface:2%'s stomach with \the [tool]." ,actors=list(user,target))

	do_mob_callback(user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		target.custom_pain("The pain in your stomach is living hell!",1)

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		user.visible_message("%knownface:1% mends %knownface:2%'s stomach with \the [tool].", \
		"You mend %knownface:2%'s stomach with \the [tool]." ,actors=list(user,target))
		target.op_stage.stomach = 0
		var/turf/T = get_turf_loc(target)
		for(var/obj/item/O in T.contents)
			if(istype(O,/obj/item/weapon/reagent_containers))
				O.reagents.trans_to(src,O.reagents.total_volume)
				del(O)
			else if(istype(O) && O.w_class<=2)
				target.stomach_contents.Add(O)
				O.loc = target

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/chest/affected = target.get_organ("chest")
		user.visible_message("\red %knownface:1%'s hand slips, leaving a small burn on %knownface:2%'s stomach with \the [tool]!", \
		"\red Your hand slips, leaving a small burn on %knownface:2%'s stomach with \the [tool]!",actors=list(user,target))
		target.apply_damage(8, BURN, affected)

/datum/surgery_step/robo_hide/
	var/datum/organ/external/affected	//affected organ
	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (target_zone == "eyes")	//there are specific steps for eye surgery
			return 0
		if (!hasorgans(target))
			return 0
		affected = target.get_organ(target_zone)
		if (affected == null)
			return 0
		if (affected.status & ORGAN_DESTROYED)
			return 0
		if (!(affected.status & ORGAN_ROBOT))
			return 0
		if(target.checkcoverage(affected,target_zone))
			return -1
		return 1

/datum/surgery_step/robo_hide/cut
	required_tool = /obj/item/weapon/scalpel
	name = "cut prothesis coverage"

	min_duration = 100
	max_duration = 150

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(target_zone == "mouth" || !(affected.status & ORGAN_ROBOT_MASKED)) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts the incision on %knownface:2%'s [affected.display_name] with \the [tool].", \
		"You start the incision on %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% detached %knownface:2%'s [affected.display_name] coverage with \the [tool].", \
		"\blue You detached %knownface:2%'s [affected.display_name] coverage with \the [tool].",actors=list(user,target))
		affected.status &= ~ORGAN_ROBOT_MASKED
		var/obj/item/stack/sheet/plasteel/SF = locate(/obj/item/stack/sheet/plasteel) in target.loc
		if(!SF)
			SF = new(target.loc)
		else
			SF.amount++
		target.UpdateDamageIcon(0)
		target.update_body()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, slicing %knownface:2%'s [affected.display_name] coverate with \the [tool]!", \
		"\red Your hand slips, slicing %knownface:2%'s [affected.display_name] coverage with \the [tool]!",actors=list(user,target))
		affected.status &= ~ORGAN_ROBOT_MASKED
		target.UpdateDamageIcon(0)
		target.update_body()

/datum/surgery_step/robo_hide/hide
	required_tool = /obj/item/stack/sheet/plasteel
	name = "apply prothesis coverage"

	min_duration = 100
	max_duration = 150

	can_use(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/obj/item/stack/sheet/plasteel/SF = tool
		if (!hasorgans(target))
			return 0
		var/datum/organ/external/affected = target.get_organ(target_zone)
		if(SF.amount<=0) return 0
		if(target_zone == "mouth" || (affected.status & ORGAN_ROBOT_MASKED)) return 0
		return ..()

	begin_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("%knownface:1% starts wrapping %knownface:2%'s [affected.display_name] with \the [tool].", \
		"You start wrapping %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))

	end_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\blue %knownface:1% wrapped %knownface:2%'s [affected.display_name] with \the [tool].", \
		"\blue You wrapped %knownface:2%'s [affected.display_name] with \the [tool].",actors=list(user,target))
		affected.status |= ORGAN_ROBOT_MASKED
		var/obj/item/stack/sheet/plasteel/SF = tool
		SF.amount--
		if(!SF.amount)
			user.u_equip(SF)
			del(SF)
		if(target_zone=="head")
			var/new_facial = input(user,"Please select facial hair color.", "Character Customization",rgb(target.r_facial,target.g_facial,target.b_facial)) as color
			if(new_facial)
				target.r_facial = hex2num(copytext(new_facial, 2, 4))
				target.g_facial = hex2num(copytext(new_facial, 4, 6))
				target.b_facial = hex2num(copytext(new_facial, 6, 8))

			var/new_hair = input(user,"Please select hair color.", "Character Customization",rgb(target.r_hair,target.g_hair,target.b_hair)) as color
			if(new_facial)
				target.r_hair = hex2num(copytext(new_hair, 2, 4))
				target.g_hair = hex2num(copytext(new_hair, 4, 6))
				target.b_hair = hex2num(copytext(new_hair, 6, 8))

			var/new_eyes = input(user,"Please select eye color.", "Character Customization",rgb(target.r_eyes,target.g_eyes,target.b_eyes)) as color
			if(new_eyes)
				target.r_eyes = hex2num(copytext(new_eyes, 2, 4))
				target.g_eyes = hex2num(copytext(new_eyes, 4, 6))
				target.b_eyes = hex2num(copytext(new_eyes, 6, 8))

			var/new_age = input(user,"Please select age.", "Character Customization",target.dna.age) as text
			if(new_age)
				new_age = text2num(new_age)
				target.dna.age = min(80,max(16,new_age))

		target.UpdateDamageIcon(0)
		target.update_body()

	fail_step(mob/user, mob/living/carbon/human/target, target_zone, obj/item/tool)
		var/datum/organ/external/affected = target.get_organ(target_zone)
		user.visible_message("\red %knownface:1%'s hand slips, failing wrapping of %knownface:2%'s [affected.display_name] with \the [tool]!", \
		"\red Your hand slips, failing wrapping of %knownface:2%'s [affected.display_name] with \the [tool]!",actors=list(user,target))
