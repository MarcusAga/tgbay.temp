//
/obj/virus
	// a virus instance that is placed on the map, moves, and infects
	invisibility = INVISIBILITY_MAXIMUM

	var/datum/disease2/disease

	New()
		..()

	proc/init(var/spread)
		step_rand(src)
		spread--
		if(spread)
			spawn(10)
				while(spread)
					step_rand(src)
					spread--
					sleep(100)
				sleep(300)
				del(src)
		else
			spawn(300) del(src)
	HasEntered(atom/movable/AM as mob|obj)
		if(!iscarbon(AM)) return
		if(disease && AM:get_infection_chance())
			infect_virus(AM,disease)

/proc/spread_viruses(var/list/viruses, var/spread, var/pos as turf|mob|obj)
	if(spread<0) return
	for(var/datum/disease2/disease/D in viruses)
		if(!("airborne" in D.spread_type)) continue
		for(var/i=0, i<spread*4, i++)
			var/obj/virus/V = new(get_turf(pos))
			V.disease = D.getcopy()
			V.init(spread)

/mob/living/carbon/proc/get_infection_chance()
	var/score = 0
	var/mob/living/carbon/M = src
	if(istype(M, /mob/living/carbon/human))
		if(M:gloves)
			score += 5
		if(istype(M:wear_suit, /obj/item/clothing/suit/space)) score += 10
		if(istype(M:wear_suit, /obj/item/clothing/suit/bio_suit)) score += 10
		if(istype(M:head, /obj/item/clothing/head/helmet/space)) score += 5
		if(istype(M:head, /obj/item/clothing/head/bio_hood)) score += 5
	if(M.wear_mask)
		score += 5
		if(istype(M:wear_mask, /obj/item/clothing/mask/surgical) && !M.internal)
			score += 10
		if(M.internal)
			score += 10

	if(score >= 30)
		return 0
	else if(score == 25 && prob(99))
		return 0
	else if(score == 20 && prob(95))
		return 0
	else if(score == 15 && prob(75))
		return 0
	else if(score == 10 && prob(55))
		return 0
	else if(score == 5 && prob(35))
		return 0

	return 1


/proc/infect_virus(var/mob/living/carbon/M,var/datum/disease2/disease/disease,var/forced = 0, var/infecting_organ)
	return
/*
	if(!islist(M.viruses))
		M.viruses = list()
	if(!disease || (ishuman(M) && M:isCyborg()) || (!forced && (!disease.spread_type || !disease.spread_type.len)))
		return

	for(var/datum/disease2/disease/dis in M.resistances)
		if(dis.issame(disease) && disease.antivirus == dis.antivirus)
			return
	if(prob(disease.infectionchance) || forced)
		// certain clothes can prevent an infection
		if(!forced && !M.get_infection_chance())
			return

		var/datum/disease2/disease/instance = disease.getcopy()
		instance.minormutate()
		if(!instance.specific_position)
			instance.infected_parts = list(infecting_organ)
		instance.init()
		M.viruses += instance
*/

/proc/infect_mob_random_lesser(var/mob/living/carbon/M)
	if(!M.viruses.len)
		var/datum/disease2/disease/instance = new /datum/disease2/disease
		instance.makerandom()
		M.viruses += instance

/proc/infect_mob_random_greater(var/mob/living/carbon/M)
	if(!M.viruses.len)
		var/datum/disease2/disease/instance = new /datum/disease2/disease
		instance.makerandom()
		M.viruses += instance

/datum/disease2/disease
	var/specific_position = 1
	var/list/infected_parts = list("chest")
	var/infectionchance = 10
	var/strength = 0	//how many of this virus is in someone -> how powerful it is
	var/list/spread_type = list("blood", "airborne") // Can also be "airborne" or "touch"
	var/internal_spread_chance = 0
	var/evolve_chance = 5
	var/surgical_heal = 0
	var/must_be_alive = 1
	var/antivirus = 0
	var/permeability_mod = 1

	var/uniqueID = 0

	var/list/datum/disease2/effectholder/effects = list()
	proc/makerandom(var/greater=0)
		var/i = rand(2,4)
		var/datum/disease2/effectholder/holder
		//holder = new /datum/disease2/effectholder
		//holder.getrandomeffect_spreader()
		//effects += holder
		while(i>0)
			holder = new /datum/disease2/effectholder
			holder.getrandomeffect(greater)
			effects += holder
			i--
		uniqueID = rand(0,10000)
		infectionchance = rand(1,20)

	proc/init()
		for(var/datum/disease2/effectholder/H in effects)
			H.myDisease = src
			if(H.effect)
				H.effect.myHolder = H

	proc/minormutate()
		if(evolve_chance && prob(evolve_chance))
			var/datum/disease2/effectholder/holder = pick(effects)
			holder.minormutate()
			infectionchance = min(10,infectionchance + rand(0,1))

	proc/issame(var/datum/disease2/disease/disease)
		var/list/types = list()
		var/list/types2 = list()
		for(var/datum/disease2/effectholder/d in effects)
			types |= d.effect.type
		var/equal = 1

		for(var/datum/disease2/effectholder/d in disease.effects)
			types2 |= d.effect.type

		for(var/type in types)
			if(!(type in types2))
				equal = 0
		return equal

	proc/activate(var/mob/living/carbon/mob)
		if(!mob) return
		if(strength<0)
			cure(mob)

		if(!antivirus)
			if(must_be_alive && mob.stat == 2)
				strength--
				return
			if(mob.radiation > 50)
				if(prob(1))
					majormutate()
			if(mob.reagents.has_reagent("spaceacillin"))
				return
		else if(mob.stat == 2)
			return

		if(mob.reagents.has_reagent("virusfood"))
			mob.reagents.remove_reagent("virusfood",0.1)
			strength = min(strength+5,1500)
		strength = min(strength+1,1500)

		if(antivirus)
			for(var/datum/disease2/disease/dis in mob.viruses)
				if(dis.issame(src) && src.antivirus != dis.antivirus)
					dis.strength -= min(strength/10,1)
		else
			for(var/datum/disease2/effectholder/e in effects)
				e.effect.isinside(mob)
				e.runeffect(mob,strength)

	proc/cure(var/mob/living/carbon/mob)
		if(isnull(mob) || !istype(mob)) return
		for(var/datum/disease2/effectholder/E in effects)
			E.effect.deactivate(mob)
			E.effect.myHolder = null
			E.effect = null
			E.myDisease = null
		effects.Cut()
		mob.viruses-=src

	proc/majormutate()
		var/datum/disease2/effectholder/holder = pick(effects)
		holder.majormutate()
		validate()

	proc/validate()
		var/list/types = list()
		for(var/datum/disease2/effectholder/holder in effects)
			if(holder.effect.type in types)
				effects -= holder
			else
				types += holder.effect.type


	proc/getcopy(var/complexity = 1)
//		world << "getting copy"
		var/datum/disease2/disease/disease = new /datum/disease2/disease
		disease.specific_position = specific_position
		disease.infectionchance = infectionchance
		disease.spread_type = spread_type
		disease.internal_spread_chance = internal_spread_chance
		disease.evolve_chance = evolve_chance
		disease.surgical_heal = surgical_heal
		disease.must_be_alive = must_be_alive
		disease.uniqueID = uniqueID
		disease.antivirus = antivirus

		for(var/datum/disease2/effectholder/holder in effects)
	//		world << "adding effects"
			var/datum/disease2/effectholder/newholder = holder.getcopy(complexity)
			disease.effects += newholder
	//		world << "[newholder.effect.name]"
	//	world << "[disease]"
		return disease
