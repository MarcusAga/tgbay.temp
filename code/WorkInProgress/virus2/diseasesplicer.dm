//
/obj/machinery/computer/diseasesplicer
	name = "Disease Splicer"
	icon = 'computer.dmi'
	icon_state = "crew"

	var/datum/disease2/disease/memorybank = null
	var/analysed = 0
	var/obj/item/weapon/virusdish/dish = null
	var/burning = 0
	var/disk_name

	var/splicing = 0
	var/scanning = 0

/obj/machinery/computer/diseasesplicer/attackby(var/obj/I as obj, var/mob/user as mob)
	if(istype(I, /obj/item/weapon/screwdriver))
		playsound(src.loc, 'Screwdriver.ogg', 50, 1)
		if(do_after(user, 20))
			if (src.stat & BROKEN)
				user << "\blue The broken glass falls out."
				var/obj/structure/computerframe/A = new /obj/structure/computerframe( src.loc )
				new /obj/item/weapon/shard( src.loc )
				//var/obj/item/weapon/circuitboard/diseasesplicer/M = new /obj/item/weapon/circuitboard/diseasesplicer( A )
				for (var/obj/C in src)
					C.loc = src.loc
				//A.circuit = M
				A.state = 3
				A.icon_state = "3"
				A.anchored = 1
				del(src)
			else
				user << "\blue You disconnect the monitor."
				var/obj/structure/computerframe/A = new /obj/structure/computerframe( src.loc )
				//var/obj/item/weapon/circuitboard/diseasesplicer/M = new /obj/item/weapon/circuitboard/diseasesplicer( A )
				for (var/obj/C in src)
					C.loc = src.loc
				//A.circuit = M
				A.state = 4
				A.icon_state = "4"
				A.anchored = 1
				del(src)
	if(istype(I,/obj/item/weapon/virusdish))
		var/mob/living/carbon/c = user
		if(!dish)

			dish = I
			c.drop_item()
			I.loc = src
	if(istype(I,/obj/item/weapon/diseasedisk))
		user << "You upload the contents of the disk into the buffer"
		memorybank = I:virus


	//else
	src.attack_hand(user)
	return

/obj/machinery/computer/diseasesplicer/attack_ai(var/mob/user as mob)
	return src.attack_hand(user)

/obj/machinery/computer/diseasesplicer/attack_paw(var/mob/user as mob)

	return src.attack_hand(user)
	return

/obj/machinery/computer/diseasesplicer/attack_hand(var/mob/user as mob)
	if(..())
		return
	user.machine = src
	var/dat
	if(splicing)
		dat = "Splicing in progress."
	else if(scanning)
		dat = "Splicing in progress."
	else if(burning)
		dat = "Data disk burning in progress."
	else
		dat += "<table border='1'><tr><th>Inserted dish</th><th>Memory</th></tr><tr><td>"

		if(dish)
			if(dish.virus)
				if(dish.growth >= 50)
					for(var/datum/disease2/effectholder/E in dish.virus.effects)
						if(dish.analysed)
							dat += "[virusEffectPairs[E.effect.type]]. Strength : [E.multiplier * 8]. Verosity : [E.chance * 15]. Starts at growth : [E.effect.minstrength/15]."
						else
							dat += "Unanalysed symptom"
						dat += " <A href='?src=\ref[src];adjust=\ref[E]'>Adjust</A>"
						if(memorybank && memorybank.effects.len && analysed)
							dat += " <A href='?src=\ref[src];add=1'>Add</A>"
						dat += "<BR>"
				else
					dat += "Insufficent cells to attempt gene splicing."
			else
				dat += "No virus found in dish."
		else
			dat += "No dish."

		dat += "</td><td>"
		if(memorybank)
			for(var/datum/disease2/effectholder/E in memorybank.effects)
				if(analysed)
					dat += "[virusEffectPairs[E.effect.type]]. Strength : [E.multiplier * 8]. Verosity : [E.chance * 15]. Starts at growth : [E.effect.minstrength/15].<BR>"
				else
					dat += "Unanalysed symptom<BR>"

			dat += "<A href='?src=\ref[src];disk=1'>Burn DNA Sequence to data storage disk</a>"
		else
			dat += "Memory empty."

		dat += "</td></tr><tr>"
		dat += "<td><A href='?src=\ref[src];eject=1'>Eject dish</a><BR><A href='?src=\ref[src];grab=1'>Copy</td>"
		dat += "<td><A href='?src=\ref[src];disk=1'>Write to disk</a><BR><A href='?src=\ref[src];flush=1'>Flush</a></td>"
		dat += "</tr></table>"


	user << browse(dat, "window=computer;size=500x400")
	onclose(user, "computer")
	return

/obj/machinery/computer/diseasesplicer/process()
	if(stat & (NOPOWER|BROKEN))
		return
	use_power(500)
	//src.updateDialog()

	if(scanning)
		scanning -= 1
		if(!scanning)
			state("The [src.name] beeps", "blue")
	if(splicing)
		splicing -= 1
		if(!splicing)
			state("The [src.name] pings", "blue")
	if(burning)
		burning -= 1
		if(!burning)
			var/obj/item/weapon/diseasedisk/d = new /obj/item/weapon/diseasedisk(src.loc)
			d.name = "GNA disk"+(disk_name?" - [disk_name]":"")
			d.virus = memorybank.getcopy(0)
			state("The [src.name] zings", "blue")

	return

/obj/machinery/computer/diseasesplicer/Topic(href, href_list)
	if(..())
		return
	if ((usr.contents.Find(src) || (in_range(src, usr) && istype(src.loc, /turf))) || (istype(usr, /mob/living/silicon)))
		usr.machine = src

		if (href_list["grab"])
			memorybank = dish.virus.getcopy(0)
			analysed = dish.analysed
			scanning = 10

		else if(href_list["eject"])
			dish.loc = src.loc
			dish = null

		else if(href_list["adjust"])
			var/datum/disease2/effectholder/E = locate(href_list["adjust"])
			if(!E) return
			var/choice = input(usr, "What to adjust?") as null|anything in list("Empower","Lessen","Lower threshold","Raise threshold","Raise verosity","Lower verosity","Remove")
			if(!choice) return
			switch(choice)
				if("Empower")
					if(E.multiplier < E.effect.maxm)
						E.multiplier ++
				if("Lessen")
					if(E.multiplier > 1)
						E.multiplier --
				if("Lower Threshold")
					E.effect.minstrength = max(100,E.effect.minstrength-50)
				if("Raise Threshold")
					E.effect.minstrength = min(1500,E.effect.minstrength+50)
				if("Raise Verosity")
					E.chance = min(E.effect.chance_maxm,E.chance+1)
				if("Lower Verosity")
					E.chance = max(1,E.chance-1)

		else if(href_list["add"])
			if(!memorybank || !memorybank.effects.len || !analysed) return
			var/list/symptoms = list()
			for(var/datum/disease2/effectholder/E in memorybank.effects)
				var/same = 0
				for(var/datum/disease2/effectholder/E2 in dish.virus.effects)
					if(E.effect.type == E2.effect.type)
						same = 1
						break
				if(!same)
					symptoms[virusEffectPairs[E.effect.type]] = E
			var/symptom = input(usr, "Select symptome to add:") as null|anything in symptoms
			if(!symptom) return
			var/datum/disease2/effectholder/Hldr = symptoms[symptom]
			if(!Hldr) return
			dish.virus.effects += Hldr.getcopy(0)
			dish.virus.validate()
			splicing = 10

		else if(href_list["disk"])
			disk_name = input(usr, "Name the disk") as text
			burning = 10

		else if(href_list["flush"])
			memorybank = null

		src.add_fingerprint(usr)
	src.updateUsrDialog()
	return

/obj/item/weapon/diseasedisk
	name = "Blank GNA disk"
	icon = 'cloning.dmi'
	icon_state = "datadisk0"
	var/datum/disease2/disease/virus = null

/obj/item/weapon/diseasedisk/premade/New()
	name = "Blank GNA disk"
	virus = new
	virus.effects = list()
