//
/datum/disease2/effect
	var/datum/disease2/effectholder/myHolder
	var/chance_maxm = 20
	var/name = "Blanking effect"
	var/maxm = 1
	var/minstrength = 0
	var/rarity = 100
	proc/isinside(var/mob/living/carbon/mob)
	proc/activate(var/mob/living/carbon/mob, var/multiplier, var/strength)
	proc/deactivate(var/mob/living/carbon/mob)

	New()
		..()
		if(!virusEffectPairs[src.type])
			var/nname = copytext(md5("[rand(1,99999999999)]"),1,10)
			var/i=0
			while((nname in virusEffectNames) && i<1000)
				nname = copytext(md5("[rand(1,99999999999)]"),1,10)
			if(i>=1000 || (nname in virusEffectNames))
				nname = "[rand(1,99999999999)]"
			virusEffectNames += nname
			virusEffectPairs[src.type] = nname

/datum/disease2/effect/gibbingtons
	name = "Gibbingtons Syndrome"
	minstrength = 1250
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.gib()
		//spread lots of virus here

/datum/disease2/effect/shakey
	name = "World Shaking syndrome"
	minstrength = 500
	maxm = 3
	activate(var/mob/living/carbon/mob,var/multiplier)
		shake_camera(mob,5*multiplier)

/datum/disease2/effect/telepathic
	name = "Telepathy Syndrome"
	minstrength = 750
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.mutations |= 104
	deactivate(var/mob/living/carbon/mob,var/multiplier)
		mob.mutations.Remove(104)

/datum/disease2/effect/monkey
	name = "Monkism syndrome"
	minstrength = 1400
	activate(var/mob/living/carbon/mob,var/multiplier)
		if(istype(mob,/mob/living/carbon/human))
			var/mob/living/carbon/human/h = mob
			h.monkeyize()

// lesser syndromes, partly just copypastes
/datum/disease2/effect/temperature
	name = "Temperature"
	minstrength = 50
	maxm = 6
	activate(var/mob/living/carbon/mob,var/multiplier)
		if(mob.bodytemperature<300+multiplier*5)
			mob.bodytemperature += 5

/datum/disease2/effect/scream
	name = "Random screaming syndrome"
	minstrength = 500
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote("scream")

/datum/disease2/effect/deaf
	name = "Hard of hearing syndrome"
	minstrength = 1000
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.ear_deaf += 20

/datum/disease2/effect/sneeze
	name = "Coldingtons Effect"
	minstrength = 100
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote("sneeze")

/datum/disease2/effect/sleepy
	name = "Resting syndrome"
	minstrength = 250
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote("collapse")

/datum/disease2/effect/toxins
	name = "Hyperacid Syndrome"
	minstrength = 750
	maxm = 3
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.adjustToxLoss((2*multiplier))

/datum/disease2/effect/mind
	name = "Lazy mind syndrome"
	minstrength = 350
	maxm = 3
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.setBrainLoss(15)

/datum/disease2/effect/drowsy
	name = "Bedroom Syndrome"
	minstrength = 250
	maxm = 2
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.drowsyness += 5

/datum/disease2/effect/deaf
	name = "Hard of hearing syndrome"
	minstrength = 750
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.ear_deaf += 5

/datum/disease2/effect/gunck
	name = "Flemmingtons"
	minstrength = 100
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob << "\red Mucous runs down the back of your throat."

/datum/disease2/effect/radian
	name = "Radian's syndrome"
	minstrength = 1000
	maxm = 6
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.radiation += multiplier

/datum/disease2/effect/cough
	name = "Anima Syndrome"
	minstrength = 250
	maxm = 2
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote(multiplier>1?"heavycough":"cough")

/datum/disease2/effect/hallucinations
	name = "Hallucinational Syndrome"
	minstrength = 500
	maxm = 5
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.hallucination += 5

/datum/disease2/effect/endorphene
	name = "Joy Syndrome"
	minstrength = 500
	maxm = 5
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.druggy += 5

/datum/disease2/effect/hungry
	name = "Appetiser Effect"
	minstrength = 250
	maxm = 5
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.nutrition = max(0, mob.nutrition - multiplier)

/datum/disease2/effect/groan
	name = "Groaning Syndrome"
	minstrength = 500
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote("groan")

/datum/disease2/effect/drool
	name = "Saliva Effect"
	minstrength = 100
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote("drool")

/datum/disease2/effect/fridge
	name = "Refridgerator Syndrome"
	minstrength = 250
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.say("shiver")

/datum/disease2/effect/twitch
	name = "Twitcher"
	minstrength = 100
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote("twitch")

/datum/disease2/effect/giggle
	name = "Uncontrolled Laughter Effect"
	minstrength = 500
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.emote("giggle")

/datum/disease2/effect/vomiting
	name = "Vomiting"
	minstrength = 500
	activate(var/mob/living/carbon/mob,var/multiplier)
		if(!ishuman(mob)) return
		if (mob.nutrition > 100)
			var/mob/living/carbon/human/H = mob
			H.vomit()
		else
			mob << "\red You gag as you want to throw up, but there's nothing in your stomach!"
			mob.Weaken(10)
			mob.adjustToxLoss(3)

/datum/disease2/effect/muscle_ache
	name = "Muscle Ache"
	minstrength = 500
	chance_maxm = 10
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob << "\red Your muscles ache."
		if(prob(20))
			mob.take_organ_damage(1)

/datum/disease2/effect/stomach_ache
	name = "Stomach Ache"
	minstrength = 500
	chance_maxm = 10
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob << "\red Your stomach hurts."
		if(prob(20))
			mob.adjustToxLoss(1)
			mob.updatehealth()

/datum/disease2/effect/speech_honk
	name = "Speech honking"
	minstrength = 200
	activate(var/mob/living/carbon/mob,var/multiplier, var/strength)
		mob.disease_mods["honk"] = strength/30
	deactivate(var/mob/living/carbon/mob,var/multiplier)
		mob.disease_mods -= "honk"

/datum/disease2/effect/random_honk
	name = "Random honking"
	minstrength = 500
	activate(var/mob/living/carbon/mob,var/multiplier)
		mob.say( pick( list("HONK!", "Honk!", "Honk.", "Honk?", "Honk!!", "Honk?!", "Honk...") ) )

/datum/disease2/effect/dna_spread
	name = "DNA-spread"
	minstrength = 200
	var/old_struc_enzymes = null
	activate(var/mob/living/carbon/mob, var/multiplier, var/strength)
		if(!("UE" in myHolder.data) && !("UI" in myHolder.data) && !("SE" in myHolder.data))
			var/num = 0
			if(prob(33))
				myHolder.data["UE"] = mob.dna.unique_enzymes
				num ++
			if(prob(num?33:50))
				myHolder.data["UI"] = mob.dna.uni_identity
				num ++
			if(!num || prob(num==2?33:50))
				myHolder.data["SE"] = mob.dna.struc_enzymes
		else
			if(myHolder.data["UE"] && myHolder.data["UE"]!=mob.dna.unique_enzymes)
				mob.dna.unique_enzymes = myHolder.data["UE"]
			if(myHolder.data["UI"] && myHolder.data["UI"]!=mob.dna.uni_identity)
				mob.dna.uni_identity = myHolder.data["UI"]
				updateappearance(mob, mob.dna.uni_identity)
			if(myHolder.data["SE"] && myHolder.data["SE"]!=mob.dna.struc_enzymes)
				if(!old_struc_enzymes)
					old_struc_enzymes = mob.dna.struc_enzymes
				mob.dna.struc_enzymes = myHolder.data["SE"]
				domutcheck(mob)
	deactivate(var/mob/living/carbon/mob,var/multiplier)
		mob.dna.unique_enzymes = mob.dna.original_unique_enzymes
		mob.dna.uni_identity = mob.dna.uni_identity
		if(old_struc_enzymes)
			mob.dna.struc_enzymes = old_struc_enzymes
		updateappearance(mob, mob.dna.uni_identity)
		domutcheck(mob)

/datum/disease2/effectholder
	var/name = "Holder"
	var/datum/disease2/disease/myDisease
	var/datum/disease2/effect/effect
	var/list/data = list()
	var/chance = 0 //Chance in percentage each tick
	var/happensonce = 0
	var/multiplier = 1 //The chance the effects are WORSE

	proc/runeffect(var/mob/living/carbon/human/mob,var/strength)
		if(happensonce > -1 && effect.minstrength <= strength && prob(chance))
			effect.activate(mob,multiplier,strength)
			if(happensonce == 1)
				happensonce = -1

	proc/getrandomeffect(var/canBeRare = 0)
		var/list/datum/disease2/effect/list = list()
		for(var/e in (typesof(/datum/disease2/effect) - /datum/disease2/effect))
		//	world << "Making [e]"
			var/datum/disease2/effect/f = new e
			if(canBeRare || (f.rarity && prob(f.rarity)))
				list += f
		effect = pick(list)
		chance = rand(0,effect.chance_maxm)
		multiplier = rand(1,effect.maxm)

	proc/minormutate()
		switch(pick(1,2,3,4,5))
			if(1)
				chance = rand(0,effect.chance_maxm)
			if(2)
				multiplier = rand(1,effect.maxm)

	proc/majormutate()
		if(prob(40)) return
		if(prob(60))
			effect.minstrength = max(1,min(1500,effect.minstrength+rand(-100,100)))
		else
			getrandomeffect(1)

	proc/getcopy(var/complexity = 1)
		var/datum/disease2/effectholder/newholder = new
		newholder.effect = new effect.type
		newholder.chance = chance
		newholder.multiplier = multiplier
		newholder.happensonce = happensonce
		newholder.effect.minstrength = effect.minstrength
		if(complexity)
			newholder.data = data
		return newholder
