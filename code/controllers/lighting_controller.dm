//
var/datum/controller/lighting/lighting_controller = new ()

datum/controller/lighting
	var/processing = 0
	var/processing_interval = 5	//setting this too low will probably kill the server. Don't be silly with it!
	var/process_cost = 0
	var/iteration = 0

	var/lighting_states = 7

	var/list/lights = list()
	var/lights_workload_max = 0
	var/list/changed_turfs = list()
	var/changed_turfs_workload_max = 0

datum/controller/lighting/New()
	lighting_states = max( 0, length(icon_states(LIGHTING_ICON))-1 )
	if(lighting_controller != src)
		if(istype(lighting_controller,/datum/controller/lighting))
			Recover()	//if we are replacing an existing lighting_controller (due to a crash) we attempt to preserve as much as we can
			del(lighting_controller)
		lighting_controller = src


//Workhorse of lighting. It cycles through each light to see which ones need their effects updating. It updates their
//effects and then processes every turf in the queue, moving the turfs to the corresponing lighting sub-area.
//All queue lists prune themselves, which will cause lights with no luminosity to be garbage collected (cheaper and safer
//than deleting them). Processing interval should be roughly half a second for best results.
//By using queues we are ensuring we don't perform more updates than are necessary
datum/controller/lighting/proc/process()
	processing = 1
	spawn(0)
		set background = 1
		while(1)
			if(processing)
				iteration++
				var/started = world.timeofday

				lights_workload_max = max(lights_workload_max,lights.len)
				for(var/i=1, i<=lights.len, i++)
					var/datum/light_source/L = lights[i]
					if(L && !L.check())
						continue
					lights.Cut(i,i+1)
					i--
				sleep(-1)

				changed_turfs_workload_max = max(changed_turfs_workload_max,changed_turfs.len)
				var/list/changed = changed_turfs.Copy()
				changed_turfs.Cut()		// reset the changed list
				for(var/i=1, i<=changed.len, i++)
					var/turf/T = changed[i]
					if(T && T.lighting_changed)
						T.lighting_changed = 0
						T.update_turf_shadows()
//					if(i%10==0)
//						sleep(-1)

				process_cost = (world.timeofday - started)

			sleep(processing_interval)

//same as above except it attempts to shift ALL turfs in the world regardless of lighting_changed status
//Does not loop. Should be run prior to process() being called for the first time.
//Note: if we get additional z-levels at runtime (e.g. if the gateway thin ever gets finished) we can initialize specific
//z-levels with the z_level argument
datum/controller/lighting/proc/Initialize(var/z_level)
	processing = 0
	spawn(-1)
		set background = 1
		for(var/i=1, i<=lights.len, i++)
			var/datum/light_source/L = lights[i]
			if(L.check())
				lights.Cut(i,i+1)
				i--


//Used to strip valid information from an existing controller and transfer it to a replacement
//It works by using spawn(-1) to transfer the data, if there is a runtime the data does not get transfered but the loop
//does not crash
datum/controller/lighting/proc/Recover()
	if(!istype(lighting_controller.lights,/list))
		lighting_controller.lights = list()

	for(var/i=1, i<=lighting_controller.lights.len, i++)
		var/datum/light_source/L = lighting_controller.lights[i]
		if(istype(L))
			spawn(-1)			//so we don't crash the loop (inefficient)
				L.check()
				lights += L		//If we didn't runtime then this will get transferred over

	var/msg = "## DEBUG: [time2text(world.timeofday)] lighting_controller restarted. Reports:\n"
	for(var/varname in lighting_controller.vars)
		switch(varname)
			if("tag","bestF","type","parent_type","vars")	continue
			else
				var/varval1 = lighting_controller.vars[varname]
				var/varval2 = vars[varname]
				if(istype(varval1,/list))
					varval1 = "/list([length(varval1)])"
					varval2 = "/list([length(varval2)])"
				msg += "\t [varname] = [varval1] -> [varval2]\n"
	world.log << msg


/datum/vision_square
	var/turf/C
	var/x1
	var/x2
	var/y1
	var/y2
	var/z
	var/radius
	New(var/turf/T, var/r)
		if(!T || !isturf(T) || r<0) return
		..()
		var/x = T.x
		var/y = T.y
		z = T.z

		C = T
		radius = r

		x1 = x-r
		x2 = x+r

		y1 = y-r
		y2 = y+r

		if(x1<1) x1 = 1
		if(y1<1) y1 = 1
		if(x2>world.maxx) x2 = world.maxx
		if(y2>world.maxy) y2 = world.maxy

	proc/intersects(var/datum/vision_square/O)
		if(!O) return 0
		if(z != O.z) return 0
		if(x1>O.x2 || y1>O.y2) return 0
		if(O.x1>x2 || O.y1>y2) return 0
		return 1

	proc/equals(var/turf/T, var/r)
		if(!T || !isturf(T) || r<0) return 0
		if(T==C && r==radius) return 1
		return 0

	proc/contains(var/Tx, var/Ty)
		if(Tx<x1 || Tx>x2) return 0
		if(Ty<y1 || Ty>y2) return 0
		return 1

/client/var/datum/vision_square/visible_turfs = null

/client/proc/update_seeing_pressure()
	var/mob/M = mob
	if(eye)
		M = eye

	var/turf/Center = get_turf(M)
	if(!Center)
		clear_seeing_shadows()
		return

	var/radius = view+2
	if(visible_turfs)
		if(visible_turfs.z != Center.z)
			clear_seeing_shadows()

	var/datum/vision_square/new_visible_turfs = new(Center, radius)
	if(visible_turfs && !visible_turfs.intersects(new_visible_turfs))
		clear_seeing_shadows()
		return

	var/z = new_visible_turfs.z
	var/x1 = visible_turfs ? min(visible_turfs.x1, new_visible_turfs.x1) : new_visible_turfs.x1
	var/x2 = visible_turfs ? max(visible_turfs.x2, new_visible_turfs.x2) : new_visible_turfs.x2
	var/y1 = visible_turfs ? min(visible_turfs.y1, new_visible_turfs.y1) : new_visible_turfs.y1
	var/y2 = visible_turfs ? max(visible_turfs.y2, new_visible_turfs.y2) : new_visible_turfs.y2
	for(var/x = x1, x <= x2, x++)
		for(var/y = y1, y <= y2, y++)
			var/old_contain = visible_turfs ? visible_turfs.contains(x,y) : 0
			var/new_contain = new_visible_turfs ? new_visible_turfs.contains(x,y) : 0
			if(!old_contain && !new_contain) continue
			var/turf/T = locate(x,y,z)
			if(!T || !isturf(T)) continue
			if(old_contain && new_contain)
				var/level = T.getPressureLevel(M)

				var/list/S = T.shadows[src]
				if(S)
					if(S["level"]==level)
						continue
					if(S["img"])
						images -= S["img"]

				var/image/img = T.getShadowObject()
				img.alpha = 80
				img.color = level
				S = list("img" = img, "level" = level)
				T.shadows[src] = S
				if(img)
					images += img
			else if(new_contain)
				var/level = T.getPressureLevel(M)

				var/image/img = T.getShadowObject()
				img.alpha = 80
				img.color = level
				var/list/S = list("img" = img, "level" = level)
				T.shadows[src] = S
				if(img)
					images += img
			else if(old_contain)
				var/list/S = T.shadows[src]
				if(S)
					if(S["img"])
						images -= S["img"]
					T.shadows -= src
	visible_turfs = new_visible_turfs

/client/proc/clear_seeing_shadows()
	if(!visible_turfs) return
	var/z = visible_turfs.z
	for(var/x = visible_turfs.x1, x <= visible_turfs.x2, x++)
		for(var/y = visible_turfs.y1, y <= visible_turfs.y2, y++)
			var/turf/T = locate(x,y,z)
			if(!T) continue
			var/list/S = T.shadows[src]
			if(S && S["img"])
				images -= S["img"]
			T.shadows -= src

	visible_turfs = null

/client/proc/update_seeing_shadows()
	var/mob/M = mob
	if(eye)
		M = eye

	var/turf/Center = get_turf(M)
	if(!Center)
		clear_seeing_shadows()
		return

	var/radius = view+2
	if(visible_turfs)
		if(visible_turfs.equals(Center, radius))
			return
		if(visible_turfs.z != Center.z)
			clear_seeing_shadows()

	var/datum/vision_square/new_visible_turfs = new(Center, radius)
	if(visible_turfs && !visible_turfs.intersects(new_visible_turfs))
		clear_seeing_shadows()
		return

	var/z = new_visible_turfs.z
	var/x1 = visible_turfs ? min(visible_turfs.x1, new_visible_turfs.x1) : new_visible_turfs.x1
	var/x2 = visible_turfs ? max(visible_turfs.x2, new_visible_turfs.x2) : new_visible_turfs.x2
	var/y1 = visible_turfs ? min(visible_turfs.y1, new_visible_turfs.y1) : new_visible_turfs.y1
	var/y2 = visible_turfs ? max(visible_turfs.y2, new_visible_turfs.y2) : new_visible_turfs.y2
	for(var/x = x1, x <= x2, x++)
		for(var/y = y1, y <= y2, y++)
			var/old_contain = visible_turfs ? visible_turfs.contains(x,y) : 0
			var/new_contain = new_visible_turfs ? new_visible_turfs.contains(x,y) : 0
			if(!old_contain && !new_contain) continue
			var/turf/T = locate(x,y,z)
			if(!T || !isturf(T)) continue
			if(old_contain && new_contain)
				var/area/A = T.loc
				if(!A.lighting_use_dynamic) continue
				var/level = T.getShadowLevel(M)

				var/list/S = T.shadows[src]
				if(S)
					if(S["level"]==level)
						continue
					if(S["img"])
						images -= S["img"]

				var/image/img = T.getShadow(level)
				S = list("img" = img, "level" = level)
				T.shadows[src] = S
				if(img)
					images += img
			else if(new_contain)
				var/area/A = T.loc
				if(!A.lighting_use_dynamic) continue
				var/level = T.getShadowLevel(M)

				var/image/img = T.getShadow(level)
				var/list/S = list("img" = img, "level" = level)
				T.shadows[src] = S
				if(img)
					images += img
			else if(old_contain)
				var/list/S = T.shadows[src]
				if(S)
					var/image/I = S["img"]
					if(I)
						images -= I
					T.shadows -= src
		sleep(-1)
	visible_turfs = new_visible_turfs

/turf/proc/update_turf_shadows()
	if(!shadow)
		shadow = new
		shadow.T = src
	if(shadow.updateShadowObjectLight())
		var/haveNulls = 0
		for(var/client/C in shadows)
			if(!C)
				haveNulls = 1
				continue
			var/list/S = shadows[C]
			var/mob/M = C.mob
			if(C.eye)
				M = C.eye
			var/level
			if(C.holder && C.holder.show_air_pressure)
				level = getPressureLevel(M)
			else
				level = getShadowLevel(M)
			if(S)
				if(S["img"])
					C.images -= S["img"]
			var/image/img
			if(C.holder && C.holder.show_air_pressure)
				img = getShadowObject()
				img.alpha = 80
				img.color = level
			else
				img = getShadow(level)
			S = list("img" = img, "level" = level)
			shadows[C] = S
			if(img)
				C.images += img
		if(haveNulls)
			shadows -= null
/turf/proc/getPressureLevel()
	return -1
/turf/simulated/getPressureLevel()
	var/level = 0
	if(parent && parent.group_processing)
		level = (parent.air ? parent.air.return_pressure() : 0)
	else
		level = (air ? air.return_pressure() : 0)
	return rgb(level>200?level-200:0, abs(level-200)<100?abs(level-200)*2.55:0, level<100?255-level*2.5:0)
/turf/proc/getShadowLevel(var/mob/M)
	var/dist = get_dist(src,M)
	var/darksee = 0
	if(istype(M))
		darksee = M.see_in_dark
	if(darksee > 50)
		return -1
	if(darksee >= dist)
		darksee -= dist
	else
		darksee = 0
	return max(lighting_lumcount,min(darksee,4))
/turf/space/getShadowLevel(var/mob/M)
	return -1
/turf/var/tmp/obj/shadow/shadow = null
/turf/proc/getShadow(var/level)
	if(!loc:lighting_use_dynamic || level==-1)
		return null
	if(!shadow)
		shadow = new
		shadow.T = src
	contents |= shadow
	return shadow.getShadow(level)
/turf/proc/getShadowObject()
	if(!shadow)
		shadow = new
		shadow.T = src
	contents |= shadow
	return image('icons/effects/whiteoverlay.dmi',shadow,layer=LIGHTING_LAYER)

/turf/space/luminosity = 1
/turf/space/getShadow(var/level)
	if(luminosity<1)
		luminosity = 1
	return null
/obj/shadow
	var/list/alphas = list(245, 195, 150, 110, 75, 40, 15, 0)
	name = "shadow"
	anchored = 1
	mouse_opacity = 0
	var/tmp/image/shadowImage = null
	var/turf/T = null
	var/lastLevel = -1
	proc/getShadowImageObject(var/level)
		var/image/Image = image('icons/effects/whiteoverlay.dmi',src,layer=LIGHTING_LAYER)
		Image.color = rgb(0,0,0)
		if(level>=0 && level<7)
			Image.alpha = alphas[level+1]
		else
			Image.alpha = 10
		return Image
	proc/updateShadowObjectLight()
		if(!T)
			del(src)
			return
		if(loc != T)
			loc = T
		var/real_light = min(max(round(T.lighting_lumcount,1),0),lighting_controller.lighting_states)
		if(lastLevel==real_light)
			return 0
		T.luminosity = real_light > 0 ? 1 : 0
		if(shadowImage && lastLevel!=real_light)
			del(shadowImage)
		shadowImage = getShadowImageObject(real_light)
		lastLevel = real_light
		return 1
	proc/getShadow(var/level)
		var/light = min(max(round(level,1),0),lighting_controller.lighting_states)
		if(light<0 || light == 7) return null
		if(light == lastLevel && shadowImage) return shadowImage
		else return getShadowImageObject(light)
	Move()
		loc = T
	New()
		..()
		verbs.Cut()

/client/New()
	..()
	spawn(0) update_seeing_shadows_routine()
/client/var/updating_seeing_shadows_routine = 0
/client/proc/update_seeing_shadows_routine()
	set background = 1
	if(updating_seeing_shadows_routine) return
	updating_seeing_shadows_routine = 1
	var/mob/lastEye = null
	var/turf/lastTurf = null
	var/lastdarksee = 0
	while(src)
		sleep(1)
		if(holder && holder.show_air_pressure)
			update_seeing_pressure()
			continue
		var/mob/cEye = mob
		if(eye)
			cEye = eye
		var/turf/cTurf = get_turf(cEye)
		var/darksee = 0
		if(ismob(cEye))
			darksee = cEye.see_in_dark
		if(cTurf==lastTurf && cEye==lastEye && lastdarksee==darksee) continue
		update_seeing_shadows()
		lastTurf = cTurf
		lastEye = cEye
		lastdarksee = darksee
