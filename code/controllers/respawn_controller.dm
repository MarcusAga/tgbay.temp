//
var/global/list/spawn_cryopods = list()

var/global/datum/lts_controller/respawn_controller = new()

/datum/lts_controller
	var/list/freezed_persons = list()
	var/list/freezed_to_loc = list()
	var/list/defreeze_persons = list()
	proc
		lts_add_freezed(var/mob/living/L,var/openmenu = 1, var/obj/machinery/lterm_sleeper/sleeper = null)
		lts_remove_freezed(var/mob/living/L)
		lts_open_menu(var/mob/living/L)
		do_respawn(var/mob/living/L)
		checkCanDefreezeSomeone()

	New()
		..()
		spawn(60*10) process()
	proc/process()
		for(var/mob/living/L in freezed_persons)
			lts_open_menu(L)
		spawn(60*10) process()

	Topic(href, href_list)
		if(href_list["BeginDefreeze"])
			var/mob/living/L = locate(href_list["BeginDefreeze"])
			defreeze_persons |= L
			checkCanDefreezeSomeone()
		lts_open_menu(usr)
	
	lts_add_freezed(var/mob/living/L,var/openmenu = 1, var/obj/machinery/lterm_sleeper/sleeper = null)
		if(!L) return //WTF was that?
		freezed_persons += L
		L.invisibility = 101
		if(openmenu) lts_open_menu(L)
		if(sleeper)
			freezed_to_loc["\ref[L]"] = sleeper
	
	lts_remove_freezed(var/mob/living/L)
		L.invisibility = 0
		freezed_persons.Remove(L)
	
	lts_open_menu(var/mob/living/L)
		if(!L.client) return
		var/dat = "You are in cryosleep<br>"
		if(!spawn_cryopods.len)
			dat += "No working cryopods present"
		else if(L in defreeze_persons)
			dat += "Decryonisation protocols active"
		else
			dat += "<a href='byond://?src=\ref[src];BeginDefreeze=\ref[L]'>Begin Decryonisation</a>"
		L << browse(dat, "window=ltsleeper;size=400x500")
		onclose(L, "sleeper", src)
	
	do_respawn(var/mob/living/L)
		lts_add_freezed(L, 0)
		defreeze_persons += L
		checkCanDefreezeSomeone()
	
	checkCanDefreezeSomeone()
		if(!spawn_cryopods.len)
			for(var/mob/living/L in freezed_persons)
				lts_open_menu(freezed_persons)
			for(var/mob/living/L in defreeze_persons)
				if(L.client)
					lts_open_menu(L)
			defreeze_persons = list()
		if(!defreeze_persons.len) return
		var/mob/living/person = pick(defreeze_persons)
		var/obj/machinery/lterm_sleeper/chosen_stl
		chosen_stl = freezed_to_loc["\ref[person]"]
		if(chosen_stl && (chosen_stl in spawn_cryopods) && (chosen_stl.occupant || chosen_stl.process!=0) || chosen_stl.isadmin)
			chosen_stl = null
		if(!chosen_stl)
			for(var/obj/machinery/lterm_sleeper/stl in shuffle(spawn_cryopods))
				if(!stl.occupant)
					chosen_stl = stl
					break
		if(!chosen_stl)
			for(var/obj/machinery/lterm_sleeper/stl in shuffle(spawn_cryopods))
				if(stl.process == 0)
					chosen_stl.eject()
					chosen_stl = stl
					break
			if(!chosen_stl) return
		chosen_stl.occupant = person
		chosen_stl.occupant.loc = chosen_stl
		defreeze_persons.Remove(person)
		lts_remove_freezed(person)
		freezed_to_loc.Remove("\ref[person]")
		chosen_stl.initiate_defreeze()
