//
var/global/datum/strategy_controller/strategy_controller = new()

/datum/strategy_controller
	var/started = 0
	var/list/targets = list()

	New()
		..()
		spawn(60*10) process()

	proc/process()
		set background = 1

		if(started) return
		started = 1
		while(started)
			for(var/who in targets)
				if(who)
					if(targets[who])
						var/datum/strategy_record/target = targets[who]
						target.process()
						sleep(-1)
					else
						targets -= who
				else
					targets -= who

			for(var/client/C in admin_list)
				if(C.holder && C.holder.strategy_mode)
					C.holder.updateStrategy()

			sleep(5)

	proc/checkInBase(var/mob/living/who, var/removing)
		var/datum/strategy_record/record = targets[who]
		if(removing && record)
			record.checkDeletion()
		else if(!record)
			record = new
			targets[who] = record
			record.setOwner(who)

	proc/assignTarget(var/list/selected, var/target)
		if(target)
			var/turf/T = get_turf(target)
			if(T.density) return
			for(var/obj/O in T)
				if(O.density)
					return
		for(var/mob/living/who in selected)
			var/datum/strategy_record/record = targets[who]
			if(record)
				record.setTarget(target)

	proc/updateIcons(var/client/clnt, var/list/selected)
		. = list()
		if(!clnt.holder.strategy_mode) return
		var/list/used_targets = list()
		for(var/mob/living/who in targets)
			if(who in selected) continue
			var/datum/strategy_record/record = targets[who]
			if(!record) continue
			var/found = 0
			if(record.me_other_icon)
				for(var/client/C in admin_list)
					if(C==clnt) continue
					if(C.holder && C.holder.strategy_mode && (who in C.holder.selected_mobs))
						clnt << record.me_other_icon
						. += record.me_other_icon
						found = 1
						break
			if(!found && record.me_inactive_icon)
				clnt << record.me_inactive_icon
				. += record.me_inactive_icon
		for(var/mob/living/who in selected)
			var/datum/strategy_record/record = targets[who]
			if(!record) continue
			if(record.me_icon)
				clnt << record.me_icon
				. += record.me_icon
			if(record.target)
				if(record.target in used_targets) continue
				used_targets += record.target
				if(record.tgt_icon)
					clnt << record.tgt_icon
					. += record.tgt_icon
				if(!record.path_images && record.path && record.path.len>1)
					var/dir=0
					var/turf/A = record.path[1]
					record.path_images = list()
					for(var/i = 2, i<=record.path.len, i++)
						var/ndir = get_dir(A,record.path[i])
						if(dir!=ndir)
							if(dir!=0)
								var/image/wpt = image('stategy_selections.dmi', A, "waypoint")
								wpt.layer = 15
								wpt.dir = ndir
								record.path_images[A] = wpt
							dir = ndir
						A = record.path[i]
				if(record.path_images && record.path_images.len)
					for(var/T in record.path_images)
						var/img = record.path_images[T]
						clnt << img
						. += img

/datum/strategy_record
	var/mob/living/me
	var/atom/target
	var/image/me_icon = null
	var/image/me_inactive_icon = null
	var/image/me_other_icon = null
	var/image/tgt_icon = null
	var/image/tgt_other_icon = null
	var/move_delay = 0
	var/list/path = list()
	var/list/path_images = null
	var/turf/last_fail = null
	var/failcount = 0
	var/made_a_step = 0

	proc/setOwner(var/mob/living/nme)
		me = nme
		me_icon = image('stategy_selections.dmi', me, "selected")
		me_icon.layer = 15
		me_inactive_icon = image('stategy_selections.dmi', me, "proceeding")
		me_inactive_icon.layer = 15
		me_other_icon = image('stategy_selections.dmi', me, "selectedbyother")
		me_other_icon.layer = 15
		if(istype(me,/mob/living/simple_animal)) me:stop_automated_movement = 1
		me.being_strategy_controlled = 1

	Del()
		if(me)
			if(istype(me,/mob/living/simple_animal)) me:stop_automated_movement = 0
			me.being_strategy_controlled = 0
		..()

	proc/setTarget(var/atom/ntgt)
		target = ntgt
		if(target)
			tgt_icon = image('stategy_selections.dmi', target)
			if(isliving(target))
				tgt_icon.icon_state = "target"
			else
				tgt_icon.icon_state = "waypointstart"
			tgt_icon.layer=15
			if(isliving(target))
				tgt_other_icon = image('stategy_selections.dmi', target, "targetbyother")
				tgt_other_icon.layer=15
			else
				tgt_other_icon = null
			path = null
		else
			tgt_icon = null
			tgt_other_icon = null
			checkDeletion()

	proc/checkDeletion()
		if(target) return
		if(me)
			var/selected = 0
			for(var/client/C in admin_list)
				if(C.holder && C.holder.strategy_mode && (me in C.holder.selected_mobs))
					selected = 1
					break
			if(!selected)
				strategy_controller.targets -= me
		else
			del(src)

	proc/process()
		if(isnull(target) || me.client) return
		if(isnull(me) || me.stat) del(src)
		var/dist = get_dist(me,target)

		var/moving = (dist>1 && path)
		me_icon.icon_state = moving?"move":"selected"
		me_other_icon.icon_state = moving?"movebyother":"selectedbyother"

		if(isliving(target) && dist<=1)
			var/mob/living/L = target
			if(L.stat==2)
				setTarget(null)
				return
//			me.a_intent = "hurt"
			if(isslime(me))
				if(!me:Victim)
					me:Feedon(target)
			else
				usr = me
				L.DblClick(null,null,null)
				usr = null
			path_images = null
			move_delay = world.time+10
		else if(!isnull(target))
			if(dist>0)
				if(me.anchored || !me.canmove || world.time < move_delay) return
				if(path && path.len && get_dist(me,path[1])>1)
					path = null
					path_images = null
				if(path && path.len)
					if(me.loc == path[1])
						path -= me.loc
						if(path_images && path_images.len)
							path_images -= me.loc
					else if(step_towards(me,path[1]))
						move_delay = world.time + me.movement_delay() + 1 + config.run_speed
						path -= me.loc
						if(path_images && path_images.len)
							path_images -= me.loc
						last_fail = null
						failcount = 0
						made_a_step = 1
					else
						failcount++
						made_a_step = 1
						if(prob(10*failcount-8))
							last_fail = path[1]
							path = null
							path_images = null
							failcount = 0
				if(dist>0 && !(path && path.len && (path[path.len]==get_turf(target) || !made_a_step)))
					sleep(0)
					if(failcount%2 == 0)
						path = AStar(me.loc, get_turf(target), /turf/proc/CardinalTurfsWithMobAccess, /turf/proc/Distance, 0, 128, id=me, exclude=last_fail)
					else
						path = AStar(me.loc, get_turf(target), /turf/proc/CardinalTurfsIgnoreDoors, /turf/proc/Distance, 0, 64, id=me, exclude=last_fail)
					if(path)
						failcount = 0
						path = reverselist(path)
					else
						failcount++
					made_a_step = 0
					path_images = null
					if(!path || !path.len)
						failcount ++
						if(failcount>=5)
							last_fail = null
					else
						failcount = 0
			else
				setTarget(null)

/turf/proc/CardinalTurfsWithMobAccess(var/mob/living/Mob)
	var/L[] = new()

	for(var/d in cardinal)
		var/turf/simulated/T = get_step_cached(src, d)
		if(istype(T) && !T.density)
			if(CanMobPassTo(Mob, src, T, 0))
				L.Add(T)
	return L

/turf/proc/CardinalTurfsIgnoreDoors(var/mob/living/Mob)
	var/L[] = new()

	for(var/d in cardinal)
		var/turf/T = get_step_cached(src, d)
		if(istype(T) && !T.density)
			if(CanMobPassTo(Mob, src, T, 1))
				L.Add(T)
	return L

/proc/CanMobPassTo(var/mob/living/Mob, var/turf/A, var/turf/B, var/ignore_doors)
	if(A.density || B.density) return 0

	var/dir = get_dir(A,B)
	for(var/obj/obstacle in A)
		if(istype(obstacle,/obj/structure/window))
			if(!obstacle.density)		continue
			if(obstacle.dir == SOUTHWEST)	return 0
			if(obstacle.dir == dir)		return 0
		else if(istype(obstacle,/obj/machinery/door))
			if(!obstacle.density || ignore_doors)	continue
			if(istype(obstacle, /obj/machinery/door/window))
				if((dir & obstacle.dir) && !obstacle:allowed(Mob)) return 0
			else if(!obstacle:allowed(Mob)) return 0
		else if(isliving(obstacle))
			if(obstacle.density && prob(50))
				return 0
		else if(!obstacle.CanPass(Mob, B, 1, 0))
			return 0
	dir = get_dir(B,A)
	for(var/obj/obstacle in B)
		if(istype(obstacle,/obj/structure/window))
			if(!obstacle.density)		continue
			if(obstacle.dir == SOUTHWEST)	return 0
			if(obstacle.dir == dir)		return 0
		else if(istype(obstacle,/obj/machinery/door))
			if(!obstacle.density || ignore_doors)	continue
			if(istype(obstacle, /obj/machinery/door/window))
				if((dir & obstacle.dir) && !obstacle:allowed(Mob)) return 0
			else if(!obstacle:allowed(Mob)) return 0
		else if(isliving(obstacle))
			if(obstacle.density && prob(50))
				return 0
		else if(!obstacle.CanPass(Mob, B, 1, 0))
			return 0
	return 1
