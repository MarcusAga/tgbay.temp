//Our own special process so that dead hosts still chestburst
/datum/disease/zombism/process()
	if(!holder) return
	if(holder == affected_mob)
		stage_act()

	if(affected_mob)
		if(affected_mob.stat == DEAD)
			var/name = affected_mob.real_name
			active_diseases -= src
			var/old_ckey = affected_mob.ckey
			spawn(rand(5,45)*10)
				if(!affected_mob) return
				if(ishuman(affected_mob))
					var/mob/living/carbon/human/H = affected_mob
					var/datum/organ/external/Head = H.get_organ("head")
					if(Head.status & ORGAN_DESTROYED)
						cure(0)
						return
					
				for(var/obj/item/I in affected_mob.contents)
					I.loc=affected_mob.loc
				var/atom/movable/overlay/animation = null
				animation = new(affected_mob.loc)
				animation.icon_state = "blank"
				animation.icon = 'icons/mob/mob.dmi'
				animation.master = affected_mob
				flick("gibbed-h", animation)

				var/mob/living/simple_animal/hostile/zombie/Z = new(affected_mob.loc)
				Z.ckey = old_ckey
				if(ismonkey(affected_mob))
					Z.melee_damage_lower*=0.5
					Z.melee_damage_upper*=0.5
					Z.health*=0.5
					Z.maxHealth*=0.5
				for(var/lang in affected_mob.known_languages)
					if(copytext(lang,1,2)!="-") lang="-[lang]"
					Z.known_languages |= lang
				Z.last_move = affected_mob.last_move
				Z.inertia_dir = affected_mob.inertia_dir
				var/turf/T = get_turf(affected_mob)
				T.inertial_drift(Z)
				del(affected_mob)
				Z.name = "[name]-zombie"
				Z.real_name = "[name]-zombie"
				Z.desc = "Looks a bit like [name], but zombie."
				Z.visible_message("<span class='warning'>[name] appears to wake from the dead, having hungry look in its eyes.</span>")

				spawn(15)
					if(animation)	del(animation)
					cure(0)
	else //the virus is in inanimate obj
		cure(0)
	return

/datum/disease/zombism
	name = "Z-Virus"
	max_stages = 5
	spread = "None"
	spread_type = BLOOD
	cure = "Unknown"
	cure_id = list("alkysine","oxycodone","sterilizine")
	cure_chance = 10
	affected_species = list("Human","Monkey")
	permeability_mod = 25
	var/gibbed = 0
	stage_minimum_age = 75
	var/last_stage = -1

/datum/disease/zombism/has_cure()//check if affected_mob has required reagents.
	var/result
	if(stage>1)
		result = affected_mob.reagents.has_reagent("alkysine")
	else
		result = 1
		for(var/C_id in cure_id)
			if(!affected_mob.reagents.has_reagent(C_id))
				result = 0

	return result

/datum/disease/zombism/stage_act()
	..()
	switch(stage)
		if(2)
			if(prob(2))
				affected_mob.emote("blink")
			if(prob(2))
				affected_mob.emote("yawn")
			if(prob(2) || last_stage!=stage)
				affected_mob << "\red Your don't feel like yourself."
			if(prob(5))
				affected_mob.adjustBrainLoss(1)
				affected_mob.updatehealth()
		if(3)
			if(prob(2))
				affected_mob.emote("stare")
			if(prob(2) || last_stage!=stage)
				affected_mob.emote("drool")
			if(prob(10) && affected_mob.getBrainLoss()<=98)//shouldn't retard you to death now
				affected_mob.adjustBrainLoss(2)
				affected_mob.updatehealth()
				if(prob(2))
					affected_mob << "\red Your try to remember something important...but can't."
		if(4)
			if(prob(3))
				affected_mob.emote("drool")
			if(prob(3) || last_stage!=stage)
				affected_mob.emote("cough")
			if(prob(4))
				affected_mob << "\red Your muscles ache."
				if(prob(20))
					affected_mob.take_organ_damage(1)
			if(prob(10))
				var/list/onear = orange(1,affected_mob)
				var/mob/living/V
				for(var/mob/living/L in onear)
					if(!V || prob(50))
						V=L
				if(V)
					V.attack_hand(affected_mob)
					V.contract_disease(src,1)
		if(5)
			affected_mob << "\red Your thoughts fade away..."
			affected_mob.adjustToxLoss(50)
			affected_mob.updatehealth()
			if(prob(10))
				if(gibbed) return 0
				affected_mob.death(0)
				gibbed = 1
				return
	last_stage=stage
