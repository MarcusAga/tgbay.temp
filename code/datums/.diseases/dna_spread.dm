/datum/disease/dnaspread
	name = "Space Retrovirus"
	max_stages = 4
	spread = "On contact"
	spread_type = CONTACT_GENERAL
	cure = "Ryetalyn"
	cure_id = "ryetalyn"
	curable = 1
	agent = "S4E1 retrovirus"
	affected_species = list("Human")
	var/list/original_dna = list()
	var/transformed = 0
	desc = "This disease transplants the genetic code of the intial vector into new hosts."
	severity = "Medium"


/datum/disease/dnaspread/stage_act()
	..()
	switch(stage)
		if(1)
			if (!strain_data["UE"] && !strain_data["UI"] && !strain_data["SE"])
				if(prob(50))
					strain_data["UE"] = affected_mob.dna.unique_enzymes
				if(prob(50))
					strain_data["UI"] = affected_mob.dna.uni_identity
				if(prob(50))
					strain_data["SE"] = affected_mob.dna.struc_enzymes
			if (!strain_data["UE"] && !strain_data["UI"] && !strain_data["SE"])
				strain_data["UE"] = affected_mob.dna.unique_enzymes
				strain_data["UI"] = affected_mob.dna.uni_identity
				strain_data["SE"] = affected_mob.dna.struc_enzymes

		if(2 || 3) //Pretend to be a cold and give time to spread.
			if(prob(8))
				affected_mob.emote("sneeze")
			if(prob(8))
				affected_mob.emote("cough")
			if(prob(1))
				affected_mob << "\red Your muscles ache."
				if(prob(20))
					affected_mob.take_organ_damage(1)
			if(prob(1))
				affected_mob << "\red Your stomach hurts."
				if(prob(20))
					affected_mob.adjustToxLoss(2)
					affected_mob.updatehealth()
		if(4)
			if(!src.transformed)
				if ((!strain_data["UE"]) && (!strain_data["UI"]) && (!strain_data["SE"]))
					affected_mob.viruses -= src
					return

				//Save original dna for when the disease is cured.
				src.original_dna["UE"] = affected_mob.dna.unique_enzymes
				src.original_dna["UI"] = affected_mob.dna.uni_identity
				src.original_dna["SE"] = affected_mob.dna.struc_enzymes

				affected_mob << "\red You don't feel like yourself.."
				if(strain_data["UI"])
					affected_mob.dna.uni_identity = strain_data["UI"]
					updateappearance(affected_mob)
				if(strain_data["SE"])
					affected_mob.dna.struc_enzymes = strain_data["SE"]
					domutcheck(affected_mob)
				if(strain_data["UE"])
					affected_mob.dna.unique_enzymes = strain_data["UE"]

				src.transformed = 1
				src.carrier = 1 //Just chill out at stage 4

	return

/datum/disease/dnaspread/Del()
	if(original_dna["UI"])
		affected_mob.dna.uni_identity = original_dna["UI"]
		updateappearance(affected_mob, affected_mob.dna.uni_identity)
	if(original_dna["SE"])
		affected_mob.dna.struc_enzymes = original_dna["SE"]
		domutcheck(affected_mob)
	if(original_dna["UE"])
		affected_mob.dna.unique_enzymes = original_dna["UE"]

		affected_mob << "\blue You feel more like yourself."
	..()
