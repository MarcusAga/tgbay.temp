/****************************************************
				EXTERNAL ORGANS
****************************************************/
/datum/organ/external
	name = "external"
	var/icon_name = null
	var/body_part = null
	var/icon_position = 0

	var/damage_state = "00"
	var/brute_dam = 0
	var/burn_dam = 0
	var/over_burn = 0
	var/max_damage = 0
	var/max_size = 0
	var/tmp/list/obj/item/weapon/implant/implant

	var/display_name
	var/list/wounds = list()
	var/number_wounds = 0 // cache the number of wounds, which is NOT wounds.len!

	var/tmp/perma_injury = 0
	var/tmp/perma_dmg = 0
	var/tmp/destspawn = 0 //Has it spawned the broken limb?
	var/tmp/amputated = 0 // Whether this has been cleanly amputated, thus causing no pain
	var/min_broken_damage = 30

	var/datum/organ/external/parent
	var/list/datum/organ/external/children

	var/damage_msg = "\red You feel an intense pain"

	var/status = 0
	var/broken_description
	var/open = 0
	var/stage = 0

	// INTERNAL germs inside the organ, this is BAD if it's greater 0
	var/germ_level = 0

		// how often wounds should be updated, a higher number means less often
	var/wound_update_accuracy = 20 // update every 20 ticks(roughly every minute)
	New(var/datum/organ/external/P)
		if(P)
			parent = P
			if(!parent.children)
				parent.children = list()
			parent.children.Add(src)
		return ..()


	proc/take_damage(brute, burn, sharp, used_weapon = null, list/forbidden_limbs = list())
		if(owner.nodamage) return
		if((brute <= 0) && (burn <= 0))
			return 0
		if(status & ORGAN_DESTROYED)
			if(body_part==HEAD)
				if(prob(brute*5) && istype(owner.head,/obj/item/weapon/organ/head))
					var/obj/item/weapon/organ/head/Hd = owner.head
					owner.u_equip(Hd)
					step(Hd,pick(cardinal))
					owner.visible_message("%knownface:1%'s head flies off",actors=list(owner))
			return 0
		if(status & ORGAN_ROBOT)
			if(status & ORGAN_ROBOT_MASKED)
				brute *= 0.5 //~2/3 damage for ROBOLIMBS
				burn *= 0.5 //~2/3 damage for ROBOLIMBS
			else
				brute *= 0.66 //~2/3 damage for ROBOLIMBS
				burn *= 0.66 //~2/3 damage for ROBOLIMBS

//		if(owner && !(status & ORGAN_ROBOT))
//			owner.pain(display_name, (brute+burn)*3, 0, burn > brute)

		if(sharp)
			var/nux = brute * rand(10,15)
			if(config.limbs_can_break && brute_dam >= max_damage * config.organ_health_multiplier)
				if(prob(5 * brute))
					droplimb(1)
					return

			else if(prob(nux))
				createwound( CUT, brute )
				if(!(status & ORGAN_ROBOT))
					owner << "You feel something wet on your [display_name]"

		else if(brute > 20)
			if(config.limbs_can_break && brute_dam >= max_damage * config.organ_health_multiplier)
				if(prob(5 * brute))
					droplimb(1)
					return

		// If the limbs can break, make sure we don't exceed the maximum damage a limb can take before breaking
		if((brute_dam + burn_dam + brute + burn) < max_damage || !config.limbs_can_break)
			if(brute)
				if( (prob(brute*2) && !sharp) || sharp )
					createwound( CUT, brute )
				else if(!sharp)
					createwound( BRUISE, brute )
			if(burn)
				createwound( BURN, burn )
		else
			// If we can't inflict the full amount of damage, spread the damage in other ways
			var/can_inflict = max_damage * config.organ_health_multiplier - (brute_dam + burn_dam) //How much damage can we actually cause?
			if(can_inflict)
				if (brute > 0)
					brute = min(brute,can_inflict)
					can_inflict -= brute
					createwound(BRUISE, brute)
				if (burn > 0)
					if(!(status&ORGAN_ROBOT))
						over_burn += max(burn-can_inflict,0)
					burn = min(can_inflict,burn)
					createwound(BURN, burn)
			else if(!(status & ORGAN_ROBOT))
				if(!(status&ORGAN_ROBOT) && burn > 0)
					over_burn += burn
				var/passed_dam = (brute + burn) //Getting how much overdamage we have.
				var/list/datum/organ/external/possible_points = list()
				if(parent)
					possible_points += parent
				if(children)
					possible_points += children
				if(forbidden_limbs.len)
					possible_points -= forbidden_limbs
				if(!possible_points.len)
					if(owner.stat != 2)
						message_admins("Oh god WHAT!  [owner]'s [src] was unable to find an organ to pass overdamage to!")
				else
					var/datum/organ/external/target = pick(possible_points)
					if(brute)
						target.take_damage(passed_dam, 0, sharp, used_weapon, forbidden_limbs + src)
					else
						target.take_damage(0, passed_dam, sharp, used_weapon, forbidden_limbs + src)
			else
				droplimb(1) //Robot limbs just kinda fail at full damage.

			if(status&ORGAN_ROBOT)
				over_burn = 0
			else if(over_burn>1000)
				over_burn = 1000

			if(!(status&(ORGAN_DESTROYED|ORGAN_HUSKED)) && over_burn>max_damage * 0.5)
				status|=ORGAN_HUSKED
				owner.update_body(0)
				if(istype(src,/datum/organ/external/head))
					src:disfigured = 1
					owner.update_hair(0)
				switch(body_part)
					if(LOWER_TORSO)
						owner.death()
					if(UPPER_TORSO)
						owner.death()
					if(HEAD)
						owner.death()

			if(status & ORGAN_BROKEN)
				owner.emote("scream")

		if(used_weapon) add_autopsy_data(used_weapon, brute + burn)

		owner.updatehealth()

		// sync the organ's damage with its wounds
		src.update_damages()

		var/result = update_icon()
		return result


	proc/heal_damage(brute, burn, internal = 0, robo_repair = 0)
		if(status & (ORGAN_HUSKED|ORGAN_DESTROYED)) return
		if((status & ORGAN_ROBOT) && !robo_repair)
			return

		if(body_part==HEAD && status & ORGAN_ROBOT) src:disfigured = 0

		// heal damage on the individual wounds
		for(var/datum/wound/W in wounds)
			if(brute == 0 && burn == 0)
				break

			// heal brute damage
			if(W.damage_type == CUT || W.damage_type == BRUISE)
				brute = W.heal_damage(brute, internal)
			else if(W.damage_type == BURN)
				burn = W.heal_damage(burn, internal)

		// sync organ damage with wound damages
		update_damages()

		if(internal)
			status &= ~ORGAN_BROKEN
			perma_injury = 0

		// sync the organ's damage with its wounds
		src.update_damages()

		owner.updatehealth()
		var/result = update_icon()
		return result

	proc/update_damages()
		number_wounds = 0
		brute_dam = 0
		burn_dam = 0
		if(!(status&ORGAN_DESTROYED))
			status &= ~ORGAN_BLEEDING
		for(var/datum/wound/W in wounds)
			if(W.damage_type == CUT || W.damage_type == BRUISE)
				brute_dam += W.damage
			else if(W.damage_type == BURN)
				burn_dam += W.damage

			if(W.bleeding())
				status |= ORGAN_BLEEDING

			number_wounds += W.amount
		if(burn_dam<=0) over_burn = 0
		if((status & ORGAN_ROBOT_MASKED) && (brute_dam+burn_dam)>max_damage*0.3)
			status &= ~ORGAN_ROBOT_MASKED
			owner.update_body(0)
		update_icon()

	proc/update_wounds()
		if(!wounds)
			wounds = list()
		for(var/datum/wound/W in wounds)
			// wounds can disappear after 10 minutes at the earliest
			if(status & ORGAN_DESTROYED)
				wounds -= W
				continue
			if(status & ORGAN_HUSKED)
				continue

			if(W.damage == 0 && W.created + 10 * 10 * 60 <= world.time)
				wounds -= W
				// let the GC handle the deletion of the wound
			if(status & ORGAN_ROBOT)
				continue

			if(W.internal && !W.is_treated() && owner.bodytemperature >= 170)
				// internal wounds get worse over time
//				W.open_wound(0.1 * wound_update_accuracy)
				owner.vessel.remove_reagent("blood",0.07 * W.damage * wound_update_accuracy)
				if(prob(1 * wound_update_accuracy))
					owner.custom_pain("You feel a stabbing pain in your [display_name]!",1)

			if(W.bandaged || W.salved)
				// slow healing
				var/amount = 0.2
				if(W.is_treated())
					amount += 10
				// amount of healing is spread over all the wounds
				W.heal_damage((wound_update_accuracy * amount * W.amount * config.organ_regeneration_multiplier) / (20*owner.number_wounds+1))

			if(W.germ_level > 100 && prob(10))
				owner.adjustToxLoss(1 * wound_update_accuracy)
			if(W.germ_level > 1000)
				owner.adjustToxLoss(1 * wound_update_accuracy)

			// Salving also helps against infection
			if(W.germ_level > 0 && W.salved && prob(2))
				W.germ_level = 0

		// sync the organ's damage with its wounds
		src.update_damages()

	proc/bandage()
		var/rval = 0
		src.status |= ORGAN_GAUZED
		src.status &= ~ORGAN_BLEEDING
		for(var/datum/wound/W in wounds)
			if(W.internal) continue
			rval |= !W.bandaged
			W.bandaged = 1
		return rval

	proc/clamp()
		var/rval = 0
		for(var/datum/wound/W in wounds)
			if(W.internal) continue
			rval |= !W.clamped
			W.clamped = 1
		return rval

	proc/salve()
		var/rval = 0
		for(var/datum/wound/W in wounds)
			rval |= !W.salved
			W.salved = 1
		return rval

	proc/get_damage()	//returns total damage
		return max(brute_dam + burn_dam - perma_injury, perma_injury)	//could use health?

	proc/get_damage_brute()
		return max(brute_dam+perma_injury, perma_injury)

	proc/get_damage_fire()
		return burn_dam

	proc/is_infected()
		for(var/datum/wound/W in wounds)
			if(W.germ_level > 100)
				return 1
		return 0

	process()
		// process wounds, doing healing etc., only do this every 4 ticks to save processing power
		if(owner.life_tick % wound_update_accuracy == 0)
			update_wounds()
		if(status & ORGAN_DESTROYED)
			if(!destspawn && config.limbs_can_break)
				droplimb()
			if(parent && (status&ORGAN_ROBOT) && parent.brute_dam==0 && parent.burn_dam==0)
				status|=ORGAN_ATTACHABLE
			if(body_part == HEAD)
				if(!src:headless)
					owner.death()
			return
		if((status & ORGAN_HUSKED) && owner.stat!=DEAD)
			switch(body_part)
				if(LOWER_TORSO)
					owner.death()
				if(UPPER_TORSO)
					owner.death()
				if(HEAD)
					owner.death()
		if(!(status & ORGAN_BROKEN))
			perma_dmg = 0
		if(parent)
			if(parent.status & ORGAN_DESTROYED)
				status |= ORGAN_DESTROYED
				owner.update_body(1)
				return
		if(config.bones_can_break && brute_dam > min_broken_damage * config.organ_health_multiplier && !(status & ORGAN_ROBOT))
			src.fracture()
		if(germ_level > 0)
			for(var/datum/wound/W in wounds) if(!W.bandaged && !W.salved)
				W.germ_level = max(W.germ_level, germ_level)
		return

	proc/fracture()
		if(status & ORGAN_BROKEN)
			return
		if(owner.get_species()=="Slime" && !(status&ORGAN_DESTROYED))
			droplimb(1)
			return
		owner.visible_message("\red You hear a loud cracking sound coming from %knownface:1%.","\red <b>Something feels like it shattered in your [display_name]!</b>","You hear a sickening crack.", actors=list(owner))
		owner.emote("scream")
		status |= ORGAN_BROKEN
		broken_description = pick("broken","fracture","hairline fracture")
		perma_injury = brute_dam

// new damage icon system
// returns just the brute/burn damage code
	proc/damage_state_text()
		if(status & ORGAN_DESTROYED)
			return "00"
		if(status & ORGAN_HUSKED)
			return "HH"

		var/tburn = 0
		var/tbrute = 0

		if(burn_dam ==0)
			tburn =0
		else if (burn_dam < (max_damage * 0.25 / 2))
			tburn = 1
		else if (burn_dam < (max_damage * 0.75 / 2))
			tburn = 2
		else
			tburn = 3

		if (brute_dam == 0)
			tbrute = 0
		else if (brute_dam < (max_damage * 0.25 / 2))
			tbrute = 1
		else if (brute_dam < (max_damage * 0.75 / 2))
			tbrute = 2
		else
			tbrute = 3

		if(open==1 && tbrute<1) tbrute = 1
		else if(open==2 && tbrute<3) tbrute = 3

//		var/masked = (status & ORGAN_ROBOT_MASKED)?"m":""
//		return "[tbrute][tburn][masked]"
		return "[tbrute][tburn]"


// new damage icon system
// adjusted to set damage_state to brute/burn code only (without r_name0 as before)
	proc/update_icon(var/silent = 0)
		var/n_is = damage_state_text()
		if (n_is != damage_state)
			damage_state = n_is
			if(!silent)
				owner.UpdateDamageIcon(1)
			return 1
		return 0

	proc/setAmputatedTree()
		for(var/datum/organ/external/O in children)
			O.amputated=amputated
			O.brute_dam = 0
			O.burn_dam = 0
			O.setAmputatedTree()

	proc/droplimb(var/override = 0,var/no_explode = 0)
		if(destspawn) return
		if(override)
			status |= ORGAN_DESTROYED
		if(status & ORGAN_DESTROYED)
			if(body_part == UPPER_TORSO)
				return

			wounds = list()
			src.update_damages()

			if(status & ORGAN_SPLINTED)
				status &= ~ORGAN_SPLINTED
			if(implant)
				for(var/implants in implant)
					del(implants)
			//owner.unlock_medal("Lost something?", 0, "Lose a limb.", "easy")

			// If any organs are attached to this, destroy them
			for(var/datum/organ/external/O in owner.organs)
				if(O.parent == src)
					O.droplimb(1)

			var/obj/item/weapon/organ/Gib
			switch(body_part)
//				if(UPPER_TORSO)
//					owner.gib()
				if(LOWER_TORSO)
					owner << "\red You are now sterile."
				if(HEAD)
					if(status & ORGAN_ROBOT)
						if(istype(src:brain,/obj/item/device/mmi))
							var/obj/item/device/mmi/MMI = src:brain
							MMI.loc = owner.loc
							add_to_mob_list(MMI.brainmob)
							if(owner.mind)	owner.mind.transfer_to(MMI.brainmob, owner)
							step(MMI,pick(cardinal))
							src:brain = null
					else
						Gib = new /obj/item/weapon/organ/head(owner.loc, owner)
						if(ishuman(owner))
							if(owner.gender == FEMALE)
								Gib.icon_state = "head_f"
							Gib.overlays += owner.generate_head_icon()
						if(!src:headless)
							Gib:transfer_identity(owner)
							Gib:brain_op_stage = owner.op_stage.brain
						else
							Gib:brain_op_stage = 4
						if(owner.dna)
							Gib:species = owner.get_species()
							Gib:unique_look = owner.dna.get_unique_face()
							Gib:unique_voice = list("voice"=owner.dna.get_unique_enzymes(), "gender"=owner.gender)
						Gib:facials = list("r_facial"=owner.r_facial, "g_facial"=owner.g_facial, "b_facial"=owner.b_facial, \
								"r_hair"=owner.r_hair, "g_hair"=owner.g_hair, "b_hair"=owner.b_hair, \
								"r_eyes"=owner.r_eyes, "g_eyes"=owner.g_eyes, "b_eyes"=owner.b_eyes, \
								"f_style"=owner.f_style, "h_style"=owner.h_style, "s_tone"=owner.s_tone )
						Gib.pixel_x = -20
						Gib.pixel_y = 12
						Gib.name = "[owner.real_name]'s head"

					owner.u_equip(owner.glasses)
					owner.u_equip(owner.head)
					owner.u_equip(owner.ears)
					owner.u_equip(owner.wear_mask)

					owner.regenerate_icons()

					owner.death()
				if(ARM_RIGHT)
					Gib = new /obj/item/weapon/organ/r_arm(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "r_arm_l"
				if(ARM_LEFT)
					Gib = new /obj/item/weapon/organ/l_arm(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "l_arm_l"
				if(LEG_RIGHT)
					Gib = new /obj/item/weapon/organ/r_leg(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "r_leg_l"
				if(LEG_LEFT)
					Gib = new /obj/item/weapon/organ/l_leg(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "l_leg_l"
				if(HAND_RIGHT)
					Gib = new /obj/item/weapon/organ/r_hand(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "r_hand_l"
					owner.u_equip(owner.gloves)
				if(HAND_LEFT)
					Gib = new /obj/item/weapon/organ/l_hand(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "l_hand_l"
					owner.u_equip(owner.gloves)
				if(FOOT_RIGHT)
					Gib = new /obj/item/weapon/organ/r_foot/(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "r_foot_l"
					owner.u_equip(owner.shoes)
				if(FOOT_LEFT)
					Gib = new /obj/item/weapon/organ/l_foot(owner.loc, owner)
					if(ismonkey(owner))
						Gib.icon_state = "l_foot_l"
					owner.u_equip(owner.shoes)
			if(Gib)
				if((status&ORGAN_HUSKED) && !istype(Gib,/obj/item/weapon/organ/head))
					del(Gib)
					Gib = new/obj/item/weapon/reagent_containers/food/snacks/badrecipe(owner.loc) //burned mess
				else if(ismonkey(owner))
					Gib.icon = 'monkey.dmi'
				else if(ishuman(owner) && owner.dna)
					var/icon/I = new(get_human_species_icon_base(owner.get_species()),Gib.icon_state,dir=SOUTH)
					var/mob/living/carbon/human/H = owner
					switch(H.get_species())
						if("Human")
							if(H.s_tone >= 0)
								I.Blend(rgb(H.s_tone, H.s_tone, H.s_tone), ICON_ADD)
							else
								I.Blend(rgb(-H.s_tone,  -H.s_tone,  -H.s_tone), ICON_SUBTRACT)
						if("Dragon")
							I.MapColors(rgb(255,0,0),rgb(H.r_hair,H.g_hair,H.b_hair),rgb(0,0,255))
						if("Tajaran")
							I.MapColors(rgb(255,0,0),rgb(H.r_facial,H.g_facial,H.b_facial),rgb(H.r_hair,H.g_hair,H.b_hair))
						if("Slime")
							I.MapColors(rgb(H.r_hair,H.g_hair,H.b_hair),rgb(0,255,0),rgb(0,0,255))

					I.BecomeLying()
					Gib.icon = I
				else
					Gib.icon_state = initial(Gib.icon_state)+"_l"

			if(status & ORGAN_ROBOT)
				del Gib
				switch(body_part)
					if(LEG_RIGHT)
						Gib = new /obj/item/robot_parts/r_leg(owner.loc)
					if(LEG_LEFT)
						Gib = new /obj/item/robot_parts/l_leg(owner.loc)
					if(ARM_RIGHT)
						Gib = new /obj/item/robot_parts/r_arm(owner.loc)
					if(ARM_LEFT)
						Gib = new /obj/item/robot_parts/l_arm(owner.loc)

			if(Gib)
				var/lol = pick(cardinal)
				step(Gib,lol)

			destspawn = 1
			if(status & ORGAN_ROBOT)
				if((body_part!=HEAD)&&(owner.stat==0))
					owner.visible_message("\red %knownface:1%'s [display_name] explodes violently!",\
					"\red <b>Your [display_name] explodes!</b>",\
					"You hear an explosion followed by a scream!", actors=list(owner))
				else
					owner.visible_message("\red %knownface:1%'s [display_name] explodes violently!",\
					"\red <b>Your [display_name] explodes!</b>",\
					"You hear an explosion!", actors=list(owner))
				if(!no_explode)
					if(prob(30))
						explosion(get_turf(owner),-1,-1,2,3)
					else if(prob(30))
						explosion(get_turf(owner),-1,-1,1,2)
					else if(prob(30))
						explosion(get_turf(owner),-1,-1,0,1)
					var/datum/effect/effect/system/spark_spread/spark_system = new /datum/effect/effect/system/spark_spread()
					spark_system.set_up(5, 0, owner)
					spark_system.attach(owner)
					spark_system.start()
					spawn(10)
						del(spark_system)
			else
				owner.visible_message("\red %knownface:1%'s [display_name] flies off in an arc.",\
				"<span class='moderate'><b>Your [display_name] goes flying off!</b></span>",\
				"You hear a terrible sound of ripping tendons and flesh.", actors=list(owner))

			// force the icon to rebuild
			owner.regenerate_icons()

	proc/createwound(var/type = CUT, var/damage)
		if(hasorgans(owner))
			if(type == CUT && status&ORGAN_ROBOT)
				type = BRUISE
			var/wound_type
			var/size = min( max( 1, damage/10 ) , 6)

			// first check whether we can widen an existing wound
			if(wounds.len > 0 && prob(max(50+owner.number_wounds*10,100)))
				if((type == CUT || type == BRUISE) && damage >= 5)
					var/datum/wound/W = pick(wounds)
					if(W.amount == 1 && W.started_healing())
						W.open_wound(damage)
						if(prob(25))
							owner.visible_message("\red The wound on %knownface:1%'s [display_name] widens with a nasty ripping voice.",\
							"\red The wound on your [display_name] widens with a nasty ripping voice.",\
							"You hear a nasty ripping noise, as if flesh is being torn apart.", actors=list(owner))

						return

			if(damage == 0) return

			// the wound we will create
			var/datum/wound/W

			switch(type)
				if(CUT)
					src.status |= ORGAN_BLEEDING
					var/list/size_names = list(/datum/wound/cut, /datum/wound/deep_cut, /datum/wound/flesh_wound, /datum/wound/gaping_wound, /datum/wound/big_gaping_wound, /datum/wound/massive_wound)
					wound_type = size_names[size]

					W = new wound_type(damage)

				if(BRUISE)
					var/list/size_names = list(/datum/wound/bruise/tiny_bruise, /datum/wound/bruise/small_bruise, /datum/wound/bruise/moderate_bruise, /datum/wound/bruise/large_bruise, /datum/wound/bruise/huge_bruise, /datum/wound/bruise/monumental_bruise)
					wound_type = size_names[size]

					W = new wound_type(damage)
				if(BURN)
					var/list/size_names = list(/datum/wound/moderate_burn, /datum/wound/large_burn, /datum/wound/severe_burn, /datum/wound/deep_burn, /datum/wound/carbonised_area, /datum/wound/carbonised_area)
					wound_type = size_names[size]

					W = new wound_type(damage)

			// Possibly trigger an internal wound, too.
			var/local_damage = brute_dam + burn_dam + damage
			if(damage > 10 && type != BURN && local_damage > 20 && prob(damage) && !(status&ORGAN_ROBOT))
				var/datum/wound/internal_bleeding/I = new (15)
				wounds += I
				owner.custom_pain("You feel something rip in your [display_name]!", 1)

			// check whether we can add the wound to an existing wound
			for(var/datum/wound/other in wounds)
				if(other.desc == W.desc)
					// okay, add it!
					other.damage += W.damage
					other.amount += 1
					W = null // to signify that the wound was added
					break

			// if we couldn't add the wound to another wound, ignore
			if(W)
				wounds += W

	proc/emp_act(severity)
		if(!(status & ORGAN_ROBOT))
			return
		var/probability = 30
		var/damage = 15
		if(severity == 2)
			probability = 1
			damage = 3
		if(body_part==HEAD)
			if(severity==2)
				owner.Weaken(15)
				owner.Stun(20)
			else if(severity==1)
				owner.Weaken(30)
				owner.Stun(50)
				if(prob(10))
					droplimb(1)
			if(!destspawn) owner << "MMI failure, rebooting system..."
		if(prob(probability))
			droplimb(1)
		else
			take_damage(damage, 0, 1, used_weapon = "EMP")

	proc/getDisplayName()
		switch(name)
			if("l_leg")
				return "left leg"
			if("r_leg")
				return "right leg"
			if("l_arm")
				return "left arm"
			if("r_arm")
				return "right arm"
			if("l_foot")
				return "left foot"
			if("r_foot")
				return "right foot"
			if("l_hand")
				return "left hand"
			if("r_hand")
				return "right hand"
			else
				return name

	proc/robotize()
		destspawn = 0
		open = 0
		src.status &= ~ORGAN_BROKEN
		src.status &= ~ORGAN_BLEEDING
		src.status &= ~ORGAN_SPLINTED
		src.status &= ~ORGAN_ATTACHABLE
		src.status &= ~ORGAN_DESTROYED
		src.status &= ~ORGAN_HUSKED
		src.status |= ORGAN_ROBOT
		if(children && children.len==1)
			for (var/datum/organ/external/T in children)
				if(T)
					T.robotize()
		wounds = list()
		src.update_damages()

	proc/add_autopsy_data(var/used_weapon, var/damage)
		var/datum/autopsy_data/W = autopsy_data[used_weapon]
		if(!W)
			W = new()
			W.weapon = used_weapon
			autopsy_data[used_weapon] = W

		W.hits += 1
		W.damage += damage
		W.time_inflicted = world.time

/datum/organ/external/chest
	name = "chest"
	icon_name = "chest"
	display_name = "chest"
	max_damage = 150
	min_broken_damage = 75
	body_part = UPPER_TORSO
	var/ruptured_lungs = 0

/datum/organ/external/groin
	name = "groin"
	icon_name = "diaper"
	display_name = "groin"
	max_damage = 115
	min_broken_damage = 70
	body_part = LOWER_TORSO
	var/appendicitis = 0
	var/appendicitis_stage = 0

	process()
		..()
		if(appendicitis<=0) return
		if(status&(ORGAN_DESTROYED|ORGAN_ROBOT))
			appendicitis = -1
			return
		if(owner.get_species()=="Slime")
			return
		appendicitis++
		if(appendicitis_stage<1)
			appendicitis_stage = 1
		if(appendicitis_stage<4 && appendicitis<appendicitis_stage*120 && prob(5))
			appendicitis_stage++
		switch(appendicitis_stage)
			if(1)
				if(prob(5))
					owner.custom_pain("\red You feel a stinging pain in your abdomen!",0.5)
					owner.emote("me",1,"winces slightly.")
			if(2)
				if(prob(3))
					owner.custom_pain("\red You feel a stinging pain in your abdomen!",0.5)
					owner.emote("me",1,"winces painfully.")
					owner.adjustToxLoss(1)
			if(3)
				if(prob(1))
					if (owner.nutrition > 100)
						owner.vomit()
					else
						owner << "\red You gag as you want to throw up, but there's nothing in your stomach!"
						owner.Weaken(10)
						owner.adjustToxLoss(3)

			if(4)
				if(prob(1))
					owner.custom_pain("\red Your abdomen is a world of pain!",1)
					owner.Weaken(10)
					owner.op_stage.appendix = 0.0
					add_autopsy_data("Appendicitis", 50)
					appendicitis = -1
					var/datum/wound/W = new /datum/wound/internal_bleeding(25)
					owner.adjustToxLoss(25)
					wounds += W

/datum/organ/external/head
	name = "head"
	icon_name = "head"
	display_name = "head"
	max_damage = 75
	min_broken_damage = 40
	body_part = HEAD
	var/disfigured = 0
	var/brain_explode = 0
	var/headless = 0
	var/obj/item/brain

/datum/organ/external/head/process()
	..()
	if(brain_explode && !destspawn && !(status&ORGAN_DESTROYED))
		playsound(get_turf_loc(owner), 'sound/effects/fuckmoimozg.wav', 50, 0, 0)
		status |= ORGAN_DESTROYED
		destspawn = 1
		brain_explode = 0

		var/atom/movable/overlay/animation = null
		animation = new(owner.loc)
		animation.icon_state = "blank"
		animation.icon = 'icons/mob/mob.dmi'
		animation.master = src
		flick("gibbed-h", animation)
		spawn(15)
			if(animation)	del(animation)

		owner.u_equip(owner.glasses)
		owner.u_equip(owner.head)
		owner.u_equip(owner.ears)
		owner.u_equip(owner.wear_mask)
		owner.regenerate_icons()
		spawn(10)
			owner << "Your brain decides it has had enough of you and leaves."
			var/obj/effect/decal/cleanable/blood/Bl = new(owner.loc)
			if(!Bl.blood_DNA) Bl.blood_DNA = list()
			if(owner && owner.dna)
				Bl.blood_DNA[owner.dna.unique_enzymes] = owner.dna.b_type
			if(brain)
				brain.loc = owner.loc
				var/obj/item/device/mmi/MMI = brain
				add_to_mob_list(MMI.brainmob)
				if(owner.mind)	owner.mind.transfer_to(MMI.brainmob, owner)
			else
				var/obj/item/brain/B = new(owner.loc)
				B.transfer_identity(owner)
				brain = B
			step(brain,pick(cardinal))
			brain = null

		spawn(60)
			if(owner) owner.death()

/datum/organ/external/l_arm
	name = "l_arm"
	display_name = "left arm"
	icon_name = "l_arm"
	max_damage = 75
	min_broken_damage = 30
	body_part = ARM_LEFT
//	icon_position = LEFT

/datum/organ/external/l_leg
	name = "l_leg"
	display_name = "left leg"
	icon_name = "l_leg"
	max_damage = 75
	min_broken_damage = 30
	body_part = LEG_LEFT
	icon_position = LEFT

/datum/organ/external/r_arm
	name = "r_arm"
	display_name = "right arm"
	icon_name = "r_arm"
	max_damage = 75
	min_broken_damage = 30
	body_part = ARM_RIGHT
//	icon_position = RIGHT

/datum/organ/external/r_leg
	name = "r_leg"
	display_name = "right leg"
	icon_name = "r_leg"
	max_damage = 75
	min_broken_damage = 30
	body_part = LEG_RIGHT
	icon_position = RIGHT

/datum/organ/external/l_foot
	name = "l_foot"
	display_name = "left foot"
	icon_name = "l_foot"
	max_damage = 40
	min_broken_damage = 15
	body_part = FOOT_LEFT
	icon_position = LEFT

/datum/organ/external/r_foot
	name = "r_foot"
	display_name = "right foot"
	icon_name = "r_foot"
	max_damage = 40
	min_broken_damage = 15
	body_part = FOOT_RIGHT
	icon_position = RIGHT

/datum/organ/external/r_hand
	name = "r_hand"
	display_name = "right hand"
	icon_name = "r_hand"
	max_damage = 40
	min_broken_damage = 15
	body_part = HAND_RIGHT
	icon_position = RIGHT

/datum/organ/external/l_hand
	name = "l_hand"
	display_name = "left hand"
	icon_name = "l_hand"
	max_damage = 40
	min_broken_damage = 15
	body_part = HAND_LEFT
	icon_position = LEFT

/****************************************************
			   EXTERNAL ORGAN ITEMS
****************************************************/

obj/item/weapon/organ
	icon = 'icons/mob/human_races/r_human.dmi'

obj/item/weapon/organ/New(loc, mob/living/carbon/human/H)
	..(loc)
	if(!istype(H))
		return
	if(H.dna)
		if(!blood_DNA)
			blood_DNA = list()
		blood_DNA[H.dna.unique_enzymes] = H.dna.b_type

	var/icon/I = new /icon(icon, icon_state)

	if (H.s_tone >= 0)
		I.Blend(rgb(H.s_tone, H.s_tone, H.s_tone), ICON_ADD)
	else
		I.Blend(rgb(-H.s_tone,  -H.s_tone,  -H.s_tone), ICON_SUBTRACT)
	icon = I

obj/item/weapon/organ/head
	name = "head"
	icon_state = "head_m"
	var/mob/living/carbon/brain/brainmob
	var/brain_op_stage = 0
	var/species = "Human"
	var/unique_look = null
	var/list/unique_voice = null
	var/list/facials = null

obj/item/weapon/organ/head/New()
	..()
	spawn(5)
	if(brainmob && brainmob.client)
		brainmob.client.screen.len = null //clear the hud

obj/item/weapon/organ/head/proc/transfer_identity(var/mob/living/carbon/human/H)//Same deal as the regular brain proc. Used for human-->head
	brainmob = new(src)
	brainmob.name = H.real_name
	brainmob.real_name = H.real_name
	brainmob.dna = H.dna
	brainmob.timeofhostdeath = H.timeofdeath
	if(H.mind)
		H.mind.transfer_to(brainmob, H)
	brainmob.container = src

obj/item/weapon/organ/head/attackby(obj/item/weapon/W as obj, mob/user as mob)
	if(istype(W,/obj/item/weapon/scalpel))
		switch(brain_op_stage)
			if(0)
				for(var/mob/O in (oviewers(brainmob) - user))
					O.show_message("\red [brainmob] is beginning to have \his head cut open with [W] by [user].", 1)
				brainmob << "\red [user] begins to cut open your head with [W]!"
				user << "\red You cut [brainmob]'s head open with [W]!"

				brain_op_stage = 1

			if(2)
				for(var/mob/O in (oviewers(brainmob) - user))
					O.show_message("\red [brainmob] is having \his connections to the brain delicately severed with [W] by [user].", 1)
				brainmob << "\red [user] begins to cut open your head with [W]!"
				user << "\red You cut [brainmob]'s brain connections with [W]!"

				brain_op_stage = 3.0
			else
				..()
	else if(istype(W,/obj/item/weapon/circular_saw))
		switch(brain_op_stage)
			if(1)
				for(var/mob/O in (oviewers(brainmob) - user))
					O.show_message("\red [brainmob] has \his skull sawed open with [W] by [user].", 1)
				brainmob << "\red [user] begins to saw open your head with [W]!"
				user << "\red You saw [brainmob]'s head open with [W]!"

				brain_op_stage = 2
			if(3)
				for(var/mob/O in (oviewers(brainmob) - user))
					O.show_message("\red [brainmob] has \his spine's connection to the brain severed with [W] by [user].", 1)
				brainmob << "\red [user] severs your brain's connection to the spine with [W]!"
				user << "\red You sever [brainmob]'s brain's connection to the spine with [W]!"

				user.attack_log += "\[[time_stamp()]\]<font color='red'> Debrained [brainmob.name] ([brainmob.ckey]) with [W.name] (INTENT: [uppertext(user.a_intent)])</font>"
				brainmob.attack_log += "\[[time_stamp()]\]<font color='orange'> Debrained by [user.name] ([user.ckey]) with [W.name] (INTENT: [uppertext(user.a_intent)])</font>"
				log_admin("ATTACK: [user] ([user.ckey]) debrained [brainmob] ([brainmob.ckey]).")
				message_admins("ATTACK: [user] ([user.ckey]) debrained [brainmob] ([brainmob.ckey]).")

				var/obj/item/brain/B = new(loc)
				B.brainmob = brainmob
				brainmob.container = B
				brainmob.loc = B
				brainmob = null

				brain_op_stage = 4.0
			else
				..()
	else
		..()

obj/item/weapon/organ/head/equipped(var/mob/user, var/slot)
	if(slot==slot_head)
		user.visible_message("%knownface:1% attaches [src] in place of it's head",actors=list(user))

obj/item/weapon/organ/l_arm
	name = "left arm"
	icon_state = "l_arm"
obj/item/weapon/organ/l_foot
	name = "left foot"
	icon_state = "l_foot"
obj/item/weapon/organ/l_hand
	name = "left hand"
	icon_state = "l_hand"
obj/item/weapon/organ/l_leg
	name = "left leg"
	icon_state = "l_leg"
obj/item/weapon/organ/r_arm
	name = "right arm"
	icon_state = "r_arm"
obj/item/weapon/organ/r_foot
	name = "right foot"
	icon_state = "r_foot"
obj/item/weapon/organ/r_hand
	name = "right hand"
	icon_state = "r_hand"
obj/item/weapon/organ/r_leg
	name = "right leg"
	icon_state = "r_leg"
