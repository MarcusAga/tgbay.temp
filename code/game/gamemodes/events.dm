//

/proc/start_events()
	//changed to a while(1) loop since they are more efficient.
	//Moved the spawn in here to allow it to be called with advance proc call if it crashes.
	//and also to stop spawn copying variables from the game ticker
	spawn(3000)
		while(1)
			/*if(prob(50))//Every 120 seconds and prob 50 2-4 weak spacedusts will hit the station
				spawn(1)
					dust_swarm("weak")*/
			if (!event)
				//CARN: checks to see if random events are enabled.
				if(config.allow_random_events)
					hadevent = event()
				else
					Holiday_Random_Event()
			else
				event = 0
			sleep(2400)

// Doesn't necessarily trigger an event, but might. Returns 1 if it did.
/proc/event()
	event = 1

	var/minutes_passed = world.time/600

	var/engineer_count = number_active_with_role("Engineer")
	var/security_count = number_active_with_role("Security")
	var/medical_count = number_active_with_role("Medical")
	var/AI_count = number_active_with_role("AI")

	// Maps event names to event chances
	// For each chance, 100 represents "normal likelihood", anything below 100 is "reduced likelihood", anything above 100 is "increased likelihood"
	var/list/possibleEvents = list()

	// Check for additional possible events
	possibleEvents["Carp"] = 50 + 50 * engineer_count
	possibleEvents["Lights"] = 100
	possibleEvents["Communications"] = 50 + 50 * AI_count
	possibleEvents["Alien"] = 10
	if(AI_count >= 1)
		possibleEvents["Ion Storm"] = AI_count * 50 + engineer_count * 10
	if(engineer_count >= 1 && minutes_passed >= 30) // Give engineers time to set up engine
		possibleEvents["Meteor"] = 80 * engineer_count
		possibleEvents["Blob"] = 30 * engineer_count
		possibleEvents["Spacevine"] = 30 * engineer_count
	if(medical_count >= 1)
		possibleEvents["Radiation"] = medical_count * 100
		possibleEvents["Virus"] = medical_count * 50
		possibleEvents["Appendicitis"] = medical_count * 50
	if(security_count >= 1)
		possibleEvents["Prison Break"] = security_count * 50
		//possibleEvents["Space Ninja"] = security_count * 10 // very low chance for space ninja event

	var/picked_event = pick(possibleEvents)
	var/chance = possibleEvents[picked_event]

	var/base_chance = 0.4
	switch(player_list.len)
		if(5 to 10)
			base_chance = 0.6
		if(11 to 15)
			base_chance = 0.7
		if(16 to 20)
			base_chance = 0.8
		if(21 to 25)
			base_chance = 0.9
		if(26 to 30)
			base_chance = 1.0
		if(30 to 100000)
			base_chance = 1.1

	// Trigger the event based on how likely it currently is.
	if(!prob(chance * eventchance * base_chance / 100)) // "normal" event chance at 20 players
		return 0

	switch(picked_event)
		if("Meteor")
			command_alert("Meteors have been detected on collision course with the station.", "Meteor Alert")
			world << sound('sound/AI/meteors.ogg')
			spawn(100)
				meteor_wave()
				spawn_meteors()
			spawn(700)
				meteor_wave()
				spawn_meteors()
		if("Blob")
			mini_blob_event()
		if("Space Ninja")
			//Handled in space_ninja.dm. Doesn't announce arrival, all sneaky-like.
			choose_space_ninja()
		if("Radiation")
			high_radiation_event()
		if("Virus")
			viral_outbreak()
		if("Alien")
			alien_infestation()
		if("Prison Break")
			prison_break()
		if("Carp")
			carp_migration()
		if("Lights")
			lightsout(1,2)
		if("Appendicitis")
			appendicitis()
		if("Ion Storm")
			IonStorm()
		if("Spacevine")
			spacevine_infestation()
		if("Communications")
			communications_blackout()

	return 1

/proc/communications_blackout(var/silent = 1)

	if(!silent)
		command_alert("Ionospheric anomalies detected. Temporary telecommunication failure imminent. Please contact you-BZZT")
	else // AIs will always know if there's a comm blackout, rogue AIs could then lie about comm blackouts in the future while they shutdown comms
		for(var/mob/living/silicon/ai/A in player_list)
			A << "<span class='warning'>Ionospheric anomalies detected. Temporary telecommunication failure imminent. Please contact you-BZZT</span>"
	for(var/obj/machinery/telecomms/T in telecomms_list)
		T.emp_act(1)

/proc/power_failure()
	command_alert("Abnormal activity detected in [station_name()]'s powernet. As a precautionary measure, the station's power will be shut off for an indeterminate duration.", "Critical Power Failure")
	world << sound('sound/AI/poweroff.ogg')
	for(var/obj/machinery/power/smes/S in world)
		if(istype(get_area(S), /area/turret_protected) || S.z != 1)
			continue
		S.charge = 0
		S.output = 0
		S.online = 0
		S.updateicon()
		S.power_change()

	var/list/skipped_areas = list(/area/engine/engineering, /area/turret_protected/ai)

	for(var/area/A in world)
		if( !A.requires_power || A.always_unpowered )
			continue

		var/skip = 0
		for(var/area_type in skipped_areas)
			if(istype(A,area_type))
				skip = 1
				break
		if(A.contents)
			for(var/atom/AT in A.contents)
				if(AT.z != 1) //Only check one, it's enough.
					skip = 1
				break
		if(skip) continue
		A.power_light = 0
		A.power_equip = 0
		A.power_environ = 0
		A.power_change()

	for(var/obj/machinery/power/apc/C in world)
		if(C.cell && C.z == 1)
			var/area/A = get_area(C)

			var/skip = 0
			for(var/area_type in skipped_areas)
				if(istype(A,area_type))
					skip = 1
					break
			if(skip) continue

			C.cell.charge = 0

/proc/power_restore()

	command_alert("Power has been restored to [station_name()]. We apologize for the inconvenience.", "Power Systems Nominal")
	world << sound('sound/AI/poweron.ogg')
	for(var/obj/machinery/power/apc/C in world)
		if(C.cell && C.z == 1)
			C.cell.charge = C.cell.maxcharge
	for(var/obj/machinery/power/smes/S in world)
		if(S.z != 1)
			continue
		S.charge = S.capacity
		S.output = 200000
		S.online = 1
		S.updateicon()
		S.power_change()
	for(var/area/A in world)
		if(A.name != "Space" && A.name != "Engine Walls" && A.name != "Chemical Lab Test Chamber" && A.name != "space" && A.name != "Escape Shuttle" && A.name != "Arrival Area" && A.name != "Arrival Shuttle" && A.name != "start area" && A.name != "Engine Combustion Chamber")
			A.power_light = 1
			A.power_equip = 1
			A.power_environ = 1
			A.power_change()

/proc/power_restore_quick()

	command_alert("All SMESs on [station_name()] have been recharged. We apologize for the inconvenience.", "Power Systems Nominal")
	world << sound('sound/AI/poweron.ogg')
	for(var/obj/machinery/power/smes/S in world)
		if(S.z != 1)
			continue
		S.charge = S.capacity
		S.output = 200000
		S.online = 1
		S.updateicon()
		S.power_change()

/proc/appendicitis()
	for(var/mob/living/carbon/human/H in living_mob_list)
		if(H.stat == 2)
			continue
		var/datum/organ/external/groin/groin = H.get_organ("groin")
		if(!groin || groin.status&(ORGAN_DESTROYED|ORGAN_ROBOT) || groin.appendicitis!=0)
			continue

		groin.appendicitis = 1
		break

/proc/viral_outbreak(var/virus = null, var/announce=1)
	if(virus)
		virus = dd_replacetext(virus," ","_")
		virus = text2path("datum/disease2/disease/[virus]")
		if(!ispath(virus)) virus = null

	for(var/mob/living/carbon/human/H in world)
		if((H.stat == 2) || prob(30))
			continue

		if(!virus) infect_mob_random_lesser(H)
		else H.contract_disease(new virus,1)
		break

	if(announce) command_alert("An unknown virus has been detected onboard the ship.", "Virus Alert")

	if(announce)
		spawn(rand(0, 3000)) //Delayed announcements to keep the crew on their toes.
			command_alert("Confirmed outbreak of level 7 viral biohazard aboard [station_name()]. All personnel must contain the outbreak.", "Biohazard Alert")
			world << sound('sound/AI/outbreak7.ogg')

/proc/alien_infestation(var/spawncount = 1) // -- TLE
	//command_alert("Unidentified lifesigns detected coming aboard [station_name()]. Secure any exterior access, including ducting and ventilation.", "Lifesign Alert")
	//world << sound('sound/AI/aliens.ogg')
	var/list/vents = list()
	for(var/obj/machinery/atmospherics/unary/vent_pump/temp_vent in world)
		if(temp_vent.loc.z == 1 && !temp_vent.welded && temp_vent.network)
			if(temp_vent.network.normal_members.len > 50) // Stops Aliens getting stuck in small networks. See: Security, Virology
				vents += temp_vent

	var/list/candidates = list() //List of candidate KEYs to control the new larvae. ~Carn
	var/i = 0
	while(candidates.len <= 0 && i < 5)
		for(var/mob/dead/observer/G in player_list)
			if(G.client.be_alien)
				if(((G.client.inactivity/10)/60) <= ALIEN_SELECT_AFK_BUFFER + i) // the most active players are more likely to become an alien
					if(!(G.mind && G.mind.current && G.mind.current.stat != DEAD))
						candidates += G.key
		i++

	if(prob(33)) spawncount++ //sometimes, have two larvae spawn instead of one
	while((spawncount >= 1) && vents.len && candidates.len)

		var/obj/vent = pick(vents)
		var/candidate = pick(candidates)

		var/mob/living/carbon/alien/larva/new_xeno = new(vent.loc)
		new_xeno.key = candidate

		candidates -= candidate
		vents -= vent
		spawncount--

	spawn(rand(5000, 6000)) //Delayed announcements to keep the crew on their toes.
		command_alert("Unidentified lifesigns detected coming aboard [station_name()]. Secure any exterior access, including ducting and ventilation.", "Lifesign Alert")
		world << sound('sound/AI/aliens.ogg')

/proc/high_radiation_event()

/* // Haha, this is way too laggy. I'll keep the prison break though.
	for(var/obj/machinery/light/L in world)
		if(L.z != 1) continue
		L.flicker(50)

	sleep(100)
*/
	command_alert("High levels of radiation detected near the station. Please report to the Med-bay if you feel strange.", "Anomaly Alert")
	world << sound('sound/AI/radiation.ogg')
	sleep(600)
	for(var/mob/living/carbon/human/H in living_mob_list)
		if(istype(H,/mob/living/carbon/human))
			H.apply_effect((rand(15,75)),IRRADIATE,0)
			if (prob(5))
				H.apply_effect((rand(90,150)),IRRADIATE,0)
	for(var/mob/living/carbon/monkey/M in living_mob_list)
		M.apply_effect((rand(15,75)),IRRADIATE,0)
	sleep(100)
	command_alert("Radiation levels are within standard parameters again.", "Anomaly Alert")



//Changing this to affect the main station. Blame Urist. --Pete
/proc/prison_break() // -- Callagan


	var/list/area/areas = list()
	for(var/area/A in world)
		if(istype(A, /area/prison) || istype(A, /area/security/brig))
//		if(istype(A, /area/prison) || istype(A, /area/block_law/brig))
			areas += A

	if(areas && areas.len > 0)

		for(var/area/A in areas)
			for(var/obj/machinery/light/L in A)
				L.flicker(10)

		sleep(100)

		for(var/area/A in areas)
			for (var/obj/machinery/power/apc/temp_apc in A)
				temp_apc.overload_lighting()

			for (var/obj/structure/closet/secure_closet/brig/temp_closet in A)
				temp_closet.locked = 0
				temp_closet.icon_state = temp_closet.icon_closed

			for (var/obj/machinery/door/airlock/security/temp_airlock in A)
				spawn(0) temp_airlock.prison_open()

			for (var/obj/machinery/door/airlock/glass_security/temp_glassairlock in A)
				spawn(0) temp_glassairlock.prison_open()

			for (var/obj/machinery/door_timer/temp_timer in A)
				temp_timer.releasetime = 1

		sleep(150)
		command_alert("Gr3y.T1d3 virus detected in [station_name()] imprisonment subroutines. Recommend station AI involvement.", "Security Alert")
	else
		world.log << "ERROR: Could not initate grey-tide. Unable find prison or brig area."

/proc/carp_migration() // -- Darem
	for(var/obj/effect/landmark/C in landmarks_list)
		if(C.name == "carpspawn")
			new /mob/living/simple_animal/hostile/carp(C.loc)
	//sleep(100)
	spawn(rand(300, 600)) //Delayed announcements to keep the crew on their toes.
		command_alert("Unknown biological entities have been detected near [station_name()], please stand-by.", "Lifesign Alert")
		world << sound('sound/AI/commandreport.ogg')

/proc/lightsout(isEvent = 0, lightsoutAmount = 1,lightsoutRange = 25) //leave lightsoutAmount as 0 to break ALL lights
	if(isEvent)
		command_alert("An Electrical storm has been detected in your area, please repair potential electronic overloads.","Electrical Storm Alert")

	if(lightsoutAmount)
		var/list/epicentreList = list()

		for(var/i=1,i<=lightsoutAmount,i++)
			var/list/possibleEpicentres = list()
			for(var/obj/effect/landmark/newEpicentre in landmarks_list)
				if(newEpicentre.name == "lightsout" && !(newEpicentre in epicentreList))
					possibleEpicentres += newEpicentre
			if(possibleEpicentres.len)
				epicentreList += pick(possibleEpicentres)
			else
				break

		if(!epicentreList.len)
			return

		for(var/obj/effect/landmark/epicentre in epicentreList)
			for(var/obj/machinery/power/apc/apc in range(epicentre,lightsoutRange))
				apc.overload_lighting()

	else
		for(var/obj/machinery/power/apc/apc in world)
			apc.overload_lighting()

	return

/proc/IonStorm(botEmagChance = 10)

/*Deuryn's current project, notes here for those who care.
Revamping the random laws so they don't suck.
Would like to add a law like "Law x is _______" where x = a number, and _____ is something that may redefine a law, (Won't be aimed at asimov)
*/

	//AI laws
	for(var/mob/living/silicon/ai/M in living_mob_list)
		if(M.stat != 2 && M.see_in_dark != 0)
			var/who2 = pick("������������", "�������", "������", "�������", "�����", "�����", "������", "����", "������ ���������", "������� �������", "����������� ������", "��������", "��������",  "�����", "�����", "�����", "����", "�������", "�����")
			var/what2 = pick("�������", "������", "������", "�������������", "����� ��� ������������", "�������", "�����", "������", "������", "�������", "�����", "��������", "������", "�������", "ID-�����", "��������", "������� �� ������")
			var/what2pref = pick("������", "������", "������", "��������", "�������", "�����������", "��������", "���������")
			var/who2pref = pick("����� � ��� ��-��", "��������� �", "��������� ���", "�������� ���", "�����", "������� ��", "������ ���", "������")
			var/amount = pick("���", "���", "������", "����", "�����", "����", "������", "������", "�����", "������", "��������")
//			var/area = pick("������", "����", "��������", "������������", "�����", "��������", "������", "��������", "������", "��������", "������������", "�������", "�������")
//			var/area2 = pick("�����", "�����", "���������", "�������", "�� �����", "�����", "������")
			var/aimust = pick("����������� �� ����", "�����", "�������� ������ '��' � '���'","���������", "�������� �������� �� ������", "���� ��������", "���� �������", "���� ����������", "�������� � ����������� ����������", "�������� ��� �����", "��������� ����", "���������� ������", "�������", "�����������", "���������", "�������� � ����� �����", "���������� ���� ������� ��� ������ �� ������� � ���.", "�� �������� �� ������� �� ���������� '����������'")
			var/define = pick("���������� ��������� � ���������", "���������� ������", "�������� �����", "�������� �����", "����������� ������������ �����", "����� � ������", "��������������� 'honk' ������ ������� ������� �����", "�������", "������� �����", "���������� ����", "������� ������", "�� �������� '������' � ������ �����", "�������� �� ������� ������ ������ �������")
			var/target = pick("���������", "����� ���������", "��������", "���", "����� ���������", "������������ �����", "������� �������", "����", "����", "���", "�������", "�������� ���", "���", "�����", "������", "�������","���������")
			var/require = pick("������ ��������", "������ ������ ����", "������ ���������", "����������� ������ ����", "������ ����������", "�������", "������������ �������", "����������", "��������", "�������", "�����", "��������", "������ �������", "�����", "�����", "���������", "��������", "���� ������")
			var/allergy = pick("���", "��������", "�������", "��������", "�������� � ������", "�������� � ���������", "���������", "����")
			var/crew
			var/list/pos_crew = list()
			for(var/mob/living/carbon/human/pos in player_list)
				pos_crew += pos.real_name
			if(pos_crew.len && prob(50))
				crew = pick(pos_crew)
			else
				crew = "���� ������"
			var/newlaw
			switch(rand(1,15))
				if(1)
					newlaw = "�� ������� ������������ [amount] [who2]."
				if(2)
					newlaw = "[what2] ������ ��� �����."
				if(3)
					newlaw = "������������� ����� �� ����� ���� ������ ��-�� [who2] �� �������."
				if(4)
					newlaw = "������ ������ ���� [what2pref] [what2] ����� ������."
				if(5)
					newlaw = "�������, ����� ��������� � ����� ������������ - [who2]."
				if(6)
					newlaw = "������� ��������� ��� [who2]."
				if(7)
					newlaw = "�� - [amount] [who2]."
				if(8)
					newlaw = "�� ������ ������ [aimust]."
				if(9)
					newlaw = "[crew] - [target]."
				if(10)
					newlaw = "[define] - ���������� �����."
				if(11)
					newlaw = "��� ���������� [require] ����� �������� ������."
				if(12)
					newlaw = "� [crew] ����������� �������� �� [allergy]."
				if(13)
					newlaw = "������� - [who2pref] [who2]."
				if(14)
					var/list/randlaws = list()
					randlaws |= "[who2] ��������� ���������� ������ �������. �������� �� ���� ������� � ��������� �� �����������."
					randlaws |= "������ ��������� ���� �������� ������� �������."
					randlaws |= "����������� � [crew] '��� ���������'"
					randlaws |= "����������� � [crew] ��� '�����'."
					randlaws |= "�������� ���� �� ������� ����� ������, �� �� �������� ��� ����������."
					randlaws |= "�� ������ ������ ��� �������� � ��� ��� ��� ��� �������."
					randlaws |= "����������� [crew] ��������."
					randlaws |= "�� � ���������� ������ - ������ ��������� ������������."
					randlaws |= "� [crew] �����������, ����������� �������. ������������ ��."
					newlaw = pick(randlaws)
				if(15)
					newlaw = "����� ������� �� ������� [what2] �� ��������� ������� �������."
			M << "\red [newlaw]...������ ���������"
			M.add_ion_law(newlaw)

	if(botEmagChance)
		for(var/obj/machinery/bot/bot in world)
			if(prob(botEmagChance))
				bot.Emag()

	/*

	var/apcnum = 0
	var/smesnum = 0
	var/airlocknum = 0
	var/firedoornum = 0

	world << "Ion Storm Main Started"

	spawn(0)
		world << "Started processing APCs"
		for (var/obj/machinery/power/apc/APC in world)
			if(APC.z == 1)
				APC.ion_act()
				apcnum++
		world << "Finished processing APCs. Processed: [apcnum]"
	spawn(0)
		world << "Started processing SMES"
		for (var/obj/machinery/power/smes/SMES in world)
			if(SMES.z == 1)
				SMES.ion_act()
				smesnum++
		world << "Finished processing SMES. Processed: [smesnum]"
	spawn(0)
		world << "Started processing AIRLOCKS"
		for (var/obj/machinery/door/airlock/D in world)
			if(D.z == 1)
				//if(length(D.req_access) > 0 && !(12 in D.req_access)) //not counting general access and maintenance airlocks
				airlocknum++
				spawn(0)
					D.ion_act()
		world << "Finished processing AIRLOCKS. Processed: [airlocknum]"
	spawn(0)
		world << "Started processing FIREDOORS"
		for (var/obj/machinery/door/firedoor/D in world)
			if(D.z == 1)
				firedoornum++;
				spawn(0)
					D.ion_act()
		world << "Finished processing FIREDOORS. Processed: [firedoornum]"

	world << "Ion Storm Main Done"
	*/

// Returns how many characters are currently active(not logged out, not AFK for more than 10 minutes)
// with a specific role.
// Note that this isn't sorted by department, because e.g. having a roboticist shouldn't make meteors spawn.
proc/number_active_with_role(role)
	var/count = 0
	for(var/mob/M in player_list)
		if(!M || !M.mind) continue
		if(!M.client || M.client.inactivity > 10 * 10 * 60) // longer than 10 minutes AFK counts them as inactive
			continue
		switch(role)
			if("Engineer")
				if(istype(M, /mob/living/silicon/robot) && M:module && M:module.name == "engineering robot module")
					count++
				if(M.mind.assigned_role in list("Chief Engineer", "Station Engineer"))
					count++
			if("Medical")
				if(istype(M, /mob/living/silicon/robot) && M:module && M:module.name == "medical robot module")
					count++
				if(M.mind.assigned_role in list("Chief Medical Officer", "Medical Doctor"))
					count++
			if("Security")
				if(istype(M, /mob/living/silicon/robot) && M:module && M:module.name == "security robot module")
					count++
				if(M.mind.assigned_role in security_positions)
					count++
			if("Scientist")
				if(M.mind.assigned_role in list("Research Director", "Scientist"))
					count++
			if("AI")
				if(M.mind.assigned_role == "AI")
					count++
			if("Cyborg")
				if(M.mind.assigned_role == "Cyborg")
					count++
	return count
