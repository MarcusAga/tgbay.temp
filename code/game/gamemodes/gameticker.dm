var/global/datum/controller/gameticker/ticker

#define GAME_STATE_PREGAME		1
#define GAME_STATE_SETTING_UP	2
#define GAME_STATE_PLAYING		3
#define GAME_STATE_FINISHED		4
#define GAME_STATE_FINISHED_PAUSED		5


/datum/controller/gameticker
	var/const/restart_timeout = 600
	var/current_state = GAME_STATE_PREGAME

	var/hide_mode = 0
	var/datum/game_mode/mode = null
	var/event_time = null
	var/event = 0

	var/login_music			// music played in pregame lobby

	var/list/datum/mind/minds = list()//The people in the game. Used for objective tracking.

	var/Bible_icon_state	// icon_state the chaplain has chosen for his bible
	var/Bible_item_state	// item_state the chaplain has chosen for his bible
	var/Bible_name			// name of the bible
	var/Bible_deity_name

	var/random_players = 0 	// if set to nonzero, ALL players who latejoin or declare-ready join will have random appearances/genders

	var/list/syndicate_coalition = list() // list of traitor-compatible factions
	var/list/factions = list()			  // list of all factions
	var/list/availablefactions = list()	  // list of factions with openings

	var/pregame_timeleft = 0

	var/delay_end = 0	//if set to nonzero, the round will not restart on it's own
	var/force_end = 0

	var/triai = 0//Global holder for Triumvirate

	//automated spawning of mice and roaches
	var/spawn_vermin = 1
	var/vermin_min_spawntime = 3000		//between 5 (3000) and 15 (9000) minutes interval
	var/vermin_max_spawntime = 9000
	var/spawning_vermin = 0
	var/max_vermin = 30
	var/list/vermin_spawn_turfs

/datum/controller/gameticker/proc/pregame()
	login_music = pick('sound/music/title1.ogg','sound/music/title2.ogg') // choose title music!

	do
		pregame_timeleft = 180
		world << "<B><FONT color='blue'>Welcome to the pre-game lobby!</FONT></B>"
		world << "Please, setup your character and select ready. Game will start in [pregame_timeleft] seconds"
		while(current_state == GAME_STATE_PREGAME)
			for(var/i=0, i<10, i++)
				sleep(1)
				vote.process()
			if(going)
				pregame_timeleft--

			if(pregame_timeleft <= 0)
				current_state = GAME_STATE_SETTING_UP
	while (!setup())

/datum/controller/gameticker/proc/setup()
	//Create and announce mode
	if(master_mode=="secret")
		src.hide_mode = 1
	var/list/datum/game_mode/runnable_modes
	if((master_mode=="random") || (master_mode=="secret"))
		runnable_modes = config.get_runnable_modes()
		if (runnable_modes.len==0)
			current_state = GAME_STATE_PREGAME
			world << "<B>Unable to choose playable game mode.</B> Reverting to pre-game lobby."
			return 0
		if(secret_force_mode != "secret")
			var/datum/game_mode/M = config.pick_mode(secret_force_mode)
			if(M.can_start())
				src.mode = config.pick_mode(secret_force_mode)
		job_master.ResetOccupations()
		if(!src.mode)
			src.mode = pickweight(runnable_modes)
		if(src.mode)
			var/mtype = src.mode.type
			src.mode = new mtype
		log_game("Selected random gamemode is [mode.name]")
		message_admins("Selected random gamemode is [mode.name]", 0, 1)
	else
		src.mode = config.pick_mode(master_mode)
	if (!src.mode.can_start())
		world << "<B>Unable to start [mode.name].</B> Not enough players, [mode.required_players] players needed. Reverting to pre-game lobby."
		del(mode)
		current_state = GAME_STATE_PREGAME
		job_master.ResetOccupations()
		return 0

	//Configure mode and assign player to special mode stuff
	job_master.DivideOccupations() //Distribute jobs
	var/can_continue = src.mode.pre_setup()//Setup special modes
	if(!can_continue)
		del(mode)
		current_state = GAME_STATE_PREGAME
		world << "<B>Error setting up [master_mode].</B> Reverting to pre-game lobby."
//		job_master.ResetOccupations()
		return 0

	if(hide_mode)
		var/list/modes = new
		for (var/datum/game_mode/M in runnable_modes)
			modes+=M.name
		modes = sortList(modes)
		world << "<B>The current game mode is - Secret!</B>"
		world << "<B>Possibilities:</B> [english_list(modes)]"
	else
		src.mode.announce()

	create_characters() //Create player characters and transfer them
	collect_minds()
	for(var/datum/mind/mnd in ticker.minds)
		if(!mnd.current) continue
		mnd.known_languages = mnd.current.known_languages
		mnd.current.known_languages = list("-Universal")
		if(!ishuman(mnd.current)) continue

		mnd.known_faces[mnd.current.dna.get_unique_face()] = "Me"
		mnd.known_voices[mnd.current.dna.get_unique_enzymes()] = "Me"
		for(var/datum/mind/mnd2 in ticker.minds)
			if(!ishuman(mnd2.current)) continue
			if(mnd.assigned_role in command_positions || prob(40))
				mnd.known_faces[mnd2.current.dna.get_unique_face()] = "[mnd2.name] - [mnd2.role_alt_title?(mnd2.role_alt_title):(mnd2.assigned_role)]"
				mnd2.known_faces[mnd.current.dna.get_unique_face()] = "[mnd.name] - [mnd.role_alt_title?(mnd.role_alt_title):(mnd.assigned_role)]"
				mnd.known_voices[mnd2.current.dna.get_unique_enzymes()] = "[mnd2.name] - [mnd2.role_alt_title?(mnd2.role_alt_title):(mnd2.assigned_role)]"
				mnd2.known_voices[mnd.current.dna.get_unique_enzymes()] = "[mnd.name] - [mnd.role_alt_title?(mnd.role_alt_title):(mnd.assigned_role)]"
	equip_characters()
	data_core.manifest()
	current_state = GAME_STATE_PLAYING

	spawn(0)//Forking here so we dont have to wait for this to finish
		mode.post_setup()
		//Cleanup some stuff
		for(var/obj/effect/landmark/start/S in landmarks_list)
			//Deleting Startpoints but we need the ai point to AI-ize people later
			if (S.name != "AI")
				del(S)
		world << "<FONT color='blue'><B>Enjoy the game!</B></FONT>"
		world << sound('sound/AI/welcome.ogg') // Skie
		//Holiday Round-start stuff	~Carn
		Holiday_Game_Start()

	start_events() //handles random events and space dust.

	var/admins_number = 0
	for(var/client/C)
		if(C.holder)
			admins_number++
	if(admins_number == 0)
		ooc_allowed = 0
		world << "<B>The OOC channel has been globally disabled because no admin is moderating it!</B>"

//		send2irc("Server", "Round just started with no admins online!")

	supply_shuttle.process() 		//Start the supply shuttle regenerating points -- TLE
	master_controller.process()		//Start master_controller.process()
	lighting_controller.process()	//Start processing DynamicAreaLighting updates


	if(config.sql_enabled)
		spawn(3000)
		statistic_cycle() // Polls population totals regularly and stores them in an SQL DB -- TLE

	return 1

/datum/controller/gameticker
	//station_explosion used to be a variable for every mob's hud. Which was a waste!
	//Now we have a general cinematic centrally held within the gameticker....far more efficient!
	var/obj/screen/cinematic = null

	//Plus it provides an easy way to make cinematics for other events. Just use this as a template :)
	proc/station_explosion_cinematic(var/station_missed=0, var/override = null)
		if( cinematic )	return	//already a cinematic in progress!

		//initialise our cinematic screen object
		cinematic = new(src)
		cinematic.icon = 'icons/effects/station_explosion.dmi'
		cinematic.icon_state = "station_intact"
		cinematic.layer = 20
		cinematic.mouse_opacity = 0
		cinematic.screen_loc = "1,0"

		var/obj/structure/stool/bed/temp_buckle = new(src)
		//Incredibly hackish. It creates a bed within the gameticker (lol) to stop mobs running around
		if(station_missed)
			for(var/mob/living/M in living_mob_list)
				M.buckled = temp_buckle				//buckles the mob so it can't do anything
				if(M.client)
					M.client.screen += cinematic	//show every client the cinematic
		else	//nuke kills everyone on z-level 1 to prevent "hurr-durr I survived"
			for(var/mob/living/M in living_mob_list)
				M.buckled = temp_buckle
				if(M.client)
					M.client.screen += cinematic

				switch(M.z)
					if(0)	//inside a crate or something
						var/turf/T = get_turf(M)
						if(T && T.z==1)				//we don't use M.death(0) because it calls a for(/mob) loop and
							M.health = 0
							M.stat = DEAD
					if(1)	//on a z-level 1 turf.
						M.health = 0
						M.stat = DEAD

		//Now animate the cinematic
		switch(station_missed)
			if(1)	//nuke was nearby but (mostly) missed
				if( mode && !override )
					override = mode.name
				switch( override )
					if("nuclear emergency") //Nuke wasn't on station when it blew up
						flick("intro_nuke",cinematic)
						sleep(35)
						world << sound('sound/effects/explosionfar.ogg')
						flick("station_intact_fade_red",cinematic)
						cinematic.icon_state = "summary_nukefail"
					else
						flick("intro_nuke",cinematic)
						sleep(35)
						world << sound('sound/effects/explosionfar.ogg')
						//flick("end",cinematic)


			if(2)	//nuke was nowhere nearby	//TODO: a really distant explosion animation
				sleep(50)
				world << sound('sound/effects/explosionfar.ogg')


			else	//station was destroyed
				if( mode && !override )
					override = mode.name
				switch( override )
					if("nuclear emergency") //Nuke Ops successfully bombed the station
						flick("intro_nuke",cinematic)
						sleep(35)
						flick("station_explode_fade_red",cinematic)
						world << sound('sound/effects/explosionfar.ogg')
						cinematic.icon_state = "summary_nukewin"
					if("AI malfunction") //Malf (screen,explosion,summary)
						flick("intro_malf",cinematic)
						sleep(76)
						flick("station_explode_fade_red",cinematic)
						world << sound('sound/effects/explosionfar.ogg')
						cinematic.icon_state = "summary_malf"
					if("blob") //Station nuked (nuke,explosion,summary)
						flick("intro_nuke",cinematic)
						sleep(35)
						flick("station_explode_fade_red",cinematic)
						world << sound('sound/effects/explosionfar.ogg')
						cinematic.icon_state = "summary_selfdes"
					else //Station nuked (nuke,explosion,summary)
						flick("intro_nuke",cinematic)
						sleep(35)
						flick("station_explode_fade_red", cinematic)
						world << sound('sound/effects/explosionfar.ogg')
						cinematic.icon_state = "summary_selfdes"

		//If its actually the end of the round, wait for it to end.
		//Otherwise if its a verb it will continue on afterwards.
		sleep(300)

		if(cinematic)	del(cinematic)		//end the cinematic
		if(temp_buckle)	del(temp_buckle)	//release everybody
		return


	proc/create_characters()
		for(var/mob/new_player/player in player_list)
			if(player.ready && player.mind)
				if(player.mind.assigned_role=="AI")
					player.close_spawn_windows()
					player.AIize()
				else
					player.create_character()
					del(player)


	proc/collect_minds()
		for(var/mob/living/player in player_list)
			if(player.mind)
				ticker.minds += player.mind


	proc/equip_characters()
		var/captainless=1
		for(var/mob/living/carbon/human/player in player_list)
			if(player && player.mind && player.mind.assigned_role)
				if(player.mind.assigned_role == "Captain")
					captainless=0
				if(player.mind.assigned_role != "MODE")
					job_master.EquipRank(player, player.mind.assigned_role, 0)
					EquipCustomItems(player)
		if(captainless)
			world << "Captainship not forced on anyone."


	var/announced_station_abandon = 0
	proc/process()
		if(current_state==GAME_STATE_FINISHED_PAUSED && force_end)
			if (mode.station_was_nuked)
				world << "\blue <B>Rebooting due to destruction of station in [restart_timeout/10] seconds</B>"
			else
				world << "\blue <B>Restarting in [restart_timeout/10] seconds</B>"

			sleep(restart_timeout)
			world.Reboot()

		if(current_state != GAME_STATE_PLAYING)
			return 0

		mode.process()

		emergency_shuttle.process()

		var/mode_finished = mode.check_finished() || (emergency_shuttle.location == 2 && emergency_shuttle.alert == 1)
		var/end_debug = ""
		if(!emergency_shuttle.location && emergency_shuttle.direction!=2)
			var/station_abandoned = 1
			var/has_aliens = 0
			for(var/mob/living/M in world)
				if(M.client && M.stat!=DEAD)
					var/turf/T = get_turf(M)
					if((!respawn_controller || !(M in respawn_controller.freezed_persons)) && (T && T.z!=2 && T.z!=6))
						if(isalien(M))
							has_aliens = 1
							end_debug += " alien([M.ckey])"
						else
							station_abandoned = 0
							end_debug += " abandon([M.ckey])"
					else
						end_debug += " onstation([M.ckey])"
			end_debug += "<br>"
			var/has_players = 0
			for(var/client/C)
				if(C.holder)
					end_debug += "admin([C.ckey])"
					has_players |= 1
				else if(C.mob && isliving(C.mob) && (!istype(C.mob,/mob/living/carbon/brain) || C.mob.in_contents_of(/obj/mecha)))
					end_debug += "living([C.ckey],C.mob.type)"
					has_players |= 2
			end_debug += "<br>"
			if(station_abandoned && has_players)
				if(has_players&1)
					if(!announced_station_abandon && has_players&2)
						if(has_aliens)
							log_game("\red Station overrun by aliens? Maybe restart is needed?")
							message_admins("\red Station overrun by aliens? Maybe restart is needed?", 0, 1)
						else
							log_game("\red No living person left on station, maybe restart is needed?")
							message_admins("\red No living person left on station, maybe restart is needed?", 0, 1)
				else
					log_game(end_debug)
					if(has_aliens)
						log_game("\red Station overrun by aliens, contact lost...")
						world << "\red Station overrun by aliens, contact lost..."
					else
						log_game("\red No living person left on station...")
						world << "\red No living person left on station..."
#ifdef CSServer
					mode_finished = 1
#endif
				if(!announced_station_abandon)
					log_game(end_debug)
					announced_station_abandon = 1
					spawn(10*60*5) announced_station_abandon = 0

		if(!mode.explosion_in_progress && (mode_finished || force_end))
			current_state = GAME_STATE_FINISHED

			if(!istype(mode,/datum/game_mode/epidemic))
				for(var/client/C)
					var/mob/living/carbon/human/H = C.mob
					if(!istype(H)) continue
					var/turf/pos = get_turf(H)
					if(pos.z!=2) continue

					var/list/disk_save=list()

					var/list/tdisks=H.search_contents_for(/obj/item/weapon/disk/tech_disk)
					for(var/obj/item/weapon/disk/tech_disk/tdisk in tdisks)
						if(tdisk.stored)
							disk_save |= "T:[tdisk.stored.id]=[tdisk.stored.level]"
					var/list/ddisks=H.search_contents_for(/obj/item/weapon/disk/design_disk)
					for(var/obj/item/weapon/disk/design_disk/ddisk in ddisks)
						if(ddisk.blueprint)
							disk_save |= "D:[ddisk.blueprint.type]"

					if(!H.storedpreferences)
						H.storedpreferences = new
						H.storedpreferences.savefile_cfg_load(H)
						H.storedpreferences.savefile_load(H)
					H.storedpreferences.research_disks = disk_save
					H.storedpreferences.savefile_save(H,"end round")

			spawn
				declare_completion()

			spawn(50)
				if (mode.station_was_nuked)
					feedback_set_details("end_proper","nuke")
					if(!delay_end)
						world << "\blue <B>Rebooting due to destruction of station in [restart_timeout/10] seconds</B>"
				else
					feedback_set_details("end_proper","proper completion")
					if(!delay_end)
						world << "\blue <B>Restarting in [restart_timeout/10] seconds</B>"


				if(blackbox)
					blackbox.save_all_data_to_sql()

				if(!delay_end)
					sleep(restart_timeout)
					world.Reboot()
				else
					world << "\blue <B>An admin has delayed the round end</B>"
					current_state = GAME_STATE_FINISHED_PAUSED

		return 1

	proc/getfactionbyname(var/name)
		for(var/datum/faction/F in factions)
			if(F.name == name)
				return F


/datum/controller/gameticker/proc/declare_completion()
	world << "GameMode was : [mode.name]"

	for (var/mob/living/silicon/ai/aiPlayer in mob_list)
		if(ishuman(aiPlayer.loc) && aiPlayer.loc:fakeAI==aiPlayer) continue
		if (aiPlayer.stat != 2)
			world << "<b>[aiPlayer.name] (Played by: [aiPlayer.key])'s laws at the end of the game were:</b>"
		else
			world << "<b>[aiPlayer.name] (Played by: [aiPlayer.key])'s laws when it was deactivated were:</b>"
		aiPlayer.show_laws(1)

		if (aiPlayer.connected_robots.len)
			var/robolist = "<b>The AI's loyal minions were:</b> "
			for(var/mob/living/silicon/robot/robo in aiPlayer.connected_robots)
				robolist += "[robo.name][robo.stat?" (Deactivated) (Played by: [robo.key]), ":" (Played by: [robo.key]), "]"
			world << "[robolist]"

	for (var/mob/living/silicon/robot/robo in mob_list)
		if (!robo.connected_ai)
			if (robo.stat != 2)
				world << "<b>[robo.name] (Played by: [robo.key]) survived as an AI-less borg! Its laws were:</b>"
			else
				world << "<b>[robo.name] (Played by: [robo.key]) was unable to survive the rigors of being a cyborg without an AI. Its laws were:</b>"

			if(robo) //How the hell do we lose robo between here and the world messages directly above this?
				robo.laws.show_laws(world)

	mode.declare_completion()//To declare normal completion.

	//calls auto_declare_completion_* for all modes
	for(var/handler in typesof(/datum/game_mode/proc))
		if (findtext("[handler]","auto_declare_completion_"))
			call(mode, handler)()

	//Print a list of antagonists to the server log
	var/list/total_antagonists = list()
	//Look into all mobs in world, dead or alive
	for(var/datum/mind/Mind in minds)
		var/temprole = Mind.special_role
		if(temprole)							//if they are an antagonist of some sort.
			if(temprole in total_antagonists)	//If the role exists already, add the name to it
				total_antagonists[temprole] += ", [Mind.name]([Mind.key])"
			else
				total_antagonists.Add(temprole) //If the role doesnt exist in the list, create it and add the mob
				total_antagonists[temprole] += ": [Mind.name]([Mind.key])"

	//Now print them all into the log!
	log_game("Antagonists at round end were...")
	for(var/i in total_antagonists)
		log_game("[i]s[total_antagonists[i]].")

	return 1
