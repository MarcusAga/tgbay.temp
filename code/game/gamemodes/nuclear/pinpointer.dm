//
/obj/item/weapon/pinpointer
	name = "pinpointer"
	icon = 'icons/obj/device.dmi'
	icon_state = "pinoff"
	flags = FPRINT | TABLEPASS| CONDUCT
	slot_flags = SLOT_BELT
	w_class = 2.0
	item_state = "electronic"
	throw_speed = 4
	throw_range = 20
	m_amt = 500
	var/obj/item/weapon/disk/nuclear/the_disk = null
	var/active = 0


	attack_self()
		if(!active)
			active = 1
			usr << "\blue You activate the pinpointer"
		else
			active = 0
			icon_state = "pinoff"
			usr << "\blue You deactivate the pinpointer"

	proc/workdisk()
		return
		if(!active) return
		if(!the_disk)
			the_disk = locate()
			if(!the_disk)
				icon_state = "pinonnull"
				return
		src.dir = get_dir(src,the_disk)
		switch(get_dist(src,the_disk))
			if(0)
				icon_state = "pinondirect"
			if(1 to 8)
				icon_state = "pinonclose"
			if(9 to 16)
				icon_state = "pinonmedium"
			if(16 to INFINITY)
				icon_state = "pinonfar"

	examine()
		..()
		for(var/obj/machinery/nuclearbomb/bomb in world)
			if(bomb.timing)
				usr << "Extreme danger.  Arming signal detected.   Time remaining: [bomb.timeleft]"

/*
/obj/item/weapon/pinpointer/New()
	..()
	processing_objects.Add(src)

/obj/item/weapon/pinpointer/Del()
	processing_objects.Remove(src)
	..()

/obj/item/weapon/pinpointer/process()
	return
	workdisk()
*/

/*
/obj/item/weapon/pinpointer/advpinpointer
	name = "Advanced Pinpointer"
	icon = 'icons/obj/device.dmi'
	desc = "A larger version of the normal pinpointer, this unit features a helpful quantum entanglement detection system to locate various objects that do not broadcast a locator signal."
	var/mode = 0  // Mode 0 locates disk, mode 1 locates coordinates.
	var/turf/location = null
	var/obj/target = null

	attack_self()
		if(!active)
			active = 1
			usr << "\blue You activate the pinpointer"
		else
			active = 0
			icon_state = "pinoff"
			usr << "\blue You deactivate the pinpointer"


	/obj/item/weapon/pinpointer/process()
		if(mode == 0)
			workdisk()
		if(mode == 1)
			worklocation()
		if(mode == 2)
			workobj()

	proc/worklocation()
		if(!active)
			return
		if(!location)
			icon_state = "pinonnull"
			return
		src.dir = get_dir(src,location)
		switch(get_dist(src,location))
			if(0)
				icon_state = "pinondirect"
			if(1 to 8)
				icon_state = "pinonclose"
			if(9 to 16)
				icon_state = "pinonmedium"
			if(16 to INFINITY)
				icon_state = "pinonfar"

	proc/workobj()
		if(!active)
			return
		if(!target)
			icon_state = "pinonnull"
			return
		src.dir = get_dir(src,target)
		switch(get_dist(src,target))
			if(0)
				icon_state = "pinondirect"
			if(1 to 8)
				icon_state = "pinonclose"
			if(9 to 16)
				icon_state = "pinonmedium"
			if(16 to INFINITY)
				icon_state = "pinonfar"

/obj/item/weapon/pinpointer/advpinpointer/verb/toggle_mode()
	set category = "Object"
	set name = "Toggle Pinpointer Mode"
	set src in view(1)

	active = 0
	icon_state = "pinoff"
	target = null
	location = null

	switch(alert("Please select the mode you want to put the pinpointer in.", "Pinpointer Mode Select", "Location", "Disk Recovery", "Other Signature"))
		if("Location")
			mode = 1

			var/locationx = input(usr, "Please input the x coordinate to search for.", "Location?" , "") as num
			if(!locationx || !(usr in view(1,src)))
				return
			var/locationy = input(usr, "Please input the y coordinate to search for.", "Location?" , "") as num
			if(!locationy || !(usr in view(1,src)))
				return

			var/turf/Z = get_turf(src)

			location = locate(locationx,locationy,Z.z)

			usr << "You set the pinpointer to locate [locationx],[locationy]"


			return attack_self()

		if("Disk Recovery")
			mode = 0
			return attack_self()

		if("Other Signature")
			mode = 2
			switch(alert("Search for item signature or DNA fragment?" , "Signature Mode Select" , "" , "Item" , "DNA"))
				if("Item")
					var/datum/objective/steal/itemlist
					itemlist = itemlist // To supress a 'variable defined but not used' error.
					var/targetitem = input("Select item to search for.", "Item Mode Select","") as null|anything in itemlist.possible_items
					if(!targetitem)
						return
					target=locate(itemlist.possible_items[targetitem])
					if(!target)
						usr << "Failed to locate [targetitem]!"
						return
					usr << "You set the pinpointer to locate [targetitem]"
				if("DNA")
					var/DNAstring = input("Input DNA string to search for." , "Please Enter String." , "")
					if(!DNAstring)
						return
					for(var/mob/living/carbon/M in mob_list)
						if(!M.dna)
							continue
						if(M.dna.unique_enzymes.findtext(DNAstring))
							target = M
							break

			return attack_self()
*/

/obj/item/weapon/pinpointer/nukeop//Used by nuke ops specifically so they can get their asses back to the shuttle
	var/mode = 0  // Mode 0 locates disk, mode 1 locates the shuttle
	var/location = null//Follow that shuttle!
	var/obj/machinery/computer/syndicate_station/home = null

	attack_self()
		if(!active)
			active = 1
			if(mode == 0)
				usr << "\blue Authentication Disk Locator active."
			if(mode == 1)
				usr << "\blue Shuttle Locator active."
		else
			active = 0
			icon_state = "pinoff"
			usr << "\blue You deactivate the pinpointer"

/obj/item/weapon/pinpointer/nukeop/process()
	return
	if(mode == 0)
		workdisk()
	if(mode == 1)
		worklocation()

/obj/item/weapon/pinpointer/nukeop/workdisk()
	return
	if(!active) return
	if(mode)//Check in case the mode changes while operating
		return
	if(bomb_set)//If the bomb is set, lead to the shuttle
		mode = 1//Ensures worklocation() continues to work
		playsound(src.loc, 'sound/machines/twobeep.ogg', 50, 1)//Plays a beep
		for (var/mob/O in hearers(1, src.loc))
			O.show_message(text("Shuttle Locator active."))//Lets the mob holding it know that the mode has changed
		return//Get outta here
	if(!the_disk)
		the_disk = locate()
		if(!the_disk)
			icon_state = "pinonnull"
			return
	if(src.loc.z >1)//If you are on a different z-level from the station
		icon_state = "pinonalert"
	else
		src.dir = get_dir(src,the_disk)
		switch(get_dist(src,the_disk))
			if(0)
				icon_state = "pinondirect"
			if(1 to 8)
				icon_state = "pinonclose"
			if(9 to 16)
				icon_state = "pinonmedium"
			if(16 to INFINITY)
				icon_state = "pinonfar"

/obj/item/weapon/pinpointer/nukeop/proc/worklocation()
	return
	if(!active)
		return
	if(!mode)
		return
	if(!bomb_set)
		mode = 0
		playsound(src.loc, 'sound/machines/twobeep.ogg', 50, 1)
		for (var/mob/O in hearers(2, src.loc))
			O.show_message(text("Authentication Disk Locator active."))
	if(!home)
		home = locate()
		if(!home)
			icon_state = "pinonnull"
			return
	if(src.loc.z >2)//If you are on a different z-level from the shuttle
		icon_state = "pinonalert"
	else
		src.dir = get_dir(src,home)
		switch(get_dist(src,home))
			if(0)
				icon_state = "pinondirect"
			if(1 to 8)
				icon_state = "pinonclose"
			if(9 to 16)
				icon_state = "pinonmedium"
			if(16 to INFINITY)
				icon_state = "pinonfar"
