#define WHITELISTFILE "data/whitelist.txt"

/datum/configuration/var/list/whitelist = list()

/proc/load_whitelist()
	var/text = file2text(WHITELISTFILE)
	if (!text)
		diary << "Failed to [WHITELISTFILE]\n"
	else
		config.whitelist = dd_text2list(text, "\n")

/proc/check_whitelist(var/ckey)
	if(!config.whitelist || !config.usewhitelist)
		return 1
	return ("[ckey]" in config.whitelist)

/datum/configuration/var/list/alien_whitelist = list()

/proc/load_alienwhitelist()
	var/text = file2text("config/alienwhitelist.txt")
	if (!text)
		diary << "Failed to load config/alienwhitelist.txt\n"
	else
		config.alien_whitelist = dd_text2list(text, "\n")

/proc/is_alien_whitelisted(mob/M, var/species)
	if(!config.alien_whitelist || !config.usealienwhitelist)
		return 1
	if(species == "Human")
		return 1
	if((M.client) && (M.client.holder) && (M.client.holder.level) && (M.client.holder.level >= 5))
		return 1
	if(M && species)
		for (var/s in config.alien_whitelist)
			if(findtext(s,"[M.ckey] - [species]"))
				return 1
			if(findtext(s,"[M.ckey] - All"))
				return 1

	return 0

/datum/configuration/var/list/head_whitelist = list()
/proc/load_headwhitelist()
	var/text = file2text("config/headwhitelist.txt")
	if (!text)
		diary << "Failed to load config/headwhitelist.txt\n"
	else
		config.head_whitelist = dd_text2list(text, "\n")

/proc/is_head_whitelisted(mob/M, var/role)
	if(!M) return 0
	if(!config.head_whitelist || !config.useheadwhitelist)
		return 1
	if(!(role in all_command_positions)) return 1
	if((M.client) && (M.client.holder) && (M.client.holder.level) && (M.client.holder.level >= 5))
		return 1
	if(M && role)
		for (var/s in config.head_whitelist)
			if(findtext(s,"[M.ckey] - [role]"))
				return 1
			if(findtext(s,"[M.ckey] - All"))
				return 1

	return 0

#undef WHITELISTFILE