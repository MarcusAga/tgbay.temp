//
#define PIPE_PUMP		9
#define PIPE_VOLUME_PUMP        16

/obj/machinery/optable
	name = "Operating Table"
	desc = "Used for advanced medical procedures."
	icon = 'icons/obj/surgery.dmi'
	icon_state = "table2-idle"
	density = 1
	anchored = 1.0
	use_power = 1
	idle_power_usage = 1
	active_power_usage = 5
	var/mob/living/carbon/human/victim = null
	var/strapped = 0.0

	var/tmp/obj/machinery/computer/operating/computer = null

	var/obj/item/weapon/tank/airTank = null
	var/image/airTankIcon = null
	var/obj/item/pipe/Pump = null
	var/pumping = 0
	var/airFromTank = 0
	var/cycle = 0

/obj/machinery/optable/New()
	..()
	for(dir in list(NORTH,EAST,SOUTH,WEST))
		computer = locate(/obj/machinery/computer/operating, get_step(src, dir))
		if (computer)
			break
//	spawn(100) //Wont the MC just call this process() before and at the 10 second mark anyway?
//		process()

/obj/machinery/optable/ex_act(severity)

	switch(severity)
		if(1.0)
			//SN src = null
			del(src)
			return
		if(2.0)
			if (prob(50))
				//SN src = null
				del(src)
				return
		if(3.0)
			if (prob(25))
				src.density = 0
		else
	return

/obj/machinery/optable/blob_act()
	if(prob(75))
		del(src)

/obj/machinery/optable/hand_p(mob/user as mob)
	return src.attack_paw(user)

/obj/machinery/optable/attack_paw(mob/user as mob)
	if (((HULK in usr.mutations) || (SUPRSTR in usr.augmentations)) && user:a_intent=="hurt")
		usr << text("\blue You destroy the operating table.")
		visible_message("\red %knownface:1% destroys the operating table!", actors=list(usr))
		src.density = 0
		del(src)
	if (!( locate(/obj/machinery/optable, user.loc) ))
		step(user, get_dir(user, src))
		if (user.loc == src.loc)
			user.layer = TURF_LAYER
			visible_message("The monkey hides under the table!")
	return

/obj/machinery/optable/attack_hand(mob/user as mob)
	if (((HULK in usr.mutations) || (SUPRSTR in usr.augmentations)) && user:a_intent=="hurt")
		usr << text("\blue You destroy the table.")
		visible_message("\red %knownface:1% destroys the operating table!", actors=list(usr))
		src.density = 0
		del(src)
	if(airTank)
		airTank.loc = src.loc
		airTank = null
		update_icon()
	return

/obj/machinery/optable/CanPass(atom/movable/mover, turf/target, height=0, air_group=0)
	if(air_group || (height==0)) return 1

	if(istype(mover) && mover.checkpass(PASSTABLE))
		return 1
	else
		return 0

/obj/machinery/optable/MouseDrop_T(obj/O as obj, mob/user as mob)
	if(O == user)
		user.loc = src.loc
		user.Weaken(5)
		visible_message("\red %knownface:1% lies down on the table.", actors=list(user))
		return

	if(istype(O,/mob/living/carbon/human))
		var/mob/living/carbon/human/H = O
		if (!(istype(H.wear_mask,/obj/item/clothing/mask) && H.wear_mask.flags&MASKINTERNALS) && !(istype(H.head,/obj/item/clothing/head) && H.head.flags&MASKINTERNALS))
			return
		H.internal = src
		user.visible_message("%knownface:1% attaches %knownface:2%'s internals to [src].", "You attach %knownface:2%'s internals to [src].", actors=list(user, H))
		return

	if (!istype(O, /obj/item)  || user.get_active_hand() != O)
		return
	user.drop_item()
	if (O.loc != src.loc)
		step(O, get_dir(O, src))
	return

/obj/machinery/optable/proc/check_victim()
	if(locate(/mob/living/carbon/human, src.loc))
		var/mob/M = locate(/mob/living/carbon/human, src.loc)
		if(M.resting)
			src.victim = M
			icon_state = "table2-active"
			return 1
	src.victim = null
	icon_state = "table2-idle"
	return 0

/obj/machinery/optable/process()
	check_victim()
	cycle = (cycle+1)%2
	if(pumping && cycle==0 && src.victim && src.victim.internal==src)
		src.victim.makeBreath(1)

/obj/machinery/optable/update_icon()
	if(airTank)
		if(!underlays.len)
			underlays = list(airTankIcon)
	else
		if(underlays.len)
			underlays.Cut()

/obj/machinery/optable/attackby(obj/item/weapon/W as obj, mob/living/carbon/user as mob)
	if (istype(W, /obj/item/weapon/grab))
		if(ismob(W:affecting))
			var/mob/M = W:affecting
			if (M.client)
				M.client.perspective = EYE_PERSPECTIVE
				M.client.eye = src
			M.resting = 1
			M.loc = src.loc
			visible_message("\red %knownface:1% has been laid on the operating table by %knownface:2%.", actors=list(M, user))
//			for(var/obj/O in src)
//				O.loc = src.loc
			src.add_fingerprint(user)
			icon_state = "table2-active"
			src.victim = M
			del(W)
			return
	if(istype(W,/obj/item/weapon/tank))
		if(airTank)
			return
		if(istype(W,/obj/item/weapon/tank/emergency_oxygen) || istype(W,/obj/item/weapon/tank/jetpack))
			return
		user.drop_from_inventory(W)
		airTank = W
		airTank.loc = src
		var/icon/ATicon = new(airTank.icon,airTank.icon_state)
		ATicon.Turn(45)
		ATicon.Scale(38,38)
		airTankIcon = image(ATicon)
		airTankIcon.pixel_y = 16
		update_icon()
		user.visible_message("%knownface:1% attaches [W] to [src].", "You attach [W] to [src].", actors=list(user))
		return
	if(istype(W,/obj/item/pipe))
		var/obj/item/pipe/P = W
		if(P.pipe_type == PIPE_PUMP || P.pipe_type == PIPE_VOLUME_PUMP)
			playsound(src.loc, 'sound/items/Ratchet.ogg', 50, 1)
			user.drop_from_inventory(P)
			Pump = P
			P.loc = src
			user.visible_message("%knownface:1% attaches [P] to [src].", "You attach [P] to [src].", actors=list(user))
		return
	if(istype(W,/obj/item/weapon/wrench) && Pump)
		playsound(src.loc, 'sound/items/Ratchet.ogg', 50, 1)
		Pump.loc = src
		user.visible_message("%knownface:1% detaches [Pump] from [src].", "You detach [Pump] from [src].", actors=list(user))
		Pump = null
		return

	user.drop_item()
	if(W && W.loc)
		W.loc = src.loc
	return

/obj/machinery/optable/remove_air_volume(volume_needed)
	if(airFromTank && airTank)
		return airTank.remove_air_volume(volume_needed)
	else
		var/turf/T = get_turf(src)
		return T.remove_air_volume(volume_needed)

#undef PIPE_PUMP
#undef PIPE_VOLUME_PUMP
