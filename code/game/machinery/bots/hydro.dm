//Floorbot assemblies
/obj/item/weapon/hydrobot_construction
	desc = ""
	name = "hydrobot construction"
	icon = 'icons/obj/hydrobot.dmi'
	icon_state = "construct0"
	force = 3.0
	throwforce = 10.0
	throw_speed = 2
	throw_range = 5
	w_class = 3.0
	flags = TABLEPASS
	var/created_name = "Hydrobot"
	var/stage = 0
	var/obj/item/weapon/reagent_containers/glass/beaker = null
	var/obj/item/weapon/reagent_containers/spray/spray = null
	var/obj/item/robot_parts/arm = null
	var/obj/item/device/assembly/prox_sensor/proxy = null

/obj/item/weapon/storage/toolbox/mechanical/attackby(var/obj/item/I, mob/user as mob)
	if(istype(I, /obj/item/device/analyzer/plant_analyzer))
		if(contents.len >= 1)
			user << "<span class='notice'>Toolbox must be empty.</span>"
			return
		if(user.s_active)
			user.s_active.close(user)
		user.u_equip(I)
		del(I)
		var/obj/item/weapon/hydrobot_construction/HC = new
		HC.loc = get_turf(src)
		user << "<span class='notice'>You add the plant analyser to the empty toolbox.</span>"
		user.drop_from_inventory(src)
		user.put_in_hands(HC)
		del(src)
		return
	..()
	
/obj/item/weapon/hydrobot_construction/examine()
	..()
	if ((in_range(src, usr) || loc == usr))
		usr << "It has plant analyser attached."
		if(beaker)
			usr << "It has [beaker] attached."
		if(spray)
			usr << "It has [spray] attached."
		if(arm)
			usr << "It has [arm] attached."
		if(proxy)
			usr << "It has [proxy] attached."
	return

/obj/item/weapon/hydrobot_construction/attackby(var/obj/item/I, mob/user as mob)
	if(istype(I,/obj/item/weapon/pen))
		var/t = stripped_input(user, "Enter new robot name", src.name, src.created_name)
		if (!t || !in_range(src, usr) && src.loc != usr) return
		src.created_name = t
		return
	switch(stage)
		if(0)
			if(istype(I,/obj/item/weapon/reagent_containers/glass))
				user.drop_from_inventory(I)
				I.loc=src
				beaker = I
				stage++
				usr<<"You attach [I] to the [src]"
		if(1)
			if(istype(I,/obj/item/weapon/reagent_containers/spray))
				user.drop_from_inventory(I)
				I.loc=src
				spray = I
				stage++
				usr<<"You attach [I] to the [src]"
		if(2)
			if(istype(I, /obj/item/robot_parts/l_arm) || istype(I, /obj/item/robot_parts/r_arm))
				user.drop_from_inventory(I)
				I.loc=src
				arm = I
				stage++
				usr<<"You attach [I] to the [src]"
		if(3)
			if(istype(I, /obj/item/device/assembly/prox_sensor))
				user.drop_from_inventory(I)
				I.loc=src
				proxy = I
				stage++
				usr<<"You attach [I] to the [src]"
		if(4)
			if(istype(I, /obj/item/weapon/screwdriver))
				stage++
				usr<<"You screw everything in [src] together"
	if(stage==5)
		var/obj/machinery/bot/hydrobot/HB = new
		HB.loc = get_turf(src)
		beaker.loc = HB
		HB.beaker = src.beaker
		spray.loc = HB
		HB.spray = src.spray
		HB.name = src.created_name
		HB.reagents = spray.reagents
		user:u_equip(src)
		del(src)
	else
		icon_state = "construct[stage]"

/obj/item/weapon/hydrobot_construction/attack_self(mob/user as mob)
	switch(stage)
		if(0)
			var/obj/item/weapon/storage/toolbox/mechanical/MT = new
			var/obj/item/device/analyzer/plant_analyzer/PA = new
			for(var/atom/A in MT)
				del(A)
			MT.loc = get_turf(src)
			PA.loc = MT.loc
			del(src)
		if(1)
			if(beaker)
				beaker.loc = get_turf(src)
				user<<"You detach [beaker] from [src]"
				beaker = null
			stage--
		if(2)
			if(spray)
				spray.loc = get_turf(src)
				user<<"You detach [spray] from [src]"
				spray = null
			stage--
		if(3)
			if(arm)
				arm.loc = get_turf(src)
				user<<"You detach [arm] from [src]"
				arm = null
			stage--
		if(4)
			if(proxy)
				proxy.loc = get_turf(src)
				user<<"You detach [proxy] from [src]"
				proxy = null
			stage--

//Floorbot
/obj/machinery/bot/hydrobot
	name = "Hydrobot"
	desc = "A little botanist robot."
	icon = 'icons/obj/hydrobot.dmi'
	icon_state = "work"
	layer = 4.0
	density = 0
	anchored = 0
	health = 25
	maxhealth = 25
	flags = FPRINT | OPENCONTAINER
	req_access = list(access_hydroponics)
	var/path[] = new()
	var/obj/item/weapon/reagent_containers/glass/beaker
	var/obj/item/weapon/reagent_containers/spray/spray
//	var/obj/item/nutrient/nutri
	var/waters = 1
//	var/nutriments = 1
	var/spray_amnt = 10
	var/spraysw = 1
	var/spraysp = 1
	var/sprayst = 0
	var/gather = 0
	var/atom/target
	var/atom/oldtarget

/obj/machinery/bot/hydrobot/complete
	New()
		..()
		beaker = new/obj/item/weapon/reagent_containers/glass/bucket
		spray = new/obj/item/weapon/reagent_containers/spray
		reagents = spray.reagents

/obj/machinery/bot/hydrobot/New()
	..()
	src.updateicon()

/obj/machinery/bot/hydrobot/turn_on()
	. = ..()
	src.updateicon()
	src.updateUsrDialog()

/obj/machinery/bot/hydrobot/turn_off()
	..()
	src.updateicon()
	src.path = new()
	src.updateUsrDialog()

/obj/machinery/bot/hydrobot/attack_hand(mob/user as mob)
	. = ..()
	if (.)
		return
	usr.machine = src
	interact(user)

/obj/machinery/bot/hydrobot/proc/interact(mob/user as mob)
	var/dat
	dat += "<TT><B>Automatic Hydroponics Helper v1.0</B></TT><BR><BR>"
	dat += "Status: <A href='?src=\ref[src];operation=start'>[src.on ? "On" : "Off"]</A><BR>"
	dat += "Spray left: [src.spray.reagents.total_volume]/[src.spray.reagents.maximum_volume]<BR>"
	dat += "Water level: [src.beaker.reagents.total_volume]/[src.beaker.reagents.maximum_volume]<BR>"
	dat += "Behaviour controls are [src.locked ? "locked" : "unlocked"]<BR>"
	if(!src.locked || isadminobserver(user))
		dat += "Waters plants: <A href='?src=\ref[src];operation=waters'>[src.waters ? "Yes" : "No"]</A><BR>"
//		dat += "Nutiments plants: <A href='?src=\ref[src];operation=nutriments'>[src.nutriments ? "Yes" : "No"]</A><BR>"
		dat += "Sprays plants if weeds: <A href='?src=\ref[src];operation=spraysw'>[src.spraysw ? "Yes" : "No"]</A><BR>"
		dat += "Sprays plants if pests: <A href='?src=\ref[src];operation=spraysp'>[src.spraysp ? "Yes" : "No"]</A><BR>"
		dat += "Sprays plants if toxic: <A href='?src=\ref[src];operation=sprayst'>[src.sprayst ? "Yes" : "No"]</A><BR>"
		dat += "Spray amount: [src.spray_amnt]. Set <A href='?src=\ref[src];operation=sprayamnt;amnt=5'>5</A> <A href='?src=\ref[src];operation=sprayamnt;amnt=10'>10</A> <A href='?src=\ref[src];operation=sprayamnt;amnt=15'>15</A><BR>"
		dat += "Gather plants: <A href='?src=\ref[src];operation=gather'>[src.gather ? "Yes" : "No"]</A><BR>"
	user << browse("<HEAD><TITLE>Hydrobot v1.0 controls</TITLE></HEAD>[dat]", "window=autorepair")
	onclose(user, "autorepair")
	return


/obj/machinery/bot/hydrobot/attackby(var/obj/item/W , mob/user as mob)
	if(istype(W, /obj/item/weapon/reagent_containers))
		var/obj/item/weapon/reagent_containers/RC = W
		spray.attackby(RC,user)
	else if(istype(W, /obj/item/weapon/card/id)||istype(W, /obj/item/device/pda))
		if(src.allowed(usr) && !open && !emagged)
			src.locked = !src.locked
			user << "<span class='notice'>You [src.locked ? "lock" : "unlock"] the [src] behaviour controls.</span>"
		else
			if(emagged)
				user << "<span class='warning'>ERROR</span>"
			else
				user << "<span class='warning'>Access denied.</span>"
		src.updateUsrDialog()
	else
		..()

/obj/machinery/bot/hydrobot/Emag(mob/user as mob)
	..()
	if(open && !locked)
		if(user) user << "<span class='notice'>The [src] buzzes and beeps.</span>"

/obj/machinery/bot/hydrobot/Topic(href, href_list)
	if(..())
		return
	usr.machine = src
	src.add_fingerprint(usr)
	switch(href_list["operation"])
		if("start")
			if (src.on)
				turn_off()
			else
				turn_on()
		if("gather")
			src.gather = !gather
			src.updateUsrDialog()
		if("waters")
			src.waters = !src.waters
			src.updateUsrDialog()
//		if("nutriments")
//			src.nutriments = !src.nutriments
//			src.updateUsrDialog()
		if("spraysw")
			src.spraysw = !src.spraysw
			src.updateUsrDialog()
		if("spraysp")
			src.spraysp = !src.spraysp
			src.updateUsrDialog()
		if("sprayst")
			src.sprayst = !src.sprayst
			src.updateUsrDialog()
		if("sprayamnt")
			var/amnt = text2num(href_list["amnt"])
			spray_amnt = max(5,min(15,amnt))
			src.updateUsrDialog()

/obj/machinery/bot/hydrobot/proc/needAssistance(var/obj/machinery/hydroponics/H)
	if(waters && H.waterlevel<=10) return 1
	if(spraysw && H.planted && H.weedlevel>=5) return 1
	if(spraysp && H.planted && H.pestlevel>=5) return 1
	if(sprayst && H.planted && H.toxic>=40) return 1
	if(gather && H.harvest && H.myseed) return 1
//	if(nutriments && H.nutrilevel<=2) return 2
	return 0

/obj/machinery/bot/hydrobot/process()
	set background = 1

	if(!beaker || !beaker.reagents || !spray || !spray.reagents)
		explode()

	if(!src.on)
		return

	if(target)
		if((!waters || src.beaker.reagents.has_reagent("water")) && (istype(target,/obj/structure/sink) || istype(target,/obj/structure/reagent_dispensers)))
			target=null
		else if(istype(target,/obj/machinery/hydroponics) && !needAssistance(target))
			target=null

	if(target==null)
		var/view = view(7,src)
		if(waters && !src.beaker.reagents.has_reagent("water"))
			var/amnt = 0
			var/best = null
			for(var/obj/structure/reagent_dispensers/RD in view)
				if(RD.reagents.has_reagent("water") && oldtarget!=RD)
					var/camnt = RD.reagents.get_reagent_amount("water")
					if(camnt>amnt)
						amnt=camnt
						best = RD
			if(!best)
				for(var/obj/structure/sink/S in view)
					if(S==oldtarget) continue
					best=S
					break
			if(best)
				target = best
		if(target==null)
			if(waters || /*nutriments ||*/ spraysw || spraysp || sprayst)
				for(var/obj/machinery/hydroponics/H in view)
					var/needAssist = needAssistance(H)
					if(needAssist && oldtarget!=H)
						target = H
						break

	if(prob(15) && waters && beaker.reagents.total_volume==0 && !(target && istype(target,/obj/structure)))
		hearable_message("[src] beeps sadly")
	else if(prob(15) && spray.reagents.total_volume<spray_amnt && (spraysw || spraysp || sprayst))
		hearable_message("[src] buzzes sadly")
	else if(prob(2))
		visible_message("[src] makes an excited booping beeping sound!")

	if(src.target && (src.target != null) && src.path.len == 0)
		spawn(0)
			if(!istype(src.target, /turf/))
				src.path = AStar(src.loc, src.target.loc, /turf/proc/AdjacentTurfsSpace, /turf/proc/Distance, 0, 30, 1)
			else
				src.path = AStar(src.loc, src.target, /turf/proc/AdjacentTurfsSpace, /turf/proc/Distance, 0, 30, 1)
			src.path = reverselist(src.path)
			if(src.path.len == 0)
				src.oldtarget = src.target
				src.target = null
			else
				src.oldtarget = null
		return

	if(in_range(src,target))
		src.path = new()
		if(istype(target,/obj/structure/reagent_dispensers))
			var/obj/structure/reagent_dispensers/RD = target
			RD.reagents.trans_id_to(beaker,"water",beaker.reagents.maximum_volume-beaker.reagents.total_volume)
			visible_message("[src] refills from [target].")
		else if(istype(target,/obj/structure/sink))
			beaker.reagents.add_reagent("water",beaker.reagents.maximum_volume-beaker.reagents.total_volume)
			visible_message("[src] refills from [target].")
		else if(istype(target,/obj/machinery/hydroponics))
			var/obj/machinery/hydroponics/H = target
			var/flicks = 0
			if(waters && H.waterlevel<=10)
				beaker.reagents.isolate_reagent("water")
				var/amnt = min(beaker.reagents.get_reagent_amount("water"),100-H.waterlevel)
				beaker.reagents.remove_reagent("water",amnt)
				H.waterlevel += amnt
				H.toxic = max(0,H.toxic-round(amnt/4))
				flicks = 1
//			if(nutriments && H.nutrilevel<=2)
			if((spraysw && H.planted && H.weedlevel>=5) || (spraysp && H.planted && H.pestlevel>=5) || (sprayst && H.planted && H.toxic>=40))
				if(spray.reagents.total_volume<spray_amnt)
					hearable_message("[src] makes sad buzz")
				if(spray.reagents.total_volume>0)
					var/obj/item/weapon/reagent_containers/syringe/fakeS = new
					fakeS.mode = 1
					spray.reagents.trans_to(fakeS,spray_amnt)
					H.attackby(spray,src)
					del(fakeS)
					flicks = 1
			if(gather && H.harvest && H.myseed)
				H.myseed.harvest(src)

			if(flicks)
				flick("bot_spray",src)
				playsound(src.loc, 'sound/effects/spray2.ogg', 50, 1, -6)
		target = null
		return

	if(src.path.len > 0 && src.target && (src.target != null))
		step_to(src, src.path[1])
		src.path -= src.path[1]
	else if(src.path.len == 1)
		step_to(src, target)
		src.path = new()

/obj/machinery/bot/hydrobot/proc/updateicon()
	src.icon_state = "bot[src.on]"

/obj/machinery/bot/hydrobot/explode()
	src.on = 0
	src.visible_message("\red <B>[src] blows apart!</B>", 1)
	var/turf/Tsec = get_turf(src)

	var/obj/item/weapon/storage/toolbox/mechanical/N = new /obj/item/weapon/storage/toolbox/mechanical(Tsec)
	N.contents = list()

	if (prob(50))
		new /obj/item/device/assembly/prox_sensor(Tsec)
	if (prob(50))
		new /obj/item/robot_parts/l_arm(Tsec)
	if (prob(50))
		new /obj/item/device/analyzer/plant_analyzer(Tsec)
	if (prob(50) && beaker)
		beaker.loc = Tsec
	if (prob(50) && spray)
		spray.loc = Tsec

	var/datum/effect/effect/system/spark_spread/s = new /datum/effect/effect/system/spark_spread
	s.set_up(3, 1, src)
	s.start()
	del(src)
	return
