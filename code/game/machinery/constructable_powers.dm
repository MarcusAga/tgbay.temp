//
/obj/item/weapon/circuitboard/solarpanel
	name = "Circuit Board (Solar Panel)"
	build_path = "/obj/machinery/power/solar"
	board_type = "machine"
	origin_tech = "engineering=4;power=4"
	frame_desc = "2 Cable Coil, 2 Capacitors, 1 Manipuator and 1 Micro-Laser."
	req_components = list(
							"/obj/item/weapon/cable_coil" = 2,
							"/obj/item/weapon/stock_parts/capacitor" = 2,
							"/obj/item/weapon/stock_parts/manipulator" = 1,
							"/obj/item/weapon/stock_parts/micro_laser" = 1)
/datum/design/circuit_solarpanel
	name = "Circuit Design (Solar Panel)"
	desc = "Allows for the construction of Solar Panels."
	id = "solar-panel"
	req_tech = list("engineering"=4, "powerstorage"=4)
	build_type = 1 //IMPRINTER
	materials = list("$glass" = 2000, "sacid" = 20)
	build_path = "/obj/item/weapon/circuitboard/solarpanel"

/obj/item/weapon/circuitboard/solartracker
	name = "Circuit Board (Solar Tracker)"
	build_path = "/obj/machinery/power/tracker"
	board_type = "machine"
	origin_tech = "engineering=3;power=1"
	frame_desc = "1 Cable Coil and 1 Micro-Laser."
	req_components = list(
							"/obj/item/weapon/cable_coil" = 1,
							"/obj/item/weapon/stock_parts/micro_laser" = 1)
/datum/design/circuit_solartracker
	name = "Circuit Design (Solar Tracker)"
	desc = "Allows for the construction of Solar Trackers."
	id = "solar-tracker"
	req_tech = list("engineering"=3, "powerstorage"=1)
	build_type = 1 //IMPRINTER
	materials = list("$glass" = 2000, "sacid" = 20)
	build_path = "/obj/item/weapon/circuitboard/solartracker"

/obj/item/weapon/circuitboard/smes
	name = "Circuit Board (SMES)"
	build_path = "/obj/machinery/power/smes"
	board_type = "machine"
	origin_tech = "engineering=4;power=4"
	frame_desc = "2 Cable Coil, 2 Capacitors, 1 Manipuator and 1 Micro-Laser."
	req_components = list(
							"/obj/item/weapon/cable_coil" = 2,
							"/obj/item/weapon/stock_parts/capacitor" = 10,
							"/obj/item/weapon/stock_parts/manipulator" = 1,
							"/obj/item/weapon/stock_parts/micro_laser" = 1)
/datum/design/circuit_smes
	name = "Circuit Design (SMES)"
	desc = "Allows for the construction of SMES-es."
	id = "solar-panel"
	req_tech = list("engineering"=4, "powerstorage"=4)
	build_type = 1 //IMPRINTER
	materials = list("$glass" = 2000, "sacid" = 20)
	build_path = "/obj/item/weapon/circuitboard/smes"
