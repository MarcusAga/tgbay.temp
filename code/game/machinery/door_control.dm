/obj/machinery/door_control/attack_ai(mob/user as mob)
	if(wires & 2)
		return src.attack_hand(user)
	else
		user << "Error, no route to host."

/obj/machinery/door_control/attack_paw(mob/user as mob)
	return src.attack_hand(user)

/obj/machinery/door_control/attackby(obj/item/weapon/W, mob/user as mob)
	/* For later implementation
	if (istype(W, /obj/item/weapon/screwdriver))
	{
		if(wiresexposed)
			icon_state = "doorctrl0"
			wiresexposed = 0

		else
			icon_state = "doorctrl-open"
			wiresexposed = 1

		return
	}
	*/
	if(istype(W, /obj/item/device/detective_scanner))
		return
	return src.attack_hand(user)

/obj/machinery/door_control/attack_hand(mob/user as mob)
	src.add_fingerprint(usr)
	if(stat & (NOPOWER|BROKEN))
		return

	if(!allowed(user) && (wires & 1))
		user << "\red Access Denied"
		flick("doorctrl-denied",src)
		return

	use_power(5)
	icon_state = "doorctrl1"
	add_fingerprint(user)

	if(normaldoorcontrol)
		for(var/obj/machinery/door/airlock/D in range(range,get_turf(src)))
			if(D.id_tag == src.id)
				if(desiredstate == 1)
					if(specialfunctions & OPEN)
						if (D.density)
							spawn( 0 )
								D.open()
								return
					if(specialfunctions & IDSCAN)
						D.aiDisabledIdScanner = 1
					if(specialfunctions & BOLTS)
						D.locked = 1
						D.update_icon()
					if(specialfunctions & SHOCK)
						D.secondsElectrified = -1
					if(specialfunctions & SAFE)
						D.safe = 0

				else
					if(specialfunctions & OPEN)
						if (!D.density)
							spawn( 0 )
								D.close()
								return
					if(specialfunctions & IDSCAN)
						D.aiDisabledIdScanner = 0
					if(specialfunctions & BOLTS)
						if(!D.isWireCut(4) && D.arePowerSystemsOn())
							D.locked = 0
							D.update_icon()
					if(specialfunctions & SHOCK)
						D.secondsElectrified = 0
					if(specialfunctions & SAFE)
						D.safe = 1

	else
		for(var/obj/machinery/door/poddoor/M in world)
			if (M.id == src.id)
				if (M.density)
					spawn( 0 )
						M.open()
						return
				else
					spawn( 0 )
						M.close()
						return

	desiredstate = !desiredstate
	spawn(15)
		if(!(stat & NOPOWER))
			icon_state = "doorctrl0"

/obj/machinery/door_control/power_change()
	..()
	if(stat & NOPOWER)
		icon_state = "doorctrl-p"
	else
		icon_state = "doorctrl0"

/obj/machinery/driver_button/attack_ai(mob/user as mob)
	return src.attack_hand(user)

/obj/machinery/driver_button/attack_paw(mob/user as mob)
	return src.attack_hand(user)

/obj/machinery/driver_button/attackby(obj/item/weapon/W, mob/user as mob)

	if(istype(W, /obj/item/device/detective_scanner))
		return
	return src.attack_hand(user)

/obj/machinery/driver_button/attack_hand(mob/user as mob)
	add_fingerprint(user)

	processDrive(0)


/obj/machinery/driver_button/proc/processDrive(var/auto as num)
	if(auto && !AutoDump) return
	
	if(stat & (NOPOWER|BROKEN))
		return
	if(active)
		return

	use_power(5)

	active = 1
	icon_state = "launcheract"

	for(var/obj/machinery/door/poddoor/M in world)
		if (M.id == src.id)
			spawn( 0 )
				M.open()
				return

	sleep(50)

	for(var/obj/machinery/mass_driver/M in world)
		if(M.id == src.id)
			M.drive()

	sleep(50)

	for(var/obj/machinery/door/poddoor/M in world)
		if (M.id == src.id)
			spawn( 0 )
				M.close()
				return

	icon_state = "launcherbtt"
	active = 0

	if(AutoDump)
		spawn(6000)
			processDrive(1)

	return

/*
/obj/machinery/driver_button/verb/ToggleAuto()
	set name = "Set Auto Activate"
	set category = "Object"
	set src in oview(1)

	if(!usr.canmove || usr.stat || usr.restrained())
		return

	if(ishuman(usr))
		AutoDump=!AutoDump
		if(AutoDump)
			usr << "\blue Auto-dump mode activated."
		else
			usr << "\blue Auto-dump mode deactivated."
		src.attack_hand(usr)
*/

/obj/machinery/door_control/pod_unlock
	name = "Pod Unlock"
	range = 128

	attack_ai(mob/user as mob)
		return
	attack_hand(mob/user as mob)
		src.add_fingerprint(usr)
		if(stat & (NOPOWER|BROKEN))
			return

		if(!allowed(user) && (wires & 1))
			user << "\red Access Denied"
			flick("doorctrl-denied",src)
			return

		use_power(5)
		add_fingerprint(user)

		for(var/obj/machinery/door/airlock/D in range(range))
			if(D.id_tag in list("pod1_door","pod2_door","pod3_door","pod4_door","pod5_door"))
				if(desiredstate == 1)
					D.locked = 1
					D.update_icon()
				else
					D.locked = 0
					D.update_icon()

		desiredstate = !desiredstate
		captain_announce("Emergency pods are now [desiredstate?"unlocked":"locked"]")
		icon_state = "doorctrl[desiredstate]"
