//
/obj/machinery/musicalcenter
	name = "Musical Center"
	desc = "Used to play music. Really."
	icon = 'icons/obj/musicalcenter.dmi'
	icon_state = "musiccenteroff"
	anchored = 1
	density = 0
	opacity = 0
	var/health = 100
	var/broken = 0
	var/tmp/songname = ""
	var/sound/track
	var/tmp/list/listeners = list()
	var/tmp/playing = 0
	var/tmp/volume = 50
	var/tmp/updated = 0

	New()
		var/obj/effect/forcefield/FF = new(src.loc)
		FF.invisibility = 101
		FF = new(locate(x+1,y,z))
		FF.invisibility = 101
		FF = new(locate(x+1,y+1,z))
		FF.invisibility = 101
		FF = new(locate(x,y+1,z))
		FF.invisibility = 101
		track = new/sound('sound/machines/chime.ogg')
		..()

	process()
		var/list/newlisteners = list()
		var/list/reallisteners = hear(15,src)
		for(var/mob/M in reallisteners)
			if(!istype(M) || M.ear_deaf || !M.client) continue
			newlisteners += M.client
			if(!(M.client in listeners))
				addListener(M.client)
		for(var/client/C in listeners)
			if(!C.mob) continue
			if(!(C in newlisteners))
				delListener(C)
		listeners = newlisteners
		if(playing && !broken)
			icon_state = "musiccenteron"
		else
			if(broken) icon_state = "musiccenterdestroyed"
			else icon_state = "musiccenteroff"
		if(updated)
			track.volume = volume
			for(var/client/C in listeners)
//				if(C.mob)
				updListener(C)
			updated = 0

	proc/addListener(var/client/C)
		if(!track.file || updated) return
		C << track
		if(playing && !broken)
			track.status = SOUND_UPDATE
		else
			track.status = SOUND_UPDATE|SOUND_PAUSED
		spawn(1)
			C << track

	proc/updListener(var/client/C)
		if(!track.file) return
		if(playing && !broken)
			track.status = SOUND_UPDATE
		else
			track.status = SOUND_UPDATE|SOUND_PAUSED|SOUND_MUTE
		C << track

	proc/delListener(var/client/C)
		track.status = SOUND_UPDATE|SOUND_MUTE
		C << track

	hitby(AM as mob|obj)
		..()
		for(var/mob/O in viewers(src, null))
			O.show_message("\red <B>[src] was hit by [AM].</B>", 1)
		var/tforce = 0
		if(ismob(AM))
			tforce = 40
		else
			tforce = AM:throwforce
		hit(tforce)

	bullet_act(var/obj/item/projectile/Proj)
		if(istype(Proj))
			src.add_hiddenprint(Proj.firer)
			hit(Proj.damage)
		..()

	ex_act(severity)
		switch(severity)
			if(1) hit(100,0)
			if(2) hit(50,0)
			if(3) hit(25,0)
	emp_act(severity)
		switch(severity)
			if(1) hit(100,0)
			else randomize()

	attackby(obj/item/weapon/W as obj, mob/user as mob)
		src.add_hiddenprint(user)
		if(!istype(W)) return//I really wish I did not need this
		if(W.damtype == BRUTE || W.damtype == BURN)
			hit(W.force)
		..()

	proc/hit(var/damage, var/sound_effect = 1)
		src.health = max(0, src.health - damage)
		if(src.health<=0)
			broken = 1
			playing = 1
		if(sound_effect)
			playsound(src.loc, 'sound/effects/Glasshit.ogg', 75, 1)

	attack_hand(var/mob/user as mob)
		if(..())
			return
		
		user.machine = src
		var/dat = "<h3>Musical Center</h3><br><br>"
		dat += "Current song: [songname?"[songname]":"none"]<br>"
		dat += "<a href='?src=\ref[src];trigger=1'>[playing?"Stop":"Play"]</a><br>"
		dat += "Volume: <a href='?src=\ref[src];volume=-10'>-</a> [volume] <a href='?src=\ref[src];volume=10'>+</a>"
		dat += "<hr>"
		dat += "<table border='1'><tr><th>Genre</th><th>Author</th><th>Name</th></tr>"

		var/list/records = tg_text2list(file2text("music/listmusic"),"\n")
		var/idx = 1
		for(var/recordstr in records)
			var/list/record = tg_text2list(recordstr," # ")
			if(record.len<4) continue
			var/genre = record[2]
			var/author = record[3]
			var/title = record[4]
			dat += "<tr><td>[genre]</td><td>[author]</td><td><a href='?src=\ref[src];track=[idx]'>[title]</a></td></tr>"
			idx++

		dat += "</table>"

		user << browse(dat, "window=jukebox;size=400x500")
		onclose(user, "jukebox")
		return

	Topic(href, href_list)
		if(..())
			usr << browse(null, "window=jukebox")
			onclose(usr, "jukebox")
			return

		if(href_list["trigger"])
			if(track.file && songname && !(stat & NOPOWER))
				playing = !playing
				updated = 1
			else if(playing)
				playing = 0
				updated = 1
		else if(href_list["volume"])
			volume = max(min(100,volume+text2num(href_list["volume"])),0)
			updated = 1
		else if(href_list["track"])
			var/list/records = tg_text2list(file2text("music/listmusic"),"\n")
			var/idx = text2num(href_list["track"])
			var/list/record = tg_text2list(records[idx]," # ")
			if(record.len<4) return
			var/fname = record[1]
			var/author = record[3]
			var/title = record[4]
			if(fexists(fname))
				world<<sound(null,channel=122)
//				world<<sound(null,channel=-1)
				del(track)
				songname = "[author] - [title]"
				track = new/sound(fname)
//				track.file = file(fname)
				track.channel = 122
				track.volume = 0
//				track.repeat = 1
				track.status = SOUND_MUTE|SOUND_PAUSED
//				world<<track
				updated = 1
			else
				usr << "Record is corrupt"
		src.add_fingerprint(usr)
		src.updateUsrDialog()
		return

	proc/randomize()
		switch(pick(1,2,3))
			if(1) playing = pick(1,0)
			if(2) volume = rand(10,100)
			if(3) 
				var/list/records = tg_text2list(file2text("music/listmusic"),"\n")
				var/id = rand(1,records.len)
				var/list/record = tg_text2list(records[id]," # ")
				if(record.len<4) return
				var/fname = record[1]
				var/author = record[3]
				var/title = record[4]
				if(fexists(fname))
					world<<sound(null,channel=122)
//					world<<sound(null,channel=-1)
					del(track)
					songname = "[author] - [title]"
					track = new/sound(fname)
//					track.file = file(fname)
					track.channel = 122
					track.volume = 0
//					track.repeat = 1
					track.status = SOUND_MUTE|SOUND_PAUSED
//					world<<track
					updated = 1

	power_change()
		..()
		if((stat & NOPOWER) && playing)
			playing = 0
			updated = 1
