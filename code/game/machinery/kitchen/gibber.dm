
/obj/machinery/gibber
	name = "Gibber"
	desc = "The name isn't descriptive enough?"
	icon = 'icons/obj/kitchen.dmi'
	icon_state = "grinder"
	density = 1
	anchored = 1
	var/operating = 0 //Is it on?
	var/dirty = 0 // Does it need cleaning?
	var/gibtime = 40 // Time from starting until meat appears
	var/mob/living/occupant // Mob who has been put inside
	use_power = 1
	idle_power_usage = 2
	active_power_usage = 500

//auto-gibs anything that bumps into it
/obj/machinery/gibber/autogibber
	var/turf/input_plate

	New()
		..()
		spawn(5)
			for(var/i in cardinal)
				var/obj/machinery/mineral/input/input_obj = locate( /obj/machinery/mineral/input, get_step(src.loc, i) )
				if(input_obj)
					if(isturf(input_obj.loc))
						input_plate = input_obj.loc
						del(input_obj)
						break

			if(!input_plate)
				diary << "a [src] didn't find an input plate."
				return

	Bumped(var/atom/A)
		if(!input_plate) return

		if(ismob(A))
			var/mob/M = A

			if(M.loc == input_plate
			)
				M.loc = src.loc
				M.gib()


/obj/machinery/gibber/New()
	..()
	src.overlays += image('icons/obj/kitchen.dmi', "grjam")

/obj/machinery/gibber/update_icon()
	overlays = null
	if (dirty)
		src.overlays += image('icons/obj/kitchen.dmi', "grbloody")
	if(stat & (NOPOWER|BROKEN))
		return
	if (!occupant)
		src.overlays += image('icons/obj/kitchen.dmi', "grjam")
	else if (operating)
		src.overlays += image('icons/obj/kitchen.dmi', "gruse")
	else
		src.overlays += image('icons/obj/kitchen.dmi', "gridle")

/obj/machinery/gibber/attack_paw(mob/user as mob)
	return src.attack_hand(user)

/obj/machinery/gibber/relaymove(mob/user as mob)
	src.go_out()
	return

/obj/machinery/gibber/attack_hand(mob/user as mob)
	if(stat & (NOPOWER|BROKEN))
		return
	if(operating)
		user << "\red It's locked and running"
		return
	else
		src.startgibbing(user)

/obj/machinery/gibber/attackby(obj/item/weapon/grab/G as obj, mob/user as mob)
	if(src.occupant)
		user << "\red The gibber is full, empty it first!"
		return
	if (!istype(G, /obj/item/weapon/grab) || !(ismonkey(G.affecting) || ishuman(G.affecting) || isalienadult(G.affecting) || isanimal(G.affecting)))
		user << "\red This item is not suitable for the gibber!"
		return
	if(istype(G.affecting,/mob/living/simple_animal))
		var/mob/living/simple_animal/SA = G.affecting
		if(!SA.meat_amount || !SA.meat_type)
			user << "\red This animal has no meat to extract!"
			return
	if(G.affecting.abiotic(1))
		user << "\red Subject may not have abiotic items on."
		return

	user.visible_message("\red %knownface:1% starts to put %knownface:2% into the gibber!", actors=list(user, G.affecting))
	src.add_fingerprint(user)
	if(do_after(user, 50) && G && G.affecting && !occupant)
		user.visible_message("\red %knownface:1% stuffs %knownface:2% into the gibber!", actors=list(user, G.affecting))
		var/mob/M = G.affecting
		if(M.client)
			M.client.perspective = EYE_PERSPECTIVE
			M.client.eye = src
		M.loc = src
		src.occupant = M
		del(G)
		update_icon()

/obj/machinery/gibber/verb/eject()
	set category = "Object"
	set name = "Empty Gibber"
	set src in oview(1)

	if (usr.stat != 0)
		return
	src.go_out()
	add_fingerprint(usr)
	return

/obj/machinery/gibber/proc/go_out()
	if (!src.occupant)
		return
	for(var/obj/O in src)
		O.loc = src.loc
	if (src.occupant.client)
		src.occupant.client.eye = src.occupant.client.mob
		src.occupant.client.perspective = MOB_PERSPECTIVE
	src.occupant.loc = src.loc
	src.occupant = null
	update_icon()
	return


/obj/machinery/gibber/proc/startgibbing(mob/user as mob)
	add_fingerprint(user)
	if(src.operating)
		return
	if(!src.occupant)
		for(var/mob/M in viewers(src, null))
			M.show_message("\red You hear a loud metallic grinding sound.", 1)
		return
	use_power(1000)
	for(var/mob/M in viewers(src, null))
		M.show_message("\red You hear a loud squelchy grinding sound.", 1)
	src.operating = 1
	update_icon()
//	var/sourcename = src.occupant.real_name
//	var/sourcejob = src.occupant.job
	var/sourcenutriment = src.occupant.nutrition / 15
	var/sourcetotalreagents
	if(src.occupant.reagents)
		sourcetotalreagents = src.occupant.reagents.total_volume
	var/totalslabs = 4
	if(istype(src.occupant,/mob/living/carbon/human))
		totalslabs = max(1,sourcenutriment/5)
	else if(istype(src.occupant,/mob/living/simple_animal))
		var/mob/living/simple_animal/SA = src.occupant
		totalslabs = SA.meat_amount

	var/obj/item/weapon/reagent_containers/food/snacks/meat/allmeat[totalslabs]
	for (var/i=1 to totalslabs)
		var/obj/item/weapon/reagent_containers/food/snacks/meat/newmeat
/*		if(istype(src.occupant,/mob/living/carbon/human))
			var/obj/item/weapon/reagent_containers/food/snacks/meat/human/newmeath = new()
			if(src.occupant.status_flags&DISFIGURED)
				newmeath.name = sourcename + "Unknown"
			else
				newmeath.name = sourcename + newmeath.name
			newmeath.subjectname = sourcename
			newmeath.subjectjob = sourcejob
			newmeat = newmeath
		else if(istype(src.occupant,/mob/living/carbon/monkey))
			newmeat = new/obj/item/weapon/reagent_containers/food/snacks/meat/monkey()
			newmeat.name = "[src.occupant.name]-monkey-[newmeat.name]"
		else if(istype(src.occupant,/mob/living/carbon/alien/humanoid))
			newmeat = new/obj/item/weapon/reagent_containers/food/snacks/xenomeat()
		else if(istype(src.occupant,/mob/living/simple_animal))
			var/mob/living/simple_animal/SA = src.occupant
			newmeat = new SA.meat_type()
			newmeat.name = "[SA.name]-[newmeat.name]"
		else
			newmeat = new/obj/item/weapon/reagent_containers/food/snacks/meat()
			newmeat.name = "[src.occupant.name]-[newmeat.name]"
*/
		if(istype(src.occupant,/mob/living/simple_animal))
			var/mob/living/simple_animal/SA = src.occupant
			newmeat = new SA.meat_type()
		else
			newmeat = new/obj/item/weapon/reagent_containers/food/snacks/meat()
			newmeat.reagents.add_reagent ("nutriment", sourcenutriment / totalslabs) // Thehehe. Fat guys go first
		if(sourcetotalreagents)
			src.occupant.reagents.trans_to (newmeat, round (sourcetotalreagents / totalslabs, 1), 0.25) // Transfer all the reagents from the
		allmeat[i] = newmeat

	src.occupant.attack_log += "\[[time_stamp()]\] Was gibbed by <b>[user]/[user.ckey]</b>" //One shall not simply gib a mob unnoticed!
	user.attack_log += "\[[time_stamp()]\] Gibbed <b>[src.occupant]/[src.occupant.ckey]</b>"
	log_attack("\[[time_stamp()]\] <b>[user]/[user.ckey]</b> gibbed <b>[src.occupant.real_name]/[src.occupant.ckey]</b>")

	log_admin("ATTACK: [user]/[user.ckey]</b> gibbed <b>[src.occupant.real_name]/[src.occupant.ckey]</b>")
	msg_admin_attack("ATTACK: [user]/[user.ckey]</b> gibbed <b>[src.occupant.real_name]/[src.occupant.ckey]") //BS12 EDIT ALG

	src.occupant.death(1)
	src.occupant.ghostize()
	var/datum/dna/dna = null
	if(ishuman(src.occupant))
		dna = src.occupant.dna
	del(src.occupant)
	spawn(src.gibtime)
		playsound(src.loc, 'sound/effects/splat.ogg', 50, 1)
		operating = 0
		for (var/i=1 to totalslabs)
			var/obj/item/meatslab = allmeat[i]
			var/turf/Tx = locate(src.x - i, src.y, src.z)
			meatslab.loc = src.loc
			meatslab.throw_at(Tx,i,3)
			if (!Tx.density)
				var/obj/effect/decal/cleanable/blood/gibs/G = new /obj/effect/decal/cleanable/blood/gibs(Tx)
				G.initGib(occupant)
				if(dna)
					G.blood_DNA = list()
					G.blood_DNA[dna.unique_enzymes] = dna.b_type
		src.operating = 0
		update_icon()


