//
/obj/machinery/lterm_sleeper
	name = "Long Term Sleeper"
	icon = 'icons/obj/longtermsleepers.dmi'
	icon_state = "long_term_0"
	density = 1
	anchored = 1
	var/isadmin = 0
	var/lts_id = ""
	var/mob/living/occupant = null
	var/process = 0
	var/left_time
	var/last_time

	New()
		..()
		req_access = list(access_medical)
		if(lts_id) name = "[initial(name)] - [lts_id]"
		if(!isadmin) spawn_cryopods+=src

	Del()
		..()
		if(!isadmin) spawn_cryopods.Remove(src)

	proc/initiate_defreeze()
		process = 1
		last_time = world.time
		left_time = 10*60*5
		if (ticker.current_state == GAME_STATE_PLAYING)
			var/obj/item/device/radio/intercom/a = new /obj/item/device/radio/intercom(null)// BS12 EDIT Arrivals Announcement Computer, rather than the AI.
			a.set_frequency(1485)
			a.autosay("\"LTS[src.lts_id?"-[src.lts_id]":""] Initiated Decryonisation Protocols.\"", "CryoSleep Controller")
			del(a)
		occupant.Paralyse(10)
		occupant.Weaken(10)
		occupant.Stun(10)
		update_icon()
		show_menu(occupant)

	attack_hand(mob/user as mob)
		if(user!=occupant)
			add_fingerprint(user)
			if(..(user)) return
		show_menu(user)

	proc/show_menu(var/mob/M)
		if(!isliving(M) && !isadminobserver(M)) return
		if(!M.client) return

		var/dat = "<b>Long Term Cryosleep Pod</b><br>"

		if(occupant)
			dat += "Inside: [occupant.name]<br>"
			if((M != occupant && allowed(M)) || isadmin)
				switch(process)
					if(2)
						dat += "Fast decryonizing<br>"
					if(1)
						dat += "<a href='byond://?src=\ref[src];Speedup=1'>Speedup Decryonisation</a><br>"
					if(0)
						dat += "<a href='byond://?src=\ref[src];Freeze=1'>Initiate Cryonisation</a><br>"
					if(-1)
						dat += "<a href='byond://?src=\ref[src];AbortFreeze=1'>Abort Cryonisation</a><br>"
			else
				switch(process)
					if(2)
						dat += "Fast decryonisation<br>"
					if(1)
						dat += "Decryonisation<br>"
					if(-1)
						dat += "Cryonisation<br>"
		else
			dat += "Empty"

		if(process != 0)
			if(process != 2)
				dat += "Time left: [round(left_time/10)] seconds<br>"
			else
				dat += "Time left: [round(left_time/50)] seconds<br>"

		if(occupant && process==0)
			dat += "<a href='byond://?src=\ref[src];Eject=1'>Eject</a><br>"

		M << browse(dat, "window=ltsleeper;size=400x500")
		onclose(M, "ltsleeper")
		M.machine = src

	Topic(href, href_list)
		if(..()) return 1
		if(ishuman(usr) && !allowed(usr) && !href_list["Eject"])
			usr<<"You don't know how to operate this"
			return

		if(!respawn_controller)
			usr << "Respawn Controller Failure"
			return 1
		if(href_list["Speedup"])
			if(process == 1)
				process = 2
		else if(href_list["Freeze"])
			if(process == 0 && occupant)
				if(occupant.stat!=DEAD)
					process = -1
					left_time = 60*10
					update_icon()
				else
					usr << "Not a good idea to freeze up dead bodies. Would be problems with getting them back."
		else if(href_list["AbortFreeze"])
			if(process == -1)
				process = 2
		else if(href_list["Eject"])
			if(process == 0)
				eject()
		return

	updateDialog()
		var/list/nearby = viewers(1, src)
		for(var/mob/M in nearby)
			if ((M.client && M.machine == src))
				src.attack_hand(M)
		if (occupant && occupant.client)
			src.attack_hand(occupant)
		AutoUpdateAI(src)

	process()
		var/dt = world.time - last_time
		last_time = world.time
		if(process == 2)
			dt *= 5
		if(isadmin)
			dt *= 30
		if(left_time>0)
			left_time = max(0,left_time-dt)
			spawn(0) updateDialog()
		if(process!=0)
			if(occupant)
				occupant.SetParalysis(10)
				occupant.SetWeakened(10)
				occupant.SetSleeping(10)
			else //wadafuk? who stolen occupant?
				process = 0
				left_time = 0
				update_icon()
				respawn_controller.checkCanDefreezeSomeone()
		if(left_time==0 && process!=0)
			if(process>0)
				occupant << "Decryonisation Protocol Finished"
				occupant.SetParalysis(0)
				occupant.SetWeakened(0)
				occupant.SetSleeping(0)
			else if(occupant && occupant.stat!=DEAD)
				respawn_controller.lts_add_freezed(occupant, 1, src)
				if(src.occupant.client)
					src.occupant.client.eye = src.occupant.client.mob
					src.occupant.client.perspective = MOB_PERSPECTIVE
				occupant.loc = pick(newplayer_start)
				occupant = null
				src.contents = list()
			playsound(src.loc, 'sound/machines/chime.ogg', 50, 1)
			process = 0
			update_icon()
			spawn(10) respawn_controller.checkCanDefreezeSomeone()

	update_icon()
		if(occupant==null)
			icon_state = "long_term_0"
		else if(process == 0)
			icon_state = "long_term_2"
		else			
			icon_state = "long_term_1"

	proc/eject()
		if(!src.occupant)
			return
		if(process!=0)
			usr << "Can't eject LTS while it is in any process"
			return
		if(src.occupant.client)
			src.occupant.client.eye = src.occupant.client.mob
			src.occupant.client.perspective = MOB_PERSPECTIVE
		for(var/obj/O in src)
			O.loc = src.loc
		src.occupant.loc = src.loc
		src.occupant.machine = null
		src.occupant << browse(null, "window=ltsleeper")
		src.occupant = null
		spawn(10) respawn_controller.checkCanDefreezeSomeone()
		update_icon()
		process = 0
		return

	verb/leave_sleeper()
		set name = "Leave Sleeper"
		set category = "Object"
		set src in oview(1)
		if(usr.stat != 0)
			return
		src.eject()
		add_fingerprint(usr)
		return


	verb/enter_sleeper()
		set name = "Enter Sleeper"
		set category = "Object"
		set src in oview(1)

		if(usr.stat != 0 || !ishuman(usr))
			return

		if(src.occupant || process!=0)
			usr << "\blue <B>The sleeper is already occupied!</B>"
			return

		if(!isemptylist(usr.search_contents_for(/obj/item/weapon/disk/nuclear)))
			usr << "You have something you just can't put in cryo..."
			return
		if(hasNonclothes(usr))
			usr << "You can't have anything except clothes..."
			return

		for(var/mob/living/carbon/slime/M in range(1,usr))
			if(M.Victim == usr)
				usr << "You're too busy getting your life sucked out of you."
				return
		visible_message("%knownface:1% starts climbing into the sleeper.", actors=list(usr))
		if(do_after(usr, 20))
			src.enterby(usr)
		return

	proc/enterby(var/mob/living/L)
		if(!istype(L)) return
		if(src.occupant || process!=0)
			usr << "\blue <B>The sleeper is already occupied!</B>"
			return 0
		if(!isemptylist(L.search_contents_for(/obj/item/weapon/disk/nuclear)))
			usr << "You have something you just can't put in cryo..."
			return 1
		if(hasNonclothes(L))
			usr << "You can't have anything except clothes..."
			return 1
		L.stop_pulling()
		if(L.client)
			L.client.perspective = EYE_PERSPECTIVE
			L.client.eye = src
		L.loc = src
		src.occupant = L
		update_icon()

		src.add_fingerprint(usr)
		return 1

	attackby(obj/item/weapon/grab/G as obj, mob/user as mob)
		if((!( istype(G, /obj/item/weapon/grab)) || !( ismob(G.affecting))))
			return
		if(src.occupant)
			user << "\blue <B>The sleeper is already occupied!</B>"
			return

		if(!isemptylist(G.affecting.search_contents_for(/obj/item/weapon/disk/nuclear)))
			usr << "[G.affecting.name] have something you just can't put in cryo..."
			return
		if(hasNonclothes(G.affecting))
			usr << "You can't have anything except clothes..."
			return

		for(var/mob/living/carbon/slime/M in range(1,G.affecting))
			if(M.Victim == G.affecting)
				usr << "[G.affecting.name] will not fit into the sleeper because they have a Slime latched onto their head."
				return

		if(!ishuman(G.affecting))
			usr << "[G.affecting.name] will not fit into the sleeper because sleepers not made for those species."
			return

		visible_message("%knownface:1% starts putting %knownface:2% into the sleeper.", actors=list(user,G.affecting))

		if(do_after(user, 20))
			if(!G || !G.affecting) return
			var/mob/M = G.affecting
			src.add_fingerprint(usr)
			if(src.enterby(M))
				del(G)
		return

	power_change()
		return

	proc/hasNonclothes(var/mob/living/L)
		if(isadmin) return 0
		if(L.abiotic()) return 1
		var/list/filter = list(	/obj/item/clothing,
					/obj/item/device/flashlight,
					/obj/item/device/radio,
					/obj/item/device/pda,
					/obj/item/weapon/card/id,
					/obj/item/weapon/implant)
		var/list/items = L.search_contents_for(/obj/item,null,filter)
		return items.len
