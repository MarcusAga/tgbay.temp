/*
		Portable Turrets:

		Constructed from metal, a gun of choice, and a prox sensor.
		Gun can be a taser or laser or energy gun.

		This code is slightly more documented than normal, as requested by XSI on IRC.

*/


/obj/machinery/porta_turret2
	name = "Naily the Turret"
	icon = 'icons/obj/NailytheturretV2.dmi'
	icon_state = "Nailyoff"
	anchored = 1
	layer = 3
	invisibility = 0		// the turret is invisible if it's inside its cover
	density = 1
	use_power = 1			// this turret uses and requires power
	idle_power_usage = 50	// when inactive, this turret takes up constant 50 Equipment power
	active_power_usage = 300// when active, this turret takes up constant 300 Equipment power
	req_access = list(access_security)
	power_channel = EQUIP	// drains power from the EQUIPMENT channel

	var/health = 80			// the turret's health
	var/locked = 1			// if the turret's behaviour control access is locked

	var/mob/target = null

	var/last_fired = 0		// 1: if the turret is cooling down from a shot, 0: turret is ready to fire
	var/shot_delay = 10		// 1 seconds between each shot
//	var/turn_delay = 20
	var/turn_mode = 1	//0 - full circle, 1 - don't turn, 2 - slight turn
	var/turn_fix_dir = 1
	var/turn_dir = 1	//1 - clockwise (?)

	var/shoot_syndie = 0	// checks if it can use the security records
	var/shoot_nonsyndie = 0	// checks if it can use the security records
	var/noshoot_unarmed = 1	// checks if it can shoot people that have a weapon they aren't authorized to have
	var/shoot_aliens = 1	// checks if it can shoot at unidentified lifeforms (ie xenos)
	var/checks_hitability = 1	// checks if it can shoot at unidentified lifeforms (ie xenos)

	var/on = 1				// determines if the turret is on
	var/tmp/datum/effect/effect/system/spark_spread/spark_system // the spark system, used for generating... sparks?

	New()
		..()
		spark_system = new /datum/effect/effect/system/spark_spread
		spark_system.set_up(5, 0, src)
		spark_system.attach(src)
		update_icon()
		myprocess()

	Del()
		..()


/obj/machinery/porta_turret2/attack_ai(mob/user as mob)
	return

/obj/machinery/porta_turret2/attack_hand(mob/user as mob)
	. = ..()
	if (.)
		return
	var/dat

	dat += text({"
<TT><B>Automatic Portable Turret Installation</B></TT><BR><BR>
Status: []<BR>
Behaviour controls are [src.locked ? "locked" : "unlocked"]"},

"<A href='?src=\ref[src];power=1'>[src.on ? "On" : "Off"]</A>" )

	if(!src.locked)
		dat += text({"<BR>
Turn mode: [] [] [][]<BR>
NoShoot UnArmed: []<BR>
Shoot Syndie: []<BR>
Shoot Non-Syndie: []<BR>
Shoot Unknown: []<BR>"},

(src.turn_mode==0?"Full-Circle":"<A href='?src=\ref[src];operation=turn_mode;m=0'>Full-Circle</A>"),
(src.turn_mode==1?"Fixed":"<A href='?src=\ref[src];operation=turn_mode;m=1'>Fixed</A>"),
(src.turn_mode==2?"Arc":"<A href='?src=\ref[src];operation=turn_mode;m=2'>Arc</A>"),
(src.turn_mode==0?"":" <A href='?src=\ref[src];operation=turn;d=1'>Left</A> <A href='?src=\ref[src];operation=turn;d=-1'>Right</A>"),
"<A href='?src=\ref[src];operation=noshoot_unarmed'>[src.noshoot_unarmed ? "Yes" : "No"]</A>",
"<A href='?src=\ref[src];operation=shoot_syndie'>[src.shoot_syndie ? "Yes" : "No"]</A>",
"<A href='?src=\ref[src];operation=shoot_nonsyndie'>[src.shoot_nonsyndie ? "Yes" : "No"]</A>",
"<A href='?src=\ref[src];operation=shoot_aliens'>[src.shoot_aliens ? "Yes" : "No"]</A>" )

	user << browse("<HEAD><TITLE>Automatic Portable Turret Installation</TITLE></HEAD>[dat]", "window=autosec")
	onclose(user, "autosec")
	return

/obj/machinery/porta_turret2/Topic(href, href_list)
	if (..())
		return
	usr.machine = src
	src.add_fingerprint(usr)
	if ((href_list["power"]) && (src.allowed(usr)))
		if(anchored) // you can't turn a turret on/off if it's not anchored/secured
			on = !on // toggle on/off
		else
			usr << "\red It has to be secured first!"

		updateUsrDialog()
		update_icon()
		return

	switch(href_list["operation"])
		// toggles customizable behavioural protocols

		if ("noshoot_unarmed")
			src.noshoot_unarmed = !src.noshoot_unarmed
			target = null
		if ("shoot_syndie")
			src.shoot_syndie = !src.shoot_syndie
			target = null
		if ("shoot_nonsyndie")
			src.shoot_nonsyndie = !src.shoot_nonsyndie
			target = null
		if ("noshoot_unarmed")
			src.shoot_aliens = !src.shoot_aliens
			target = null

		if ("turn_mode")
			switch(href_list["m"])
				if("0")
					src.turn_mode = 0
				if("1")
					src.turn_mode = 1
				if("2")
					src.turn_mode = 2
		if("turn")
			switch(href_list["d"])
				if("-1")
					src.turn_fix_dir = turn(src.turn_fix_dir,-45)
				if("1")
					src.turn_fix_dir = turn(src.turn_fix_dir,45)
	updateUsrDialog()


/obj/machinery/porta_turret2/power_change()
	if(!anchored) return
	if(!(stat & BROKEN))
		if( powered() )
			stat &= ~NOPOWER
		else
			stat |= NOPOWER
	update_icon()

/obj/machinery/porta_turret2/update_icon()
	if(!anchored)
		icon_state = "Nailyoff"
		return
	if(stat & BROKEN)
		icon_state = "Nailydestroyed"
	else if(on && !(stat&NOPOWER))
		if(src.icon_state == "Nailyoff")
			flick("Nailymoveout",src)
			src.icon_state = "Nailyon"
	else
		if(src.icon_state != "Nailyoff")
			flick("Nailymovein",src)
			src.icon_state = "Nailyoff"



/obj/machinery/porta_turret2/attackby(obj/item/W as obj, mob/user as mob)
	if(stat & BROKEN)
		if(istype(W, /obj/item/weapon/crowbar))

			// If the turret is destroyed, you can remove it with a crowbar to
			// try and salvage its components
			user << "You begin prying the metal coverings off."
			sleep(20)
			if(prob(70))
				user << "You remove the turret and salvage some components."
				if(prob(50)) new /obj/item/stack/sheet/metal( loc, rand(1,4))
				if(prob(50)) new /obj/item/device/assembly/prox_sensor(locate(x,y,z))
			else
				user << "You remove the turret but did not manage to salvage anything."
			del(src)

	else if (istype(W, /obj/item/weapon/card/id)||istype(W, /obj/item/device/pda))
		// Behavior lock/unlock mangement
		if (allowed(user))
			locked = !src.locked
			user << "Controls are now [locked ? "locked." : "unlocked."]"
		else
			user << "\red Access denied."

	else
		// if the turret was attacked with the intention of harming it:
		src.health -= W.force * 0.5
		if (src.health <= 0)
			src.die()
		..()


/obj/machinery/porta_turret2/bullet_act(var/obj/item/projectile/Proj)
	src.health -= Proj.damage
	..()
	if(prob(45) && Proj.damage > 0) src.spark_system.start()
	if (src.health <= 0)
		src.die() // the death process :(
	return

/obj/machinery/porta_turret2/emp_act(severity)
	if(on)
		src.noshoot_unarmed = pick(0,0,1)
		src.shoot_syndie = pick(0,0,1)
		src.shoot_nonsyndie = pick(0,1,1)
		src.shoot_aliens = pick(0,0,1)
		src.turn_mode = pick(0,0,0,0,1,2,2)
		src.turn_fix_dir = pick(1,5,4,6,2,10,8,9)

	..()

/obj/machinery/porta_turret2/ex_act(severity)
	if(severity == 1) // turret dies if an explosion touches it!
		del(src)
	else if(severity == 2 && prob(50))
		src.die()

/obj/machinery/porta_turret2/proc/die() // called when the turret dies, ie, health <= 0
	src.health = 0
	src.stat |= BROKEN // enables the BROKEN bit
	update_icon()
	invisibility=0
	src.spark_system.start() // creates some sparks because they look cool
	src.density=1

/obj/machinery/porta_turret2/proc/myprocess()
	set background = 1

	spawn(10) myprocess()

	if(stat & (NOPOWER|BROKEN))
		return

	if(!on)
		use_power = 0
		return

	use_power(100)

	var/list/myview = view()

	if(target && !(target in myview))
		target=null
	if(target && target.stat)
		target=null
		if(prob(50))
			src.loc.visible_message("<span class='game say'><span class='name'>[src]</span> <span class='message'>queries, Are you there?</span></span>")

	if(!target)
		var/list/targets = list()

		if(src.shoot_aliens)
			for(var/mob/living/simple_animal/C in view(7,src))
				if(get_dir(src,C)!=dir) continue
				if(!C.stat)
					targets += C

		for (var/mob/living/carbon/C in view(7,src))
			if (C.stat)
				continue
			if(get_dir(src,C)!=dir) continue
			if(istype(C, /mob/living/carbon/human))
				var/mob/living/carbon/human/H = C
				if(noshoot_unarmed)
					var/armed = 0
					if(istype(H.l_hand, /obj/item/weapon/gun) || istype(H.l_hand, /obj/item/weapon/melee))
						armed = 1
					if(istype(H.r_hand, /obj/item/weapon/gun) || istype(H.r_hand, /obj/item/weapon/melee))
						armed = 1
					if(istype(H.belt, /obj/item/weapon/gun) || istype(H:belt, /obj/item/weapon/melee))
						armed = 1
					if(!armed) continue
				if(C.mind)
					if(C.mind.special_role && shoot_syndie || !C.mind.special_role && shoot_nonsyndie)
						targets += C
			else if(src.shoot_aliens && !istype(C,/mob/living/simple_animal))
				targets += C

		if (targets.len>0)
			target = pick(targets)
			if(target && prob(50))
				src.loc.visible_message("<span class='game say'><span class='name'>[src]</span> <span class='message'>states, Hello!</span></span>")

	if(!target)
		use_power = 1
		switch(turn_mode)
			if(0)
				dir=turn(dir,45)
			if(1)
				dir=turn_fix_dir
			if(2)
				if(dir==turn_fix_dir)
					dir = turn(turn_fix_dir,turn_dir?45:-45)
					turn_dir=!turn_dir
				else
					dir = turn_fix_dir
	else
		use_power = 2
		var/tdir = get_dir(src,target)
		if(tdir!=dir)
			if(turn_mode==0)
				dir=tdir
			else
				var/d1 = turn(dir,45)
				var/d2 = turn(dir,45)
				if(tdir==d1)
					dir=d1
				else if(tdir==d2)
					dir=d2
				else
					target=null
					if(prob(50))
						src.loc.visible_message("<span class='game say'><span class='name'>[src]</span> <span class='message'>queries, Where are you?</span></span>")
					return
		shootAt(target)

/obj/machinery/porta_turret2/proc/canHit(var/atom/movable/target)
	var/turf/T = get_turf(target)
	var/obj/item/weapon/dummy/D = new(src)
	D.pass_flags = PASSTABLE | PASSGLASS | PASSGRILLE | PASSBLOB
	D.flags = TABLEPASS
	D.density = 1
	var/step = 0

	while(step<10)
		var/turf/C = get_turf(D)
		if(C==T)
			del(D)
			return 1
		if(!step_towards(D,target))
			del(D)
			return 0

	del(D)
	return 0

/obj/machinery/porta_turret2/proc/shootAt(var/atom/movable/target) // shoots at a target
	if(last_fired) return // prevents rapid-fire shooting, unless it's been emagged
	if(checks_hitability && !canHit(target)) return
	last_fired = 1
	spawn()
		sleep(shot_delay)
		last_fired = 0

	var/turf/T = get_turf(src)
	var/turf/U = get_turf(target)
	if (!istype(T) || !istype(U))
		return

	flick("Nailyfire",src)
	playsound(src.loc, 'sound/weapons/Gunshot.ogg', 50, 1)
	var/obj/item/projectile/A = new/obj/item/projectile/bullet/midbullet2( loc )
	A.original = target.loc
	A.current = T
	A.yo = U.y - T.y
	A.xo = U.x - T.x
	spawn(1)
		A.process()
	return
