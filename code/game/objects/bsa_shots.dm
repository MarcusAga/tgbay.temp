//

/obj/bsa_projectile
	name = "BSA projectile"
	icon = 'icons/effects/BSA.dmi'
	icon_state = "bsashoot"
	var/turf/target
	var/unprecision = 0
	var/trail = 0
	density = 0
	pixel_x = -64
	pixel_y = -64

	New()
		..()
		var/turf/T = loc
		target = T
		var/dx = rand(-16,16)
		var/dy = rand(-16,16)
		if(abs(dx)<abs(dy))
			dx/=abs(dy)
			dy/=abs(dy)
		else
			dy/=abs(dx)
			dx/=abs(dx)
		var/x = (dx+1)*world.maxx/2
		var/y = (dy+1)*world.maxy/2
		x = max(min(x,world.maxx),1)
		y = max(min(y,world.maxy),1)
		T = locate(x,y,T.z)
		if(T)
			src.loc = T

		spawn(0) process()

	process()
		if(!target || loc==target)
			explode()
		else
			var/turf/T = loc
			if(T.z!=target.z)
				loc = locate(T.x,T.y,target.z)
			if(unprecision || trail)
				var/dist = get_dist(src,target)
				if(trail && trail>=dist)
					trail()
				if(unprecision && prob(100*(1-dist/unprecision)))
					explode()
					return
			if(!step_towards(src,target))
				loc = get_step_towards(src,target)
			spawn(2)
				process()

	proc/explode()
		icon_state = "bsaexplosion"
		spawn(20) del(src)
	proc/trail()
		var/obj/effect/E = new
		E.icon = 'icons/effects/BSA.dmi'
		E.icon_state = "lesserbsaexplosion"
		E.name=""
		E.desc=""
		E.anchored = 1
		spawn(15) del(E)

	napalm
		name = "Napalm projectile"
		icon_state = "bsashoottox"
		explode()
			..()
			var/turf/target_tile = loc

			var/datum/gas_mixture/napalm = new

			napalm.toxins = 500
			napalm.oxygen = 500
			napalm.temperature = 400+T0C
			napalm.update_values()

			target_tile.assume_air(napalm)
			spawn (0) target_tile.hotspot_expose(700, 400)
		unprecise
			unprecision=8
		veryunprecise
			unprecision=16

	explosion
		explode()
			..()
			explosion(loc,4,8,16,20)
		unprecise
			unprecision=8
		veryunprecise
			unprecision=16
	subcaliber
		trail=7
		trail()
			..()
			explosion(loc,-1,1,3,6)
		explode()
			..()
			explosion(loc,0,4,12,8)
		unprecise
			unprecision=8
		veryunprecise
			unprecision=16
		long
			trail=24
	point
		trail=7
		trail()
			..()
			explosion(loc,-1,0,2,4)
		explode()
			..()
			explosion(loc,-1,0,4,8)
		unprecise
			unprecision=8
		veryunprecise
			unprecision=16
		long
			trail=24
