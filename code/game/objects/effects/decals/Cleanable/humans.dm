//
/obj/effect/decal/cleanable/blood
	name = "blood"
	desc = "It's red and gooey. Perhaps it's the chef's cooking?"
	gender = PLURAL
	density = 0
	anchored = 1
	layer = 2
	icon = 'icons/effects/blood.dmi'
	icon_state = "floor1"
	random_icon_states = list("floor1", "floor2", "floor3", "floor4", "floor5", "floor6", "floor7")
	var/list/viruses = list()
	blood_DNA = list()
	slipChance = 2
	var/volume = 2

/obj/effect/decal/cleanable/blood/Del()
	for(var/datum/disease2/disease/D in viruses)
		D.cure(0)
	..()

/obj/effect/decal/cleanable/blood/New(var/loc, var/owner = null)
	..()
	if(istype(src, /obj/effect/decal/cleanable/blood/gibs))
		return
	if(src.type == /obj/effect/decal/cleanable/blood)
		if(src.loc && isturf(src.loc))
			for(var/obj/effect/decal/cleanable/blood/B in src.loc)
				if(B != src)
					volume += B.volume
					del(B)

/obj/effect/decal/cleanable/blood/proc/updateVolume()
	if(volume>29)
		var/direction = pick(cardinal)
		var/obj/item/weapon/dummy/test = new(loc)
		test.name = "liquid"
		if(step_towards(test, get_step_towards(test, direction)))
			volume = volume/2
			var/obj/effect/decal/cleanable/blood/B = locate() in test.loc
			if(!B)
				B = new(test.loc)
				B.volume = 0
			B.volume += volume
		spawn(5)
			updateVolume()
		del test

	slipChance = volume/2
	if(volume<12)
		icon = 'icons/effects/whiteoverlay.dmi'
		icon_state = "blank"
		var/list/states = list("1","2","3","4","5")
		for(var/image/I in overlays)
			states -= I.icon_state
		while((overlays.len)*3<volume && states.len>0)
			var/s = pick(states)
			overlays += image(icon='icons/effects/drip.dmi', icon_state=s)
			states -= s
		track_amt = 0
	else
		track_amt = initial(track_amt)
		icon = initial(icon)
		if(!icon_state || !(icon_state in random_icon_states))
			icon_state = pick(random_icon_states)

/obj/effect/decal/cleanable/blood/splatter
	slipChance = 20
	random_icon_states = list("gibbl1", "gibbl2", "gibbl3", "gibbl4", "gibbl5")

/obj/effect/decal/cleanable/blood/tracks
	icon_state = "tracks"
	desc = "They look like tracks left by wheels."
	gender = PLURAL
	random_icon_states = null
	slipChance = 15

/obj/effect/decal/cleanable/blood/gibs
	name = "gibs"
	desc = "They look bloody and gruesome."
	gender = PLURAL
	density = 0
	anchored = 1
	layer = 2
	icon = 'icons/effects/blood.dmi'
	icon_state = "gibbl5"
	random_icon_states = list("gib1", "gib2", "gib3", "gib4", "gib5", "gib6")
	slipChance = 35
	flags = PASSTABLE | OPENCONTAINER

	proc/initGib(var/owner)
		reagents = new /datum/reagents(5)
		var/datum/reagent/blood = new /datum/reagent/blood()
		blood.holder = src
		blood.volume = 5
		if(iscarbon(owner))
			if(owner:dna)
				var/datum/dna/dna = owner:dna
				blood.data = list("donor"=owner,"viruses"=null,"dna"=dna.toList(),"blood_DNA"=dna.unique_enzymes,"blood_type"=dna.b_type,"resistances"=null,"trace_chem"=null)
			else
				blood.data = list("donor"=owner,"viruses"=null,"dna"=null,"blood_DNA"=null,"blood_type"=null,"resistances"=null,"trace_chem"=null)
		else
			blood.data = list()
		reagents.reagent_list += blood
		reagents.update_total()

/obj/effect/decal/cleanable/blood/gibs/up
	random_icon_states = list("gib1", "gib2", "gib3", "gib4", "gib5", "gib6","gibup1","gibup1","gibup1")

/obj/effect/decal/cleanable/blood/gibs/down
	random_icon_states = list("gib1", "gib2", "gib3", "gib4", "gib5", "gib6","gibdown1","gibdown1","gibdown1")

/obj/effect/decal/cleanable/blood/gibs/body
	random_icon_states = list("gibhead", "gibtorso")

/obj/effect/decal/cleanable/blood/gibs/limb
	random_icon_states = list("gibleg", "gibarm")

/obj/effect/decal/cleanable/blood/gibs/core
	random_icon_states = list("gibmid1", "gibmid2", "gibmid3")


/obj/effect/decal/cleanable/blood/gibs/proc/streak(var/list/directions)
	spawn (0)
		var/direction = pick(directions)
		for (var/i = 0, i < pick(1, 200; 2, 150; 3, 50; 4), i++)
			sleep(3)
			if (i > 0)
				var/obj/effect/decal/cleanable/blood/b = new /obj/effect/decal/cleanable/blood/splatter(src.loc)
				for(var/datum/disease2/disease/D in src.viruses)
					b.viruses += D.getcopy()
			if (step_to(src, get_step(src, direction), 0))
				break


/obj/effect/decal/cleanable/mucus
	name = "mucus"
	desc = "Disgusting mucus."
	gender = PLURAL
	density = 0
	anchored = 1
	layer = 2
	icon = 'blood.dmi'
	icon_state = "mucus"
	random_icon_states = list("mucus")
	slipChance = 25
