//generic procs copied from obj/effect/alien
/obj/effect/spider
	name = "web"
	desc = "it's stringy and sticky"
	icon = 'icons/effects/effects.dmi'
	anchored = 1
	density = 0
	var/health = 15

//similar to weeds, but only barfed out by nurses manually
/obj/effect/spider/ex_act(severity)
	switch(severity)
		if(1.0)
			del(src)
		if(2.0)
			if (prob(50))
				del(src)
		if(3.0)
			if (prob(5))
				del(src)
	return

/obj/effect/spider/attackby(var/obj/item/weapon/W, var/mob/user)
	if(W.attack_verb.len)
		visible_message("\red <B>\The [src] have been [pick(W.attack_verb)] with \the [W][(user ? " by %knownface:1%." : ".")]", actors=list(user))
	else
		visible_message("\red <B>\The [src] have been attacked with \the [W][(user ? " by %knownface:1%." : ".")]", actors=list(user))

	var/damage = W.force / 4.0

	if(istype(W, /obj/item/weapon/weldingtool))
		var/obj/item/weapon/weldingtool/WT = W

		if(WT.remove_fuel(0, user))
			damage = 15
			playsound(loc, 'sound/items/Welder.ogg', 100, 1)

	health -= damage
	healthcheck()

/obj/effect/spider/bullet_act(var/obj/item/projectile/Proj)
	..()
	health -= Proj.damage
	healthcheck()

/obj/effect/spider/proc/healthcheck()
	if(health <= 0)
		del(src)

/obj/effect/spider/temperature_expose(datum/gas_mixture/air, exposed_temperature, exposed_volume)
	if(exposed_temperature > 300)
		health -= 5
		healthcheck()

/obj/effect/spider/stickyweb
	icon_state = "stickyweb1"
	New()
		if(prob(50))
			icon_state = "stickyweb2"

/obj/effect/spider/stickyweb/CanPass(atom/movable/mover, turf/target, height=0, air_group=0)
	if(air_group || (height==0)) return 1
	if(istype(mover, /mob/living/simple_animal/hostile/giant_spider))
		return 1
	else if(istype(mover, /mob/living))
		if(prob(50))
			mover << "\red You get stuck in \the [src] for a moment."
			return 0
	else if(istype(mover, /obj/item/projectile))
		return prob(30)
	return 1

/obj/effect/spider/eggcluster
	name = "egg cluster"
	desc = "They seem to pulse slightly with an inner life"
	icon_state = "eggs"
	var/amount_grown = 0
	New()
		pixel_x = rand(6,-6)
		pixel_y = rand(6,-6)
		processing_objects.Add(src)

/obj/effect/spider/eggcluster/process()
	amount_grown += rand(0,2)
	if(amount_grown >= 100)
		var/num = rand(2,8)
		for(var/i=0, i<num, i++)
			new /obj/effect/spider/spiderling(src.loc)
		del(src)

/obj/effect/spider/spiderling
	name = "spiderling"
	desc = "It never stays still for long."
	icon_state = "spiderling"
	anchored = 0
	layer = 2.7
	health = 3
	var/amount_grown = 0
	var/obj/machinery/atmospherics/unary/vent_pump/entry_vent
	var/travelling_in_vent = 0
	New()
		pixel_x = rand(12,-12)
		pixel_y = rand(12,-12)
		processing_objects.Add(src)

/obj/effect/spider/spiderling/attack_hand(mob/user as mob)
	if(ishuman(user))
		src << "Smashes [src]"
		die()

/obj/effect/spider/spiderling/Bump(atom/user)
	if(istype(user, /obj/structure/table))
		src.loc = user.loc
	else
		..()

/obj/effect/spider/spiderling/proc/die()
	visible_message("<span class='alert'>[src] dies!</span>")
	icon_state = "greenshatter"

/obj/effect/spider/spiderling/healthcheck()
	if(health <= 0)
		die()

/obj/effect/spider/spiderling/process()
	if(travelling_in_vent)
		if(istype(src.loc, /turf))
			travelling_in_vent = 0
			entry_vent = null
	else if(entry_vent)
		if(get_dist(src, entry_vent) <= 1)
			if(entry_vent.network && entry_vent.network.normal_members.len)
				var/list/vents = list()
				for(var/obj/machinery/atmospherics/unary/vent_pump/temp_vent in entry_vent.network.normal_members)
					vents.Add(temp_vent)
				if(!vents.len)
					entry_vent = null
					return
				var/obj/machinery/atmospherics/unary/vent_pump/exit_vent = pick(vents)
				if(prob(50))
					src.visible_message("<B>[src] scrambles into the ventillation ducts!</B>")

				spawn(rand(20,60))
					loc = exit_vent
					var/travel_time = round(get_dist(loc, exit_vent.loc) / 2)
					spawn(travel_time)

						if(!exit_vent || exit_vent.welded)
							loc = entry_vent
							entry_vent = null
							return

						if(prob(50))
							src.visible_message("\blue You hear something squeezing through the ventilation ducts.",2)
						sleep(travel_time)

						if(!exit_vent || exit_vent.welded)
							loc = entry_vent
							entry_vent = null
							return
						loc = exit_vent.loc
						entry_vent = null
						var/area/new_area = get_area(loc)
						if(new_area)
							new_area.Entered(src)
			else
				entry_vent = null
	//=================

	else if(prob(33))
		var/list/nearby = oview(10, src)
		if(nearby.len)
			var/target_atom = pick(nearby)
			walk_to(src, target_atom, 5)
			if(prob(40))
				src.visible_message("\blue \the [src] skitters[pick(" away"," around","")].")
	else if(prob(10))
		//ventcrawl!
		for(var/obj/machinery/atmospherics/unary/vent_pump/v in view(7,src))
			if(!v.welded)
				entry_vent = v
				walk_to(src, entry_vent, 5)
				break
	if(isturf(loc))
		amount_grown += rand(0,2)
		if(amount_grown >= 100)
			var/spawn_type = pick(typesof(/mob/living/simple_animal/hostile/giant_spider))
			new spawn_type(src.loc)
			del(src)

/obj/effect/spider/cocoon
	name = "cocoon"
	desc = "Something wrapped in silky spider web"
	icon_state = "cocoon1"
	health = 60
	density = 1

	New()
		spawn(1)
			var/icon/ico = new('icons/effects/spider_cocoon.dmi',icon_state="void")
			if(!contents.len)
				icon_state = pick("cocoon1","cocoon2","cocoon3")
				return
			for(var/atom/movable/AM in contents)
				var/icon/ico2 = build_composite_icon(AM)
//				var/icon/ico2 = build_composite_icon_omnidir(AM)
				ico.Blend(ico2,ICON_OVERLAY)
			var/icon/overl = new('icons/effects/spider_cocoon.dmi',icon_state="cocoon")
//			overl.MapColors(1,0,0, 0,1,0, 0,0,1, 0.1,0.1,0.1)
//			ico.GrayScale()
			ico.MapColors(1,0,0, 0,1,0, 0,0,1, 1,1,1)
			ico.Blend(overl,ICON_MULTIPLY)
			var/icon/test = new (ico)
			for(var/x = 1, x <= ico.Width(), x++)
				var/last=-1
				var/cur=-1
				for(var/y = 1, y <= ico.Height(), y++)
					cur = (test.GetPixel(x,y)==null?0:1)
					if(last!=-1 && last!=cur && cur==0)
						ico.DrawBox("#434343",x,y)
					else if(last!=-1 && last!=cur && cur==1)
						ico.DrawBox("#434343",x,y-1)
					last = cur
			for(var/y = 1, y <= ico.Height(), y++)
				var/last=-1
				var/cur=-1
				for(var/x = 1, x <= ico.Width(), x++)
					cur = (test.GetPixel(x,y)==null?0:1)
					if(last!=-1 && last!=cur && cur==0)
						ico.DrawBox("#434343",x,y)
					else if(last!=-1 && last!=cur && cur==1)
						ico.DrawBox("#434343",x-1,y)
					last = cur
			icon = ico
			icon_state = ""

/obj/effect/spider/cocoon/CanPass(atom/movable/mover, turf/target, height=1.5, air_group = 0)
	if(mover && istype(mover,/mob/living/simple_animal/hostile/giant_spider)) return 1
	return (!density || !height || air_group)

/obj/effect/spider/cocoon/Del()
	src.visible_message("\red \the [src] splits open.")
	for(var/atom/movable/A in contents)
		A.loc = src.loc
	..()
