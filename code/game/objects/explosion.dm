//TODO: Flash range does nothing currently

//A very crude linear approximatiaon of pythagoras theorem.
/proc/cheap_pythag(var/dx, var/dy)
	dx = abs(dx); dy = abs(dy);
	if(dx>=dy)	return dx + (0.5*dy)	//The longest side add half the shortest side approximates the hypotenuse
	else		return dy + (0.5*dx)

proc/explosion(turf/epicenter, devastation_range, heavy_impact_range, light_impact_range, flash_range, adminlog = 1)
	if(config.recursive_explosions)
		explosion_rec(epicenter, light_impact_range*4*config.recursive_explosions)

	src = null	//so we don't abort once src is deleted
	spawn(0)
		var/start = world.timeofday
		epicenter = get_turf(epicenter)
		if(!epicenter) return


		playsound(epicenter, 'sound/effects/explosionfar.ogg', 100, 1, round(devastation_range*2,1) )
		playsound(epicenter, "explosion", 100, 1, round(devastation_range,1) )

		var/close = range(world.view+round(devastation_range,1), epicenter)
		// to all distanced mobs play a different sound
		for(var/mob/M in world) if(M.z == epicenter.z) if(!(M in close))
			// check if the mob can hear
			if(M.ear_deaf <= 0 || !M.ear_deaf) if(!istype(M.loc,/turf/space))
				M << 'explosionfar.ogg'
		if(adminlog)
			message_admins("Explosion with size ([devastation_range], [heavy_impact_range], [light_impact_range]) in area [epicenter.loc.name] ([epicenter.x],[epicenter.y],[epicenter.z])")
			log_game("Explosion with size ([devastation_range], [heavy_impact_range], [light_impact_range]) in area [epicenter.loc.name] ")

		var/lighting_controller_was_processing = lighting_controller.processing	//Pause the lighting updates for a bit
		lighting_controller.processing = 0
		var/powernet_rebuild_was_deferred_already = defer_powernet_rebuild
		if(defer_powernet_rebuild != 2)
			defer_powernet_rebuild = 1

		if(heavy_impact_range > 1)
			var/datum/effect/system/explosion/E = new/datum/effect/system/explosion()
			E.set_up(epicenter)
			E.start()

		var/x0 = epicenter.x
		var/y0 = epicenter.y
		var/z0 = epicenter.z

		var/radius = max(devastation_range, heavy_impact_range, light_impact_range)

		var/dist = 0
		if(0 < devastation_range)	dist = 1
		else if(0 < heavy_impact_range)	dist = 2
		else if(0 < light_impact_range)	dist = 3
		if(dist)
			for(var/atom_movable in epicenter.contents)	//bypass type checking since only atom/movable can be contained by turfs anyway
				var/atom/movable/AM = atom_movable
				if(AM)	AM.ex_act(dist)
			epicenter.ex_act(dist)
			if(!epicenter)
				epicenter = locate(x0,y0,z0)
		var/list/turfs = list(epicenter)
		var/list/turfs_edge = list(epicenter)
		var/list/mobs_catched_in_blast = list()
		for(var/idx=0, idx<radius, idx++)
			var/list/new_edge = list()
			for(var/turf/T1 in turfs_edge)
				for(var/dir in alldirs)
					var/turf/TT = get_step_cached(T1,dir)
					if(!TT || (TT in turfs)) continue

					dist = cheap_pythag(TT.x - x0,TT.y - y0)
					if(dist < devastation_range)		dist = 1
					else if(dist < heavy_impact_range)	dist = 2
					else if(dist < light_impact_range)	dist = 3
					else dist=0
					if(dist)
						for(var/atom/movable/atom_movable in TT.contents)	//bypass type checking since only atom/movable can be contained by turfs anyway
							var/atom/movable/AM = atom_movable
							if(AM)
								AM.ex_act(dist)
								if(ismob(AM))
									mobs_catched_in_blast += AM
						TT.ex_act(dist)
						if(!TT)
							TT = get_step_cached(T1,dir)
					new_edge += TT
					turfs += TT
			turfs_edge = new_edge
			sleep(idx%2)

		for(var/client/C)
			if(isliving(C.mob) && !(C.mob in mobs_catched_in_blast))
				var/turf/T = get_turf(C.mob)
				if(T.z != z0)
					continue
				dist = cheap_pythag(T.x - x0,T.y - y0)
				var/shaky = 80*light_impact_range*light_impact_range/(1+dist)
				if(shaky>320) shaky = 320
				spawn(0)
					shake_camera_pixel(C.mob, 5, shaky) // unbuckled, HOLY SHIT SHAKE THE ROOM

/*		for(var/turf/T in range(epicenter, max(devastation_range, heavy_impact_range, light_impact_range)))
			var/dist = cheap_pythag(T.x - x0,T.y - y0)

			if(dist < devastation_range)		dist = 1
			else if(dist < heavy_impact_range)	dist = 2
			else if(dist < light_impact_range)	dist = 3
			else continue

			spawn(0)
				if(T)
					for(var/atom_movable in T.contents)	//bypass type checking since only atom/movable can be contained by turfs anyway
						var/atom/movable/AM = atom_movable
						if(AM)	AM.ex_act(dist)
					T.ex_act(dist)*/

		var/took = (world.timeofday-start)/10
		//You need to press the DebugGame verb to see these now....they were getting annoying and we've collected a fair bit of data. Just -test- changes  to explosion code using this please so we can compare
		if(Debug2)	world.log << "## DEBUG: Explosion([x0],[y0],[z0])(d[devastation_range],h[heavy_impact_range],l[light_impact_range]): Took [took] seconds."

		//Machines which report explosions.
		for(var/i,i<=doppler_arrays.len,i++)
			var/obj/machinery/doppler_array/Array = doppler_arrays[i]
			if(Array)
				Array.sense_explosion(x0,y0,z0,devastation_range,heavy_impact_range,light_impact_range,took)

		sleep(8)

		if(!lighting_controller.processing)	lighting_controller.processing = lighting_controller_was_processing
		if(!powernet_rebuild_was_deferred_already)
			if(defer_powernet_rebuild != 2)
				defer_powernet_rebuild = 0

	return 1



proc/secondaryexplosion(turf/epicenter, range)
	for(var/turf/tile in range(range, epicenter))
		tile.ex_act(2)
