//
/obj/item/device/taperecorder
	desc = "A device that can record up to an hour of dialogue and play it back."
	name = "universal recorder"
	icon_state = "taperecorderidle"
	item_state = "analyzer"
	w_class = 1.0
	m_amt = 60
	g_amt = 30
	var/emagged = 0.0
	var/recording = 0.0
	var/playing = 0.0
	var/timerecorded = 0.0
	var/playsleepseconds = 0.0
	var/list/storedinfo = new/list()
	var/list/timestamp = new/list()
	var/canprint = 1
	var/universal_decoder = 0
	flags = FPRINT | TABLEPASS| CONDUCT
	throwforce = 2
	throw_speed = 4
	throw_range = 20

/obj/item/device/taperecorder/hear_talk(mob/living/M as mob, var/list/msg)
	if(recording)
		timestamp += timerecorded
		msg["time"] = time2text(timerecorded*10,"mm:ss")
		storedinfo += list(msg)
		return

/obj/item/device/taperecorder/attackby(obj/item/weapon/W as obj, mob/user as mob)
	..()
	if(istype(W, /obj/item/weapon/card/emag))
		if(emagged == 0)
			emagged = 1
			recording = 0
			user << "<span class='warning'>PZZTTPFFFT</span>"
			icon_state = "taperecorderidle"
		else
			user << "<span class='warning'>It is already emagged!</span>"

/obj/item/device/taperecorder/proc/explode()
	var/turf/T = get_turf(loc)
	if(ismob(loc))
		var/mob/M = loc
		M << "<span class='danger'>\The [src] explodes!</span>"
	if(T)
		T.hotspot_expose(700,125)
		explosion(T, -1, -1, 0, 4)
	del(src)
	return

/obj/item/device/taperecorder/verb/record()
	set name = "Start Recording"
	set category = "Object"

	if(usr.stat)
		return
	if(emagged == 1)
		usr << "\red The tape recorder makes a scratchy noise."
		return
	icon_state = "taperecorderrecording"
	if(timerecorded < 3600 && playing == 0)
		usr << "<span class='notice'>Recording started.</span>"
		recording = 1
		timestamp+= timerecorded
		storedinfo += "\[[time2text(timerecorded*10,"mm:ss")]\] Recording started."
		for(timerecorded, timerecorded<3600)
			if(recording == 0)
				break
			timerecorded++
			sleep(10)
		recording = 0
		icon_state = "taperecorderidle"
		return
	else
		usr << "<span class='notice'>Either your tape recorder's memory is full, or it is currently playing back its memory.</span>"


/obj/item/device/taperecorder/verb/stop()
	set name = "Stop"
	set category = "Object"

	if(usr.stat)
		return
	if(emagged == 1)
		usr << "\red The tape recorder makes a scratchy noise."
		return
	if(recording == 1)
		recording = 0
		timestamp+= timerecorded
		storedinfo += "\[[time2text(timerecorded*10,"mm:ss")]\] Recording stopped."
		usr << "<span class='notice'>Recording stopped.</span>"
		icon_state = "taperecorderidle"
		return
	else if(playing == 1)
		playing = 0
		var/turf/T = get_turf(src)
		T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: Playback stopped.</font>")
		icon_state = "taperecorderidle"
		return


/obj/item/device/taperecorder/verb/clear_memory()
	set name = "Clear Memory"
	set category = "Object"

	if(usr.stat)
		return
	if(emagged == 1)
		usr << "<span class='warning'>The tape recorder makes a scratchy noise.</span>"
		return
	if(recording == 1 || playing == 1)
		usr << "<span class='notice'>You can't clear the memory while playing or recording!</span>"
		return
	else
		if(storedinfo)	storedinfo.Cut()
		if(timestamp)	timestamp.Cut()
		timerecorded = 0
		usr << "<span class='notice'>Memory cleared.</span>"
		return


/obj/item/device/taperecorder/verb/playback_memory()
	set name = "Playback Memory"
	set category = "Object"
	playback_memory_proc(0)

/obj/item/device/taperecorder/proc/playback_memory_proc(var/silent)
	var/verbose = usr && !silent
	if(usr && usr.stat)
		return
	if(emagged == 1)
		if(usr) usr << "\red The tape recorder makes a scratchy noise."
		return
	if(recording == 1)
		if(usr) usr << "<span class='notice'>You can't playback when recording!</span>"
		return
	if(playing == 1)
		if(usr) usr << "<span class='notice'>You're already playing!</span>"
		return
	playing = 1
	icon_state = "taperecorderplaying"
	if(iswrapper(loc)) loc:update_icon()
	if(verbose) usr << "<span class='notice'>Playing started.</span>"
	var/mob/living/fakemob
	for(var/i=1,timerecorded<3600,sleep(10 * (playsleepseconds) ))
		if(playing == 0)
			break
		if(storedinfo.len < i)
			break
		var/turf/T = get_turf(src)
		if(istext(storedinfo[i]))
			if(verbose)
				var/msg = "<font color=Maroon><B>Tape Recorder</B>: [storedinfo[i]]</font>"
				T.hearable_message(msg)
		else
			var/list/record = storedinfo[i]
			for(var/mob/M in hear(6,T))
				var/voice = record["voice"]
				if(record["uni_voice"] && (ishuman(M) || ismonkey(M)) && M.mind && M.mind.known_voices[record["uni_voice"]])
					var/uni = record["uni_voice"]
					voice = "<a href='?src=\ref[M:mind];voice=[uni]'>[sanitize_simple(M:mind.known_voices[uni])]</a>"
				var/msg
				if(verbose)
					if(universal_decoder || M.say_understands(record["lang"]))
						msg = "[voice] [record["msg"]]"
					else
						msg = "[voice] [record["vmsg"]]"
					M.show_message("<font color=Maroon><B>Tape Recorder</B>: \[[record["time"]]\]: [msg]</font>", 2)
				else
					if(universal_decoder || M.say_understands(record["lang"]))
						msg = "<span class='game say'><span class='name'>[voice]</span> <span class='message'>[record["msg"]]</span></span>"
					else
						msg = "<span class='game say'><span class='name'>[voice]</span> <span class='message'>[record["vmsg"]]</span></span>"
					M.show_message(msg, 2)
			if(!fakemob)
				fakemob = new
			fakemob.name = record["voice"]
			var/list/radios = new()
			for(var/obj/item/device/radio/R in range(1,T))
				radios += R
			var/atom/A = loc
			while(A && !isarea(A) && !isturf(A))
				for(var/obj/item/device/radio/R in A)
					radios -= R
					radios += R
				A=A.loc
			for(var/obj/item/device/radio/R in radios)
				R.hear_talk(fakemob, record)
		if(storedinfo.len < i+1)
			playsleepseconds = 1
			sleep(10)
			if(verbose)
				T = get_turf(src)
				T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: End of recording.</font>")
		else
			playsleepseconds = timestamp[i+1] - timestamp[i]
		if(playsleepseconds > 14)
			sleep(10)
			if(verbose)
				T = get_turf(src)
				T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: Skipping [playsleepseconds] seconds of silence</font>")
			playsleepseconds = 1
		i++
	if(fakemob) del(fakemob)
	icon_state = "taperecorderidle"
	if(iswrapper(loc)) loc:update_icon()
	playing = 0
	if(emagged == 1.0)
		var/turf/T = get_turf(src)
		T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: This tape recorder will self-destruct in... Five.</font>")
		sleep(10)
		T = get_turf(src)
		T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: Four.</font>")
		sleep(10)
		T = get_turf(src)
		T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: Three.</font>")
		sleep(10)
		T = get_turf(src)
		T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: Two.</font>")
		sleep(10)
		T = get_turf(src)
		T.hearable_message("<font color=Maroon><B>Tape Recorder</B>: One.</font>")
		sleep(10)
		explode()


/obj/item/device/taperecorder/verb/print_transcript()
	set name = "Print Transcript"
	set category = "Object"

	if(usr.stat)
		return
	if(emagged == 1)
		usr << "\red The tape recorder makes a scratchy noise."
		return
	if(!canprint)
		usr << "<span class='notice'>The recorder can't print that fast!</span>"
		return
	if(recording == 1 || playing == 1)
		usr << "<span class='notice'>You can't print the transcript while playing or recording!</span>"
		return
	usr << "<span class='notice'>Transcript printed.</span>"
	var/obj/item/weapon/paper/P = new /obj/item/weapon/paper(get_turf(src))
	var/t1 = "<B>Transcript:</B><BR><BR>"
	for(var/i=1,storedinfo.len >= i,i++)
		if(istext(storedinfo[i]))
			t1 += "[storedinfo[i]]<BR>"
		else
			var/list/record = storedinfo[i]
			var/msg
			if(!record["lang"] || record["lang"]=="Human" || record["lang"]=="Universal" || universal_decoder)
				msg = record["msg"]
			else
				msg = record["vmsg"]
			msg = sanitize_simple(msg,list("&#255;"="&#1103;"))
			t1 += "[record["voice"]] [msg]<BR>"
	P.info = t1
	P.name = "paper- 'Transcript'"
	canprint = 0
	sleep(300)
	canprint = 1


/obj/item/device/taperecorder/attack_self(mob/user)
	if(recording == 0 && playing == 0)
		if(usr.stat)
			return
		if(emagged == 1)
			usr << "\red The tape recorder makes a scratchy noise."
			return
		icon_state = "taperecorderrecording"
		if(timerecorded < 3600 && playing == 0)
			usr << "\blue Recording started."
			recording = 1
			timestamp+= timerecorded
			storedinfo += "\[[time2text(timerecorded*10,"mm:ss")]\] Recording started."
			for(timerecorded, timerecorded<3600)
				if(recording == 0)
					break
				timerecorded++
				sleep(10)
			recording = 0
			icon_state = "taperecorderidle"
			return
		else
			usr << "\red Either your tape recorder's memory is full, or it is currently playing back its memory."
	else
		if(usr.stat)
			usr << "Not when you're incapacitated."
			return
		if(recording == 1)
			recording = 0
			timestamp+= timerecorded
			storedinfo += "\[[time2text(timerecorded*10,"mm:ss")]\] Recording stopped."
			usr << "\blue Recording stopped."
			icon_state = "taperecorderidle"
			return
		else if(playing == 1)
			playing = 0
			var/turf/T = get_turf(src)
			for(var/mob/O in hearers(world.view-1, T))
				O.show_message("<font color=Maroon><B>Tape Recorder</B>: Playback stopped.</font>",2)
			icon_state = "taperecorderidle"
			return
		else
			usr << "\red Stop what?"
			return
