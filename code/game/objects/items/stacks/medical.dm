
//What is this even used for?

/obj/item/stack/medical/attack(mob/living/carbon/M as mob, mob/user as mob)
//	if (M.stat == 2)
//		var/t_him = "it"
//		if (M.gender == MALE)
//			t_him = "him"
//		else if (M.gender == FEMALE)
//			t_him = "her"
//		user << "\red \The [M] is dead, you cannot help [t_him]!"
//		return 1

	if (!istype(M))
		user << "\red \The [src] cannot be applied to [M]!"
		return 1

	if ( ! (istype(user, /mob/living/carbon/human) || \
			istype(user, /mob/living/silicon) || \
			istype(user, /mob/living/carbon/monkey) && ticker && ticker.mode.name == "monkey") )
		user << "\red You don't have the dexterity to do this!"
		return 1

	if (istype(M, /mob/living/carbon/human))
		var/mob/living/carbon/human/H = M
		var/datum/organ/external/affecting = H.get_organ(user.zone_sel.selecting)
		if(affecting.status & ORGAN_ROBOT)
			user << "\red This isn't useful at all on a robotic limb.."
			return 1
		
		if(affecting.status & ORGAN_HUSKED)
			user << "\red This bodypart is burned beyond any healing possibilities.."
			return 1

		
		if(istype(src,/obj/item/stack/medical/splint))
			if(affecting.status & ORGAN_SPLINTED)
				user << "\red This organ is already splinted."
				return 1
			else if(affecting.status & ORGAN_DESTROYED)
				user << "\red This organ is not present."
				return 1
			else if(!(affecting.status & ORGAN_BROKEN))
				user << "\red This organ is not broken."
				return 1
			else
				affecting.status |= ORGAN_SPLINTED
				user.visible_message( "\blue %knownface:1% applies splints on %knownface:2%'s [affecting.display_name].", \
									"\blue You apply splints on %knownface:2%'s [affecting.display_name].", actors=list(user,M))
		else
			var/obj/item/cover = H.checkcoverage(affecting)
			if(istype(cover))
				user << "\red Can't use [src] through the [cover]"
				return 1

			if(src.heal_brute)
				if(!affecting.bandage())
					user << "\red The wounds on [M]'s [affecting.display_name] have already been bandaged."
					return 1
				else
					user.visible_message( 	"\blue %knownface:1% bandages wounds on %knownface:2%'s [affecting.display_name].", \
										"\blue You bandage wounds on %knownface:2%'s [affecting.display_name].", actors=list(user,M))
			else if(src.heal_burn)
				if(!affecting.salve())
					user << "\red The wounds on [M]'s [affecting.display_name] have already been salved."
					return 1
				else
					user.visible_message( 	"\blue %knownface:1% salves wounds on %knownface:2%'s [affecting.display_name].", \
										"\blue You salve wounds on %knownface:2%'s [affecting.display_name].", actors=list(user,M))
		H.UpdateDamageIcon()
	else
		M.heal_organ_damage((src.heal_brute/2), (src.heal_burn/2))
		user.visible_message( \
			"\blue %knownface:1% has been applied with [src] by %knownface:2%.", \
			"\blue You apply \the [src] to %knownface:1%.", actors=list(M,user) \
		)

	use(1)
	M.updatehealth()
