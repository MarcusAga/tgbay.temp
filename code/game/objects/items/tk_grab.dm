//This file was auto-corrected by findeclaration.exe on 25.5.2012 20:42:32

/obj/item/tk_grab
	name = "Telekinetic Grab"
	desc = "Magic"
	icon = 'icons/obj/magic.dmi'//Needs sprites
	icon_state = "2"
	flags = USEDELAY
	//item_state = null
	w_class = 10.0
	layer = 20

	var/last_throw = 0
	var/atom/movable/focus = null
	var/mob/living/host = null
	var/last_loc


	dropped(mob/user as mob)
		del(src)
		return


	//stops TK grabs being equipped anywhere but into hands
	equipped(var/mob/user, var/slot)
		if( (slot == slot_l_hand) || (slot== slot_r_hand) )	return
		del(src)
		return

	attack_self(mob/user as mob)
		if(!focus) return
//		if(!istype(focus,/obj/item))	return
//		if(!check_path())	return//No clear path

		last_loc = focus.loc
		if(istype(focus,/obj/item))
			var/obj/item/I = focus
			I.attack_self(user)
		return

	afterattack(atom/target as mob|obj|turf|area, mob/living/user as mob|obj, flag)//TODO: go over this
		if(!target || !user)	return
		if(last_throw+3 > world.time)	return
		if(!host)
			del(src)
			return
		if(!(TK in host.mutations))
			del(src)
			return
		if(isobj(target))
			if(!target.loc || !isturf(target.loc))
				del(src)
				return
		if((JediLightning in user.mutations) && user.a_intent == "hurt")
			var/obj/item/projectile/beam/jedi_lightning/JL = new(user.loc)
			var/turf/TT = get_turf(target)
			var/turf/UT = get_turf(user)
			if(!TT || !UT || TT==UT) return
			JL.def_zone = user.zone_sel.selecting
			JL.firer = user
			JL.original = TT
			JL.current = TT
			JL.starting = UT
			JL.yo = TT.y - UT.y
			JL.xo = TT.x - UT.x
			spawn()
				JL.process()
			return
		if(isliving(focus) || (!focus && isliving(target)))
			attack_living(target, user)
			return
		if(!focus)
			focus_object(target, user)
			return
		var/focusturf = get_turf(focus)
		last_loc = focus.loc
		if(get_dist(focusturf, target) <= 1 && !istype(target, /turf))
			if(focus == target)
				focus.attack_hand(user)
			else
				target.attackby(focus, user, user:get_organ_target())

		else if(get_dist(focusturf, target) <= 16)
			apply_focus_overlay()
			focus.throw_at(target, 10, 1)
			last_throw = world.time
		return

	proc/attack_living(var/mob/living/target, var/mob/living/user)
		var/grab = (JediGrab in user.mutations)
		var/push = (JediPush in user.mutations)
		if(!grab && !push) return

		if(user.a_intent == "grab" && grab)
			if(user==target)
				focus = null
				update_icon()
			else if(focus==target)
				if(!ishuman(focus) || !(JediReflect in focus:mutations) || prob(10))
					target.Stun(10)
					target.emote("gasp")
					target.losebreath += 5
					target.adjustOxyLoss(10)
					user.visible_message("%knownface:1% strangles %knownface:2%'s throat by Force","You strangle %knownface:2% by Force.", actors=list(user,target))
					apply_focus_overlay()
			else if(focus)
				focus.throw_at(target, 10, 1)
				if(!ishuman(focus) || !(JediReflect in focus:mutations) || prob(10))
					focus:Stun(5)
				last_throw = world.time
				apply_focus_overlay()
			else
				if(!isturf(target.loc))
					return
				focus = target
				update_icon()
				apply_focus_overlay()
		else if(user.a_intent == "disarm")
			if(!focus && push)
				var/turf/TT = get_turf(target)
				var/turf/UT = get_turf(user)
				if(get_dist(TT, UT) <= 5)
					var/x = 2*TT.x-UT.x
					var/y = 2*TT.y-UT.y
					target.throw_at(locate(x,y,TT.z),4,1)
					if(!ishuman(focus) || !(JediReflect in focus:mutations) || prob(10))
						target.Stun(5)
						target.Weaken(5)
					focus = target
					apply_focus_overlay()
					focus = null
					var/datum/effect/effect/system/steam_trail_follow/STF = new()
					STF.set_up(target)
					STF.start()
					spawn(5)
						STF.stop()
						del(STF)
			else if(focus==target && grab)
				if(!ishuman(focus) || !(JediReflect in focus:mutations) || prob(10))
					target.Stun(10)
					target.emote("gasp")
					target.losebreath += 5
					target.adjustOxyLoss(10)
					user.visible_message("%knownface:1% strangles %knownface:2%'s throat by Force","You strangle %knownface:2% by Force.", actors=list(user,target))
					apply_focus_overlay()
			else if(isliving(focus) && grab)
				focus.throw_at(target, 10, 1)
				if(!ishuman(focus) || !(JediReflect in focus:mutations) || prob(10))
					focus:Stun(5)
				last_throw = world.time
				apply_focus_overlay()

	proc/focus_object(var/obj/target, var/mob/living/user)
		if(!istype(target,/obj))	return//Cant throw non objects atm might let it do mobs later
		if(target.anchored)
			last_loc = target.loc
			target.attack_hand(user) // you can use shit now!
			return//No throwing anchored things
		if(!isturf(target.loc))
			return
		focus = target
		update_icon()
		apply_focus_overlay()
		return


	proc/apply_focus_overlay()
		if(!focus)	return
		var/obj/effect/overlay/O = new /obj/effect/overlay(locate(focus.x,focus.y,focus.z))
		O.name = "sparkles"
		O.anchored = 1
		O.density = 0
		O.layer = FLY_LAYER
		O.dir = pick(cardinal)
		O.icon = 'icons/effects/effects.dmi'
		O.icon_state = "nothing"
		flick("empdisable",O)
		spawn(5)
			del(O)
		return


	update_icon()
		overlays = null
		if(focus && focus.icon && focus.icon_state)
			overlays += icon(focus.icon,focus.icon_state)
		return

/*Not quite done likely needs to use something thats not get_step_to
	proc/check_path()
		var/turf/ref = get_turf(src.loc)
		var/turf/target = get_turf(focus.loc)
		if(!ref || !target)	return 0
		var/distance = get_dist(ref, target)
		if(distance >= 10)	return 0
		for(var/i = 1 to distance)
			ref = get_step_to(ref, target, 0)
		if(ref != target)	return 0
		return 1
*/

//equip_to_slot_or_del(obj/item/W, slot, del_on_fail = 1)
/*
		if(istype(user, /mob/living/carbon))
			if(user:mutations & TK && get_dist(source, user) <= 7)
				if(user:get_active_hand())	return 0
				var/X = source:x
				var/Y = source:y
				var/Z = source:z

*/

