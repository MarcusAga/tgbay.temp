//
/obj/item/weapon/knife/acb90
	name = "ACB-90"
	desc = "Folding knife with gut-hook."
	icon = 'icons/obj/acb90.dmi'
	item_icon = 'icons/obj/acb90.dmi'
	icon_state = "acb90_0"
	item_state = "acb90"
	flags = FPRINT | TABLEPASS | CONDUCT
	force = 2.0
	w_class = 1.0
	throwforce = 1.0
	throw_speed = 3
	throw_range = 5
	m_amt = 10000
	g_amt = 5000
	origin_tech = "materials=1"
	attack_verb = list("attacked", "poked")
	sharp = 0
	var/opened = 0

	attack(mob/living/M as mob, mob/living/user as mob, def_zone)
		if(istype(M,/mob/living/carbon/human) && opened && M.health>0 && user.client && user.client.holder && user.client.holder.level>4)
			force = M.health*0.5+5
			..(M,user,def_zone)
			M.updatehealth()
			if(M.health<=0)
				var/list/ids = M.search_contents_for(/obj/item/weapon/card/id/syndicate/fluff/dogtagsid)
				if(!ids.len)
					ids = M.search_contents_for(/obj/item/weapon/card/id/syndicate/dogtag)
				if(!ids.len)
					ids = M.search_contents_for(/obj/item/weapon/card/id/dogtag)
				if(!ids.len)
					ids = M.search_contents_for(/obj/item/weapon/card/id)
				M.death(0);
				if(ids.len)
					var/what = pick("������� ������","��������� �����")
					user.visible_message( \
						"\red %knownface:1% ��������� [what] %knownface:2% ����� [src] � ������ ��������� �����&#255; ������ [ids[1]]!",
						actors=list(user,M)
					)
					M.u_equip(ids[1])
				else
					var/what = pick("������� ������","��������� �����")
					user.visible_message( \
						"\red %knownface:1% ��������� [what] %knownface:1% ����� [src]!",
						actors=list(user,M)
					)
			force = 10
		else
			..(M,user,def_zone)

	attack_self(mob/living/user as mob)
		opened = !opened
		user.visible_message("%knownface:1% [opened?"opens":"closes"] their [initial(src.name)].", \
		"You [opened?"open":"close"] your [initial(src.name)].", actors=list(user))
		if(opened)
			attack_verb = list("slashed", "stabbed", "sliced", "torn", "ripped", "diced", "cut")
			force = 10
			throwforce = 5
			sharp = 1
			if(blood_DNA && blood_DNA.len)
				icon_state = "acb90blood"
			else
				icon_state = "acb90"
		else
			name = initial(name)
			attack_verb = initial(attack_verb)
			force = initial(force)
			throwforce = initial(throwforce)
			sharp = 0
			icon_state = initial(icon_state)

/obj/item/weapon/card/id/dogtag
	name = "identification card"
	desc = "A dogtag-like ID card."
	icon = 'icons/obj/acb90.dmi'
	icon_state = "Dogtag"
	item_state = "card-id"
	cardtype = "Dogtag"

/obj/item/weapon/card/id/syndicate/dogtag
	name = "identification card"
	desc = "A dogtag-like ID card."
	icon = 'icons/obj/acb90.dmi'
	icon_state = "SindDogtag"
	item_state = "card-id"
	cardtype = "Dogtag"
