//
/obj/item/weapon/defibrillator
	name = "Defibrillators"
	desc = "Medical defibrillators."
	icon = 'icons/obj/defibrillator.dmi'
	item_icon = 'icons/obj/defibrillator.dmi'
	icon_state = "Defibunit"
	item_state = "Defibunit"
	flags = FPRINT | TABLEPASS
	w_class = 1.0
	damtype = "brute"
	force = 4
	var/charged = 0
	var/charge_level = 0
	var/charges = 8
	var/discharges = 20
	origin_tech = "combat=2;biotech=2"
	m_amt = 2000
	g_amt = 50

	New()
		..()
		spawn(5)
			check_charge()

	proc/check_charge()
		while(src && src.discharges)
			sleep(5)
			if(charges>0 && icon_state=="Defibunit_nopower")
				icon_state = "Defibunit"
			if(charge_level)
				charge_level--
				if(!charge_level)
					discharge()

	attack_self(mob/user as mob)
		if(!charged)
			if(charges)
				user.visible_message("%knownface:1% charges their [src].", "You charge your [src].</span>", "You hear electrical zap.", actors=list(user))
				charged = 1
				spawn(25)
					charges--
					charged = 2
					icon_state = "Defibunit_on"
					damtype = "fire"
					force = 20
					charge_level = discharges
			else
				user<<"Internal battery worn out. Recharge needed."

	cyborg
		charges = 1
		discharges = 0
		discharge()
			icon_state = "Defibunit"
			damtype = "brute"
			charged = 1
			force = initial(force)
			spawn(60)
				charges++
				if(loc)
					loc.visible_message("[loc] charged their [src].", "You charged your [src].</span>", "You hear electrical zap.")
				charged = 2
				icon_state = "Defibunit_on"
				damtype = "fire"
				force = 20

	proc/discharge()
		if(charges>0)
			icon_state = "Defibunit"
		else
			icon_state = "Defibunit_nopower"
		damtype = "brute"
		charged = 0
		force = initial(force)
//		charges--
		charge_level = 0

	attack(mob/M as mob, mob/user as mob)
		if(charged == 2 && istype(M,/mob/living/carbon))
			var/mob/living/carbon/C = M
			user.visible_message("%knownface:1% shocks %knownface:2% with [src].", "You shock %knownface:2% with [src].</span>", "You hear electricity zaps flesh.", actors=list(user, M))
			var/cyborg = ishuman(C) && C:isCyborg()
			if(cyborg)
				C.nutrition += 5
			else
				C.apply_effect(10, STUN, 0)
				C.apply_effect(10, WEAKEN, 0)
				C.apply_effect(10, STUTTER, 0)
				if(C.jitteriness<=100)
					C.make_jittery(150)
				else
					C.make_jittery(50)
				C.losebreath = min(-8,C.losebreath)
			if(C.health<=config.health_threshold_crit || prob(10))
				var/suff = min(C.getOxyLoss(), 20)
				C.adjustOxyLoss(-suff)
				C.updatehealth()
				if(C.stat == DEAD && C.health>config.health_threshold_dead)
					C.stat = UNCONSCIOUS
			else
				if(C.stat == DEAD && C.health>config.health_threshold_dead)
					C.stat = CONSCIOUS
				else if(!cyborg)
					M.attack_log += text("\[[time_stamp()]\] <font color='orange'>Has been harm-defibrillated with [src.name] by [user.name] ([user.ckey])</font>")
					user.attack_log += text("\[[time_stamp()]\] <font color='red'>Used the [src.name] to harm-defibrillate [M.name] ([M.ckey])</font>")
					if(prob(20))
						C.death(1)
					else
						C.adjustFireLoss(20)
//				if(charges>0)
//					charges--
				if(prob(40) && charges>0)
					charges--
			discharge()
			var/datum/effect/effect/system/spark_spread/s = new /datum/effect/effect/system/spark_spread
			s.set_up(3, 1, C)
			s.start()
		else return ..(M,user)

datum/design/defibrillators
	name = "Defibrillators"
	desc = "Defibrillators to revive people."
	id = "defibrillators"
	req_tech = list("combat" = 2,"biotech" = 2)
	build_type = 2 //PROTOLATHE
	materials = list("$metal" = 2000, "$glass" = 50)
	build_path = "/obj/item/weapon/defibrillator"
