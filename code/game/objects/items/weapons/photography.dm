/* Photography!
 * Contains:
 *		Camera
 *		Photos
 *		Photo Albums
 */

/*
 * Camera
 */
/obj/item/weapon/camera_test
	name = "camera"
	icon = 'icons/obj/items.dmi'
	desc = "A polaroid camera. 30 photos left."
	icon_state = "camera"
	item_state = "electropack"
	w_class = 2.0
	flags = FPRINT | CONDUCT | USEDELAY | TABLEPASS
	slot_flags = SLOT_BELT
	m_amt = 2000
	throwforce = 5
	throw_speed = 4
	throw_range = 10
	var/pictures_max = 30
	var/pictures_left = 30
	var/can_use = 1

/obj/item/weapon/photo
	name = "photo"
	icon = 'icons/obj/items.dmi'
	icon_state = "photo"
	item_state = "clipboard"
	w_class = 1.0
	var/icon/img	//Big photo image
	var/scribble	//Scribble on the back.
	var/alt_desc = ""
	var/list/faces

/obj/item/weapon/photo/New()

	..()
	src.pixel_y = 0
	src.pixel_x = 0
	return

/obj/item/weapon/photo/attack_self(var/mob/user as mob)
		..()
		examine()

/obj/item/weapon/photo/attackby(obj/item/weapon/P as obj, mob/user as mob)
	if (istype(P, /obj/item/weapon/pen) || istype(P, /obj/item/toy/crayon))
		var/txt = input(usr, "What would you like to write on the back?", "Photo Writing", null)  as text
		txt = copytext(txt, 1, 128)
		if ((loc == usr && usr.stat == 0))
			scribble = txt

	..()

/obj/item/weapon/photo/examine()
	set src in oview(2)
	if(faces)
		if (!( usr ))
			return
		usr << "That's \a [src]."
		var/rdesc = desc
		if(usr:mind)
			for(var/face in faces)
				if(!usr:mind.known_faces[face])
					usr:mind.known_faces[face] = "Unknown"
				rdesc=dd_replacetext(rdesc,"(human:[face])","<a href='?src=\ref[usr:mind];face=[face]'>[sanitize_simple(usr:mind.known_faces[face])]</a>")
		else
			if(!alt_desc)
				for(var/face in faces)
					var/name = faces[face]
					rdesc=dd_replacetext(rdesc,"(human:[face])",name)
				alt_desc = rdesc
			else
				rdesc = alt_desc
		usr << sanitize_simple(rdesc)
	else
		..()
	if (scribble)
		usr << "\blue you see something written on photo's back. "
	show(usr)

/obj/item/weapon/photo/proc/show(var/mob/usr)
	if(!img)
		usr << "Empty photopaper"
		return
	var/override = 0
	if(ishuman(usr))
		var/mob/living/carbon/human/H = usr
		if((mTrippyVision in H.mutations) && H.druggy > 30)
			override = 1
	if(override)
		usr << browse_rsc(icon('160x160.dmi',"singularity_s5"), "tmp_photo.png")
	else
		usr << browse_rsc(src.img, "tmp_photo.png")
	usr << browse("<html><head><title>Photo</title></head>" \
		+ "<body style='overflow:hidden'>" \
		+ "<div> <img src='tmp_photo.png' width = '180'/>" \
		+ "[scribble ? "<div> Writings on the back:<br><i>[scribble]</i>" : "" ]"\
		+ "</body></html>", "window=book;size=200x[scribble ? 400 : 200]")
	onclose(usr, "[name]")
	return

/obj/item/weapon/photo/verb/rename()
	set name = "Rename photo"
	set category = "Object"
	set src in usr

	var/n_name = input(usr, "What would you like to label the photo?", "Photo Labelling", src.name)  as text
	n_name = copytext(n_name, 1, 32)
	//loc.loc check is for making possible renaming photos in clipboards
	if (( (src.loc == usr || (src.loc.loc && src.loc.loc == usr)) && usr.stat == 0))
		name = "photo[(n_name ? text("- '[n_name]'") : null)]"
	add_fingerprint(usr)
	return

/obj/item/weapon/camera_film
	name = "film cartridge"
	icon = 'items.dmi'
	desc = "A camera film cartridge. Insert it into a camera to reload it."
	icon_state = "film"
	item_state = "electropack"
	w_class = 1.0


//////////////////////////////////////////////////////////////////////////////////////////////////
/obj/item/weapon/camera_test/attack(mob/living/carbon/human/M as mob, mob/user as mob)
	return

/obj/item/weapon/camera_test/proc/get_icon(turf/the_turf as turf)
	//Bigger icon base to capture those icons that were shifted to the next tile
	//i.e. pretty much all wall-mounted machinery
	var/icon/res = icon('96x96.dmi',"")

	var/icon/turficon = build_composite_icon(the_turf)
	res.Blend(turficon,ICON_OVERLAY,65,65)

	var/atoms[] 	= list()
	for(var/atom/A in the_turf)
		if(A.invisibility) continue
		atoms.Add(A)

	//Sorting icons based on levels
	var/gap = atoms.len
	var/swapped = 1
	while (gap > 1 || swapped)
		swapped = 0
		if (gap > 1)
			gap = round(gap / 1.247330950103979)
		if (gap < 1)
			gap = 1
		for (var/i = 1; gap + i <= atoms.len; i++)
			var/atom/l = atoms[i]		//Fucking hate
			var/atom/r = atoms[gap+i]	//how lists work here
			if (l.layer > r.layer)		//no "atoms[i].layer" for me
				atoms.Swap(i, gap + i)
				swapped = 1

	for (var/i; i <= atoms.len; i++)
		var/atom/A = atoms[i]
		if (A)
//			var/icon/img = getFlatIcon(A,A.dir)//build_composite_icon(A)
			var/icon/img = build_composite_icon(A)
			if(istype(img, /icon))
				res.Blend(new/icon(img,A.dir),ICON_OVERLAY,65+A.pixel_x,65+A.pixel_y)
	return res

/obj/item/weapon/camera_test/attack_self(var/mob/user as mob)
	..()
	if (can_use)
		can_use = 0
		icon_state = "camera_off"
		usr << "\red You turn the camera off."
	else
		can_use = 1
		icon_state = "camera"
		usr << "\blue You turn the camera on."

/obj/item/weapon/camera_test/proc/get_mobs(turf/the_turf as turf,var/mob_detail=null, var/place_id=0)
	for(var/mob/living/carbon/A in the_turf)
		if(A.invisibility) continue
		var/holding = null
		if(A.l_hand || A.r_hand)
			if(A.l_hand) holding = "They are holding \a [A.l_hand]"
			if(A.r_hand)
				if(holding)
					holding += " and \a [A.r_hand]"
				else
					holding = "They are holding \a [A.r_hand]"

		var/mob_name = ""
		if(ishuman(A))
			var/mob/living/carbon/human/H = A
			if(H.isHeadRecognizable())
				mob_name = "(human:[H.dna.get_unique_face()])"
			else
				mob_name = "unknown"
		else
			mob_name = "[A]"

		var/place = ""
		switch(place_id)
			if(1)
				place="top-left"
			if(2)
				place="top"
			if(3)
				place="top-right"
			if(4)
				place="left"
			if(5)
				place="center"
			if(6)
				place="right"
			if(7)
				place="bottom-left"
			if(8)
				place="bottom"
			if(9)
				place="bottom-right"
		if(!mob_detail)
			mob_detail = "You can see [mob_name] on the [place] of photo[A:health < 75 ? " - [A] looks hurt":""].[holding ? " [holding]":"."]. "
		else
			mob_detail += "You can also see [mob_name] on the photo[A:health < 75 ? " - [A] looks hurt":""].[holding ? " [holding]":"."]."
	return mob_detail

/obj/item/weapon/camera_test/proc/get_faces(turf/the_turf as turf)
	var/list/faces = list()
	for(var/mob/living/carbon/A in the_turf)
		if(A.invisibility) continue
		if(ishuman(A) && A:isHeadRecognizable())
			faces["[A:dna.get_unique_face()]"]="[A]"
	return faces

/obj/item/weapon/camera_test/attackby(obj/item/weapon/P as obj, mob/user as mob)
	if(istype(P,/obj/item/weapon/camera_film) && !pictures_left)
		pictures_left = !pictures_max
		usr << "You reload camera with new [P.name]"
		del(P)
		return

/obj/item/weapon/camera_test/afterattack(atom/target as mob|obj|turf|area, mob/user as mob, flag)
	if (!can_use || !pictures_left || ismob(target.loc)) return

	var/x_c = target.x - 1
	var/y_c = target.y + 1
	var/z_c	= target.z

	var/icon/temp = icon('96x96.dmi',"")
	var/icon/black = icon('space.dmi', "black")
	var/mobs = ""
	var/mob/dummy = new()	//Go go visibility check dummy
	var/list/faces = list()
	for (var/i = 1; i <= 3; i++)
		for (var/j = 1; j <= 3; j++)
			var/turf/T = locate(x_c,y_c,z_c)
			dummy.loc = T
			var/viewer = user
			if (user.client)		//To make shooting through security cameras possible
				viewer = user.client.eye
			var/visible = (dummy in view(world.view, viewer))
			if(visible)
				temp.Blend(get_icon(T),ICON_OVERLAY,64*(j-1-1),64 - 64*(i-1))
			else
				temp.Blend(black,ICON_OVERLAY,64*(j-1),64 - 64*(i-1-1))
			mobs = get_mobs(T,mobs,i*3+j-3)
			faces += get_faces(T)
			x_c++
		y_c--
		x_c = x_c - 3
	del dummy	//Alas, nameless creature

	var/obj/item/weapon/photo/P = new/obj/item/weapon/photo()
	P.loc = usr.loc
	if(!user.get_inactive_hand())
		usr.put_in_inactive_hand(P)
	var/icon/small_img = icon(temp)
	var/icon/ic = icon('items.dmi',"photo")
	small_img.Scale(16,16)
	ic.Blend(small_img,ICON_OVERLAY,19,25)
	P.icon = ic
	P.img = temp
	P.desc = mobs
	P.faces = faces
	P.pixel_x = rand(-20,20)
	P.pixel_y = rand(-20,20)
	playsound(src.loc, pick('polaroid1.ogg','polaroid2.ogg'), 75, 1, -3)

	pictures_left--
	src.desc = "A polaroid camera. It has [pictures_left] photos left."
	user << "\blue [pictures_left] photos left."
	can_use = 0
	icon_state = "camera_off"
	spawn(50)	//Icon operations are pretty costy, so don't want them to spam photos
		can_use = 1
		icon_state = "camera"



/*
 * Photo Albums
 */
/obj/item/weapon/storage/photo_album
	name = "Photo album"
	icon = 'icons/obj/items.dmi'
	icon_state = "album"
	item_state = "briefcase"
	can_hold = list("/obj/item/weapon/photo",)

/obj/item/weapon/storage/photo_album/MouseDrop(obj/over_object as obj)

	if ((istype(usr, /mob/living/carbon/human) || (ticker && ticker.mode.name == "monkey")))
		var/mob/M = usr
		if (!( istype(over_object, /obj/screen) ))
			return ..()
		playsound(src.loc, "rustle", 50, 1, -5)
		if ((!( M.restrained() ) && !( M.stat ) && M.back == src))
			switch(over_object.name)
				if("r_hand")
					M.u_equip(src)
					M.put_in_r_hand(src)
				if("l_hand")
					M.u_equip(src)
					M.put_in_l_hand(src)
			src.add_fingerprint(usr)
			return
		if(over_object == usr && in_range(src, usr) || usr.contents.Find(src))
			if (usr.s_active)
				usr.s_active.close(usr)
			src.show_to(usr)
			return
	return

/obj/item/weapon/storage/photo_album/attackby(obj/item/weapon/W as obj, mob/user as mob)
	..()