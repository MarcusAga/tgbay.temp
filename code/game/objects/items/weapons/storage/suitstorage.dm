//
/obj/item/weapon/storage/suitstorage
	name = "Suit package"
	desc = "Package with clothes."
	icon = 'icons/obj/cloth_pack.dmi'
	icon_state = "standartcloth"
	item_state = "contsolid"
	w_class = 4
	max_w_class = 3
	max_combined_w_class = 18
	can_hold = list("/obj/item/clothing",
			"/obj/item/device",
			"/obj/item/weapon/handcuffs")

	New()
		..()
		contents = list()
		sleep(1)

	med
		name = "Medical Doctor Pack"
		icon_state = "mediccloth"

		New()
			..()
			new /obj/item/device/radio/headset/headset_med(src)
			new /obj/item/clothing/shoes/white(src) //w_class = 3
			new /obj/item/device/flashlight/pen(src) //w_class = 2

		medical_doctor
			New()
				..()
				new /obj/item/device/pda/medical(src) //w_class = 1
				new /obj/item/clothing/under/rank/medical(src) //w_class = 3
				new /obj/item/clothing/suit/labcoat(src) //w_class = 3
		emergency_physician
			name = "Emergency Physician Pack"
			New()
				..()
				new /obj/item/device/pda/medical(src) //w_class = 1
				new /obj/item/clothing/under/rank/medical(src) //w_class = 3
				new /obj/item/clothing/suit/fr_jacket(src) //w_class = 3
		surgeon
			name = "Surgeon Pack"
			New()
				..()
				new /obj/item/device/pda/medical(src) //w_class = 1
				new /obj/item/clothing/under/rank/medical/blue(src)
				new /obj/item/clothing/suit/labcoat(src)
		virologist
			name = "Virologist Pack"
			New()
				..()
				new /obj/item/device/pda/medical(src) //w_class = 1
				new /obj/item/clothing/suit/labcoat/virologist(src)
				new /obj/item/clothing/under/rank/virologist(src)
				new /obj/item/clothing/mask/surgical(src)
		chemist
			name = "Chemist Pack"
			New()
				..()
				new /obj/item/device/pda/chemist(src)
				new /obj/item/clothing/under/rank/chemist(src)
				new /obj/item/clothing/suit/labcoat/chemist(src)

	chief_medical_officer
		name = "CMO Pack"
		icon_state = "bluecloth"
		New()
			..()
			new /obj/item/device/radio/headset/heads/cmo(src)
			new /obj/item/clothing/under/rank/chief_medical_officer(src)
			new /obj/item/clothing/suit/labcoat/cmo(src)
			new /obj/item/clothing/shoes/brown(src)
			new /obj/item/device/pda/heads/cmo(src)
			new /obj/item/device/flashlight/pen(src)

	hos
		name = "HoS Pack"
		icon_state = "sbcloth"
		New()
			..()
			new /obj/item/device/radio/headset/heads/hos(src)
			new /obj/item/clothing/under/rank/head_of_security(src)
			new /obj/item/clothing/shoes/jackboots(src)
			new /obj/item/device/pda/heads/hos(src)
			new /obj/item/clothing/gloves/black(src)
			new /obj/item/clothing/glasses/sunglasses/sechud(src)

	security
		icon_state = "sbcloth"
		New()
			..()
			new /obj/item/device/radio/headset/headset_sec(src)
			new /obj/item/clothing/shoes/jackboots(src)
			new /obj/item/device/flash(src)

		warden
			name = "Warden Pack"
			New()
				..()
				new /obj/item/clothing/under/rank/warden(src)
				new /obj/item/device/pda/warden(src)
				new /obj/item/clothing/gloves/black(src)
				new /obj/item/clothing/glasses/sunglasses/sechud(src)

		security_officer
			name = "Security Officer Pack"
			icon_state = "sbcloth"
			New()
				..()
				new /obj/item/clothing/under/rank/security(src)
				new /obj/item/device/pda/security(src)
				new /obj/item/weapon/handcuffs(src)
				new /obj/item/weapon/handcuffs(src)

	detectives
		icon_state = "sbcloth"
		New()
			..()
			new /obj/item/device/radio/headset/headset_sec(src)
			new /obj/item/clothing/under/det(src)
			new /obj/item/clothing/shoes/brown(src)
			new /obj/item/device/pda/detective(src)
			new /obj/item/clothing/gloves/black(src)

		detective
			name = "Detective Pack"
			New()
				..()
				new /obj/item/clothing/suit/det_suit(src)
				new /obj/item/clothing/head/det_hat(src)
		forensic
			name = "Forensic Technician Pack"
			New()
				..()
				new /obj/item/clothing/suit/forensics/blue(src)

	geneticist
		name = "Geneticist Pack"
		icon_state = "mediccloth"
		New()
			..()
			new /obj/item/device/radio/headset/headset_medsci(src)
			new /obj/item/clothing/under/rank/geneticist(src)
			new /obj/item/clothing/shoes/white(src)
			new /obj/item/device/pda/geneticist(src)
			new /obj/item/clothing/suit/labcoat/genetics(src)
			new /obj/item/device/flashlight/pen(src)

	rd
		name = "RD Pack"
		icon_state = "scientistcloth"
		New()
			..()
			new /obj/item/device/radio/headset/heads/rd(src)
			new /obj/item/clothing/shoes/brown(src)
			new /obj/item/clothing/under/rank/research_director(src)
			new /obj/item/device/pda/heads/rd(src)
			new /obj/item/clothing/suit/labcoat(src)

	scientists
		icon_state = "scientistcloth"
		New()
			..()
			new /obj/item/device/radio/headset/headset_sci(src)
			new /obj/item/clothing/under/rank/scientist(src)
			new /obj/item/clothing/shoes/white(src)
			new /obj/item/device/pda/toxins(src)
			new /obj/item/clothing/suit/labcoat/science(src)

		scientist
			name = "Scientist Pack"
		xenoarch
			name = "Xenoarcheologist Pack"
		anomalist
			name = "Anomalist Pack"
		plasma
			name = "Plasma Researcher Pack"
		xenobiologist
			name = "Xenobiologist Pack"

	ce
		name = "CE Pack"
		icon_state = "engecloth"
		New()
			..()
			new /obj/item/device/radio/headset/heads/ce(src)
			new /obj/item/clothing/under/rank/chief_engineer(src)
			new /obj/item/device/pda/heads/ce(src)
			new /obj/item/clothing/shoes/brown(src)
			new /obj/item/clothing/head/hardhat/white(src)
			new /obj/item/clothing/gloves/black(src)
	engineer
		name = "Engineer Pack"
		icon_state = "engecloth"
		New()
			..()
			new /obj/item/device/radio/headset/headset_eng(src)
			new /obj/item/clothing/under/rank/engineer(src)
			new /obj/item/clothing/shoes/orange(src)
			new /obj/item/clothing/head/hardhat(src)
			new /obj/item/device/t_scanner(src)
			new /obj/item/device/pda/engineering(src)

	engineer_atmos
		name = "Atmospherics Pack"
		icon_state = "engecloth"
		New()
			..()
			new /obj/item/device/radio/headset/headset_eng(src)
			new /obj/item/clothing/under/rank/atmospheric_technician(src)
			new /obj/item/clothing/shoes/black(src)
			new /obj/item/device/pda/atmos(src)

	roboticist
		name = "Roboticist Pack"
		icon_state = "engecloth"
		New()
			..()
			new /obj/item/device/radio/headset/headset_rob(src)
			new /obj/item/clothing/under/rank/roboticist(src)
			new /obj/item/clothing/shoes/black(src)
			new /obj/item/device/pda/roboticist(src)
			new /obj/item/clothing/suit/labcoat(src)
			new /obj/item/clothing/gloves/black(src)

	civilian
		icon_state = "yellowcloth"
		New()
			..()
			new /obj/item/clothing/shoes/black(src)

		bartender
			name = "Bartender Pack"
			New()
				..()
				new /obj/item/clothing/suit/armor/vest(src)
				new /obj/item/clothing/under/rank/bartender(src)
				new /obj/item/device/pda/bar(src)

		chef
			name = "Chef Pack"
			New()
				..()
				new /obj/item/clothing/under/rank/chef(src)
				new /obj/item/clothing/suit/chef(src)
				new /obj/item/clothing/head/chefhat(src)
				new /obj/item/device/pda/chef(src)

		botanist
			name = "Botanist Pack"
			New()
				..()
				new /obj/item/clothing/under/rank/hydroponics(src)
				new /obj/item/clothing/gloves/botanic_leather(src)
				new /obj/item/clothing/suit/apron(src)
				new /obj/item/device/pda/botanist(src)

		cargo_tech
			name = "Cargo Technician Pack"
			icon_state = "standartcloth"
			New()
				..()
				new /obj/item/device/radio/headset/headset_cargo(src)
				new /obj/item/clothing/under/rank/cargotech(src)
				new /obj/item/device/pda/cargo(src)
				new /obj/item/clothing/gloves/black(src)

		miner
			name = "Shaft Miner Pack"
			icon_state = "standartcloth"
			New()
				..()
				new /obj/item/device/radio/headset/headset_mine (src)
				new /obj/item/device/pda/shaftminer(src)
				new /obj/item/clothing/under/rank/miner(src)
				new /obj/item/clothing/gloves/black(src)

		janitor
			name = "Janitor Pack"
			icon_state = "standartcloth"
			New()
				..()
				new /obj/item/clothing/under/rank/janitor(src)
				new /obj/item/device/pda/janitor(src)

		librarian
			name = "Librarian Pack"
			icon_state = "standartcloth"
			New()
				..()
				new /obj/item/clothing/under/suit_jacket/red(src)
				new /obj/item/device/pda/librarian(src)

		chaplain
			name = "Chaplain"
			icon_state = "standartcloth"
			New()
				..()
				new /obj/item/device/pda/chaplain(src)
				new /obj/item/clothing/under/rank/chaplain(src)

	clownmime
		icon_state = "redcloth"
		clown
			name = "Clown Pack"
			New()
				..()
				new /obj/item/clothing/under/rank/clown(src)
				new /obj/item/clothing/shoes/clown_shoes(src)
				new /obj/item/device/pda/clown(src)
				new /obj/item/clothing/mask/gas/clown_hat(src)

		mime
			name = "Mime Pack"
			New()
				..()
				new /obj/item/clothing/under/mime(src)
				new /obj/item/clothing/shoes/black(src)
				new /obj/item/device/pda/mime(src)
				new /obj/item/clothing/gloves/white(src)
				new /obj/item/clothing/mask/gas/mime(src)
				new /obj/item/clothing/head/beret(src)
				new /obj/item/clothing/suit/suspenders(src)

	qm
		name = "Quartermaster Pack"
		New()
			..()
			new /obj/item/device/radio/headset/heads/qm(src)
			new /obj/item/clothing/under/rank/cargo(src)
			new /obj/item/clothing/shoes/brown(src)
			new /obj/item/device/pda/quartermaster(src)
			new /obj/item/clothing/gloves/black(src)
			new /obj/item/clothing/glasses/sunglasses(src)

	lawyer
		icon_state = "standartcloth"
		New()
			..()
			new /obj/item/clothing/shoes/brown(src)
			new /obj/item/device/pda/lawyer(src)
		ver1
			name = "Lawyer Pack V1"
			New()
				..()
				new /obj/item/clothing/under/lawyer/bluesuit(src)
				new /obj/item/clothing/suit/lawyer/bluejacket(src)
		ver2
			name = "Lawyer Pack V2"
			New()
				..()
				new /obj/item/clothing/under/lawyer/purpsuit(src)
				new /obj/item/clothing/suit/lawyer/purpjacket(src)

	captain
		name = "Captain"
		icon_state = "bluecloth"
		New()
			..()
			new /obj/item/device/radio/headset/heads/captain(src)
			var/obj/item/clothing/under/U = new /obj/item/clothing/under/rank/captain(src)
			U.hastie = new /obj/item/clothing/tie/medal/gold/captain(U)
			new /obj/item/device/pda/captain(src)
			new /obj/item/clothing/suit/captunic(src)
			new /obj/item/clothing/head/helmet/cap(src)
			new /obj/item/clothing/glasses/sunglasses(src)


	hop
		name = "Head of Personnel"
		icon_state = "bluecloth"
		New()
			..()
			new /obj/item/device/radio/headset/heads/hop(src)
			new /obj/item/clothing/under/rank/head_of_personnel(src)
			new /obj/item/clothing/shoes/brown(src)
			new /obj/item/device/pda/heads/hop(src)
