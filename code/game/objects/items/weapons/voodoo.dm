//
/obj/item/stack/rods/attackby(obj/item/W as obj, mob/user as mob)
	if(istype(W,/obj/item/clothing/under))
		user.u_equip(W)
		for(var/obj/O in W)
			O.loc = get_turf(W)
		del(W)
		user.put_in_active_hand(new/obj/item/weapon/voodoo_doll(get_turf(loc)))
		use(1)
		return
	else ..()

/obj/item/weapon/voodoo_doll
	name = "Voodoo Doll"
	desc = "Odd looking doll."
	icon = 'icons/obj/voodoodoll.dmi'
	icon_state = "voodoo_doll"
	item_state = "cigpacket"
	flags = FPRINT | TABLEPASS | OPENCONTAINER
	var/named

	New()
		reagents = new(50)
		reagents.my_atom = src

	on_reagent_change()
		if(!named || !reagents.total_volume) return
		var/list/allowed_reagents = list("space_drugs","stoxin","psilocybin","ethanol","impedrezene","mercury","serotrotium")
		for(var/mob/living/carbon/human/H in living_mob_list)
			if(lowertext(H.real_name) == lowertext(named))
				for (var/datum/reagent/current_reagent in src.reagents.reagent_list)
					var/current_reagent_transfer = min(current_reagent.volume,5-H.reagents.get_reagent_amount(current_reagent.id))
					var/trans_data = current_reagent.data
					if(current_reagent.id in allowed_reagents)
						H.reagents.add_reagent(current_reagent.id, current_reagent_transfer, trans_data)
				src.reagents.clear_reagents()
				break
				
	attackby(obj/item/W as obj, mob/user as mob)
		if(istype(W,/obj/item/weapon/pen))
			named = input(user,"Who it would be?","Name?") as text
			if(named)
				name = "[named]'s [initial(name)]"
				icon_state = "voodoo_doll_named"
			else
				name = initial(name)
				icon_state = "voodoo_doll"
		else
			for(var/mob/living/carbon/human/H in living_mob_list)
				if(lowertext(H.real_name) == lowertext(named))
					var/datum/organ/external/affecting = H.get_organ(check_zone(user.zone_sel.selecting))
					if (!affecting)
						continue
					user.visible_message("%knownface:1% attacks his [src] with [W]", "You attack [src] with [W] in [affecting.display_name]", actors=list(user))
					var/dmg = min(W.force,10)
					if(dmg)
						if(H.health>10 && !istype(H.wear_suit,/obj/item/clothing/suit/powered))
							user.attack_log += text("\[[time_stamp()]\]<font color='red'> Attacked [H.name] ([H.ckey]) with [W.name] via VooDoo-doll magic</font>")
							H.attack_log += text("\[[time_stamp()]\]<font color='orange'> Attacked by [user.name] ([user.ckey]) with [W.name] via VooDoo-doll magic</font>")
							log_attack("<font color='red'>[user.name] ([user.ckey]) attacked [H.name] ([H.ckey]) with [W.name] via VooDoo-doll magic</font>" )

							if(affecting.brute_dam + affecting.burn_dam + 5 + dmg < affecting.max_damage)
								H.apply_damage(dmg, BRUTE, affecting, 0, W.sharp)
							if(prob(50))
								H<<"You feel sudden pain in your [affecting.display_name]"
								H.Stun(2)
								if(prob(50))
									H.Stun(2)
									H.Weaken(2)
							var/selfdmg = max(dmg*(90-H.health)/100,0)
							if(selfdmg) user:apply_damage(selfdmg, BRUTE)
							if(user.zone_sel.selecting == "eyes")
								var/datum/organ/external/head = H.get_organ("head")
								if(!(head.status&ORGAN_ROBOT))
									H.eye_blurry += rand(3,4)
									H.eye_stat += rand(2,4)
									if (H.eye_stat >= 10)
										H.eye_blurry += 15+(0.1*H.eye_blurry)
										H.disabilities |= NEARSIGHTED
										if(H.stat != 2)
											H << "\red Your eyes start to bleed profusely!"
											if(prob(50))
												H << "\red You drop what you're holding and clutch at your eyes!"
												H.drop_item()
										H.eye_blurry += 10
										H.Paralyse(1)
										H.Weaken(4)
									if (prob(H.eye_stat - 10 + 1))
										if(H.stat != 2 && !(H.sdisabilities&NEARSIGHTED))
											H << "\red You go blind!"
											H.sdisabilities |= BLIND
						else
							user:apply_damage(2*dmg, BRUTE)
					break

/*	throw_impact(atom/hit_atom)
		if(!named) return
		for(var/mob/living/carbon/human/H in living_mob_list)
			if(lowertext(H.real_name) == lowertext(named))
				H<<"Strange force threw you"
				H.Weaken(2)
				H.throw_at(get_step(H.loc,src.dir), 5, 1)
				break
*/

	temperature_expose(datum/gas_mixture/air, exposed_temperature, exposed_volume)
		if(!named) return
		for(var/mob/living/carbon/human/H in living_mob_list)
			if(lowertext(H.real_name) == lowertext(named))
				if(H.fireloss>75)
					break
				H.bodytemperature = 0.6*H.bodytemperature + 0.4*exposed_temperature
				break
