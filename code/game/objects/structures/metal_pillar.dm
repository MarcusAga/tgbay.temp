//
/obj/structure/metal_pillar
	name = "Metal Pillar"
	desc = "Simple, round metal pillar"
	icon = 'icons/obj/metalpillar.dmi'
	icon_state = "100"
	density = 1
	var/metal_sheets = 10
	var/obj/structure/foamedliving/statue = null
	var/datum/mind/creator = null
	var/progress = 0
	var/clip = 64
	flags = FPRINT|CONDUCT

	Del()
		if(metal_sheets)
			var/obj/item/stack/sheet/metal/M = new(loc)
			M.amount = metal_sheets
		..()

	attackby(obj/item/weapon/W as obj, mob/user as mob)
		if(is_hot(W) > 3000)
			del(src)
			return
		if(istype(W, /obj/item/weapon/circular_saw))
			var/datum/mind/M = user.mind
			if(!statue)
				if(!M)
					user << "You have no mind"
					return
				creator = M
				var/list/ProtoTypes = list()
				for(var/mob/living/P in view(3,user))
					var/n
					if(ishuman(P))
						var/mob/living/carbon/human/H = P
						n = user.client.GetHumanName(H)
					else
						n = "[P]"
					n = sanitize_simple(n,list("�"="�"))
					if(ProtoTypes[n])
						var/idx = 1
						var/nn
						do
							++idx
							nn = "[n] ([idx])"
						while(ProtoTypes[nn])
						n = nn
					ProtoTypes[n] = P
				var/mob/living/prototype = input(user, "Select prototype") as null|anything in ProtoTypes
				if(!prototype) return
				prototype = ProtoTypes[prototype]
				if(!prototype) return
				var/noface = alert(user, "Precise face?", "Statue", "As Is", "Unknown")
				if(noface == "Unknown")
					noface = 1
				else
					noface = 0
				statue = new(src)
				statue.init(prototype, 1, 0, noface)
				statue.flags = FPRINT|CONDUCT
				desc = "Round metal pillar, which had been sawed with something."
			if(creator && creator!=M)
				user << "It is not yours creation"
				return

			var/turf/T = user.loc
			var/holding = user.get_active_hand()

			user.visible_message( \
				"%knownface:1% starts working on [src].", \
				"\blue You start working on \the [src].", \
				"You hear metal grinding.", actors=list(user))
			playsound(get_turf(src), 'sound/weapons/circsawhit.ogg', 50, 1, -1)
			while(progress<100)
				sleep(10)
				if(!user || user.stat || user.weakened || user.stunned || usr.paralysis || !(user.loc == T) || !(user.get_active_hand() == holding))
					user << "You stopped your work"
					return
				progress += 5
				playsound(get_turf(src), 'sound/weapons/circsawhit.ogg', 50, 1, -1)
				update_icon()
			if(progress>=100)
				metal_sheets = 0
				statue.dir = dir
				statue.loc = loc
				for(var/i=0, i<metal_sheets, i++)
					var/obj/item/weapon/ore/iron/Loot = new(loc)
					Loot.loc = statue
				del(src)
			return
		..()

	update_icon()
		var/nclip = 64
		switch(progress)
			if(0 to 25)
				icon_state = "100"
				nclip = 64
			if(26 to 50)
				icon_state = "75"
				nclip = 46
			if(51 to 75)
				icon_state = "50"
				nclip = 32
			if(76 to 100)
				icon_state = "25"
				nclip = 17
		if(clip!=nclip)
			clip = nclip
			overlays.Cut()
			var/icon/I = icon(statue.icon)
			var/w = I.Width()
//			var/h = I.Height()
			I.DrawBox(null, 1, 1, w, nclip)
//			I.Crop(1,nclip, w, h)
//			I.Crop(1, 1, w, h)
//			I.Shift(1, nclip)
			overlays += I


/obj/structure/human_statue_spawner
	parent_type = /obj
	icon = 'icons/obj/metalpillar.dmi'
	icon_state = "100"

	var/r_hair = 0
	var/g_hair = 0
	var/b_hair = 0
	var/h_style = "Bald"

	//Facial hair colour and style
	var/r_facial = 0
	var/g_facial = 0
	var/b_facial = 0
	var/f_style = "Shaved"

	//Eye colour
	var/r_eyes = 0
	var/g_eyes = 0
	var/b_eyes = 0

	var/s_tone = 0	//Skin tone

	var/underwear = 0

	var/wear_suit = null
	var/wear_mask = null
	var/wear_uniform = null
	var/wear_shoes = null
	var/wear_belt = null
	var/wear_gloves = null
	var/wear_glasses = null
	var/wear_head = null
	var/wear_ears = null
	var/wear_id = null
	var/wear_back = null
	var/wear_l_hand = null
	var/wear_r_hand = null

	var/assignment = null
	var/race = null

	// 0 - none
	// 1 - normal
	// 2 - protesis
	var/organs = list("head"=1, "chest"=1, "groin"=1, "l_arm"=1, "r_arm"=1, "l_hand"=1, "r_hand"=1, "l_leg"=1, "r_leg"=1, "l_foot"=1, "r_foot"=1)

	New()
		..()
		spawn(0)
			var/mob/living/carbon/human/H = new()
			H.r_hair = r_hair
			H.g_hair = g_hair
			H.b_hair = b_hair
			H.h_style = h_style
			H.r_facial = r_facial
			H.g_facial = g_facial
			H.b_facial = b_facial
			H.f_style = f_style
			H.r_eyes = r_eyes
			H.g_eyes = g_eyes
			H.b_eyes = b_eyes
			H.s_tone = s_tone
			H.underwear = underwear
			if(H.dna)
				H.dna.mutantrace = race

			for(var/org in organs)
				var/datum/organ/external/O = H.organs_by_name[org]
				switch(organs[org])
					if(0)
						O.destspawn = 1
						O.status |= ORGAN_DESTROYED
					if(2)
						O.status |= ORGAN_ROBOT

			makeClothing(H, "wear_suit", wear_suit)
			makeClothing(H, "wear_mask", wear_mask)
			makeClothing(H, "w_uniform", wear_uniform)
			makeClothing(H, "shoes", wear_shoes)
			makeClothing(H, "belt", wear_belt)
			makeClothing(H, "gloves", wear_gloves)
			makeClothing(H, "glasses", wear_glasses)
			makeClothing(H, "head", wear_head)
			makeClothing(H, "ears", wear_ears)
			makeClothing(H, "back", wear_back)
			makeClothing(H, "l_hand", wear_l_hand)
			makeClothing(H, "r_hand", wear_r_hand)
			makeClothing(H, "wear_id", wear_id)

			for(var/obj/item/weapon/card/id/ID in H)
				ID.name = "[name]'s [ID.cardtype][assignment?" ([assignment])":""]"

			H.regenerate_icons()
			var/obj/structure/foamedliving/statue = new(loc)
			statue.init(H, 1, 0, 1)
			statue.flags = FPRINT|CONDUCT
			statue.dir = dir

			del(H)
			del(src)

	proc/makeClothing(var/mob/living/carbon/human/H, var/slot, var/proto)
		if(isnull(proto)) return
		if(istext(proto))
			var/p = text2path(proto)
			if(!p)
				world << "Statue '[name]' failed to determine item class '[proto]' for slot [slot]"
				return
			proto = p
		var/item = new proto(H)
		H.vars[slot] = item
