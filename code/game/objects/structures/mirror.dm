//wip wip wup
/obj/structure/mirror
	name = "mirror"
	desc = "Mirror mirror on the wall, who's the most robust of them all?"
	icon = 'icons/obj/watercloset.dmi'
	icon_state = "mirror"
	density = 0
	anchored = 1

/obj/structure/mirror/attack_hand(mob/user as mob)
	if(ishuman(user))
		var/mob/living/carbon/human/H = user

		var/userloc = H.loc

		//see code/modules/mob/new_player/preferences.dm at approx line 545 for comments!
		//this is largely copypasted from there.

		var/list/valid_facestyles = list()
		for(var/facestyle in facial_hair_styles_list)
			var/datum/sprite_accessory/S = facial_hair_styles_list[facestyle]
			if(gender == MALE && !S.choose_male)
				continue
			if(gender == FEMALE && !S.choose_female)
				continue
			if( !(H.get_species() in S.species_allowed))
				continue

			valid_facestyles[facestyle] = facial_hair_styles_list[facestyle]

		//handle facial hair (if necessary)
		if(valid_facestyles.len > 0)
			var/new_style = input(user, "Select a facial hair style", "Grooming")  as null|anything in valid_facestyles
			if(userloc != H.loc) return	//no tele-grooming
			if(new_style)
				H.f_style = new_style


		var/list/valid_hairstyles = list()
		for(var/hairstyle in hair_styles_list)
			var/datum/sprite_accessory/S = hair_styles_list[hairstyle]
			if(gender == MALE && !S.choose_male)
				continue
			if(gender == FEMALE && !S.choose_female)
				continue
			if( !(H.get_species() in S.species_allowed))
				continue

			valid_hairstyles[hairstyle] = hair_styles_list[hairstyle]

		//handle normal hair
		if(valid_hairstyles.len > 0)
			var/new_style = input(user, "Select a hair style", "Grooming")  as null|anything in valid_hairstyles
			if(userloc != H.loc) return	//no tele-grooming
			if(new_style)
				H.h_style = new_style

		var/datum/organ/external/head/head_organ = H.get_organ("head")
		if(head_organ.headless && head_organ.status&ORGAN_DESTROYED)
			if(istype(H.head,/obj/item/weapon/organ/head))
				var/obj/item/weapon/organ/head/fakehead = H.head
				fakehead.facials["f_style"] = H.f_style
				fakehead.facials["h_style"] = H.h_style

		H.update_hair()
