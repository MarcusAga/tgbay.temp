/* Beds... get your mind out of the gutter, they're for sleeping!
 * Contains:
 * 		Beds
 *		Roller beds
 */

/*
 * Beds
 */
/obj/structure/stool/bed
	name = "bed"
	desc = "This is used to lie in, sleep in or strap on."
	icon_state = "bed"

/obj/structure/stool/bed/alien
	name = "resting contraption"
	desc = "This looks similar to contraptions from earth. Could aliens be stealing our technology?"
	icon_state = "abed"

/obj/structure/stool/Del()
	unbuckle()
	..()
	return

/obj/structure/stool/attack_paw(mob/user as mob)
	return src.attack_hand(user)

/obj/structure/stool/attack_hand(mob/user as mob)
	manual_unbuckle(user)
	return

/obj/structure/stool/bed/MouseDrop_T(mob/M as mob, mob/user as mob)
	if(!istype(M)) return
	buckle_mob(M, user)
	return

/obj/structure/stool/proc/unbuckle()
	if(buckled_mob)
		if(buckled_mob.buckled == src)	//this is probably unneccesary, but it doesn't hurt
			buckled_mob.buckled = null
			buckled_mob.anchored = initial(buckled_mob.anchored)
			buckled_mob.update_canmove()
			buckled_mob = null
	return

/obj/structure/stool/proc/manual_unbuckle(mob/user as mob)
	if(buckled_mob)
		if(buckled_mob.buckled == src)
			if(buckled_mob != user)
				buckled_mob.visible_message(\
					"\blue %knownface:1% was unbuckled by %knownface:2%!",\
					"You were unbuckled from [src] by %knownface:2%.",\
					"You hear metal clanking", actors=list(buckled_mob,user))
			else
				var/himself = user.gender==MALE?"himself":(user.gender==FEMALE?"herself":"itself")
				buckled_mob.visible_message(\
					"\blue %knownface:1% unbuckled [himself]!",\
					"You unbuckle yourself from [src].",\
					"You hear metal clanking", actors=list(user))
			unbuckle()
			src.add_fingerprint(user)
	return

/obj/structure/stool/proc/buckle_mob(mob/M as mob, mob/user as mob)
	if (!ticker)
		user << "You can't buckle anyone in before the game starts."
	if ( !ismob(M) || (get_dist(src, user) > 1) || (M.loc != src.loc) || user.restrained() || user.lying || user.stat || M.buckled || istype(user, /mob/living/silicon/pai) )
		return

	unbuckle()

	if (M == usr)
		M.visible_message(\
			"\blue %knownface:1% buckles in!",\
			"You buckle yourself to [src].",\
			"You hear metal clanking", actors=list(M))
	else
		M.visible_message(\
			"\blue %knownface:1% is buckled in to [src] by %knownface:2%!",\
			"You are buckled in to [src] by %knownface:2%.",\
			"You hear metal clanking", actors=list(M,user))
	M.buckled = src
	M.loc = src.loc
	M.update_canmove()
	src.buckled_mob = M
	src.add_fingerprint(user)
	return

/obj/structure/stool/bed/buckle_mob(mob/M as mob, mob/user as mob)
	..()
	M.dir=src.dir

/*
 * Roller beds
 */
/obj/structure/stool/bed/roller
	name = "roller bed"
	icon = 'icons/obj/rollerbed.dmi'
	icon_state = "down"
	anchored = 0

/obj/item/roller
	name = "roller bed"
	desc = "A collapsed roller bed that can be carried around."
	icon = 'rollerbed.dmi'
	icon_state = "folded"
	w_class = 4.0 // Can't be put in backpacks. Oh well.

	attack_self(mob/user)
		var/obj/structure/stool/bed/roller/R = new /obj/structure/stool/bed/roller(user.loc)
		R.add_fingerprint(user)
		del(src)

/obj/structure/stool/bed/roller/Move()
	..()
	if(buckled_mob)
		if(buckled_mob.buckled == src)
			buckled_mob.loc = src.loc
		else
			buckled_mob = null

/obj/structure/stool/bed/roller/buckle_mob(mob/M as mob, mob/user as mob)
	if ( !ismob(M) || (get_dist(src, user) > 1) || (M.loc != src.loc) || user.restrained() || user.lying || user.stat || M.buckled || istype(usr, /mob/living/silicon/pai) )
		return
	M.pixel_y = 12
	density = 1
	icon_state = "up"
	..()
	return

/obj/structure/stool/bed/roller/manual_unbuckle(mob/user as mob)
	if(buckled_mob)
		if(buckled_mob.buckled == src)	//this is probably unneccesary, but it doesn't hurt
			buckled_mob.pixel_y = 0
			buckled_mob.anchored = initial(buckled_mob.anchored)
			buckled_mob.buckled = null
			buckled_mob.update_canmove()
			buckled_mob = null
	density = 0
	icon_state = "down"
	..()
	return

/obj/structure/stool/bed/roller/MouseDrop(over_object, src_location, over_location)
	..()
	if((over_object == usr && (in_range(src, usr) || usr.contents.Find(src))))
		if(!ishuman(usr))	return
		if(buckled_mob)	return 0
		visible_message("%knownface:1% collapses \the [src.name]", actors=list(usr))
		new/obj/item/roller(get_turf(src))
		spawn(0)
			del(src)
		return

/obj/structure/stool/verb/Buckle()
	set name = "Buckle"
	set category = "Object"
	set src in view(0)
	buckle_mob(usr, usr)
/obj/structure/stool/verb/Unbuckle()
	set name = "Unbuckle"
	set category = "Object"
	set src in view(0)
	if(usr==buckled_mob && !usr.restrained())
		manual_unbuckle(usr)
