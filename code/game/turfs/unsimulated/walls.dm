/turf/unsimulated/wall
	name = "wall"
	icon = 'icons/turf/walls.dmi'
	icon_state = "riveted"
	opacity = 1
	density = 1

turf/unsimulated/wall/splashscreen
	name = "Space Station 13"
	icon = 'icons/misc/fullscreen.dmi'
	layer = FLY_LAYER

turf/unsimulated/wall/splashscreen/New()
	icon_state = "title[rand(0,8)]"
	var/image = image('icons/misc/shadowpaw.dmi')
	overlays += image

/turf/unsimulated/wall/other
	icon_state = "r_wall"