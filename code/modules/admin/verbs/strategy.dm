//

/client/proc/toggleStrategy()
	set category = "Fun"
	set name = "Toggle Strategy"

	if(holder && holder.level>=5)
		holder.strategy_mode = !holder.strategy_mode
		src << "You toggle strategy [holder.strategy_mode?"On":"Off"]"
		if(holder.strategy_mode)
			verbs += /client/proc/strategyClearSelection
			verbs += /client/proc/strategyCancelOrders
			verbs += /client/proc/strategyAssignGroup
			verbs += /client/proc/strategyRecallGroup
			verbs += /client/proc/strategyShowSelected
			verbs += /client/proc/strategySetIntent
		else
			verbs -= /client/proc/strategyClearSelection
			verbs -= /client/proc/strategyCancelOrders
			verbs -= /client/proc/strategyAssignGroup
			verbs -= /client/proc/strategyRecallGroup
			verbs -= /client/proc/strategyShowSelected
			verbs -= /client/proc/strategySetIntent

/client/proc/strategyClearSelection()
	set category = "Strategy"
	set name = "Clear selection"

	if(holder && holder.level>=5)
		var/list/old_select = holder.selected_mobs
		holder.selected_mobs = list()
		for(var/mob/living/M in old_select)
			strategy_controller.checkInBase(M, 1)
		holder.updateStrategy()

/client/proc/strategyCancelOrders()
	set category = "Strategy"
	set name = "Cancel orders"

	if(holder && holder.level>=5)
		strategy_controller.assignTarget(holder.selected_mobs, null)

/client/proc/strategyAssignGroup()
	set category = "Strategy"
	set name = "Assign Group"

	if(holder && holder.level>=5 && holder.strategy_mode)
		var/idx = input(usr,"Select Group:","Groups") as null|anything in list("1","2","3","4")
		if(!idx) return
		idx = text2num(idx)

		holder.selected_mobs_groups[idx] = holder.selected_mobs

/client/proc/strategyRecallGroup()
	set category = "Strategy"
	set name = "Recall Group"

	if(holder && holder.level>=5 && holder.strategy_mode)
		var/idx = input(usr,"Select Group:","Groups") as null|anything in list("1","2","3","4")
		if(!idx) return
		idx = text2num(idx)

		var/list/old_select = holder.selected_mobs
		holder.selected_mobs = list()
		for(var/mob/living/M in old_select)
			strategy_controller.checkInBase(M, 1)
		holder.selected_mobs = holder.selected_mobs_groups[idx]
		for(var/mob/living/M in holder.selected_mobs)
			strategy_controller.checkInBase(M, 0)
		holder.updateStrategy()

/client/proc/strategyShowSelected()
	set category = "Strategy"
	set name = "Show Selected"

	if(!isobserver(usr)) return
	if(holder && holder.level>=5 && holder.strategy_mode)
		if(!holder.selected_mobs || !holder.selected_mobs.len) return
		var/mob/living/L = pick(holder.selected_mobs)
		mob.loc = get_turf(L)

/client/proc/strategySetIntent()
	set category = "Strategy"
	set name = "Set Intent"

	if(holder && holder.level>=5 && holder.strategy_mode)
		var/n_intent = input(usr, "New intent", "Select intent") as null|anything in list ("help","hurt","disarm")
		if(!n_intent) return
		for(var/mob/living/M in holder.selected_mobs)
			M.a_intent = n_intent
