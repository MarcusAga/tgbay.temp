/obj/item/device/assembly/igniter
	name = "igniter"
	desc = "A small electronic device able to ignite combustable substances."
	icon_state = "igniter"
	m_amt = 500
	g_amt = 50
	w_amt = 10
	origin_tech = "magnets=1"

	wires = WIRE_RECEIVE

	secured = 1


	activate()
		if(!..())	return 0//Cooldown check

		if(holder && istype(holder.loc,/obj/item/weapon/grenade/chem_grenade))
			var/obj/item/weapon/grenade/chem_grenade/grenade = holder.loc
			grenade.explode()
		else
			var/turf/location = get_turf(loc)
			if(location)
				location.hotspot_expose(1000,1000)
			if (istype(src.loc,/obj/item/device/assembly_holder))
				if (istype(src.loc.loc, /obj/structure/reagent_dispensers/fueltank/))
					var/obj/structure/reagent_dispensers/fueltank/tank = src.loc.loc
					if (tank && tank.modded)
						var/turf/T = get_turf(tank)
						var/area/A = get_area(T)
						var/log_str = "Fueltank blown up at <A HREF='?src=%holder_ref%;adminplayerobservecoodjump=1;X=[T.x];Y=[T.y];Z=[T.z]'>[A.name]</a>"

						if(tank.riggedby)
							log_str += " rigged by [tank.riggedby.ckey]([tank.riggedby.real_name])]"
						message_admins(log_str, 0, 1)
						log_game(log_str)
						
						if(fueltank_moderation)
							if(tank.riggedby)
								tank.riggedby.gib()
						else
							tank.explode()

			var/datum/effect/effect/system/spark_spread/s = new /datum/effect/effect/system/spark_spread
			s.set_up(3, 1, src)
			s.start()

		return 1


	attack_self(mob/user as mob)
		activate()
		add_fingerprint(user)
		return