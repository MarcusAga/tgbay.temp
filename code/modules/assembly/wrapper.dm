/obj/item/device/assembly/wrapper
	name="Wrapper"
	secured = 0
	var/obj/item/wrapObj

	proc/setObj(var/obj/item/I)
		wrapObj=I
		name=I.name
		desc=I.desc
		I.loc=src

/obj/item/device/assembly/wrapper/taperecorder
	activate()
		if(!wrapObj) return
		if(!secured || (cooldown > 0))	return 0
		cooldown = 2
		wrapObj:playback_memory_proc(1)
		spawn(10)
			process_cooldown()
		return 1

/obj/item/device/assembly/wrapper/deathtracker
	var/mob/living/lastOwner = null
	var/lastState = -1

	Del()
		processing_objects -= src
		..()

	setObj(I)
		..(I)
		processing_objects.Add(src)

	process()
		var/atom/location = loc
		if(istype(location,/obj/item/device/assembly_holder))
			location=location.loc
		if(isliving(location))
			if(lastOwner!=location)
				lastOwner = location
				lastState = -1
			if(lastOwner.stat!=lastState)
				lastState = lastOwner.stat
				if(lastState==DEAD)
					pulse(0)
		else
			lastOwner = null
			lastState = -1
