/obj/item/clothing/mask/gas
	name = "gas mask"
	desc = "A face-covering mask that can be connected to an air supply."
	icon_state = "gas_alt"
	flags = FPRINT | TABLEPASS | MASKCOVERSMOUTH | MASKCOVERSEYES | BLOCK_GAS_SMOKE_EFFECT | MASKINTERNALS
	flags_inv = HIDEEARS|HIDEEYES|HIDEFACE
	w_class = 3.0
	item_state = "gas_alt"
	gas_transfer_coefficient = 0.01
	permeability_coefficient = 0.01

//Plague Dr suit can be found in clothing/suits/bio.dm
/obj/item/clothing/mask/gas/plaguedoctor
	name = "plague doctor mask"
	desc = "A modernised version of the classic design, this mask will not only filter out toxins but it can also be connected to an air supply."
	icon_state = "plaguedoctor"
	item_state = "gas_mask"
	armor = list(melee = 0, bullet = 0, laser = 2,energy = 2, bomb = 0, bio = 75, rad = 0)

/obj/item/clothing/mask/gas/swat
	name = "\improper SWAT mask"
	desc = "A close-fitting tactical mask that can be connected to an air supply."
	icon_state = "swat"

/obj/item/clothing/mask/gas/syndicate
	name = "syndicate mask"
	desc = "A close-fitting tactical mask that can be connected to an air supply."
	icon_state = "swat"

/obj/item/clothing/mask/gas/voice
	name = "gas mask"
	//desc = "A face-covering mask that can be connected to an air supply. It seems to house some odd electronics."
	var/mode = 0// 0==Scouter | 1==Night Vision | 2==Thermal | 3==Meson
	var/list/voice = list()
	var/vchange = 0//This didn't do anything before. It now checks if the mask has special functions/N
	origin_tech = "syndicate=4"

	New()
		verbs += /obj/item/clothing/mask/gas/voice/proc/togglev

	proc/togglev()
		set name = "Toggle Voice"
		set desc = "Toggles the voice synthesizer on or off."
		set category = "Object"

		var/mob/living/U
		if(istype(loc,/obj/item/weapon/implant/modulator))
			U = loc.loc
		else
			U = loc
		if(!istype(U)) return

		var/option = (alert("Would you like to synthesize a new name or turn off the voice synthesizer?",,"New Voice","Mimic","Turn Off"))
		if(option=="New Voice")
			var/voice_hash = md5("[rand(1,1024)][rand(1,1024)][rand(1,1024)][rand(1,1024)][rand(1,1024)]")
			var/voice_gender = pick(MALE, FEMALE)
			if(prob(5))
				var/datum/data/record/R = pick(data_core.locked)
				if(!ishuman(usr) || usr:dna.get_unique_enzymes() != R.fields["b_dna"])
					voice_hash = R.fields["b_dna"]
					voice_gender = R.fields["sex"]
			voice = list("hash"=voice_hash, "sex"=voice_gender)
			vchange = 1
			U << "You are now mimicking <B>[usr.mind.known_voices[voice_hash]?usr.mind.known_voices[voice_hash]:"random [voice_gender==MALE?"male":"female"] voice"]</B>."
		else if(option=="Mimic")
			if(!usr.mind) return
			var/list/voices = list()
			for(var/hash in usr.mind.known_voices)
				voices[sanitize_simple(usr.mind.known_voices[hash])] = hash
			var/mimic_voice = input("Enter a name to mimic.", "Mimic Voice", null) as null|anything in voices
			if(!mimic_voice)
				return
			var/voice_hash = voices[mimic_voice]
			var/voice_gender = NEUTER
			for(var/datum/data/record/R in data_core.locked)
				if(R.fields["b_dna"] == mimic_voice)
					voice_gender = R.fields["sex"]
					break
			voice = list("hash"=voice_hash, "sex"=voice_gender)
			vchange = 1
			U << "You are now mimicking <B>[usr.mind.known_voices[voice_hash]?usr.mind.known_voices[voice_hash]:"unknown [voice_gender==MALE?"male":"female"] voice"]</B>."
		else if(option=="Turn Off")
			U << "The voice synthesizer is [voice["hash"]?"now":"already"] deactivated."
			voice = list()
			vchange = 0
		return

/obj/item/clothing/mask/gas/voice/space_ninja
	name = "ninja mask"
	desc = "A close-fitting mask that acts both as an air filter and a post-modern fashion statement."
	icon_state = "s-ninja"
	item_state = "s-ninja_mask"

	New()
		..()
		verbs += /obj/item/clothing/mask/gas/voice/space_ninja/proc/switchm

/obj/item/clothing/mask/gas/clown_hat
	name = "clown wig and mask"
	desc = "A true prankster's facial attire. A clown is incomplete without his wig and mask."
	icon_state = "clown"
	item_state = "clown_hat"

/obj/item/clothing/mask/gas/sexyclown
	name = "sexy-clown wig and mask"
	desc = "A feminine clown mask for the dabbling crossdressers or female entertainers."
	icon_state = "sexyclown"
	item_state = "sexyclown"

/obj/item/clothing/mask/gas/mime
	name = "mime mask"
	desc = "The traditional mime's mask. It has an eerie facial posture."
	icon_state = "mime"
	item_state = "mime"

/obj/item/clothing/mask/gas/monkeymask
	name = "monkey mask"
	desc = "A mask used when acting as a monkey."
	icon_state = "monkeymask"
	item_state = "monkeymask"

/obj/item/clothing/mask/gas/sexymime
	name = "sexy mime mask"
	desc = "A traditional female mime's mask."
	icon_state = "sexymime"
	item_state = "sexymime"

/obj/item/clothing/mask/gas/death_commando
	name = "Death Commando Mask"
	icon_state = "death_commando_mask"
	item_state = "death_commando_mask"

/obj/item/clothing/mask/gas/cyborg
	name = "cyborg visor"
	desc = "Beep boop"
	icon_state = "death"