/obj/item/clothing/tie
	name = "tie"
	desc = "A neosilk clip-on tie."
	icon = 'icons/obj/clothing/ties.dmi'
	icon_state = "bluetie"
	item_state = ""	//no inhands
	_color = "bluetie"
	flags = FPRINT | TABLEPASS
	slot_flags = 0
	w_class = 2.0

/obj/item/clothing/tie/blue
	name = "blue tie"
	icon_state = "bluetie"
	_color = "bluetie"

/obj/item/clothing/tie/red
	name = "red tie"
	icon_state = "redtie"
	_color = "redtie"

/obj/item/clothing/tie/horrible
	name = "horrible tie"
	desc = "A neosilk clip-on tie. This one is disgusting."
	icon_state = "horribletie"
	_color = "horribletie"

/obj/item/clothing/tie/stethoscope
	name = "stethoscope"
	desc = "An outdated medical apparatus for listening to the sounds of the human body. It also makes you look like you know what you're doing."
	icon_state = "stethoscope"
	_color = "stethoscope"

/obj/item/clothing/tie/stethoscope/attack(mob/living/carbon/human/M, mob/living/user)
	if(ishuman(M) && isliving(user))
		if(user.a_intent == "help")
			var/body_part = parse_zone(user.zone_sel.selecting)
			if(body_part)
				var/their = "their"
				switch(M.gender)
					if(MALE)	their = "his"
					if(FEMALE)	their = "her"

				var/sound = "pulse"
				var/sound_strength

				if(M.stat == DEAD || (M.status_flags&FAKEDEATH))
					sound_strength = "cannot hear"
					sound = "anything"
				else if(M:isCyborg())
					sound_strength = "hear a weak"
					sound = "electronics buzzing"
				else
					sound_strength = "hear a weak"
					switch(body_part)
						if("chest")
							if(M.oxyloss < 50)
								sound_strength = "hear a healthy"
							sound = "pulse and respiration"
						if("eyes","mouth")
							sound_strength = "cannot hear"
							sound = "anything"
						else
							sound_strength = "hear a weak"

				user.visible_message("%knownface:1% places [src] against %knownface:2%'s [body_part] and listens attentively.", "You place [src] against [their] [body_part]. You [sound_strength] [sound].", actors=list(user,M))
				return
	return ..(M,user)


/obj/item/clothing/tie/holster
	name = "shoulder holster"
	desc = "A handgun holster."
	icon_state = "holster"
	_color = "holster"
	var/obj/item/weapon/gun/holstered = null

/obj/item/clothing/tie/holster/armpit
	name = "shoulder holster"
	desc = "A worn-out handgun holster. Perfect for concealed carry"
	icon_state = "holster"
	_color = "holster"

/obj/item/clothing/tie/holster/waist
	name = "shoulder holster"
	desc = "A handgun holster. Made of expensive leather."
	icon_state = "holster"
	_color = "holster_low"

//Medals
/obj/item/clothing/tie/medal
	name = "bronze medal"
	desc = "A bronze medal."
	icon_state = "bronze"
	_color = "bronze"

/obj/item/clothing/tie/medal/conduct
	name = "distinguished conduct medal"
	desc = "A bronze medal awarded for distinguished conduct. Whilst a great honor, this is most basic award given by Nanotrasen. It is often awarded by a captain to a member of his crew."

/obj/item/clothing/tie/medal/bronze_heart
	name = "bronze heart medal"
	desc = "A bronze heart-shaped medal awarded for sacrifice. It is often awarded posthumously or for severe injury in the line of duty."
	icon_state = "bronze_heart"

/obj/item/clothing/tie/medal/nobel_science
	name = "nobel sciences award"
	desc = "A bronze medal which represents significant contributions to the field of science or engineering."

/obj/item/clothing/tie/medal/silver
	name = "silver medal"
	desc = "A silver medal."
	icon_state = "silver"
	_color = "silver"

/obj/item/clothing/tie/medal/silver/valor
	name = "medal of valor"
	desc = "A silver medal awarded for acts of exceptional valor."

/obj/item/clothing/tie/medal/silver/security
	name = "robust security award"
	desc = "An award for distinguished combat and sacrifice in defence of Nanotrasen's commercial interests. Often awarded to security staff."

/obj/item/clothing/tie/medal/gold
	name = "gold medal"
	desc = "A prestigious golden medal."
	icon_state = "gold"
	_color = "gold"

/obj/item/clothing/tie/medal/gold/captain
	name = "medal of captaincy"
	desc = "A golden medal awarded exclusively to those promoted to the rank of captain. It signifies the codified responsibilities of a captain to Nanotrasen, and their undisputable authority over their crew."

/obj/item/clothing/tie/medal/gold/heroism
	name = "medal of exceptional heroism"
	desc = "An extremely rare golden medal awarded only by CentComm. To recieve such a medal is the highest honor and as such, very few exist. This medal is almost never awarded to anybody but commanders."

/obj/item/clothing/tie/medal/goof
	name = "goof medal"
	desc = "A medal, awarded for most stupid death."
	icon_state = "goof"
	_color = "bronze"

/obj/item/clothing/tie/medal/timed/frny
	name = "long-service medal"
	desc = "A medal, awarded for longest time in service."
	icon_state = "frny"
	_color = "gold"

/obj/item/clothing/tie/medal/timed/frstart
	name = "'from the start' medal"
	desc = "A medal, awarded for even longer than longest time in service."
	icon_state = "frstart"
	_color = "silver"

/obj/item/clothing/tie/medal/coolguy
	name = "coolguy medal"
	desc = "A medal, awarded for being good NT worker."
	icon_state = "coolguy"
	_color = "gold"

/obj/item/clothing/tie/medal/achiv
	name = "achievement medal"
	desc = "A medal, awarded for helping NT achieve greater good."
	icon_state = "achiv"
	_color = "gold"

/obj/item/clothing/tie/medal/animal_luv
	name = "animal saviour medal"
	desc = "A medal, awarded for saving animals."
	icon_state = "animal<3"
	_color = "gold"

/obj/item/clothing/tie/medal/ntbronze
	name = "bronze medal"
	desc = "A medal, awarded for good service, may signify promotion in meantime."
	icon_state = "ntbronze"
	_color = "bronze"

/obj/item/clothing/tie/medal/ntsilver
	name = "silver medal"
	desc = "A medal, awarded for very good service, may signify even more promotion in meantime."
	icon_state = "ntsilver"
	_color = "silver"

/obj/item/clothing/tie/medal/ntgold
	name = "gold medal"
	desc = "A medal, awarded for very very good service, may signify extra high promotion in meantime."
	icon_state = "ntgold"
	_color = "gold"

/obj/item/clothing/tie/medal/ntplatinum
	name = "platinum medal"
	desc = "A medal, awarded for very very extra good service, may signify even more mega super promotion in meantime."
	icon_state = "ntplatinum"
	_color = "silver"

/obj/item/clothing/tie/medal/onemanarmy
	name = "one-man-army medal"
	desc = "A medal, awarded for being in extreme combat situation and live through it."
	icon_state = "1manarmy"
	_color = "gold"

/obj/item/clothing/tie/medal/firstcontact
	name = "first contact medal"
	desc = "A medal, awarded for participating in first contact with unknown species."
	icon_state = "1contact"
	_color = "silver"

/obj/item/clothing/tie/medal/sinbronze
	name = "bronze medal"
	desc = "A medal, awarded for good service, may signify promotion in meantime. This one has red ribbon."
	icon_state = "sinbronze"
	_color = "bronze"

/obj/item/clothing/tie/medal/sinsilver
	name = "silver medal"
	desc = "A medal, awarded for very good service, may signify even more promotion in meantime. This one has red ribbon."
	icon_state = "sinsilver"
	_color = "silver"

/obj/item/clothing/tie/medal/singold
	name = "gold medal"
	desc = "A medal, awarded for very very good service, may signify extra high promotion in meantime. This one has red ribbon."
	icon_state = "singold"
	_color = "gold"

/obj/item/clothing/tie/medal/sinplatinum
	name = "platinum medal"
	desc = "A medal, awarded for very very extra good service, may signify even more mega super promotion in meantime. This one has red ribbon."
	icon_state = "sinplatinum"
	_color = "silver"
