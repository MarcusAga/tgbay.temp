//switch this out to use a database at some point
//list of ckey/ real_name and item paths
//gives item to specific people when they join if it can
//for multiple items just add mutliple entries, unless i change it to be a listlistlist
//yes, it has to be an item, you can't pick up nonitems

/proc/EquipCustomItems(mob/living/carbon/human/M)
	// load lines
	var/file = file2text("config/custom_items.txt")
	var/lines = dd_text2list(file, "\n")

	for(var/line in lines)
		// split & clean up
		var/list/Entry = dd_text2list(line, ":")
		for(var/i = 1 to Entry.len)
			Entry[i] = trim(Entry[i])

		if(Entry.len < 3)
			continue;

		if(Entry[1] == M.ckey && (Entry[2] == "*" || Entry[2] == M.real_name || Entry[2]=="@[M.get_assignment()]"))
			var/list/Paths = dd_text2list(Entry[3], ",")
			for(var/P in Paths)
				var/ok = 0  // 1 if the item was placed successfully
				P = trim(P)
				var/doreplace = 0
				if(findtext(P,";"))
					var/list/Parts = dd_text2list(P, ";")
					P=Parts[2]
					doreplace=text2num(Parts[1])
					if(doreplace>20 || doreplace<0 || doreplace==slot_wear_id) doreplace = 0
				var/path = text2path(P)
				var/obj/item/Item = new path()
				if(istype(Item,/obj/item/weapon/card/id))
					//id card needs to replace the original ID
					if(M.ckey == "nerezza" && M.real_name == "Asher Spock" && M.mind.role_alt_title && M.mind.role_alt_title != "Emergency Physician")
						//only spawn ID if asher is joining as an emergency physician
						ok = 1
						del(Item)
						goto skip
					var/obj/item/weapon/card/id/I = Item
					for(var/obj/item/weapon/card/id/C in M)
						//default settings
						I.name = "[M.real_name]'s [I.cardtype] ([M.mind.role_alt_title ? M.mind.role_alt_title : M.mind.assigned_role])"
						I.registered_name = M.real_name
						I.access = C.access
						I.assignment = C.assignment
						I.blood_type = C.blood_type
						I.dna_hash = C.dna_hash
						I.fingerprint_hash = C.fingerprint_hash
						I.pin = C.pin

						del(C)
						ok = M.equip_if_possible(I, slot_wear_id, 0)	//if 1, last argument deletes on fail
						break
				else if(doreplace)
					if(doreplace == slot_tie)
						var/obj/item/clothing/under/U = locate() in M.contents
						if(U)
							if(U.hastie)
								var/obj/item/clothing/tie/T = U.hastie
								U.hastie = null
								del(T)
							U.hastie = Item
							ok = 1
					else
						var/obj/item/Have = M.get_inventory_by_slot(doreplace)
						if(Have)
							M.u_equip(Have, -1)
							del(Have)
						ok = M.equip_if_possible(Item, doreplace, 0)
				else if(istype(M.back,/obj/item/weapon/storage) && M.back:contents.len < M.back:storage_slots) // Try to place it in something on the mob's back
					Item.loc = M.back
					ok = 1

				else
					for(var/obj/item/weapon/storage/S in M.contents) // Try to place it in any item that can store stuff, on the mob.
						if (S.contents.len < S.storage_slots)
							Item.loc = S
							ok = 1
							break

				skip:
				if (ok == 0) // Finally, since everything else failed, place it on the ground
					Item.loc = get_turf(M.loc)
