/mob/living/carbon/alien/humanoid/sentinel
	name = "alien sentinel"
	caste = "s"
	maxHealth = 125
	health = 125
	storedPlasma = 100
	max_plasma = 250
	icon_state = "aliens_s"
	plasma_rate = 10

/mob/living/carbon/alien/humanoid/sentinel/New()
	var/datum/reagents/R = new/datum/reagents(100)
	reagents = R
	R.my_atom = src
	if(name == "alien sentinel")
		name = text("alien sentinel ([rand(1, 1000)])")
	real_name = name
	verbs.Add(/mob/living/carbon/alien/humanoid/proc/corrosive_acid,/mob/living/carbon/alien/humanoid/proc/neurotoxin)
	add_to_mob_list(src)

/mob/living/carbon/alien/humanoid/sentinel


	handle_regular_hud_updates()

		..() //-Yvarov

		if (hud_healths)
			if (stat != 2)
				switch(health)
					if(125 to INFINITY)
						hud_healths.icon_state = "health0"
					if(100 to 125)
						hud_healths.icon_state = "health1"
					if(75 to 100)
						hud_healths.icon_state = "health2"
					if(25 to 75)
						hud_healths.icon_state = "health3"
					if(0 to 25)
						hud_healths.icon_state = "health4"
					else
						hud_healths.icon_state = "health5"
			else
				hud_healths.icon_state = "health6"
