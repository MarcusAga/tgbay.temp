/mob/living/carbon/alien/humanoid/death(gibbed)
	if(stat == DEAD)	return
	if(hud_healths)			hud_healths.icon_state = "health6"
	stat = DEAD

	if(chemical_reagents_list["pacid"])
		var/datum/reagent/acid = chemical_reagents_list["pacid"]
		for(var/mob/living/M in range(src,1))
			if(isalien(M)) continue
			if(prob(80)) acid.reaction_mob(M,TOUCH,10)
			if(prob(80))
				if(issilicon(M))
					M.apply_damage(85,BRUTE)
				else
					M.apply_damage(50,TOX)
		for(var/obj/item/O in get_turf(src))
			acid.reaction_obj(O,10)
	else
		for(var/mob/living/M in range(src,1))
			if(prob(90)) M.apply_damage(75,TOX)
	if(!gibbed)
		playsound(loc, 'sound/voice/hiss6.ogg', 80, 1, 1)
		for(var/mob/O in viewers(src, null))
			O.show_message("<B>[src]</B> lets out a waning guttural screech, green blood bubbling from its maw...", 1)
		update_canmove()
		if(client)	hud_blind.layer = 0

	tod = worldtime2text() //weasellos time of death patch
//	if(mind) 	mind.store_memory("Time of death: [tod]", 0)

	return ..(gibbed)
