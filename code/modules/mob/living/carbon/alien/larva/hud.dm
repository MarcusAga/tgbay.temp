
/datum/hud/proc/larva_hud()

	src.adding = list()
	src.other = list()

	var/obj/screen/using

	using = new /obj/screen()
	using.name = "act_intent"
	using.dir = SOUTHWEST
	using.icon = 'icons/mob/screen1_alien.dmi'
	using.icon_state = (mymob.a_intent == "hurt" ? "harm" : mymob.a_intent)
	using.screen_loc = ui_acti
	using.layer = 20
	src.adding += using
	action_intent = using

	using = new /obj/screen()
	using.name = "mov_intent"
	using.dir = SOUTHWEST
	using.icon = 'icons/mob/screen1_alien.dmi'
	using.icon_state = (mymob.m_intent == "run" ? "running" : "walking")
	using.screen_loc = ui_movi
	using.layer = 20
	src.adding += using
	move_intent = using

	mymob.hud_oxygen = new /obj/screen()
	mymob.hud_oxygen.icon = 'icons/mob/screen1_alien.dmi'
	mymob.hud_oxygen.icon_state = "oxy0"
	mymob.hud_oxygen.name = "oxygen"
	mymob.hud_oxygen.screen_loc = ui_alien_oxygen

	mymob.hud_toxin = new /obj/screen()
	mymob.hud_toxin.icon = 'icons/mob/screen1_alien.dmi'
	mymob.hud_toxin.icon_state = "tox0"
	mymob.hud_toxin.name = "toxin"
	mymob.hud_toxin.screen_loc = ui_alien_toxin


	mymob.hud_fire = new /obj/screen()
	mymob.hud_fire.icon = 'icons/mob/screen1_alien.dmi'
	mymob.hud_fire.icon_state = "fire0"
	mymob.hud_fire.name = "fire"
	mymob.hud_fire.screen_loc = ui_alien_fire


	mymob.hud_healths = new /obj/screen()
	mymob.hud_healths.icon = 'icons/mob/screen1_alien.dmi'
	mymob.hud_healths.icon_state = "health0"
	mymob.hud_healths.name = "health"
	mymob.hud_healths.screen_loc = ui_alien_health

	mymob.hud_pullin = new /obj/screen()
	mymob.hud_pullin.icon = 'icons/mob/screen1_alien.dmi'
	mymob.hud_pullin.icon_state = "pull0"
	mymob.hud_pullin.name = "pull"
	mymob.hud_pullin.screen_loc = ui_pull_resist

	mymob.hud_blind = new /obj/screen()
	mymob.hud_blind.icon = 'icons/mob/screen1_full.dmi'
	mymob.hud_blind.icon_state = "blackimageoverlay"
	mymob.hud_blind.name = " "
	mymob.hud_blind.screen_loc = "1,1"
	mymob.hud_blind.layer = 0

	mymob.hud_flash = new /obj/screen()
	mymob.hud_flash.icon = 'icons/mob/screen1_alien.dmi'
	mymob.hud_flash.icon_state = "blank"
	mymob.hud_flash.name = "flash"
	mymob.hud_flash.screen_loc = "1,1 to 15,15"
	mymob.hud_flash.layer = 17

	mymob.zone_sel = new /obj/screen/zone_sel()
	var/icon/ico = new('icons/mob/screen1_alien.dmi', "zone_sel")
	ico.Scale(128,128)
	mymob.zone_sel.icon = ico
	mymob.zone_sel.overlays = null
	mymob.zone_sel.overlays += image("icon" = 'icons/mob/zone_sel.dmi', "icon_state" = text("[]", mymob.zone_sel.selecting))

	mymob.client.screen = null

	mymob.client.screen += list( mymob.zone_sel, mymob.hud_oxygen, mymob.hud_toxin, mymob.hud_fire, mymob.hud_healths, mymob.hud_pullin, mymob.hud_blind, mymob.hud_flash) //, mymob.rest, mymob.sleep, mymob.mach )
	mymob.client.screen += src.adding + src.other



