//
/datum/embryo/alien
	var/mob/living/carbon/Victim
	var/gibbed = 0
	var/stage = 1
	var/age = 0

	New(var/mob/living/carbon/target)
		if(!istype(target)) return
		..()
		Victim = target
		Victim.parasites.Add(src)
		spawn(0)
			if (Victim)
				AddInfectionImages(Victim)

	Del()
		if (Victim)
			Victim.parasites.Remove(src)
			RemoveInfectionImages(Victim)
		..()

	proc/stage_act()
		age++
		if(stage<5 && age > 90 * stage && prob(5))
			stage++

		switch(stage)
			if(2)
				if(prob(1))
					Victim.emote("sneeze")
				if(prob(1))
					Victim.emote("cough")
				if(prob(1))
					Victim << "\red Your throat feels sore."
				if(prob(1))
					Victim << "\red Mucous runs down the back of your throat."
			if(3)
				if(prob(1))
					Victim.emote("sneeze")
				if(prob(1))
					Victim.emote("cough")
				if(prob(1))
					Victim << "\red Your throat feels sore."
				if(prob(1))
					Victim << "\red Mucous runs down the back of your throat."
			if(4)
				if(prob(1))
					Victim.emote("sneeze")
				if(prob(1))
					Victim.emote("cough")
				if(prob(2))
					Victim << "\red Your muscles ache."
					if(prob(20))
						Victim.take_organ_damage(1)
				if(prob(2))
					Victim << "\red Your stomach hurts."
					if(prob(20))
						Victim.adjustToxLoss(1)
						Victim.updatehealth()
			if(5)
				Victim << "\red You feel something tearing its way out of your stomach..."
				Victim.adjustToxLoss(10)
				Victim.updatehealth()
				if(prob(50))
					if(gibbed != 0) return 0
					var/mob/living/carbon/alien/larva/new_xeno = new(Victim.loc)
					var/list/candidates = list() //List of candidate KEYS to assume control of the new larva ~Carn
	//				var/i = 0
					var/add = 1
					if(Victim.client && Victim.client.be_alien)
						candidates += Victim.ckey
						add = prob(50)
					if(add)
						for(var/mob/dead/observer/G in player_list)
							if(G.client && G.client.be_alien)
								if(((G.client.inactivity/10)/60) <= ALIEN_SELECT_AFK_BUFFER+9) // the most active players are more likely to become an alien
									if(!(G.mind && G.mind.current && G.mind.current.stat != DEAD))
										candidates += G.ckey

					if(candidates.len && !(Victim.ckey in candidates))
						new_xeno.ckey = pick(candidates)
					else
						new_xeno.ckey = Victim.ckey

					new_xeno << sound('sound/voice/hiss5.ogg',0,0,0,100)	//To get the player's attention
					Victim.gib()
					gibbed = 1
					del(src)
					return

	proc/AddInfectionImages(var/mob/living/carbon/C)
		if (C)
			for (var/mob/living/carbon/alien/alien in world)
				if (alien.client)
					var/datum/embryo/alien/A = locate() in C.parasites
					if (A)
						var/I = image('icons/mob/alien.dmi', loc = C, icon_state = "infected")
						alien.client.images += I
		return

	proc/RemoveInfectionImages(var/mob/living/carbon/C)
		if (C)
			for (var/mob/living/carbon/alien/alien in world)
				if (alien.client)
					for(var/image/I in alien.client.images)
						if(I.loc == C)
							if(I.icon_state == "infected")
								del(I)
