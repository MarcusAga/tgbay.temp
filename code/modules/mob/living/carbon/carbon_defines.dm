/mob/living/carbon/
	gender = MALE
	var/list/stomach_contents = list()

	var/list/viruses = list()
	var/list/resistances = list()

	var/silent = null 		//Can't talk. Value goes down every life proc.
	var/life_tick = 0      // The amount of life ticks that have processed on this mob.
	var/analgesic = 0 // when this is set, the mob isn't affected by shock or pain
					  // life should decrease this by 1 every tick
	// total amount of wounds on mob, used to spread out healing and the like over all wounds
	var/number_wounds = 0
	var/list/disease_mods = list()

	// a list of all the parasites in the mob
	var/list/parasites = list()
