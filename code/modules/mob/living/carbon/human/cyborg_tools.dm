//

/mob/living/carbon/human/Del()
	if(fakeAI) del(fakeAI)
	..()

/mob/living/carbon/human/verb/enableAI()
	set name = ".enableAI"
//	set hidden = 1
	if (is_alien_whitelisted(src, "CyborgAI"))
		src << "\blue You are unable to do this."
		return
	if (!isCyborgHead())
		src << "\blue You are not cybernetic enough."
		return
	if(!fakeAI)
		fakeAI = new(src,new/datum/ai_laws/bioroid,null,2)
	fakeAI.name = "Bioroid [real_name]"
	usr << "Enabled AI emulators"

/mob/living/carbon/human/verb/checkBorgs()
	set name = ".checkBorgs"
//	set hidden = 1
	if (is_alien_whitelisted(src, "CyborgAI"))
		src << "\blue You are unable to do this."
		return
	if (!isCyborgHead())
		src << "\blue You are not cybernetic enough."
		return
	if(!fakeAI)
		fakeAI = new(src,new/datum/ai_laws/bioroid,null,2)
	fakeAI.name = "Bioroid [real_name]"
	fakeAI.ai_cyborgslist()

/mob/living/carbon/human/verb/laws()
	set name = ".laws"
//	set hidden = 1
	if (is_alien_whitelisted(src, "CyborgAI"))
		src << "\blue You are unable to do this."
		return
	if (!isCyborgHead())
		src << "\blue You are not cybernetic enough."
		return
	if(!fakeAI)
		fakeAI = new(src,new/datum/ai_laws/bioroid,null,2)
	fakeAI.name = "Bioroid [real_name]"
	fakeAI.configLaws()

/mob/living/carbon/human/verb/hack()
	set name = ".hack"
//	set hidden = 1
	if (is_alien_whitelisted(src, "CyborgAI"))
		src << "\blue You are unable to do this."
		return
	if (!isCyborgHead())
		src << "\blue You are not cybernetic enough."
		return
	if(!fakeAI)
		fakeAI = new(src,new/datum/ai_laws/bioroid,null,2)
	fakeAI.name = "Bioroid [real_name]"

	var/turf/T = get_turf(loc)
	var/myZ = T.z
	var/list/victims = list()
	for(var/mob/living/silicon/robot/R in living_mob_list)
		T = get_turf(R)
		if(!R.scrambledcodes && myZ==T.z && R.connected_ai!=fakeAI)
			victims+=R

	var/mob/living/silicon/robot/Victim = null
	if(victims.len) Victim = pick(victims)
	if(!Victim)
		usr << "Found no targets in nearest networks"
		return

	var/text
	usr << "Starting hacking of [Victim]"

	for(var/k = 1, k <= 8, k++)
		T = get_turf(loc)
		myZ = T.z
		T = get_turf(Victim)
		if(Victim.scrambledcodes || myZ!=T.z)
			usr << "Connection dropped"
			return
		text=""
		for(var/j = 1, j <= 16, j++)
			text += pick("#","@","*","&","%","$","/", "<", ">", ";","1","0","T","F")
		Victim << text
		sleep(100)

	var/mob/living/silicon/ai/oldAI = Victim.connected_ai
	if(oldAI)
		oldAI.connected_robots -= Victim
	Victim.connected_ai=fakeAI
	fakeAI.connected_robots += Victim
	Victim.lawsync()
	usr << "Hack successfull"

/mob/living/silicon/ai/proc/configLaws()
	var/list = ""

	if (src.laws.zeroth)
		list += {"<A href='byond://?src=\ref[src];law_edit=0;type=zero'>E</A> <A href='byond://?src=\ref[src];law_remove=0;type=zero'>R</A> 0: [src.laws.zeroth]<BR>"}
	else
		list += {"<A href='byond://?src=\ref[src];law_edit=0;type=zero'>Add</A><BR>"}

	var/number = 1
	for (var/index = 1, index <= src.laws.ion.len, index++)
		var/law = src.laws.ion[index]

		if (length(law) > 0)
			list += {"<A href='byond://?src=\ref[src];law_remove=[index];type=ion'>R</A> [number]: [law]<BR>"}
			src.ioncheck.len += 1
			number++

	for (var/index = 1, index <= src.laws.inherent.len, index++)
		var/law = src.laws.inherent[index]
		if (length(law) > 0)
			src.lawcheck.len += 1
			list += {"<A href='byond://?src=\ref[src];law_edit=[index];type=inh'>E</A> <A href='byond://?src=\ref[src];law_remove=[index];type=inh'>R</A> [number]: [law]<BR>"}
			number++

	for (var/index = 1, index <= src.laws.supplied.len, index++)
		var/law = src.laws.supplied[index]
		if (length(law) > 0)
			src.lawcheck.len += 1
			list += {"<A href='byond://?src=\ref[src];law_edit=[index];type=sup'>E</A> <A href='byond://?src=\ref[src];law_remove=[index];type=sup'>R</A> [number]: [law]<BR>"}
			number++
	list += {"<A href='byond://?src=\ref[src];law_add=1'>Add</A><BR>"}

	usr << browse(sanitize_easy(list), "window=laws")

/mob/living/silicon/ai/proc/LawEditTopic(href, href_list)
	var/mob/owner = usr
	if(!owner) owner = src.loc
	if(!ishuman(owner) || !owner:isCyborgHead())
		owner << "You have no access to this"
		return

	if(href_list["law_edit"])
		var/id=text2num(href_list["law_edit"])
		var/type=href_list["type"]

		var/list/pre_edit_repl = list("�"="�")
		var/list/edit_repl = list("\t"=" ","\n"="<br>")
		if(type=="zero")
			laws.zeroth = sanitize_simple(input(usr,"New zeroth law:","0:",sanitize_simple(laws.zeroth,pre_edit_repl)) as message,edit_repl)
		else if(type=="inh" && laws.inherent.len>=id)
			laws.inherent[id] = sanitize_simple(input(usr,"New inherent law:","[id]:",sanitize_simple(laws.inherent[id],pre_edit_repl)) as message,edit_repl)
			if(!laws.inherent[id])
				laws.inherent.Cut(id,id+1)
		else if(type=="sup")
			laws.supplied[id] = sanitize_simple(input(usr,"New supplied law:","[id]:",sanitize_simple(laws.supplied[id],pre_edit_repl)) as message,edit_repl)
			if(!laws.supplied[id])
				laws.supplied.Cut(id,id+1)
	else if(href_list["law_remove"])
		var/id=text2num(href_list["law_remove"])
		var/type=href_list["type"]
		if(type=="zero")
			laws.zeroth = null
		else if(type=="ion" && laws.ion.len>=id)
			laws.inherent.Cut(id,id+1)
		else if(type=="inh" && laws.inherent.len>=id)
			laws.inherent.Cut(id,id+1)
		else if(type=="sup" && laws.supplied.len>=id)
			laws.supplied.Cut(id,id+1)
	else if(href_list["law_add"])
		var/newlaw = stripped_input(usr,"New law:","New:")
		if(newlaw)
			laws.supplied+=newlaw
	configLaws()
