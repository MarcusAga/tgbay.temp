//
/mob/living/carbon/human/emote(var/act,var/m_type=1,var/message = null)
	var/param = null
	var/spread_diseases = -1

	if(stat) return

	if (findtext(act, "-", 1, null))
		var/t1 = findtext(act, "-", 1, null)
		param = copytext(act, t1 + 1, length(act) + 1)
		act = copytext(act, 1, t1)

	var/muzzled = istype(src.wear_mask, /obj/item/clothing/mask/muzzle)
	//var/m_type = 1

	for (var/obj/item/weapon/implant/I in src)
		if (I.implanted)
			I.trigger(act, src)

	if(src.stat == 2.0 && (act != "deathgasp"))
		return

	var/list/hearableemote = null
	var/list/heared = list()
	var/his = gender==MALE?"his":(gender==FEMALE?"her":"its")
	var/He = gender==MALE?"He":(gender==FEMALE?"She":"It")
	var/himself = gender==MALE?"himself":(gender==FEMALE?"herself":"itself")
	switch(act)
		if ("airguitar")
			if (!src.restrained())
				message = "<B>%knownface:1%</B> is strumming the air and headbanging like a safari chimp."
				m_type = 1

		if ("blink")
			message = "<B>%knownface:1%</B> blinks."
			m_type = 1

		if ("blink_r")
			message = "<B>%knownface:1%</B> blinks rapidly."
			m_type = 1

		if ("bow")
			if (!src.buckled)
				var/M = null
				if (param)
					for (var/mob/A in view(8, src))
						if (param == A.name)
							M = A
							break
				if (!M)
					param = null

				if (param)
					message = "<B>%knownface:1%</B> bows to [param]."
				else
					message = "<B>%knownface:1%</B> bows."
			m_type = 1

		if ("custom")
			if(!param)
				var/input = copytext(sanitize(input("Choose an emote to display.") as text|null),1,MAX_MESSAGE_LEN)
				if (!input)
					return
				var/input2 = input("Is this a visible or hearable emote?") in list("Visible","Hearable")
				if (input2 == "Visible")
					m_type = 1
				else if (input2 == "Hearable")
					if (src.miming)
						return
					m_type = 2
				else
					alert("Unable to use this emote, must be either hearable or visible.")
					return
				param = input
			message = "<B>%knownface:1%</B> [param]"

		if ("me")
			if(silent)
				return
			if (src.client)
				if (client.muted & MUTE_IC)
					src << "\red You cannot send IC messages (muted)."
					return
				if (src.client.handle_spam_prevention(message,MUTE_IC))
					return
			if (stat)
				return
			if(!(message))
				return
			else
				message = "<B>%knownface:1%</B> [message]"

		if ("salute")
			if (!src.buckled)
				var/M = null
				if (param)
					for (var/mob/A in view(8, src))
						if (param == A.name)
							M = A
							break
						if(ishuman(A) && src:mind)
							if(param in dd_text2list(src:mind.known_faces[A:dna.get_unique_face()], " "))
								M = A
								break
							if(param in dd_text2list(A:name, " "))
								M = A
								break
				if (!M)
					param = null

				if (param)
					message = "<B>%knownface:1%</B> salutes to [param]."
				else
					message = "<B>%knownface:1%</b> salutes."
			m_type = 1

		if ("choke")
			if (!muzzled)
				message = "<B>%knownface:1%</B> chokes!"
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a strong noise."
				m_type = 2

		if ("clap")
			if (!src.restrained())
				message = "<B>%knownface:1%</B> claps."
				m_type = 2
/*  lolwut?
		if ("flap")
			if (!src.restrained())
				message = "<B>%knownface:1%</B> flaps his wings."
				m_type = 2

		if ("aflap")
			if (!src.restrained())
				message = "<B>%knownface:1%</B> flaps his wings ANGRILY!"
				m_type = 2
*/
		if ("drool")
			message = "<B>%knownface:1%</B> drools."
			m_type = 1

		if ("eyebrow")
			message = "<B>%knownface:1%</B> raises an eyebrow."
			m_type = 1

		if ("chuckle")
			if (!muzzled)
				message = "<B>%knownface:1%</B> chuckles."
				hearableemote = list("voice"=voice_name,"msg"="chuckles.")
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a noise."
				m_type = 2

		if ("twitch")
			message = "<B>%knownface:1%</B> twitches violently."
			m_type = 1

		if ("twitch_s")
			message = "<B>%knownface:1%</B> twitches."
			m_type = 1

		if ("faint")
			message = "<B>%knownface:1%</B> faints."
			if(src.sleeping)
				return //Can't faint while asleep
			src.sleeping += 10 //Short-short nap
			m_type = 1

		if ("cough")
			if (!muzzled)
				message = "<B>%knownface:1%</B> coughs!"
				hearableemote = list("voice"=voice_name,"msg"="coughs!")
				m_type = 2
				spread_diseases = 2
			else
				message = "<B>%knownface:1%</B> makes a strong noise."
				m_type = 2

		if ("frown")
			message = "<B>%knownface:1%</B> frowns."
			m_type = 1

		if ("nod")
			message = "<B>%knownface:1%</B> nods."
			m_type = 1

		if ("blush")
			message = "<B>%knownface:1%</B> blushes."
			m_type = 1

		if ("wave")
			message = "<B>%knownface:1%</B> waves."
			m_type = 1

		if ("gasp")
			if(hold_breath)
				hold_breath = 0
				hud_holdbreath.icon_state = "breathing"
				src << "\red You are unable to hold your breath any longer!"
			if (!muzzled)
				message = "<B>%knownface:1%</B> gasps!"
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a weak noise."
				m_type = 2

		if ("deathgasp")
			message = "<B>%knownface:1%</B> seizes up and falls limp, [his] eyes dead and lifeless..."
			m_type = 1

		if ("giggle")
			if (!muzzled)
				message = "<B>%knownface:1%</B> giggles."
				hearableemote = list("voice"=voice_name,"msg"="giggles.")
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a noise."
				m_type = 2

		if ("glare")
			var/M = null
			if (param)
				for (var/mob/A in view(8, src))
					if (param == A.name)
						M = A
						break
					if(ishuman(A) && src:mind)
						if(param in dd_text2list(src:mind.known_faces[A:dna.get_unique_face()], " "))
							M = A
							break
						if(param in dd_text2list(A:name, " "))
							M = A
							break
			if (!M)
				param = null

			if (param)
				message = "<B>%knownface:1%</B> glares at [param]."
			else
				message = "<B>%knownface:1%</B> glares."

		if ("stare")
			var/M = null
			if (param)
				for (var/mob/A in view(8, src))
					if (param == A.name)
						M = A
						break
					if(ishuman(A) && src:mind)
						if(param in dd_text2list(src:mind.known_faces[A:dna.get_unique_face()], " "))
							M = A
							break
						if(param in dd_text2list(A:name, " "))
							M = A
							break
			if (!M)
				param = null

			if (param)
				message = "<B>%knownface:1%</B> stares at [param]."
			else
				message = "<B>%knownface:1%</B> stares."

		if ("look")
			var/M = null
			if (param)
				for (var/mob/A in view(8, src))
					if (param == A.name)
						M = A
						break
					if(ishuman(A) && src:mind)
						if(param in dd_text2list(src:mind.known_faces[A:dna.get_unique_face()], " "))
							M = A
							break
						if(param in dd_text2list(A:name, " "))
							M = A
							break

			if (!M)
				param = null

			if (param)
				message = "<B>%knownface:1%</B> looks at [param]."
			else
				message = "<B>%knownface:1%</B> looks."
			m_type = 1

		if ("grin")
			message = "<B>%knownface:1%</B> grins."
			m_type = 1

		if ("cry")
			if (!muzzled)
				message = "<B>%knownface:1%</B> cries."
				hearableemote = list("voice"=voice_name,"msg"="cries.")
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a weak noise. [He] frowns."
				m_type = 2

		if ("sigh")
			if (!muzzled)
				message = "<B>%knownface:1%</B> sighs."
				hearableemote = list("voice"=voice_name,"msg"="sighs.")
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a weak noise."
				m_type = 2

		if ("laugh")
			if (!muzzled)
				message = "<B>%knownface:1%</B> laughs."
				hearableemote = list("voice"=voice_name,"msg"="laughs.")
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a noise."
				m_type = 2

		if ("mumble")
			message = "<B>%knownface:1%</B> mumbles!"
			m_type = 2

		if ("grumble")
			if (!muzzled)
				message = "<B>%knownface:1%</B> grumbles!"
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a noise."
				m_type = 2

		if ("groan")
			if (!muzzled)
				message = "<B>%knownface:1%</B> groans!"
				hearableemote = list("voice"=voice_name,"msg"="groans!")
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a loud noise."
				m_type = 2

		if ("moan")
			message = "<B>%knownface:1%</B> moans!"
			hearableemote = list("voice"=voice_name,"msg"="moans!")
			m_type = 2

/*		if ("johnny")
			var/M
			if (param)
				M = param
			if (!M)
				param = null
			else
				message = "<B>%knownface:1%</B> says, \"[M], please. He had a family.\" [src.name] takes a drag from a cigarette and blows his name out in smoke."
				m_type = 2
*/
		if ("point")
			if (!src.restrained())
				var/mob/M = null
				if (param)
					for (var/atom/A as mob|obj|turf|area in view(8, src))
						if (param == A.name)
							M = A
							break
						if(ishuman(A) && src:mind)
							var/mob/living/carbon/human/H = A
							if(param in dd_text2list(src:mind.known_faces[H.dna.get_unique_face()], " "))
								M = A
								break
							if(param in dd_text2list(A.name, " "))
								M = A
								break

				if (!M)
					message = "<B>%knownface:1%</B> points."
				else
					M.point()
					message = "<B>%knownface:1%</B> points to [M]."
			m_type = 1

		if ("raise")
			if (!src.restrained())
				message = "<B>%knownface:1%</B> raises a hand."
			m_type = 1

		if("shake")
			message = "<B>%knownface:1%</B> shakes [his] head."
			m_type = 1

		if ("shrug")
			message = "<B>%knownface:1%</B> shrugs."
			m_type = 1

		if ("signal")
			if (!src.restrained())
				var/t1 = round(text2num(param))
				if (isnum(t1))
					if (t1 <= 5 && (!src.r_hand || !src.l_hand))
						message = "<B>%knownface:1%</B> raises [t1] finger\s."
					else if (t1 <= 10 && (!src.r_hand && !src.l_hand))
						message = "<B>%knownface:1%</B> raises [t1] finger\s."
			m_type = 1

		if ("smile")
			message = "<B>%knownface:1%</B> smiles."
			m_type = 1

		if ("shiver")
			message = "<B>%knownface:1%</B> shivers."
			m_type = 2

		if ("pale")
			message = "<B>%knownface:1%</B> goes pale for a second."
			m_type = 1

		if ("tremble")
			message = "<B>%knownface:1%</B> trembles in fear!"
			m_type = 1

		if ("sneeze")
			if (!muzzled)
				message = "<B>%knownface:1%</B> sneezes."
				hearableemote = list("voice"=voice_name,"msg"="sneezes.")
				spread_diseases = 3
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a strange noise."
				m_type = 2

		if ("sniff")
			message = "<B>%knownface:1%</B> sniffs."
			m_type = 2

		if ("snore")
			if (!muzzled)
				message = "<B>%knownface:1%</B> snores."
				hearableemote = list("voice"=voice_name,"msg"="snores.")
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a noise."
				m_type = 2

		if ("whimper")
			if (!muzzled)
				message = "<B>%knownface:1%</B> whimpers."
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a weak noise."
				m_type = 2

		if ("wink")
			message = "<B>%knownface:1%</B> winks."
			m_type = 1

		if ("yawn")
			if (!muzzled)
				message = "<B>%knownface:1%</B> yawns."
				m_type = 2

		if ("collapse")
			Paralyse(2)
			message = "<B>%knownface:1%</B> collapses!"
			m_type = 2

		if("hug")
			m_type = 1
			if (!src.restrained())
				var/M = null
				if (param)
					for (var/mob/A in view(1, src))
						if (param == A.name)
							M = A
							break
						if(ishuman(A) && src:mind)
							var/mob/living/carbon/human/H = A
							if(param in dd_text2list(src:mind.known_faces[H.dna.get_unique_face()], " "))
								M = A
								break
							if(param in dd_text2list(A:name, " "))
								M = A
								break
				if (M == src)
					M = null

				if (M)
					message = "<B>%knownface:1%</B> hugs [M]."
				else
					message = "<B>%knownface:1%</B> hugs [himself]."

		if ("handshake")
			m_type = 1
			if (!src.restrained() && !src.r_hand)
				var/mob/M = null
				if (param)
					for (var/mob/A in view(1, src))
						if (param == A.name)
							M = A
							break
						if(ishuman(A) && src:mind)
							var/mob/living/carbon/human/H = A
							if(param in dd_text2list(src:mind.known_faces[H.dna.get_unique_face()], " "))
								M = A
								break
							if(param in dd_text2list(A:name, " "))
								M = A
								break
				if (M == src)
					M = null

				if (M)
					if (M.canmove && !M.r_hand && !M.restrained())
						message = "<B>%knownface:1%</B> shakes hands with [M]."
					else
						message = "<B>%knownface:1%</B> holds out [his] hand to [M]."

		if("daps")
			m_type = 1
			if (!src.restrained())
				var/M = null
				if (param)
					for (var/mob/A in view(1, src))
						if (param == A.name)
							M = A
							break
						if(ishuman(A) && src:mind)
							var/mob/living/carbon/human/H = A
							if(param in dd_text2list(src:mind.known_faces[H.dna.get_unique_face()], " "))
								M = A
								break
							if(param in dd_text2list(A:name, " "))
								M = A
								break
				if (M)
					message = "<B>%knownface:1%</B> gives daps to [M]."
				else
					message = "<B>%knownface:1%</B> sadly can't find anybody to give daps to, and daps [himself]. Shameful."

		if ("scream")
			if (!muzzled)
				message = "<B>%knownface:1%</B> screams!"
				hearableemote = list("voice"=voice_name,"msg"="screams!")
				for(var/obj/item/device/radio/R in src.contents)
					heared += R
					R.talk_into(src,hearableemote)
				m_type = 2
			else
				message = "<B>%knownface:1%</B> makes a very loud noise."
				m_type = 2

		if ("vomit")
			if(lastpuke || isCyborg()) return
			lastpuke = 1
			Stun(5)

			message = "<spawn class='warning'>%knownface:1% throws up!"
			m_type = 1
			playsound(loc, 'sound/effects/splat.ogg', 50, 1)

			var/turf/location = loc
			if (istype(location, /turf/simulated))
				location.add_vomit_floor(src, 1)

			spread_diseases = 1
			nutrition -= 40
			adjustToxLoss(-1)
			spawn(200)
				lastpuke = 0

		if ("help")
			usr << "blink, blink_r, blush, bow-(none)/mob, burp, choke, chuckle, clap, collapse, cough,\ncry, custom, deathgasp, drool, eyebrow, frown, gasp, giggle, groan, grumble, handshake, hug-(none)/mob, glare-(none)/mob,\ngrin, laugh, look-(none)/mob, moan, mumble, nod, pale, point-atom, raise, salute, shake, shiver, shrug,\nsigh, signal-#1-10, smile, sneeze, sniff, snore, stare-(none)/mob, tremble, twitch, twitch_s, whimper,\nwink, yawn"

		else
			usr << "\blue Unusable emote '[act]'. Say *help for a list."





	if (message)
		log_emote("[name]/[key] : [dd_replacetext_case(message, "%knownface:1%", real_name)]")

 //Hearing gasp and such every five seconds is not good emotes were not global for a reason.
 // Maybe some people are okay with that.

		for(var/mob/M in dead_mob_list)
			if (!M.client || istype(M, /mob/new_player))
				continue //skip monkeys, leavers and new players
			if(M.stat == 2 && M.client.ghost_sight && !(M in viewers(src,null)))
				M.show_message(message, actors=list(src))


		if (m_type & 1)
			for (var/mob/O in viewers(world.view,src))
				O.show_message(message, m_type, actors=list(src))
		else if (m_type & 2)
			for (var/mob/O in get_mobs_in_hear(world.view,src))
				O.show_message(message, m_type, actors=list(src))

			if(hearableemote)
				var/message_range = 3 //constant for now
				var/list/W = hear(message_range, get_turf(src))

				for (var/obj/O in contents)
					W |= O

				for (var/mob/M in W)
					W |= M.contents

				for (var/atom/A in W)
					if(istype(A, /obj/)) //radio in pocket could work, radio in backpack wouldn't --rastaf0
						var/obj/O = A
						spawn (0)
							if(O && !istype(O.loc, /obj/item/weapon/storage))
								O.hear_talk(src, hearableemote)

	if(spread_diseases>-1)
		spread_viruses(src.viruses,spread_diseases, src.loc)
