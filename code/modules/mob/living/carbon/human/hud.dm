//
/datum/hud/proc/human_hud(var/ui_style='icons/mob/screen1_old.dmi')

	src.adding = list()
	src.other = list()
	src.hotkeybuttons = list() //These can be disabled for hotkey usersx

	var/obj/screen/using
	var/obj/screen/inventory/inv_box

	using = new /obj/screen()
	using.name = "act_intent"
	using.dir = SOUTHWEST
	using.icon = ui_style
	using.icon_state = "intent_"+mymob.a_intent
	using.screen_loc = ui_acti
	using.layer = 20
	src.adding += using
	action_intent = using

//intent small hud objects
	var/icon/ico

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),1,ico.Height()/2,ico.Width()/2,ico.Height())
	using = new /obj/screen( src )
	using.name = "help"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	help_intent = using

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),ico.Width()/2,ico.Height()/2,ico.Width(),ico.Height())
	using = new /obj/screen( src )
	using.name = "disarm"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	disarm_intent = using

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),ico.Width()/2,1,ico.Width(),ico.Height()/2)
	using = new /obj/screen( src )
	using.name = "grab"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	grab_intent = using

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),1,1,ico.Width()/2,ico.Height()/2)
	using = new /obj/screen( src )
	using.name = "harm"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	hurt_intent = using

//end intent small hud objects

	using = new /obj/screen()
	using.name = "mov_intent"
	using.dir = SOUTHWEST
	using.icon = ui_style
	using.icon_state = (mymob.m_intent == "run" ? "running" : "walking")
	using.screen_loc = ui_movi
	using.layer = 20
	src.adding += using
	move_intent = using

	using = new /obj/screen()
	using.name = "drop"
	using.icon = ui_style
	using.icon_state = "act_drop"
	using.screen_loc = ui_drop_throw
	using.layer = 19
	src.hotkeybuttons += using

	inv_box = new /obj/screen/inventory()
	inv_box.name = "i_clothing"
	inv_box.dir = SOUTH
	inv_box.icon = ui_style
	inv_box.slot_id = slot_w_uniform
	inv_box.icon_state = "center"
	inv_box.screen_loc = ui_iclothing
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "o_clothing"
	inv_box.dir = SOUTH
	inv_box.icon = ui_style
	inv_box.slot_id = slot_wear_suit
	inv_box.icon_state = "equip"
	inv_box.screen_loc = ui_oclothing
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "r_hand"
	inv_box.dir = WEST
	inv_box.icon = ui_style
	inv_box.icon_state = "hand_inactive"
	if(mymob && !mymob.hand)	//This being 0 or null means the right hand is in use
		inv_box.icon_state = "hand_active"
	inv_box.screen_loc = ui_rhand
	inv_box.slot_id = slot_r_hand
	inv_box.layer = 19
	src.r_hand_hud_object = inv_box
	src.adding += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "l_hand"
	inv_box.dir = EAST
	inv_box.icon = ui_style
	inv_box.icon_state = "hand_inactive"
	if(mymob && mymob.hand)	//This being 1 means the left hand is in use
		inv_box.icon_state = "hand_active"
	inv_box.screen_loc = ui_lhand
	inv_box.slot_id = slot_l_hand
	inv_box.layer = 19
	src.l_hand_hud_object = inv_box
	src.adding += inv_box

	using = new /obj/screen/inventory()
	using.name = "hand"
	using.dir = SOUTH
	using.icon = ui_style
	using.icon_state = "hand1"
	using.screen_loc = ui_swaphand1
	using.layer = 19
	src.adding += using

	using = new /obj/screen/inventory()
	using.name = "hand"
	using.dir = SOUTH
	using.icon = ui_style
	using.icon_state = "hand2"
	using.screen_loc = ui_swaphand2
	using.layer = 19
	src.adding += using

	inv_box = new /obj/screen/inventory()
	inv_box.name = "id"
	inv_box.dir = NORTH
	inv_box.icon = ui_style
	inv_box.icon_state = "id"
	inv_box.screen_loc = ui_id
	inv_box.slot_id = slot_wear_id
	inv_box.layer = 19
	src.adding += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "mask"
	inv_box.dir = NORTH
	inv_box.icon = ui_style
	inv_box.icon_state = "equip"
	inv_box.screen_loc = ui_mask
	inv_box.slot_id = slot_wear_mask
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "back"
	inv_box.dir = NORTH
	inv_box.icon = ui_style
	inv_box.icon_state = "back"
	inv_box.screen_loc = ui_back
	inv_box.slot_id = slot_back
	inv_box.layer = 19
	src.adding += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "storage1"
	inv_box.icon = ui_style
	inv_box.icon_state = "pocket"
	inv_box.screen_loc = ui_storage1
	inv_box.slot_id = slot_l_store
	inv_box.layer = 19
	src.adding += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "storage2"
	inv_box.icon = ui_style
	inv_box.icon_state = "pocket"
	inv_box.screen_loc = ui_storage2
	inv_box.slot_id = slot_r_store
	inv_box.layer = 19
	src.adding += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "suit storage"
	inv_box.icon = ui_style
	inv_box.dir = 8 //The sprite at dir=8 has the background whereas the others don't.
	inv_box.icon_state = "belt"
	inv_box.screen_loc = ui_sstore1
	inv_box.slot_id = slot_s_store
	inv_box.layer = 19
	src.adding += inv_box

	using = new /obj/screen()
	using.name = "resist"
	using.icon = ui_style
	using.icon_state = "act_resist"
	using.screen_loc = ui_pull_resist
	using.layer = 19
	src.hotkeybuttons += using

	using = new /obj/screen()
	using.name = "other"
	using.icon = ui_style
	using.icon_state = "other"
	using.screen_loc = ui_inventory
	using.layer = 20
	src.adding += using

	using = new /obj/screen()
	using.name = "equip"
	using.icon = ui_style
	using.icon_state = "act_equip"
	using.screen_loc = ui_equip
	using.layer = 20
	src.adding += using

	inv_box = new /obj/screen/inventory()
	inv_box.name = "gloves"
	inv_box.icon = ui_style
	inv_box.icon_state = "gloves"
	inv_box.screen_loc = ui_gloves
	inv_box.slot_id = slot_gloves
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "eyes"
	inv_box.icon = ui_style
	inv_box.icon_state = "glasses"
	inv_box.screen_loc = ui_glasses
	inv_box.slot_id = slot_glasses
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "ears"
	inv_box.icon = ui_style
	inv_box.icon_state = "ears"
	inv_box.screen_loc = ui_ears
	inv_box.slot_id = slot_ears
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "head"
	inv_box.icon = ui_style
	inv_box.icon_state = "hair"
	inv_box.screen_loc = ui_head
	inv_box.slot_id = slot_head
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "shoes"
	inv_box.icon = ui_style
	inv_box.icon_state = "shoes"
	inv_box.screen_loc = ui_shoes
	inv_box.slot_id = slot_shoes
	inv_box.layer = 19
	src.other += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "belt"
	inv_box.icon = ui_style
	inv_box.icon_state = "belt"
	inv_box.screen_loc = ui_belt
	inv_box.slot_id = slot_belt
	inv_box.layer = 19
	src.adding += inv_box

	mymob.hud_throw_icon = new /obj/screen()
	mymob.hud_throw_icon.icon = ui_style
	mymob.hud_throw_icon.icon_state = "act_throw_off"
	mymob.hud_throw_icon.name = "throw"
	mymob.hud_throw_icon.screen_loc = ui_drop_throw
	src.hotkeybuttons += mymob.hud_throw_icon

//	mymob.hud_oxygen = new /obj/screen()
//	mymob.hud_oxygen.icon = ui_style
//	mymob.hud_oxygen.icon_state = "oxy0"
//	mymob.hud_oxygen.name = "oxygen"
//	mymob.hud_oxygen.screen_loc = ui_oxygen

//	mymob.hud_pressure = new /obj/screen()
//	mymob.hud_pressure.icon = ui_style
//	mymob.hud_pressure.icon_state = "pressure0"
//	mymob.hud_pressure.name = "pressure"
//	mymob.hud_pressure.screen_loc = ui_pressure

//	mymob.hud_toxin = new /obj/screen()
//	mymob.hud_toxin.icon = ui_style
//	mymob.hud_toxin.icon_state = "tox0"
//	mymob.hud_toxin.name = "toxin"
//	mymob.hud_toxin.screen_loc = ui_toxin

	mymob.hud_internals = new /obj/screen()
	mymob.hud_internals.icon = ui_style
	mymob.hud_internals.icon_state = "internal0"
	mymob.hud_internals.name = "internal"
	mymob.hud_internals.screen_loc = ui_internal

//	mymob.hud_fire = new /obj/screen()
//	mymob.hud_fire.icon = ui_style
//	mymob.hud_fire.icon_state = "fire0"
//	mymob.hud_fire.name = "fire"
//	mymob.hud_fire.screen_loc = ui_fire

//	mymob.hud_bodytemp = new /obj/screen()
//	mymob.hud_bodytemp.icon = ui_style
//	mymob.hud_bodytemp.icon_state = "temp1"
//	mymob.hud_bodytemp.name = "body temperature"
//	mymob.hud_bodytemp.screen_loc = ui_temp

	mymob.hud_healths = new /obj/screen()
	mymob.hud_healths.icon = ui_style
	mymob.hud_healths.icon_state = "health0"
	mymob.hud_healths.name = "health"
	mymob.hud_healths.screen_loc = ui_health

	if(ishuman(mymob))
		mymob:hud_holdbreath = new /obj/screen()
		mymob:hud_holdbreath.icon = 'icons/mob/stamina.dmi'
		mymob:hud_holdbreath.icon_state = "breathing"
		mymob:hud_holdbreath.name = "hold breath"
		mymob:hud_holdbreath.screen_loc = ui_holdbreath

		mymob:hud_stamina = new /obj/screen()
		mymob:hud_stamina.icon = 'icons/mob/stamina.dmi'
		mymob:hud_stamina.icon_state = "stamina10"
		mymob:hud_stamina.name = "stamina"
		mymob:hud_stamina.screen_loc = ui_stamina

//	mymob.hud_nutrition_icon = new /obj/screen()
//	mymob.hud_nutrition_icon.icon = ui_style
//	mymob.hud_nutrition_icon.icon_state = "nutrition0"
//	mymob.hud_nutrition_icon.name = "nutrition"
//	mymob.hud_nutrition_icon.screen_loc = ui_nutrition

	mymob.hud_pullin = new /obj/screen()
	mymob.hud_pullin.icon = ui_style
	mymob.hud_pullin.icon_state = "pull0"
	mymob.hud_pullin.name = "pull"
	mymob.hud_pullin.screen_loc = ui_pull_resist
	src.hotkeybuttons += mymob.hud_pullin

	mymob.hud_blind = new /obj/screen()
	mymob.hud_blind.icon = 'icons/mob/screen1_full.dmi'
	mymob.hud_blind.icon_state = "blackimageoverlay"
	mymob.hud_blind.name = " "
	mymob.hud_blind.screen_loc = "1,1"
	mymob.hud_blind.mouse_opacity = 0
	mymob.hud_blind.layer = 0

	mymob.hud_damageoverlay = new /obj/screen()
	mymob.hud_damageoverlay.icon = 'icons/mob/screen1_full.dmi'
	mymob.hud_damageoverlay.icon_state = "oxydamageoverlay0"
	mymob.hud_damageoverlay.name = "dmg"
	mymob.hud_damageoverlay.screen_loc = "1,1"
	mymob.hud_damageoverlay.mouse_opacity = 0
	mymob.hud_damageoverlay.layer = 18.1 //The black screen overlay sets layer to 18 to display it, this one has to be just on top.

	mymob.hud_flash = new /obj/screen()
	mymob.hud_flash.icon = ui_style
	mymob.hud_flash.icon_state = "blank"
	mymob.hud_flash.name = "flash"
	mymob.hud_flash.screen_loc = "1,1 to 15,15"
	mymob.hud_flash.layer = 17

	mymob.hud_pain = new /obj/screen( null )

/*
	mymob.hands = new /obj/screen( null )
	mymob.hands.icon = ui_style
	mymob.hands.icon_state = "hand"
	mymob.hands.name = "hand"
	mymob.hands.screen_loc = ui_hand
	mymob.hands.dir = NORTH

	mymob.sleep = new /obj/screen( null )
	mymob.sleep.icon = ui_style
	mymob.sleep.icon_state = "sleep0"
	mymob.sleep.name = "sleep"
	mymob.sleep.screen_loc = ui_sleep

	mymob.rest = new /obj/screen( null )
	mymob.rest.icon = ui_style
	mymob.rest.icon_state = "rest0"
	mymob.rest.name = "rest"
	mymob.rest.screen_loc = ui_rest
*/

	/*/Monkey blockers

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_ears
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_belt
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_shoes
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_storage2
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_glasses
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_gloves
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_storage1
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_headset
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_oclothing
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_iclothing
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_id
	using.layer = 20
	src.mon_blo += using

	using = new /obj/screen( src )
	using.name = "blocked"
	using.icon_state = "blocked"
	using.screen_loc = ui_head
	using.layer = 20
	src.mon_blo += using
//Monkey blockers
*/

	mymob.zone_sel = new /obj/screen/zone_sel( null )
	ico = new/icon(ui_style, "zone_sel")
	ico.Scale(128,128)
	mymob.zone_sel.icon = ico
	mymob.zone_sel.overlays = null
	mymob.zone_sel.overlays += image('icons/mob/zone_sel.dmi', "[mymob.zone_sel.selecting]")

	mymob.client.screen = null

	mymob.client.screen += list( mymob.hud_throw_icon, mymob.zone_sel, /*mymob.hud_oxygen, mymob.hud_pressure, mymob.hud_toxin, mymob.hud_bodytemp,*/ mymob.hud_internals, /*mymob.hud_fire,*/ mymob.hud_healths, /*mymob.hud_nutrition_icon,*/ mymob.hud_pullin, mymob.hud_blind, mymob.hud_flash, mymob.hud_damageoverlay) //, mymob.hands, mymob.rest, mymob.sleep) //, mymob.mach )
	mymob.client.screen += src.adding + src.hotkeybuttons
	if(ishuman(mymob))
		mymob.client.screen += mymob:hud_stamina
		mymob.client.screen += mymob:hud_holdbreath
	inventory_shown = 0;

	return


/mob/living/carbon/human/verb/toggle_hotkey_verbs()
	set category = "OOC"
	set name = "Toggle hotkey buttons"
	set desc = "This disables or enables the user interface buttons which can be used with hotkeys."

	if(hud_used.hotkey_ui_hidden)
		client.screen += src.hud_used.hotkeybuttons
		src.hud_used.hotkey_ui_hidden = 0
	else
		client.screen -= src.hud_used.hotkeybuttons
		src.hud_used.hotkey_ui_hidden = 1



/*

Radar-related things

*/

/mob/living/carbon/human/proc/close_radar()
	radar_open = 0
	for(var/obj/screen/x in client.screen)
		if( (x.name == "radar" && x.icon == 'icons/misc/radar.dmi') || (x in radar_blips) )
			client.screen -= x
			del(x)

	place_radar_closed()

/mob/living/carbon/human/proc/place_radar_closed()
	var/obj/screen/closedradar = new()
	closedradar.icon = 'icons/misc/radar.dmi'
	closedradar.icon_state = "radarclosed"
	closedradar.screen_loc = "WEST,NORTH-1"
	closedradar.name = "radar closed"
	client.screen += closedradar

/mob/living/carbon/human/proc/start_radar()

	for(var/obj/screen/x in client.screen)
		if(x.name == "radar closed" && x.icon == 'icons/misc/radar.dmi')
			client.screen -= x
			del(x)

	var/obj/screen/cornerA = new()
	cornerA.icon = 'icons/misc/radar.dmi'
	cornerA.icon_state = "radar(1,1)"
	cornerA.screen_loc = "WEST,NORTH-2"
	cornerA.name = "radar"

	var/obj/screen/cornerB = new()
	cornerB.icon = 'icons/misc/radar.dmi'
	cornerB.icon_state = "radar(2,1)"
	cornerB.screen_loc = "WEST+1,NORTH-2"
	cornerB.name = "radar"

	var/obj/screen/cornerC = new()
	cornerC.icon = 'icons/misc/radar.dmi'
	cornerC.icon_state = "radar(1,2)"
	cornerC.screen_loc = "WEST,NORTH-1"
	cornerC.name = "radar"

	var/obj/screen/cornerD = new()
	cornerD.icon = 'icons/misc/radar.dmi'
	cornerD.icon_state = "radar(2,2)"
	cornerD.screen_loc = "WEST+1,NORTH-1"
	cornerD.name = "radar"

	client.screen += cornerA
	client.screen += cornerB
	client.screen += cornerC
	client.screen += cornerD

	radar_open = 1

	while(radar_open && (RADAR in augmentations))
		update_radar()
		sleep(6)

/mob/living/carbon/human/proc/update_radar()

	if(!client) return
	var/list/found_targets = list()

	var/max_dist = 29 // 29 tiles is the max distance

	// If the mob is inside a turf, set the center to the object they're in
	var/atom/distance_ref = src
	if(!isturf(src.loc))
		distance_ref = loc

	// Clear the radar_blips cache
	for(var/x in radar_blips)
		client.screen -= x
		del(x)
	radar_blips = list()

	var/starting_px = 3
	var/starting_py = 3

	for(var/mob/living/M in orange(max_dist, distance_ref))
		if(M.stat == 2) continue
		found_targets.Add(M)

	for(var/obj/effect/critter/C in orange(max_dist, distance_ref))
		if(!C.alive) continue
		found_targets.Add(C)

	for(var/obj/mecha/M in orange(max_dist, distance_ref))
		if(!M.occupant) continue
		found_targets.Add(M)

	for(var/obj/structure/closet/C in orange(max_dist, distance_ref))
		for(var/mob/living/M in C.contents)
			if(M.stat == 2) continue
			found_targets.Add(M)

	// Loop through all living mobs in a range.
	for(var/atom/A in found_targets)

		var/a_x = A.x
		var/a_y = A.y

		if(!isturf(A.loc))
			a_x = A.loc.x
			a_y = A.loc.y

		var/blip_x = max_dist + (-( distance_ref.x-a_x ) ) + starting_px
		var/blip_y = max_dist + (-( distance_ref.y-a_y ) ) + starting_py
		var/obj/screen/blip = new()
		blip.icon = 'icons/misc/radar.dmi'
		blip.name = "Blip"
		blip.layer = 21
		blip.screen_loc = "WEST:[blip_x-1],NORTH-2:[blip_y-1]" // offset -1 because the center of the blip is not at the bottomleft corner (14)

		if(istype(A, /mob/living))
			var/mob/living/M = A
			if(ishuman(M))
				if(M:wear_id)
					var/job = M:wear_id:GetJobName()
					if(job == "Security Officer")
						blip.icon_state = "secblip"
						blip.name = "Security Officer"
					else if(job == "Captain" || job == "Research Director" || job == "Chief Engineer" || job == "Chief Medical Officer" || job == "Head of Security" || job == "Head of Personnel")
						blip.icon_state = "headblip"
						blip.name = "Station Head"
					else
						blip.icon_state = "civblip"
						blip.name = "Civilian"
				else
					blip.icon_state = "civblip"
					blip.name = "Civilian"

			else if(issilicon(M))
				blip.icon_state = "roboblip"
				blip.name = "Robotic Organism"

			else
				blip.icon_state = "unknownblip"
				blip.name = "Unknown Organism"

		else if(istype(A, /obj/effect/critter))
			blip.icon_state = "unknownblip"
			blip.name = "Unknown Organism"

		else if(istype(A, /obj/mecha))
			blip.icon_state = "roboblip"
			blip.name = "Robotic Organism"

		radar_blips.Add(blip)
		client.screen += blip


/mob/living/carbon/human/update_action_buttons()
	var/pos = 1
	if(!src.hud_used) return
	if(!src.client) return

	if(!hud_used.hud_shown)	//Hud toggled to minimal
		return

	src.client.screen -= src.hud_used.item_action_list
	hud_used.item_action_list = list()

	for(var/obj/item/I in src)
		if(I.icon_action_button)
			var/obj/screen/item_action/A = new(src.hud_used)
			A.icon = 'icons/mob/screen1_action.dmi'
			A.icon_state = "action_blank"
//			A.icon_state = I.icon_action_button
//			if(I.icon_action_button == "action_blank")
//				A.overlays += I
			A.overlays += I
			if(I.action_button_name)
				A.name = I.action_button_name
			else
				A.name = "Use [I.name]"
			A.owner = I
			hud_used.item_action_list += A
			A.screen_loc = ui_action_slots
			if(pos>=5) break
			pos++

	src.client.screen += src.hud_used.item_action_list
