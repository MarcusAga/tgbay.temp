/mob/living/carbon/human/attack_paw(mob/M as mob)
	..()
	if (M.a_intent == "help")
		help_shake_act(M)
	else
		if (hasvar(M,wear_mask) && istype(M:wear_mask, /obj/item/clothing/mask/muzzle))
			return

		for(var/mob/O in viewers(src, null))
			O.show_message(text("\red <B>[M.name] has bit []!</B>", src), 1)

		var/damage = rand(1, 3)
		var/dam_zone = pick("chest", "l_hand", "r_hand", "l_leg", "r_leg")
		var/datum/organ/external/affecting = get_organ(ran_zone(dam_zone))
		apply_damage(damage, BRUTE, affecting, run_armor_check(affecting, "melee"))

/*		for(var/datum/disease/D in M.viruses)
			if(istype(D, /datum/disease/jungle_fever))
				var/mob/living/carbon/human/H = src
				src = null
				src = H.monkeyize()
				contract_disease(D,1,0)*/
	return

/mob/living/carbon/human/attack_animal(mob/living/simple_animal/M as mob)
	if(M.melee_damage_upper == 0 || M.a_intent == "help")
		M.emote("[M.friendly] %knownface:1%", actors=list(src))
	else
		if(M.attack_sound)
			playsound(loc, M.attack_sound, 50, 1, 1)
		for(var/mob/O in viewers(src, null))
			O.show_message("\red <B>[M]</B> [M.attacktext] [src]!", 1)
		M.attack_log += text("\[[time_stamp()]\] <font color='red'>attacked [src.name] ([src.ckey])</font>")
		src.attack_log += text("\[[time_stamp()]\] <font color='orange'>was attacked by [M.name] ([M.ckey])</font>")
		var/damage = rand(M.melee_damage_lower, M.melee_damage_upper)
		var/dam_zone = pick("chest", "l_hand", "r_hand", "l_leg", "r_leg")
		var/datum/organ/external/affecting = get_organ(ran_zone(dam_zone))
		apply_damage(damage, BRUTE, affecting, run_armor_check(affecting, "melee"))
		if(istype(M,/mob/living/simple_animal/hostile/zombie) && prob(33))
			src.contract_disease(new /datum/disease2/disease/overtaking/zombism,1)
