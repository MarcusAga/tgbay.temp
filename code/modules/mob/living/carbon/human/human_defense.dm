/*
Contains most of the procs that are called when a mob is attacked by something

bullet_act
ex_act
meteor_act
emp_act

*/

/mob/living/carbon/human/bullet_act(var/obj/item/projectile/P, var/def_zone)
	if(!P.nodamage && P.firer && P.firer!=src && anal_moderation)
		P.firer << "Behold the power of anal moderation!"
		if(istype(P.firer,/mob/living/carbon/human))
			P.firer:brain_explode()
		else
			P.firer.gib()

	if(wear_suit && istype(wear_suit, /obj/item/clothing/suit/armor/laserproof))
		if(istype(P, /obj/item/projectile/energy) || istype(P, /obj/item/projectile/beam))
			var/reflectchance = 40 - round(P.damage/3)
			if(!(def_zone in list("chest", "groin")))
				reflectchance /= 2
			if(prob(reflectchance))
				visible_message("\red <B>The [P.name] gets reflected by %knownface:1%'s [wear_suit.name]!</B>", actors=list(src))

				// Find a turf near or on the original location to bounce to
				if(P.starting)
					var/new_x = P.starting.x + pick(0, 0, 0, 0, 0, -1, 1, -2, 2)
					var/new_y = P.starting.y + pick(0, 0, 0, 0, 0, -1, 1, -2, 2)
					var/turf/curloc = get_turf(src)

					// redirect the projectile
					P.original = locate(new_x, new_y, P.z)
					P.starting = curloc
					P.current = curloc
					P.firer = src
					P.yo = new_y - curloc.y
					P.xo = new_x - curloc.x

				return -1 // complete projectile permutation

	var/deflect_skill = 0
	if(TK in src.mutations)
		deflect_skill++
	if(JediReflect in src.mutations)
		deflect_skill++
	if(REFLEXES in src.augmentations)
		deflect_skill++
	if(deflect_skill)
		var/deflect
		if(l_hand)
			if(istype(l_hand,/obj/item/weapon/melee/energy/sword) && l_hand:active)
				if(prob(75*deflect_skill/3)) deflect = l_hand
			if(istype(l_hand,/obj/item/weapon/twohanded/dualsaber) && l_hand:wielded)
				if(prob(85*deflect_skill/3)) deflect = l_hand
		if(r_hand)
			if(istype(r_hand,/obj/item/weapon/melee/energy/sword) && r_hand:active)
				if(prob(75*deflect_skill/3)) deflect = r_hand
			if(istype(r_hand,/obj/item/weapon/twohanded/dualsaber) && r_hand:wielded)
				if(prob(85*deflect_skill/3)) deflect = r_hand
		if(deflect && (istype(P, /obj/item/projectile/energy) || istype(P, /obj/item/projectile/beam) || prob(40)))
			visible_message("\red <B>%knownface:1% skillfully deflects [P.name] by [deflect]!</B>", actors=list(src))
	
			// Find a turf near or on the original location to bounce to
			if(P.starting)
				var/new_x = P.starting.x + pick(0, 0, 0, 0, 0, -1, 1, -2, 2)
				var/new_y = P.starting.y + pick(0, 0, 0, 0, 0, -1, 1, -2, 2)
				var/turf/curloc = get_turf(src)
	
				// redirect the projectile
				P.original = locate(new_x, new_y, P.z)
				P.starting = curloc
				P.current = curloc
				P.firer = src
				P.yo = new_y - curloc.y
				P.xo = new_x - curloc.x
				var/r = (abs(P.yo)+abs(P.xo)/3)
				P.xo+=rand(-r,r)
				P.yo+=rand(-r,r)
	
			return -1 // complete projectile permutation

	if(get_species() == "Dragon")
		P.damage *= 0.5

	if(check_shields(P.damage, "the [P.name]"))
		P.on_hit(src, 2)
		return 2
	return (..(P , def_zone))


/mob/living/carbon/human/getarmor(var/def_zone, var/type)
	var/armorval = 0
	var/organnum = 0

	if(def_zone)
		if(isorgan(def_zone))
			return checkarmor(def_zone, type)
		var/datum/organ/external/affecting = get_organ(ran_zone(def_zone))
		return checkarmor(affecting, type)
		//If a specific bodypart is targetted, check how that bodypart is protected and return the value.

	//If you don't specify a bodypart, it checks ALL your bodyparts for protection, and averages out the values
	for(var/datum/organ/external/organ in organs)
		armorval += checkarmor(organ, type)
		organnum++
	return (armorval/max(organnum, 1))


/mob/living/carbon/human/proc/checkarmor(var/datum/organ/external/def_zone, var/type)
	if(!type)	return 0
	var/protection = 0
	var/list/body_parts = list(head, wear_mask, wear_suit, w_uniform)
	for(var/bp in body_parts)
		if(!bp)	continue
		if(bp && istype(bp ,/obj/item/clothing))
			var/obj/item/clothing/C = bp
			if(C.body_parts_covered & def_zone.body_part)
				protection += C.armor[type]
	return protection


/mob/living/carbon/human/proc/checkcoverage(var/zone, var/precision)
	var/datum/organ/external/def_zone
	if(isorgan(zone))
		def_zone = zone
	else
		def_zone = get_organ(zone)
	var/list/body_parts = list(head, wear_mask, wear_suit, w_uniform, shoes, gloves, glasses)
	for(var/bp in body_parts)
		if(!bp)	continue
		if(istype(bp, /obj/item/clothing))
			var/obj/item/clothing/C = bp
			if(precision)
				if(precision=="mouth")
					if(bp==wear_mask && C.flags&MASKCOVERSMOUTH)
						return C
					if(bp==head && C.flags&HEADCOVERSMOUTH)
						return C
				else if(precision=="eyes")
					if(bp==glasses && C.flags&GLASSESCOVERSEYES)
						return C
					if(bp==wear_mask && C.flags&MASKCOVERSEYES)
						return C
					if(bp==head && C.flags&HEADCOVERSEYES)
						return C
				else if(C.body_parts_covered & def_zone.body_part)
					return C
			else if(C.body_parts_covered & def_zone.body_part)
				return C
	return null


/mob/living/carbon/human/proc/check_shields(var/damage = 0, var/attack_text = "the attack")
	if(l_hand && istype(l_hand, /obj/item/weapon))//Current base is the prob(50-d/3)
		var/obj/item/weapon/I = l_hand
		if(I.IsShield() && (prob(50 - round(damage / 3))))
			visible_message("\red <B>%knownface:1% blocks [attack_text] with the [l_hand.name]!</B>", actors=list(src))
			return 1
	if(r_hand && istype(r_hand, /obj/item/weapon))
		var/obj/item/weapon/I = r_hand
		if(I.IsShield() && (prob(50 - round(damage / 3))))
			visible_message("\red <B>%knownface:1% blocks [attack_text] with the [r_hand.name]!</B>", actors=list(src))
			return 1
	if(wear_suit && istype(wear_suit, /obj/item/))
		var/obj/item/I = wear_suit
		if(I.IsShield() && (prob(35)))
			visible_message("\red <B>The reactive teleport system flings %knownface:1% clear of [attack_text]!</B>", actors=list(src))
			var/list/turfs = new/list()
			for(var/turf/T in orange(6))
				if(istype(T,/turf/space)) continue
				if(T.density) continue
				if(T.x>world.maxx-6 || T.x<6)	continue
				if(T.y>world.maxy-6 || T.y<6)	continue
				turfs += T
			if(!turfs.len) turfs += pick(/turf in orange(6))
			var/turf/picked = pick(turfs)
			if(!isturf(picked)) return
			src.loc = picked
			return 1
	return 0

/mob/living/carbon/human/emp_act(severity)
	for(var/obj/O in src)
		if(!O)	continue
		O.emp_act(severity)
	for(var/datum/organ/external/O  in organs)
		if(O.status & ORGAN_DESTROYED)	continue
		O.emp_act(severity)
	if(isCyborg())
		hallucination+=50*severity
		druggy+=5*severity
		step(src,pick(cardinal))
		Weaken(10)
	..()


/mob/living/carbon/human/proc/attacked_by(var/obj/item/I, var/mob/living/user, var/def_zone)
	if(nodamage) return 0
	if(!I || !user)	return 0

	if(user!=src && anal_moderation)
		user << "Behold the power of anal moderation!"
		if(istype(user,/mob/living/carbon/human))
			user:brain_explode()
		else
			user.gib()

	var/target_zone
	if(ishuman(user))
		var/skill_diff = src.getSkill("combat")-user:getSkill("combat")
		target_zone = get_zone_with_miss_chance(user.zone_sel.selecting, src, skill_diff*10)
	else
		target_zone = get_zone_with_miss_chance(user.zone_sel.selecting, src, 0)
	if(!target_zone)
		visible_message("\red <B>%knownface:1% misses %knownface:2% with \the [I]!", actors=list(user,src))
		return
		
	var/datum/organ/external/affecting = get_organ(target_zone)
	if (!affecting)
		return
	if(affecting.status & ORGAN_DESTROYED)
		user << "What [affecting.display_name]?"
		return
	var/hit_area = affecting.display_name

	if((user != src) && check_shields(I.force, "the [I.name]"))
		return 0

	var/disarm = 0
	if(user.a_intent=="disarm")
		disarm = 1

	if(disarm)
		visible_message("\red <B>%knownface:2% tried to disarm %knownface:1% with [I.name] on [hit_area]!</B>", actors=list(src, user))
	else if(I.attack_verb.len)
		visible_message("\red <B>%knownface:1% has been [pick(I.attack_verb)] in the [hit_area] with [I.name] by %knownface:2%!</B>", actors=list(src, user))
	else
		visible_message("\red <B>%knownface:1% has been attacked in the [hit_area] with [I.name] by %knownface:2%!</B>", actors=list(src, user))

	var/armor = run_armor_check(affecting, "melee", "Your armor has protected your [hit_area].", "Your armor has softened hit to your [hit_area].")
	if(armor >= 2)	return 0
	if(!I.force)	return 0

	var/force = I.force
	if(get_species() == "Dragon")
		force *= 0.5

	if(disarm)
		force*=2
	else
		apply_damage(force, I.damtype, affecting, armor , I.sharp, I.name)

	var/bloody = 0
	if(((I.damtype == BRUTE) || (I.damtype == HALLOSS)) && prob(25 + (force * 2)))
		if(!disarm)
			I.add_blood(src)	//Make the weapon bloody, not the person.
//		if(user.hand)	user.update_inv_l_hand()	//updates the attacker's overlay for the (now bloodied) weapon
//		else			user.update_inv_r_hand()	//removed because weapons don't have on-mob blood overlays
		if(!disarm && prob(33))
			bloody = 1
			var/turf/location = loc
			if(istype(location, /turf/simulated))
				location.add_blood(src)
			if(ishuman(user))
				var/mob/living/carbon/human/H = user
				if(get_dist(H, src) > 1) //people with TK won't get smeared with blood
					H.bloody_body(src)
					H.bloody_hands(src)

		switch(hit_area)
			if("head")//Harder to score a stun but if you do it lasts a bit longer
				if(prob(force))
					apply_effect(20, PARALYZE, armor)
					visible_message("\red <B>%knownface:1% has been knocked unconscious!</B>", actors=list(src))
					if(src != user && I.damtype == BRUTE)
						ticker.mode.remove_revolutionary(mind)

				if(bloody)//Apply blood
					if(wear_mask)
						wear_mask.add_blood(src)
						update_inv_wear_mask(0)
					if(head)
						head.add_blood(src)
						update_inv_head(0)
					if(glasses && prob(33))
						glasses.add_blood(src)
						update_inv_glasses(0)

			if("chest")//Easier to score a stun but lasts less time
				if(prob(force + 10))
					apply_effect(5, WEAKEN, armor)
					visible_message("\red <B>%knownface:1% has been knocked down!</B>", actors=list(src))

				if(bloody)
					bloody_body(src)

/mob/living/carbon/human/proc/bloody_hands(var/mob/living/source, var/amount = 2)
	if (gloves)
		gloves.add_blood(source)
		gloves:transfer_blood = amount
		gloves:bloody_hands_mob = source
	else
		add_blood(source)
		bloody_hands = amount
		bloody_hands_mob = source
	update_inv_gloves()		//updates on-mob overlays for bloody hands and/or bloody gloves

/mob/living/carbon/human/proc/bloody_body(var/mob/living/source)
	if(wear_suit)
		wear_suit.add_blood(source)
		update_inv_wear_suit(0)
	if(w_uniform)
		w_uniform.add_blood(source)
		update_inv_w_uniform(0)
