//This file was auto-corrected by findeclaration.exe on 25.5.2012 20:42:32

//NOTE: Breathing happens once per FOUR TICKS, unless the last breath fails. In which case it happens once per ONE TICK! So oxyloss healing is done once per 4 ticks while oxyloss damage is applied once per tick!
#define HUMAN_MAX_OXYLOSS 7 //Defines how much oxyloss humans can get per tick. A tile with no air at all (such as space) applies this value, otherwise it's a percentage of it.
#define HUMAN_CRIT_MAX_OXYLOSS 7 //The amount of damage you'll get when in critical condition. We want this to be a 5 minute deal = 300s. There are 100HP to get through, so (1/3)*last_tick_duration per second. Breaths however only happen every 4 ticks.

#define HEAT_DAMAGE_LEVEL_1 2 //Amount of damage applied when your body temperature just passes the 360.15k safety point
#define HEAT_DAMAGE_LEVEL_2 4 //Amount of damage applied when your body temperature passes the 400K point
#define HEAT_DAMAGE_LEVEL_3 8 //Amount of damage applied when your body temperature passes the 1000K point

#define COLD_DAMAGE_LEVEL_1 1 //Amount of damage applied when your body temperature just passes the 260.15k safety point
#define COLD_DAMAGE_LEVEL_2 2 //Amount of damage applied when your body temperature passes the 200K point
#define COLD_DAMAGE_LEVEL_3 4 //Amount of damage applied when your body temperature passes the 120K point

//Note that gas heat damage is only applied once every FOUR ticks.
#define HEAT_GAS_DAMAGE_LEVEL_1 2 //Amount of damage applied when the current breath's temperature just passes the 360.15k safety point
#define HEAT_GAS_DAMAGE_LEVEL_2 4 //Amount of damage applied when the current breath's temperature passes the 400K point
#define HEAT_GAS_DAMAGE_LEVEL_3 8 //Amount of damage applied when the current breath's temperature passes the 1000K point

#define COLD_GAS_DAMAGE_LEVEL_1 1 //Amount of damage applied when the current breath's temperature just passes the 260.15k safety point
#define COLD_GAS_DAMAGE_LEVEL_2 2 //Amount of damage applied when the current breath's temperature passes the 200K point
#define COLD_GAS_DAMAGE_LEVEL_3 4 //Amount of damage applied when the current breath's temperature passes the 120K point

var/const/BLOOD_VOLUME_SAFE = 501
var/const/BLOOD_VOLUME_OKAY = 336
var/const/BLOOD_VOLUME_BAD = 224
var/const/BLOOD_VOLUME_SURVIVE = 122

/mob/living/carbon/human
	var/oxygen_alert = 0
	var/toxins_alert = 0
	var/fire_alert = 0
	var/prev_gender = null // Debug for plural genders
	var/temperature_alert = 0
	var/mpain_counter = 0
	var/shock_stage_announced = 0

/mob/living/carbon/human/Life()
	set invisibility = 0
	set background = 1

	if (monkeyizing)	return
	if(!loc)			return	// Fixing a null error that occurs when the mob isn't found in the world -- TLE

	..()

	/*
	//This code is here to try to determine what causes the gender switch to plural error. Once the error is tracked down and fixed, this code should be deleted
	//Also delete var/prev_gender once this is removed.
	if(prev_gender != gender)
		prev_gender = gender
		if(gender in list(PLURAL, NEUTER))
			message_admins("[src] ([ckey]) gender has been changed to plural or neuter. Please record what has happened recently to the person and then notify coders. (<A HREF='?src=%holder_ref%;adminmoreinfo=\ref[src]'>?</A>)  (<A HREF='?src=%holder_ref%;adminplayervars=\ref[src]'>VV</A>) (<A HREF='?src=%admin_ref%;priv_msg=\ref[src]'>PM</A>) (<A HREF='?src=%holder_ref%;adminplayerobservejump=\ref[src]'>JMP</A>)",1,1) //The 1,1 at the end is there to make '%holder_ref%' get replaced with the actual ref object
	*/
	//Apparently, the person who wrote this code designed it so that
	//blinded get reset each cycle and then get activated later in the
	//code. Very ugly. I dont care. Moving this stuff here so its easy
	//to find it.
	blinded = null
	fire_alert = 0 //Reset this here, because both breathe() and handle_environment() have a chance to set it.

	//TODO: seperate this out
	// update the current life tick, can be used to e.g. only do something every 4 ticks
	life_tick++
	var/datum/gas_mixture/environment = loc.return_air()

	//No need to update all of these procs if the guy is dead.
	var/isCyborg = isCyborg()
	if(stat != DEAD)
		breath_cycle++
		if(!isCyborg() && (breath_cycle%2 == 0 || failed_last_breath)) 	//First, resolve location and get a breath
			makeBreath()
			breath_cycle = 0
		else //Still give containing object the chance to interact
			if(istype(loc, /obj/))
				var/obj/location_as_object = loc
				location_as_object.handle_internal_lifeform(src, 0)

		if(!isCyborg)
			handle_breath()

		//Disease Check
			handle_virus_updates()

		//Updates the number of stored chemicals for powers
			handle_changeling()

		//Mutations and radiation
			handle_mutations_and_radiation()
		else
			for(var/datum/disease2/disease/D in viruses)
				D.cure()

		//Chemicals in the body
		handle_chemicals_in_body()

		//Disabilities
		handle_disabilities()

		//Random events (vomiting etc)
		handle_random_events()

	if(!isCyborg)
	//Handle temperature/pressure differences between body and environment
		handle_environment(environment)

	//stuff in the stomach
		handle_stomach()

		handle_shock()

		handle_pain()
	else
		nutrition = min(nutrition, 600)
		toxloss = 0
		cloneloss = 0
		shock_stage = max(0,shock_stage-1)
		if(fakeAI)
			fakeAI.loc = src
			fakeAI.oxyloss = oxyloss
			fakeAI.bruteloss = bruteloss
			fakeAI.fireloss = fireloss
			fakeAI.toxloss = toxloss
			fakeAI.stat = stat
			fakeAI.updatehealth()

	if(l_hand && prob(5))
		l_hand.add_fibers(src)
	if(r_hand && prob(5))
		r_hand.add_fibers(src)

	if(stat==DEAD || isCyborg)
		adjust_temperature_to_environment(environment)

	//Status updates, death etc.
	handle_regular_status_updates()		//TODO: optimise ~Carn
	update_canmove()

	//Update our name based on whether our face is obscured/disfigured
	name = get_visible_name()

	handle_regular_hud_updates()

	// Grabbing
	for(var/obj/item/weapon/grab/G in src)
		G.process()


/mob/living/carbon/human/calculate_affecting_pressure(var/pressure)
	..()
	var/pressure_difference = abs( pressure - ONE_ATMOSPHERE )

	var/pressure_adjustment_coefficient = 1	//Determins how much the clothing you are wearing protects you in percent.
	if(wear_suit && wear_suit:stops_pressure_damage)
		pressure_adjustment_coefficient -= PRESSURE_SUIT_REDUCTION_COEFFICIENT
	if(head && head:stops_pressure_damage)
		pressure_adjustment_coefficient -= PRESSURE_HEAD_REDUCTION_COEFFICIENT
	pressure_adjustment_coefficient = max(pressure_adjustment_coefficient,0) //So it isn't less than 0
	pressure_difference = pressure_difference * pressure_adjustment_coefficient
	if(pressure > ONE_ATMOSPHERE)
		return ONE_ATMOSPHERE + pressure_difference
	else
		return ONE_ATMOSPHERE - pressure_difference

/mob/living/carbon/human

	proc/handle_blood()
		if(isCyborg())
			vessel.del_reagent("blood")
			return
		// take care of blood and blood loss
		if(stat < 2 && bodytemperature >= 170)
			var/blood_volume = round(vessel.get_reagent_amount("blood"))
			if(blood_volume < 560 && blood_volume)
				var/datum/reagent/blood/B = locate() in vessel.reagent_list //Grab some blood
				if(B) // Make sure there's some blood at all
					if(src != B.data["donor"]) //If it's not theirs, then we look for theirs
						for(var/datum/reagent/blood/D in vessel.reagent_list)
							if(src == D.data["donor"])
								B = D
								break
					var/datum/reagent/nutriment/F = locate() in reagents.reagent_list
					var/ratio = 1
					if(get_species()=="Tajaran")
						ratio = 1.3
					if(F != null)
						if(F.volume >= 1)
							// nutriment speeds it up quite a bit
							B.volume += 0.4*ratio
							F.volume -= 0.1
					else
						//At this point, we dun care which blood we are adding to, as long as they get more blood.
						B.volume = B.volume + 0.1*ratio // regenerate blood VERY slowly
					if(prob(5))
						vessel.update_total()

			switch(blood_volume)
				if(BLOOD_VOLUME_SAFE to 10000)
					if(pale)
						pale = 0
						update_body()
				if(BLOOD_VOLUME_OKAY to BLOOD_VOLUME_SAFE)
					if(!pale)
						pale = 1
						update_body()
						var/word = pick("dizzy","woosey","faint")
						src << "\red You feel [word]"
					if(prob(1))
						var/word = pick("dizzy","woosey","faint")
						src << "\red You feel [word]"
					if(oxyloss < 20)
						// hint that they're getting close to suffocation
						oxyloss += 3
				if(BLOOD_VOLUME_BAD to BLOOD_VOLUME_OKAY)
					if(!pale)
						pale = 1
						update_body()
					eye_blurry += 6
					if(oxyloss < 50)
						oxyloss += 10
					oxyloss += 1
					if(prob(15))
						Paralyse(rand(1,3))
						var/word = pick("dizzy","woosey","faint")
						src << "\red You feel extremely [word]"
				if(BLOOD_VOLUME_SURVIVE to BLOOD_VOLUME_BAD)
					oxyloss += 5
					toxloss += 5
					if(prob(15))
						var/word = pick("dizzy","woosey","faint")
						src << "\red You feel extremely [word]"
				if(0 to BLOOD_VOLUME_SURVIVE)
					// There currently is a strange bug here. If the mob is not below -100 health
					// when death() is called, apparently they will be just fine, and this way it'll
					// spam deathgasp. Adjusting toxloss ensures the mob will stay dead.
					toxloss += 300 // just to be safe!
					death()

			// Without enough blood you slowly go hungry.
			if(blood_volume < BLOOD_VOLUME_SAFE)
				if(nutrition >= 300)
					nutrition -= 10
				else if(nutrition >= 200)
					nutrition -= 3

		if(bodytemperature >= 170)
			var/blood_max = 0
			for(var/datum/organ/external/temp in organs)
				if(!(temp.status & ORGAN_BLEEDING) || (temp.status & ORGAN_ROBOT))
					continue
				if(temp.parent && (temp.parent.status & (ORGAN_ROBOT|ORGAN_DESTROYED)))
					continue
				for(var/datum/wound/W in temp.wounds) if(W.bleeding())
					blood_max += W.damage / 4
				if(temp.status & ORGAN_DESTROYED && !(temp.status & ORGAN_GAUZED))
					blood_max += 10*(temp.amputated?0.25:1) //Yer missing a fucking limb.
			if(blood_max)
				drip(blood_max)

	proc/handle_disabilities()
		if (disabilities & EPILEPSY)
			if ((prob(1) && paralysis < 1))
				src << "\red You have a seizure!"
				for(var/mob/O in viewers(src, null))
					if(O == src)
						continue
					O.show_message(text("\red <B>[src] starts having a seizure!"), 1)
				Paralyse(10)
				make_jittery(1000)
		if (disabilities & COUGHING)
			if ((prob(5) && paralysis <= 1))
				drop_item()
				spawn( 0 )
					emote("cough")
					return
		if (disabilities & TOURETTES)
			if ((prob(10) && paralysis <= 1))
				Stun(10)
				spawn( 0 )
					switch(rand(1, 3))
						if(1)
							emote("twitch")
						if(2 to 3)
							say("[prob(50) ? ";" : ""][pick("������", "�����", "�����", "������", "�����", "������", "�����", "�� ���� ����")]")
					var/old_x = pixel_x
					var/old_y = pixel_y
					pixel_x += rand(-4,4)
					pixel_y += rand(-2,2)
					sleep(2)
					pixel_x = old_x
					pixel_y = old_y
					return
		if (disabilities & NERVOUS)
			if (prob(10))
				stuttering = max(10, stuttering)
		if (getBrainLoss() >= 60 && stat != 2)
			if (prob(7))
				switch(pick(1,2,3))
					if(1)
						say(pick("� - �����, �����������", "��� ��������� ���� �� ������?", "������� - ������", "����� ������������ - �����", "����� ������ ��� [pick("���������","�����","���������")]?", "� ���� ��������� ����������", "������ ������� ����!!!!", "���������#", "��� ��������!", "��������� �������"))
					if(2)
						say(pick("FUS RO DAH", "��������!!!", "������� ������� ����� �������", "�� ����������!!", "�����������!!", "��� �������� ��� �� ��� ������", "�� ��� �����? �����, �� ��������!", "� ���� ������� ����� �����!", "����� ��� ������!", "������, ����������!", "�������� �������", "����� ����!"))
					if(3)
						emote("drool")

	proc/handle_organs()
		// take care of organ related updates, such as broken and missing limbs

		// recalculate number of wounds
		number_wounds = 0
		for(var/datum/organ/external/E in organs)
			if(!E)
				world << name
				continue
			number_wounds += E.number_wounds

		var/datum/organ/external/head/Head = get_organ("head")
		if(!Head || (Head.status & (ORGAN_DESTROYED|ORGAN_HUSKED)) && !Head.headless)
			adjustOxyLoss(100)
			death()

		var/leg_tally = 2
		var/canstand_l = 1  //Can stand on left leg
		var/canstand_r = 1  //Can stand on right leg
		var/hasleg_l = 1  //Have left leg
		var/hasleg_r = 1  //Have right leg
		var/hasarm_l = 1  //Have left arm
		var/hasarm_r = 1  //Have right arm
		for(var/datum/organ/external/E in organs)
			E.process()
			if(E.status & ORGAN_ROBOT && prob(E.brute_dam + E.burn_dam))
				if(E.name == "l_hand" || E.name == "l_arm")
					if(hand && equipped())
						drop_item()
						emote("me", 1, "drops what they were holding, their [E.display_name?"[E.display_name]":"[E]"] malfunctioning!")
						var/datum/effect/effect/system/spark_spread/spark_system = new /datum/effect/effect/system/spark_spread()
						spark_system.set_up(5, 0, src)
						spark_system.attach(src)
						spark_system.start()
						spawn(10)
							del(spark_system)
				else if(E.name == "r_hand" || E.name == "r_arm")
					if(!hand && equipped())
						drop_item()
						emote("me", 1, "drops what they were holding, their [E.display_name?"[E.display_name]":"[E]"] malfunctioning!")
						var/datum/effect/effect/system/spark_spread/spark_system = new /datum/effect/effect/system/spark_spread()
						spark_system.set_up(5, 0, src)
						spark_system.attach(src)
						spark_system.start()
						spawn(10)
							del(spark_system)
				else if(E.name == "l_leg" || E.name == "l_foot" \
					|| E.name == "r_leg" || E.name == "r_foot" && !lying)
					leg_tally--									// let it fail even if just foot&leg
			if(E.status & ORGAN_BROKEN || (E.status & ORGAN_DESTROYED && !E.amputated) || E.status & ORGAN_HUSKED)
				if(E.name == "l_hand" || E.name == "l_arm")
					if(hand && equipped())
						if(E.status & ORGAN_HUSKED)
							drop_item()
						else if(!(E.status & ORGAN_SPLINTED) || prob(5))
							drop_item()
							emote("scream")
				else if(E.name == "r_hand" || E.name == "r_arm")
					if(!hand && equipped())
						if(E.status & ORGAN_HUSKED)
							drop_item()
						else if(!(E.status & ORGAN_SPLINTED) || prob(5))
							drop_item()
							emote("scream")
				else if(E.name == "l_leg" || E.name == "l_foot" \
					|| E.name == "r_leg" || E.name == "r_foot" && !lying)
					if((E.status & ORGAN_HUSKED) || !(E.status & ORGAN_SPLINTED))
						leg_tally--									// let it fail even if just foot&leg

		// standing is poor
		if(leg_tally <= 0 && !paralysis && !(lying || resting) && prob(5))
			emote("scream")
			emote("collapse")
			paralysis = 10


		//Check arms and legs for existence
		var/datum/organ/external/E
		E = get_organ("l_leg")
		if(E && (E.status & ORGAN_DESTROYED && !(E.status & ORGAN_SPLINTED)) || (E.status&ORGAN_HUSKED))
			canstand_l = 0
			hasleg_l = 0
		E = get_organ("r_leg")
		if(E && (E.status & ORGAN_DESTROYED && !(E.status & ORGAN_SPLINTED)) || (E.status&ORGAN_HUSKED))
			canstand_r = 0
			hasleg_r = 0
		E = get_organ("l_foot")
		if(E && (E.status & ORGAN_DESTROYED && !(E.status & ORGAN_SPLINTED)) || (E.status&ORGAN_HUSKED))
			canstand_l = 0
		E = get_organ("r_foot")
		if(E && (E.status & ORGAN_DESTROYED && !(E.status & ORGAN_SPLINTED)) || (E.status&ORGAN_HUSKED))
			canstand_r = 0
		E = get_organ("l_arm")
		if(E && (E.status & ORGAN_DESTROYED && !(E.status & ORGAN_SPLINTED)) || (E.status&ORGAN_HUSKED))
			hasarm_l = 0
		E = get_organ("r_arm")
		if(E && (E.status & ORGAN_DESTROYED && !(E.status & ORGAN_SPLINTED)) || (E.status&ORGAN_HUSKED))
			hasarm_r = 0

		// Can stand if have at least one full leg (with leg and foot parts present)
		// Has limbs to move around if at least one arm or leg is at least partially there
		can_stand = canstand_l||canstand_r
		has_limbs = hasleg_l||hasleg_r||hasarm_l||hasarm_r


	proc/handle_mutations_and_radiation()
		if(dna.time_to_genetic_recheck>-1 && dna.time_to_genetic_recheck<=world.time)
			dna.time_to_genetic_recheck = -1
			if(dna.original_face)
				if(dna.unique_enzymes!=dna.original_unique_enzymes)
					if( (!wear_mask || !(wear_mask.flags_inv&HIDEFACE)) && (!head || !(head.flags_inv&HIDEFACE)))
						src.visible_message("\red %knownface:1% face changes!", actors=list(src))
				dna.original_face = 0
				updateappearance(src)

			domutcheck(src,null,1)

		if(getFireLoss())
			if((COLD_RESISTANCE in mutations) || (prob(1)))
				heal_organ_damage(0,1)

		if(mHallucination in mutations)
			hallucination = 100
			halloss = 0

		if(mSmallsize in mutations)
			if(!(pass_flags & PASSTABLE))
				pass_flags |= PASSTABLE
		else
			if(pass_flags & PASSTABLE)
				pass_flags &= ~PASSTABLE

		// Make nanoregen heal youu, -3 all damage types
		if((NANOREGEN in augmentations) || (mRegen in mutations) || (get_species() == "Dragon"))
			var/healed = 0
			var/hptoreg = 0
			if(NANOREGEN in augmentations)
				hptoreg += 3
			if(mRegen in mutations)
				hptoreg += 2
			if(get_species() == "Dragon" || get_species() == "Slime")
				hptoreg += 1
			if(stat==UNCONSCIOUS)
				if(hptoreg>=2 || prob(50)) hptoreg/=2
			if(stat==DEAD) hptoreg=0

			for(var/i=1, i<=hptoreg, i++)
				var/list/damages = new/list()
				if(getToxLoss())
					damages+="tox"
				if(getOxyLoss())
					damages+="oxy"
				if(getCloneLoss())
					damages+="clone"
				if(getBruteLoss())
					damages+="brute"
				if(getFireLoss())
					damages+="burn"
				if(halloss != 0)
					damages+="hal"

				if(damages.len)
					switch(pick(damages))
						if("tox")
							adjustToxLoss(-1)
							healed = 1
						if("oxy")
							adjustOxyLoss(-1)
							healed = 1
						if("clone")
							adjustCloneLoss(-1)
							healed = 1
						if("brute")
							heal_organ_damage(1,0)
							healed = 1
						if("burn")
							heal_organ_damage(0,1)
							healed = 1
						if("hal")
							if(halloss > 0)
								halloss -= 1
							if(halloss < 0)
								halloss = 0
							healed = 1
				else
					break

			if(healed)
				if(prob(5))
					src << "\blue You feel your wounds mending..."

		if(!(/mob/living/carbon/human/proc/morph in src.verbs))
			if(mMorph in mutations)
				src.verbs += /mob/living/carbon/human/proc/morph
		else
			if(!(mMorph in mutations))
				src.verbs -= /mob/living/carbon/human/proc/morph

		if(!(/mob/living/carbon/human/proc/remoteobserve in src.verbs))
			if(mRemote in mutations)
				src.verbs += /mob/living/carbon/human/proc/remoteobserve
		else
			if(!(mRemote in mutations))
				src.verbs -= /mob/living/carbon/human/proc/remoteobserve

		if(!(/mob/living/carbon/human/proc/remotesay in src.verbs))
			if(mRemotetalk in mutations)
				src.verbs += /mob/living/carbon/human/proc/remotesay
		else
			if(!(mRemotetalk in mutations))
				src.verbs -= /mob/living/carbon/human/proc/remotesay

		if ((HULK in mutations) && health <= 25)
			mutations.Remove(HULK)
			update_mutations()		//update our mutation overlays
			src << "\red You suddenly feel very weak."
			Weaken(3)
			emote("collapse")

		if (radiation)
			if (radiation > 100)
				radiation = 100
				Weaken(10)
				src << "\red You feel weak."
				emote("collapse")

			if (radiation < 0)
				radiation = 0

			else
				var/damage = 0
				switch(radiation)
					if(1 to 49)
						radiation--
						if(prob(25))
							adjustToxLoss(1)
							damage = 1
							updatehealth()

					if(50 to 74)
						radiation -= 2
						damage = 1
						adjustToxLoss(1)
						if(prob(5))
							radiation -= 5
							Weaken(3)
							src << "\red You feel weak."
							emote("collapse")
						updatehealth()

					if(75 to 100)
						radiation -= 3
						adjustToxLoss(3)
						damage = 1
						if(prob(1))
							src << "\red You mutate!"
							randmutb(src)
							domutcheck(src,null)
							emote("gasp")
						updatehealth()

				if(damage && organs.len)
					var/datum/organ/external/O = pick(organs)
					if(istype(O)) O.add_autopsy_data("Radiation Poisoning", damage)

	proc/makeBreath(var/forcedBreath = 0)
		if(istype(loc,/obj/machinery/atmospherics/unary/cryo_cell) && !forcedBreath) return
		if(reagents.has_reagent("lexorin")) return

		if((!hold_breath || stat) && breath)
//		        var/leave_moles = breath.temperature>0 ? (2 / (R_IDEAL_GAS_EQUATION * breath.temperature)) : 0
//			var/datum/gas_mixture/removed = breath.remove_ratio(leave_moles/breath.total_moles())
//			loc.assume_air(removed)
			loc.assume_air(breath)
			breath = null

		if(!forcedBreath && health < config.health_threshold_crit && losebreath>=0)
			return

		var/lung_ruptured = is_lung_ruptured()

		if(lung_ruptured && prob(2))
			spawn emote("me", 1, "coughs up blood!")
			src.drip(10)

		var/datum/gas_mixture/new_breath = null
		if(health < 0 || losebreath < 0)
			losebreath++
		if(lung_ruptured && prob(4))
			spawn emote("me", 1, "gasps for air!")
			losebreath += 5
		if(!forcedBreath && losebreath>0) //Suffocating so do not take a breath
			losebreath--
			if (prob(10)) //Gasp per 10 ticks? Sounds about right.
				spawn emote("gasp")
			if(istype(loc, /obj/))
				var/obj/location_as_object = loc
				location_as_object.handle_internal_lifeform(src, 0)
		else if(forcedBreath || !hold_breath)
			//First, check for air from internal atmosphere (using an air tank and mask generally)
			new_breath = get_breath_from_internal(BREATH_VOLUME)
			//breath = get_breath_from_internal(0.5) // Manually setting to old BREATH_VOLUME amount -- TLE

			//No breath from internal atmosphere so get breath from location
			if(!new_breath)
				if(isobj(loc))
					var/obj/location_as_object = loc
					new_breath = location_as_object.handle_internal_lifeform(src, BREATH_VOLUME)
				else if(isturf(loc))
					new_breath = loc.remove_air_volume(BREATH_VOLUME)
					// Handle chem smoke effect  -- Doohl
					var/block = 0
					if(wear_mask)
						if(wear_mask.flags & BLOCK_GAS_SMOKE_EFFECT)
							block = 1
					if(glasses)
						if(glasses.flags & BLOCK_GAS_SMOKE_EFFECT)
							block = 1
					if(head)
						if(head.flags & BLOCK_GAS_SMOKE_EFFECT)
							block = 1

					if(!block)

						for(var/obj/effect/effect/chem_smoke/smoke in view(1, src))
							if(smoke.reagents.total_volume)
								smoke.reagents.reaction(src, INGEST)
								spawn(5)
									if(smoke)
										smoke.reagents.copy_to(src, 10) // I dunno, maybe the reagents enter the blood stream through the lungs?
								break // If they breathe in the nasty stuff once, no need to continue checking

				if(!new_breath || new_breath.total_moles() < BREATH_MOLES / 8)
					losebreath = max(2,losebreath)

				if(!lung_ruptured && new_breath)
					if(new_breath.total_moles() < BREATH_MOLES / 8 || new_breath.total_moles() > BREATH_MOLES * 5)
						if(prob(5) && !nodamage)
							rupture_lung()
			else //Still give containing object the chance to interact
				if(istype(loc, /obj/))
					var/obj/location_as_object = loc
					location_as_object.handle_internal_lifeform(src, 0)

		if(hold_breath && health<90 && prob(90-health) || stat)
			emote("gasp")

		if(new_breath)
//			breath.merge(new_breath)
			breath = new_breath


	proc/get_breath_from_internal(volume_needed)
		if(istype(wear_suit, /obj/item/clothing/suit/powered))
			var/obj/item/clothing/suit/powered/Suit = wear_suit
			if(Suit.helm && istype(Suit.atmoseal,/obj/item/powerarmor/atmoseal/ironman) && !Suit.atmoseal:face_open)
				var/datum/gas_mixture/GM = new
				GM.volume = volume_needed
				GM.temperature = T20C
				GM.oxygen = (ONE_ATMOSPHERE*GM.volume)/(R_IDEAL_GAS_EQUATION*GM.temperature)
				return GM
		if(internal)
			if (istype(internal,/obj/machinery/optable))
				if(get_turf(src)!=get_turf(internal))
					internal = null
			else if (!contents.Find(internal))
				internal = null
			if ((!wear_mask || !(wear_mask.flags & MASKINTERNALS)) && (!head || !(head.flags & MASKINTERNALS)))
				internal = null
			if(internal)
				return internal.remove_air_volume(volume_needed)
			else
				src << "You are no longer running on internals"
				if(hud_internals)
					hud_internals.icon_state = "internal0"
		return null

	proc/handle_breath()
		if(nodamage || (REBREATHER in augmentations) || (mNobreath in mutations))
			return

		if(!breath || (breath.total_moles() == 0) || suiciding)
			if(reagents.has_reagent("inaprovaline"))
				return
			if(suiciding)
				adjustOxyLoss(2)//If you are suiciding, you should die a little bit faster
				failed_last_breath = 1
				oxygen_alert = max(oxygen_alert, 1)
				return 0
			if(health > 0)
				adjustOxyLoss(HUMAN_MAX_OXYLOSS)
				failed_last_breath = 1
			else
				adjustOxyLoss(HUMAN_CRIT_MAX_OXYLOSS)
				failed_last_breath = 1

			oxygen_alert = max(oxygen_alert, 1)

			return 0

//		var/average_temperature = 0.25*breath.temperature + 0.75*src.bodytemperature
//		src.bodytemperature = average_temperature
//		breath.temperature = src.bodytemperature

		var/safe_oxygen_min = 16 // Minimum safe partial pressure of O2, in kPa
		//var/safe_oxygen_max = 140 // Maximum safe partial pressure of O2, in kPa (Not used for now)
		var/safe_co2_max = 10 // Yes it's an arbitrary value who cares?
		var/safe_toxins_max = 0.005
		var/SA_para_min = 1
		var/SA_sleep_min = 5
		var/oxygen_used = 0
		var/breath_pressure = (breath.total_moles()*R_IDEAL_GAS_EQUATION*breath.temperature)/BREATH_VOLUME

		//Partial pressure of the O2 in our breath
		var/O2_pp = (breath.oxygen/breath.total_moles())*breath_pressure
		// Same, but for the toxins
		var/Toxins_pp = (breath.toxins/breath.total_moles())*breath_pressure
		// And CO2, lets say a PP of more than 10 will be bad (It's a little less really, but eh, being passed out all round aint no fun)
		var/CO2_pp = (breath.carbon_dioxide/breath.total_moles())*breath_pressure // Tweaking to fit the hacky bullshit I've done with atmo -- TLE
		//var/CO2_pp = (breath.carbon_dioxide/breath.total_moles())*0.5 // The default pressure value

//		src << "Oxy: [O2_pp] = [breath.oxygen]/[breath.total_moles()]*[breath_pressure] < [safe_oxygen_min]"
		if(O2_pp < safe_oxygen_min) 			// Too little oxygen
			if(!hold_breath && prob(20))
				spawn(0) emote("gasp")
			var/ratio = sqrt((safe_oxygen_min-O2_pp)/safe_oxygen_min)
			adjustOxyLoss(min(5*ratio, HUMAN_MAX_OXYLOSS)) // Don't fuck them up too fast (space only does HUMAN_MAX_OXYLOSS after all!)
			failed_last_breath = 1
			oxygen_used = breath.oxygen*ratio/6
//			if(O2_pp > 0)
//				var/ratio = safe_oxygen_min/O2_pp
//				adjustOxyLoss(min(5*ratio, HUMAN_MAX_OXYLOSS)) // Don't fuck them up too fast (space only does HUMAN_MAX_OXYLOSS after all!)
//				failed_last_breath = 1
//				oxygen_used = breath.oxygen*ratio/6
//			else
//				adjustOxyLoss(HUMAN_MAX_OXYLOSS)
//				failed_last_breath = 1
			oxygen_alert = max(oxygen_alert, 1)
		/*else if (O2_pp > safe_oxygen_max) 		// Too much oxygen (commented this out for now, I'll deal with pressure damage elsewhere I suppose)
			spawn(0) emote("cough")
			var/ratio = O2_pp/safe_oxygen_max
			oxyloss += 5*ratio
			oxygen_used = breath.oxygen*ratio/6
			oxygen_alert = max(oxygen_alert, 1)*/
		else								// We're in safe limits
			failed_last_breath = 0
			adjustOxyLoss(-10)
			oxygen_used = breath.oxygen/6
			oxygen_alert = 0

		breath.oxygen -= oxygen_used
		breath.carbon_dioxide += oxygen_used

		//CO2 does not affect failed_last_breath. So if there was enough oxygen in the air but too much co2, this will hurt you, but only once per 4 ticks, instead of once per tick.
		if(CO2_pp > safe_co2_max)
			if(!co2overloadtime) // If it's the first breath with too much CO2 in it, lets start a counter, then have them pass out after 12s or so.
				co2overloadtime = world.time
			else if(world.time - co2overloadtime > 120)
				Paralyse(3)
				adjustOxyLoss(3) // Lets hurt em a little, let them know we mean business
				if(world.time - co2overloadtime > 300) // They've been in here 30s now, lets start to kill them for their own good!
					adjustOxyLoss(8)
			if(!hold_breath && prob(20)) // Lets give them some chance to know somethings not right though I guess.
				spawn(0) emote("cough")

		else
			co2overloadtime = 0

		if(Toxins_pp > safe_toxins_max) // Too much toxins
			var/ratio = breath.toxins/safe_toxins_max
			adjustToxLoss(min(ratio, MIN_PLASMA_DAMAGE))	//Limit amount of damage toxin exposure can do per second
			toxins_alert = max(toxins_alert, 1)
		else
			toxins_alert = 0

		if(breath.sleeping_agent > 0)	// If there's some other shit in the air lets deal with it here.
			var/SA_pp = (breath.sleeping_agent/breath.total_moles())*breath_pressure
			if(SA_pp > SA_para_min) // Enough to make us paralysed for a bit
				Paralyse(3) // 3 gives them one second to wake up and run away a bit!
				if(SA_pp > SA_sleep_min) // Enough to make us sleep as well
					sleeping = max(sleeping+2, 10)
			else if(SA_pp > 0.01)	// There is sleeping gas in their lungs, but only a little, so give them a bit of a warning
				if(prob(20))
					spawn(0) emote(pick("giggle", "laugh"))
			breath.sleeping_agent = 0

		if(istype(loc,/obj/machinery/atmospherics/unary/cryo_cell))
			return

		if( (abs(310.15 - breath.temperature) > 50) && !(COLD_RESISTANCE in mutations)) // Hot air hurts :(
			if(breath.temperature < 260.15)
				if(prob(20))
					src << "\red You feel your face freezing and an icicle forming in your lungs!"
			else if(breath.temperature > 360.15)
				if(prob(20))
					src << "\red You feel your face burning and a searing heat in your lungs!"

			switch(breath.temperature)
				if(-INFINITY to 120)
					apply_damage(COLD_GAS_DAMAGE_LEVEL_3, BURN, "head", used_weapon = "Excessive Cold")
					fire_alert = max(fire_alert, 1)
				if(120 to 200)
					apply_damage(COLD_GAS_DAMAGE_LEVEL_2, BURN, "head", used_weapon = "Excessive Cold")
					fire_alert = max(fire_alert, 1)
				if(200 to 260)
					apply_damage(COLD_GAS_DAMAGE_LEVEL_1, BURN, "head", used_weapon = "Excessive Cold")
					fire_alert = max(fire_alert, 1)
				if(360 to 400)
					apply_damage(HEAT_GAS_DAMAGE_LEVEL_1, BURN, "head", used_weapon = "Excessive Heat")
					fire_alert = max(fire_alert, 2)
				if(400 to 1000)
					apply_damage(HEAT_GAS_DAMAGE_LEVEL_2, BURN, "head", used_weapon = "Excessive Heat")
					fire_alert = max(fire_alert, 2)
				if(1000 to INFINITY)
					apply_damage(HEAT_GAS_DAMAGE_LEVEL_3, BURN, "head", used_weapon = "Excessive Heat")
					fire_alert = max(fire_alert, 2)

		//Temporary fixes to the alerts.

		return 1

	proc/handle_environment(datum/gas_mixture/environment)
		if(!environment)
			return
		var/loc_temp = T0C
		if(istype(loc, /obj/mecha))
			var/obj/mecha/M = loc
			loc_temp =  M.return_temperature()
		else if(istype(loc, /obj/machinery/atmospherics/unary/cryo_cell))
			if(loc:air_contents.total_moles() >= 10)
				loc_temp = loc:air_contents.temperature
			else
				loc_temp = bodytemperature
		else if(istype(get_turf(src), /turf/space))
			var/turf/heat_turf = get_turf(src)
			loc_temp = heat_turf.temperature
		else
			loc_temp = environment.temperature

		if(dna)
			switch(dna.mutantrace)
				if("lizard")
					loc_temp -= 25
				if("tajaran")
					loc_temp += 25
		//world << "Loc temp: [loc_temp] - Body temp: [bodytemperature] - Fireloss: [getFireLoss()] - Thermal protection: [get_thermal_protection()] - Fire protection: [thermal_protection + add_fire_protection(loc_temp)] - Heat capacity: [environment_heat_capacity] - Location: [loc] - src: [src]"

		//After then, it reacts to the surrounding atmosphere based on your thermal protection
		if(loc_temp < bodytemperature)
			//Place is colder than we are
			var/thermal_protection = get_cold_protection(loc_temp) //This returns a 0 - 1 value, which corresponds to the percentage of protection based on what you're wearing and what you're exposed to.
			if(thermal_protection < 1)
				bodytemperature += (1-thermal_protection) * ((loc_temp - bodytemperature) / BODYTEMP_COLD_DIVISOR)
		else
			//Place is hotter than we are
			var/thermal_protection = get_heat_protection(loc_temp) //This returns a 0 - 1 value, which corresponds to the percentage of protection based on what you're wearing and what you're exposed to.
			if(thermal_protection < 1)
				bodytemperature += (1-thermal_protection) * ((loc_temp - bodytemperature) / BODYTEMP_HEAT_DIVISOR)

		//Body temperature is adjusted in two steps. Firstly your body tries to stabilize itself a bit.
		if(stat != 2)
			stabilize_temperature_from_calories()

		// +/- 50 degrees from 310.15K is the 'safe' zone, where no damage is dealt.
		var/mod = 1
		if(get_species()=="Slime")
			mod = 2
		if(bodytemperature > 360.15)
			//Body temperature is too hot.
			fire_alert = max(fire_alert, 1)
			switch(bodytemperature)
				if(360 to 400)
					var/datum/organ/external/O = any_organ()
					if(istype(O)) O.add_autopsy_data("Heat", 5)
					apply_damage(HEAT_DAMAGE_LEVEL_1*mod, BURN, O)
					fire_alert = max(fire_alert, 2)
				if(400 to 1000)
					var/datum/organ/external/O = any_organ()
					if(istype(O)) O.add_autopsy_data("High Heat", 5)
					apply_damage(HEAT_DAMAGE_LEVEL_2*mod, BURN, O)
					fire_alert = max(fire_alert, 2)
				if(1000 to INFINITY)
					var/datum/organ/external/O = any_organ()
					if(istype(O)) O.add_autopsy_data("Excessive Heat", 5)
					apply_damage(HEAT_DAMAGE_LEVEL_3*mod, BURN, O)
					fire_alert = max(fire_alert, 2)

		else if(bodytemperature < 260.15)
			fire_alert = max(fire_alert, 1)
			if(!istype(loc, /obj/machinery/atmospherics/unary/cryo_cell))
				switch(bodytemperature)
					if(200 to 260)
						var/datum/organ/external/O = any_organ()
						if(istype(O)) O.add_autopsy_data("Cold", 5)
						apply_damage(COLD_DAMAGE_LEVEL_1*mod, BURN, O)
						fire_alert = max(fire_alert, 1)
					if(120 to 200)
						var/datum/organ/external/O = any_organ()
						if(istype(O)) O.add_autopsy_data("High Cold", 5)
						apply_damage(COLD_DAMAGE_LEVEL_2*mod, BURN, O)
						fire_alert = max(fire_alert, 1)
					if(-INFINITY to 120)
						var/datum/organ/external/O = any_organ()
						if(istype(O)) O.add_autopsy_data("Excessive Cold", 5)
						apply_damage(COLD_DAMAGE_LEVEL_3*mod, BURN, O)
						fire_alert = max(fire_alert, 1)

		// Account for massive pressure differences.  Done by Polymorph
		// Made it possible to actually have something that can protect against high pressure... Done by Errorage. Polymorph now has an axe sticking from his head for his previous hardcoded nonsense!

		var/pressure = environment.return_pressure()
		if(pressure > HAZARD_HIGH_PRESSURE)
			var/adjusted_pressure = calculate_affecting_pressure(pressure) //Returns how much pressure actually affects the mob.
			if(adjusted_pressure > HAZARD_HIGH_PRESSURE)
				adjustBruteLoss( min( (adjusted_pressure / HAZARD_HIGH_PRESSURE)*PRESSURE_DAMAGE_COEFFICIENT , MAX_PRESSURE_DAMAGE) )
		else if(pressure <= 50)
			var/adjusted_pressure = calculate_affecting_pressure(pressure) //Returns how much pressure actually affects the mob.
			if(adjusted_pressure < 50)
				adjustBruteLoss( (50 - adjusted_pressure) / 50 )
			var/datum/organ/external/head_organ = get_organ("head")
			if(pressure < 15 && !(head_organ.status&ORGAN_ROBOT))
				if(!head || !head:stops_pressure_damage)
					var/ratio = (15-pressure)/15
					if(!(src.sdisabilities&BLIND) && prob(30*ratio))
						src << "\red Your eyes boil!"
					eye_stat += rand(3, 6)*ratio
					eye_blurry += rand(4, 8)*ratio
					if (prob(src.eye_stat - 25 + 1) && !(src.sdisabilities&BLIND))
						src << "\red You go blind!"
						src.sdisabilities |= BLIND
					else if (prob(src.eye_stat - 15 + 1))
						src.disabilities |= NEARSIGHTED
						src.eye_blind += 5
						src.eye_blurry += 5
			if(pressure < 25 && !isCyborg())
				if(!(wear_mask && wear_mask.flags&MASKINTERNALS) && !(head && head.flags&MASKINTERNALS))
					var/ratio = (25-pressure)/25
					if(hold_breath && prob(ratio*100))
						hold_breath = 0
						hud_holdbreath.icon_state = "breathing"
						src << "\red Low pressure rips air out from your lungs"
						if(breath)
							loc.assume_air(breath)
						breath = null
		return

	proc/stabilize_temperature_from_calories()
		switch(bodytemperature)
			if(-INFINITY to 260.15) //260.15 is 310.15 - 50, the temperature where you start to feel effects.
				if(nutrition >= 2) //If we are very, very cold we'll use up quite a bit of nutriment to heat us up.
					nutrition -= 2
				var/body_temperature_difference = TBODYC - bodytemperature
				bodytemperature += max((body_temperature_difference / BODYTEMP_AUTORECOVERY_DIVISOR), BODYTEMP_AUTORECOVERY_MINIMUM)
			if(260.15 to 360.15)
				var/body_temperature_difference = TBODYC - bodytemperature
				var/body_temperature_change = body_temperature_difference
				if(body_temperature_change>0)
					body_temperature_change = max(2, body_temperature_change / BODYTEMP_AUTORECOVERY_DIVISOR)
					body_temperature_change = min(body_temperature_change, body_temperature_difference)
				else
					body_temperature_change = min(-2, body_temperature_change / BODYTEMP_AUTORECOVERY_DIVISOR)
					body_temperature_change = max(body_temperature_change, body_temperature_difference)
				bodytemperature += body_temperature_change
			if(360.15 to INFINITY) //360.15 is 310.15 + 50, the temperature where you start to feel effects.
				//We totally need a sweat system cause it totally makes sense...~
				var/body_temperature_difference = TBODYC - bodytemperature
				bodytemperature += min((body_temperature_difference / BODYTEMP_AUTORECOVERY_DIVISOR), -BODYTEMP_AUTORECOVERY_MINIMUM)	//We're dealing with negative numbers

	proc/adjust_temperature_to_environment(var/datum/gas_mixture/environment)
		var/body_temperature_difference = (environment ? (environment.temperature) : 0) + (stat<2 ? 20 : 0) - bodytemperature
		bodytemperature += body_temperature_difference / BODYTEMP_AUTORECOVERY_DIVISOR

	//This proc returns a number made up of the flags for body parts which you are protected on. (such as HEAD, UPPER_TORSO, LOWER_TORSO, etc. See setup.dm for the full list)
	proc/get_heat_protection_flags(temperature) //Temperature is the temperature you're being exposed to.
		var/thermal_protection_flags = 0
		//Handle normal clothing
		if(head)
			if(head.max_heat_protection_temperature && head.max_heat_protection_temperature >= temperature)
				thermal_protection_flags |= head.heat_protection
		if(wear_suit)
			if(wear_suit.max_heat_protection_temperature && wear_suit.max_heat_protection_temperature >= temperature)
				thermal_protection_flags |= wear_suit.heat_protection
		if(w_uniform)
			if(w_uniform.max_heat_protection_temperature && w_uniform.max_heat_protection_temperature >= temperature)
				thermal_protection_flags |= w_uniform.heat_protection
		if(shoes)
			if(shoes.max_heat_protection_temperature && shoes.max_heat_protection_temperature >= temperature)
				thermal_protection_flags |= shoes.heat_protection
		if(gloves)
			if(gloves.max_heat_protection_temperature && gloves.max_heat_protection_temperature >= temperature)
				thermal_protection_flags |= gloves.heat_protection
		if(wear_mask)
			if(wear_mask.max_heat_protection_temperature && wear_mask.max_heat_protection_temperature >= temperature)
				thermal_protection_flags |= wear_mask.heat_protection

		return thermal_protection_flags

	proc/get_heat_protection(temperature) //Temperature is the temperature you're being exposed to.
		var/thermal_protection_flags = get_heat_protection_flags(temperature)

		var/thermal_protection = 0.0
		if(thermal_protection_flags)
			if(thermal_protection_flags & HEAD)
				thermal_protection += THERMAL_PROTECTION_HEAD
			if(thermal_protection_flags & UPPER_TORSO)
				thermal_protection += THERMAL_PROTECTION_UPPER_TORSO
			if(thermal_protection_flags & LOWER_TORSO)
				thermal_protection += THERMAL_PROTECTION_LOWER_TORSO
			if(thermal_protection_flags & LEG_LEFT)
				thermal_protection += THERMAL_PROTECTION_LEG_LEFT
			if(thermal_protection_flags & LEG_RIGHT)
				thermal_protection += THERMAL_PROTECTION_LEG_RIGHT
			if(thermal_protection_flags & FOOT_LEFT)
				thermal_protection += THERMAL_PROTECTION_FOOT_LEFT
			if(thermal_protection_flags & FOOT_RIGHT)
				thermal_protection += THERMAL_PROTECTION_FOOT_RIGHT
			if(thermal_protection_flags & ARM_LEFT)
				thermal_protection += THERMAL_PROTECTION_ARM_LEFT
			if(thermal_protection_flags & ARM_RIGHT)
				thermal_protection += THERMAL_PROTECTION_ARM_RIGHT
			if(thermal_protection_flags & HAND_LEFT)
				thermal_protection += THERMAL_PROTECTION_HAND_LEFT
			if(thermal_protection_flags & HAND_RIGHT)
				thermal_protection += THERMAL_PROTECTION_HAND_RIGHT


		return min(1,thermal_protection)

	//See proc/get_heat_protection_flags(temperature) for the description of this proc.
	proc/get_cold_protection_flags(temperature)
		var/thermal_protection_flags = 0
		//Handle normal clothing

		if(head)
			if(head.min_cold_protection_temperature && head.min_cold_protection_temperature <= temperature)
				thermal_protection_flags |= head.cold_protection
		if(wear_suit)
			if(wear_suit.min_cold_protection_temperature && wear_suit.min_cold_protection_temperature <= temperature)
				thermal_protection_flags |= wear_suit.cold_protection
		if(w_uniform)
			if(w_uniform.min_cold_protection_temperature && w_uniform.min_cold_protection_temperature <= temperature)
				thermal_protection_flags |= w_uniform.cold_protection
		if(shoes)
			if(shoes.min_cold_protection_temperature && shoes.min_cold_protection_temperature <= temperature)
				thermal_protection_flags |= shoes.cold_protection
		if(gloves)
			if(gloves.min_cold_protection_temperature && gloves.min_cold_protection_temperature <= temperature)
				thermal_protection_flags |= gloves.cold_protection
		if(wear_mask)
			if(wear_mask.min_cold_protection_temperature && wear_mask.min_cold_protection_temperature <= temperature)
				thermal_protection_flags |= wear_mask.cold_protection

		return thermal_protection_flags

	proc/get_cold_protection(temperature)

		if(COLD_RESISTANCE in mutations)
			return 1 //Fully protected from the cold.

		temperature = max(temperature, 2.7) //There is an occasional bug where the temperature is miscalculated in ares with a small amount of gas on them, so this is necessary to ensure that that bug does not affect this calculation. Space's temperature is 2.7K and most suits that are intended to protect against any cold, protect down to 2.0K.
		var/thermal_protection_flags = get_cold_protection_flags(temperature)

		var/thermal_protection = 0.0
		if(thermal_protection_flags)
			if(thermal_protection_flags & HEAD)
				thermal_protection += THERMAL_PROTECTION_HEAD
			if(thermal_protection_flags & UPPER_TORSO)
				thermal_protection += THERMAL_PROTECTION_UPPER_TORSO
			if(thermal_protection_flags & LOWER_TORSO)
				thermal_protection += THERMAL_PROTECTION_LOWER_TORSO
			if(thermal_protection_flags & LEG_LEFT)
				thermal_protection += THERMAL_PROTECTION_LEG_LEFT
			if(thermal_protection_flags & LEG_RIGHT)
				thermal_protection += THERMAL_PROTECTION_LEG_RIGHT
			if(thermal_protection_flags & FOOT_LEFT)
				thermal_protection += THERMAL_PROTECTION_FOOT_LEFT
			if(thermal_protection_flags & FOOT_RIGHT)
				thermal_protection += THERMAL_PROTECTION_FOOT_RIGHT
			if(thermal_protection_flags & ARM_LEFT)
				thermal_protection += THERMAL_PROTECTION_ARM_LEFT
			if(thermal_protection_flags & ARM_RIGHT)
				thermal_protection += THERMAL_PROTECTION_ARM_RIGHT
			if(thermal_protection_flags & HAND_LEFT)
				thermal_protection += THERMAL_PROTECTION_HAND_LEFT
			if(thermal_protection_flags & HAND_RIGHT)
				thermal_protection += THERMAL_PROTECTION_HAND_RIGHT

		return min(1,thermal_protection)

	proc/handle_chemicals_in_body()
		if(reagents)
			if(isCyborg())
				for(var/A in reagents.reagent_list)
					var/datum/reagent/R = A
					if(R)
						reagents.remove_reagent(R.id, REAGENTS_METABOLISM*R.metabolism_ratio) //By default it slowly disappears.
				reagents.update_total()
			else
				reagents.metabolize(src)
				if((get_species()=="Tajaran" || get_species()=="Slime") && prob(30))
					reagents.metabolize(src)

		if(!mind)
			return

//		if(dna && dna.mutantrace == "plant") //couldn't think of a better place to place it, since it handles nutrition -- Urist
		if(PLANT in mutations)
			var/light_amount = 0 //how much light there is in the place, affects receiving nutrition and healing
			if(isturf(loc)) //else, there's considered to be no light
				var/turf/T = loc
				var/area/A = T.loc
				if(A)
					if(A.lighting_use_dynamic)	light_amount = min(10,T.lighting_lumcount) - 5 //hardcapped so it's not abused by having a ton of flashlights
					else						light_amount =  5
			nutrition += light_amount
			if(nutrition > 500)
				nutrition = 500
			if(light_amount > 2) //if there's enough light, heal
				heal_overall_damage(1,1)
				adjustToxLoss(-1)
				adjustOxyLoss(-1)

		//The fucking FAT mutation is the dumbest shit ever. It makes the code so difficult to work with
		if(FAT in mutations)
			if(overeatduration < 100)
				src << "\blue You feel fit again!"
				mutations.Remove(FAT)
				update_mutantrace(0)
				update_mutations(0)
				update_inv_w_uniform(0)
				update_inv_wear_suit()
			else if(overeatduration > 750 && get_species()=="Slime")
				var/mob/living/carbon/slime/S
				var/turf/Loc = get_turf(src)
				S = new(Loc)
				S = new(Loc)
				S = new(Loc)
				S = new(Loc)
				if(mind)
					mind.transfer_to(S, src)
				visible_message("\red %knownface:1% collapses!", actors=list(src))
				for(var/obj/item in src) u_equip(item)
				del(src)
				return
		else
			if(overeatduration > 500)
				src << "\red You suddenly feel blubbery!"
				mutations.Add(FAT)
				update_mutantrace(0)
				update_mutations(0)
				update_inv_w_uniform(0)
				update_inv_wear_suit()


		var/datum/organ/external/head = get_organ("head")
		var/has_cyborg_head = head.status&ORGAN_ROBOT

		// nutrition decrease
		var/isCyborg = isCyborg()
		if (nutrition > 0 && stat != 2)
			if(isCyborg)
				if(!sleeping && !stat)
					nutrition = max (0, nutrition - HUNGER_FACTOR*2.5)
			else if(has_cyborg_head)
				nutrition = max (0, nutrition - HUNGER_FACTOR*1.6)
			else if(get_species()=="Tajaran")
				nutrition = max (0, nutrition - HUNGER_FACTOR*1.4)
			else
				nutrition = max (0, nutrition - HUNGER_FACTOR)
		else
			if(isCyborg)
				stat = max(1,stat)
				Weaken(10)
			else if(!stat && prob(40))
				toxloss += 5
				var/datum/organ/external/O = pick("chest")
				if(istype(O)) O.add_autopsy_data("Excessive Hunger", 5)

		if (nutrition > 450 && !isCyborg())
			if(overeatduration < 600) //capped so people don't take forever to unfat
				overeatduration++
		else
			if(overeatduration > 1)
				overeatduration -= 2 //doubled the unfat rate

//		if(dna && dna.mutantrace == "plant")
		if(PLANT in mutations)
			if(nutrition < 200)
				take_overall_damage(2,0)

		if (drowsyness)
			drowsyness--
			eye_blurry = max(2, eye_blurry)
			if (!shock_stage && prob(5))
				sleeping += 1
				Paralyse(5)

		confused = max(0, confused - 1)
		// decrement dizziness counter, clamped to 0
		if(resting)
			dizziness = max(0, dizziness - 15)
			jitteriness = max(0, jitteriness - 15)
		else
			dizziness = max(0, dizziness - 3)
			jitteriness = max(0, jitteriness - 3)


		if(life_tick % 10 == 0)
			// handle trace chemicals for autopsy
			for(var/datum/organ/O in organs)
				for(var/chemID in O.trace_chemicals)
					O.trace_chemicals[chemID] = O.trace_chemicals[chemID] - 1
					if(O.trace_chemicals[chemID] <= 0)
						O.trace_chemicals.Remove(chemID)
		for(var/datum/reagent/A in reagents.reagent_list)
			// add chemistry traces to a random organ
			var/datum/organ/O = pick(organs)
			O.trace_chemicals[A.name] = 100

		updatehealth()

		return //TODO: DEFERRED

	proc/handle_regular_status_updates()
		if(stat) hold_breath = 0

		handle_blood()
		if(stat == DEAD)	//DEAD. BROWN BREAD. SWIMMING WITH THE SPESS CARP
			blinded = 1
			silent = 0
		else				//ALIVE. LIGHTS ARE ON
			updatehealth()	//TODO
			handle_organs()
			if((health <= config.health_threshold_dead || op_stage.brain == 4.0) && losebreath>=0)
				death()
				blinded = 1
				silent = 0
				return 1

			// the analgesic effect wears off slowly
			analgesic = max(0, analgesic - 1)

			//UNCONSCIOUS. NO-ONE IS HOME
			if( ((getOxyLoss() > 50) || (config.health_threshold_crit > health)) && losebreath>=0)
				Paralyse(3)

				/* Done by handle_breath()
				if( health <= 20 && prob(1) )
					spawn(0)
						emote("gasp")
				if(!reagents.has_reagent("inaprovaline"))
					adjustOxyLoss(1)*/

			var/trippyFakesChance = 0
			if(hallucination)
				if(isCyborgHead())
					hallucination = 0
				else if(hallucination >= 20)
					if(prob(3))
						fake_attack(src)
					trippyFakesChance = 5
					if(!handling_hal)
						spawn handle_hallucinations() //The not boring kind!

				if(hallucination<=2)
					hallucination = 0
					halloss = 0
				else
					hallucination -= 2

			else
				for(var/atom/a in hallucinations)
					del a

			if(mTrippyVision in mutations)
				trippyFakesChance = 100
			else if(client && druggy > 30)
				trippyFakesChance = 100

			if(trippyFakesChance && client)
				makeTrippyFakes(trippyFakesChance)
			else if((fake_visions.len>0 || fake_visions_ignore.len>0) && druggy < 30 && hallucination<100)
				for(var/mob/M in fake_visions)
					var/list/Data = fake_visions[M]
					var/image/I = Data["img"]
					I.override = 0
					if(client)
						client.images -= I
					del(I)
				fake_visions.len = 0
				fake_visions_ignore.len = 0

			if(halloss > 100 && !lying)
				src << "<span class='notice'>You're too tired to keep going...</span>"
				for(var/mob/O in oviewers(src, null))
					O.show_message("<B>[src]</B> slumps to the ground panting, too weak to fight.", 1)
				Paralyse(3)
				setHalLoss(99)

			overweight = -9
			overweight_info.Cut()
			for(var/obj/item/Item in src.search_contents_for(/obj/item,null,list(/obj/item/weapon/storage/belt,/obj/item/device/pda,/obj/item/weapon/gun,/obj/item/foameditem),1))
				if(Item==head || Item==shoes || Item==gloves || Item==w_uniform || Item==wear_id) continue
				if(istype(Item,/obj/item/weapon/storage/belt) && Item==belt) continue
				if(Item==wear_suit && istype(Item,/obj/item/clothing/suit/labcoat)) continue
				if(istype(Item,/obj/item/weapon/storage/box)) continue
				if(istype(Item,/obj/item/projectile)) continue
				var/weight = Item.w_class-1
//				if(istype(Item,/obj/item/clothing/suit))
//					weight += Item:slowdown
				if(weight>0)
					overweight_info["[Item.name] \ref[Item]"] = weight
					overweight += weight
			overweight = max(0,overweight)

			if(stamina<0)
				stamina=0
				src << "<span class='notice'>You're too tired to keep going...</span>"
				for(var/mob/O in oviewers(src, null))
					O.show_message("<B>[src]</B> falls to the ground panting, too weak to run.", 1)
				Paralyse(5)
			if(isCyborg())
				nutrition -= (100-stamina)*0.1*HUNGER_FACTOR
				stamina=100
				halloss=min(halloss,10)
				update_stamina_hud()
			else if(stamina<100 && !hold_breath)
				var/skill = getSkill("athletics")
				if(skill>0) skill--
				skill*=2
				if(m_intent!="run")
					nutrition = max (0, nutrition - HUNGER_FACTOR*0.4)
					stamina = min(100,stamina+4+skill*2)
				if(stamina<100 && client && world.time>client.move_delay+50)
					nutrition = max (0, nutrition - HUNGER_FACTOR*1.0)
					stamina = min(100,stamina+rand(4,12)+skill*2)
				update_stamina_hud()

			if(forced_sleep && !shock_stage)
				sleeping = max(sleeping,5)

			if(paralysis)
				AdjustParalysis(-1)
				blinded = 1
				stat = UNCONSCIOUS
			else if(sleeping)
				handle_dreams()
				adjustHalLoss(-5)
				if(prob(50) && isCyborg())
					if(get_damaged_organs(1,0) && prob(5))
						heal_overall_damage(1,0,1,1)
					else if(get_damaged_organs(0,1))
						heal_overall_damage(0,1,1,1)
					else
						heal_overall_damage(1,0,1,1)
				sleeping = max(sleeping-1, 0)
				blinded = 1
				stat = UNCONSCIOUS
				if( prob(10) && health && !hal_crit )
					spawn(0)
						emote("snore")
				stamina=min(100,stamina+10)
			//CONSCIOUS
			else
				stat = CONSCIOUS

			var/datum/organ/external/head_organ = get_organ("head")
			var/has_cyborg_head = head_organ.status&ORGAN_ROBOT

			//Eyes
			if(sdisabilities & BLIND)	//disabled-blind, doesn't get better on its own
				blinded = 1
				if(has_cyborg_head && !eye_blind)
					sdisabilities &= ~BLIND
			else if(istype(glasses, /obj/item/clothing/glasses/sunglasses/blindfold))	//resting your eyes with a blindfold heals blurry eyes faster
				if(prob(5))
					eye_stat = max(eye_stat-1, 0)
				eye_blind = max(eye_blind-1,0)
				eye_blurry = max(eye_blurry-3, 0)
				blinded = 1
			else if(eye_blind)			//blindness, heals slowly over time
				if(prob(5))
					eye_blind = max(eye_blind-1,0)
				blinded = 1
			else if(isCyborgHead())
				if(prob(5))
					eye_stat = max(eye_stat-1, 0)
				eye_blurry = max(eye_blurry-3, 0)
			else if(eye_blurry)	//blurry eyes heal slowly
				eye_blurry = max(eye_blurry-1, 0)

			//Ears
			if(sdisabilities & DEAF)	//disabled-deaf, doesn't get better on its own
				ear_deaf = max(ear_deaf, 1)
			else if(istype(ears, /obj/item/clothing/ears/earmuffs))	//resting your ears with earmuffs heals ear damage faster
				ear_damage = max(ear_damage-0.15, 0)
				ear_deaf = max(ear_deaf, 1)
			else if(ear_deaf)			//deafness, heals slowly over time
				ear_deaf = max(ear_deaf-1, 0)
			else if(ear_damage < 25)	//ear damage heals slowly under this threshold. otherwise you'll need earmuffs
				ear_damage = max(ear_damage-0.05, 0)

			//Other
			if(stunned)
				AdjustStunned(-1)

			if(weakened)
				weakened = max(weakened-1,0)	//before you get mad Rockdtben: I done this so update_canmove isn't called multiple times

			if(stuttering)
				stuttering = max(stuttering-1, 0)
			if (src.slurring)
				slurring = max(slurring-1, 0)
			if(silent)
				silent = max(silent-1, 0)

			if(iamhigh)
				iamhigh = max(iamhigh-1, 0)
			if(druggy)
				druggy = max(druggy-1, 0)
				iamhigh = max(iamhigh, druggy)

			// Increase germ_level regularly
			if(prob(40))
				germ_level += 1
			// If you're dirty, your gloves will become dirty, too.
			if(gloves && germ_level > gloves.germ_level && prob(10))
				gloves.germ_level += 1
		return 1

	proc/handle_regular_hud_updates()
		if(!client)	return 0

		for(var/image/hud in client.images)
			if(copytext(hud.icon_state,1,4) == "hud") //ugly, but icon comparison is worse, I believe
				del(hud)

		client.screen.Remove(global_hud.blurry, global_hud.druggy, global_hud.vimpaired, global_hud.darkMask)

		update_action_buttons()
		update_status_icons()

		if(hud_damageoverlay.overlays)
			hud_damageoverlay.overlays = list()

		if(stat == UNCONSCIOUS)
			//Critical damage passage overlay
			if(health <= 0)
				var/image/I
				switch(health)
					if(-20 to -10)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage1")
					if(-30 to -20)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage2")
					if(-40 to -30)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage3")
					if(-50 to -40)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage4")
					if(-60 to -50)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage5")
					if(-70 to -60)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage6")
					if(-80 to -70)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage7")
					if(-90 to -80)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage8")
					if(-95 to -90)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage9")
					if(-INFINITY to -95)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "passage10")
				hud_damageoverlay.overlays += I
		else
			//Oxygen damage overlay
			if(oxyloss)
				var/image/I
				switch(oxyloss)
					if(10 to 20)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "oxydamageoverlay1")
					if(20 to 25)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "oxydamageoverlay2")
					if(25 to 30)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "oxydamageoverlay3")
					if(30 to 35)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "oxydamageoverlay4")
					if(35 to 40)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "oxydamageoverlay5")
					if(40 to 45)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "oxydamageoverlay6")
					if(45 to INFINITY)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "oxydamageoverlay7")
				hud_damageoverlay.overlays += I

			//Fire and Brute damage overlay (BSSR)
			var/hurtdamage = src.getBruteLoss() + src.getFireLoss()
			if(hurtdamage)
				var/image/I
				switch(hurtdamage)
					if(10 to 25)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "brutedamageoverlay1")
					if(25 to 40)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "brutedamageoverlay2")
					if(40 to 55)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "brutedamageoverlay3")
					if(55 to 70)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "brutedamageoverlay4")
					if(70 to 85)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "brutedamageoverlay5")
					if(85 to INFINITY)
						I = image("icon" = 'icons/mob/screen1_full.dmi', "icon_state" = "brutedamageoverlay6")
				hud_damageoverlay.overlays += I

		if(losebreath>0 || hold_breath)
			hud_holdbreath.icon_state = "holdbreath"
		else
			hud_holdbreath.icon_state = "breathing"

		if( stat == DEAD )
			sight |= (SEE_TURFS|SEE_MOBS|SEE_OBJS)
			see_in_dark = 8
			if(!druggy)		see_invisible = SEE_INVISIBLE_LEVEL_TWO
			if(hud_healths)		hud_healths.icon_state = "health7"	//DEAD healthmeter
		else
			sight &= ~(SEE_TURFS|SEE_MOBS|SEE_OBJS)

			var/datum/organ/external/head_organ = get_organ("head")
			var/has_cyborg_head = head_organ.status&ORGAN_ROBOT

			if(dna && !has_cyborg_head)
				switch(dna.mutantrace)
					if("metroid")
						see_in_dark = 3
						see_invisible = SEE_INVISIBLE_LEVEL_ONE
					if("lizard")
						see_in_dark = 3
					if("tajaran")
						see_in_dark = 8
					if("dragon")
						see_in_dark = 5
					else
						see_in_dark = 2
			else
				see_in_dark = 1

			if((XRAY in mutations) && !has_cyborg_head)
				sight |= SEE_TURFS|SEE_MOBS|SEE_OBJS
				see_in_dark = 8
				if(!druggy)		see_invisible = SEE_INVISIBLE_LEVEL_TWO

			if(istype(wear_mask, /obj/item/clothing/mask/gas/voice/space_ninja))
				var/obj/item/clothing/mask/gas/voice/space_ninja/O = wear_mask
				switch(O.mode)
					if(0)
						var/target_list[] = list()
						for(var/mob/living/target in oview(src))
							if( target.mind&&(target.mind.special_role||issilicon(target)) )//They need to have a mind.
								target_list += target
						if(target_list.len)//Everything else is handled by the ninja mask proc.
							O.assess_targets(target_list, src)
						if(!druggy)		see_invisible = SEE_INVISIBLE_LIVING
					if(1)
						see_in_dark = 5
						if(!druggy)		see_invisible = SEE_INVISIBLE_LIVING
					if(2)
						sight |= SEE_MOBS
						if(!druggy)		see_invisible = SEE_INVISIBLE_LEVEL_TWO
					if(3)
						sight |= SEE_TURFS
						if(!druggy)		see_invisible = SEE_INVISIBLE_LIVING

			if(glasses)
				if(istype(glasses, /obj/item/clothing/glasses/meson))
					sight |= SEE_TURFS
					if(!druggy)
						see_invisible = SEE_INVISIBLE_MINIMUM
				else if(istype(glasses, /obj/item/clothing/glasses/night))
					see_in_dark = 5
					if(!druggy)
						see_invisible = SEE_INVISIBLE_MINIMUM
				else if(istype(glasses, /obj/item/clothing/glasses/thermal))
					sight |= SEE_MOBS
					if(!druggy)
						see_invisible = SEE_INVISIBLE_MINIMUM
				else if(istype(glasses, /obj/item/clothing/glasses/material))
					sight |= SEE_OBJS
					if(!druggy)
						see_invisible = SEE_INVISIBLE_MINIMUM

	/* HUD shit goes here, as long as it doesn't modify sight flags */
	// The purpose of this is to stop xray and w/e from preventing you from using huds -- Love, Doohl

				else if(istype(glasses, /obj/item/clothing/glasses/sunglasses))
					see_in_dark = 1
					if(istype(glasses, /obj/item/clothing/glasses/sunglasses/sechud))
						var/obj/item/clothing/glasses/sunglasses/sechud/O = glasses
						if(O.hud)		O.hud.process_hud(src)
						if(!druggy)		see_invisible = SEE_INVISIBLE_LIVING

				else if(istype(glasses, /obj/item/clothing/glasses/hud))
					var/obj/item/clothing/glasses/hud/O = glasses

					O.process_hud(src)
					if(!druggy)		see_invisible = SEE_INVISIBLE_LIVING
				else
					see_invisible = SEE_INVISIBLE_LIVING
			else
				see_invisible = SEE_INVISIBLE_LIVING

			if(seer)
				var/obj/effect/rune/R = locate() in loc
				if(R && R.word1 == wordsee && R.word2 == wordhell && R.word3 == wordjoin)
					see_invisible = SEE_INVISIBLE_OBSERVER
				else
					see_invisible = SEE_INVISIBLE_LIVING
					seer = 0

			if(hud_healths)
				switch(hal_screwyhud)
					if(1)	hud_healths.icon_state = "health6"
					if(2)	hud_healths.icon_state = "health7"
					else
						switch(health - halloss)
							if(100 to INFINITY)		hud_healths.icon_state = "health0"
							if(80 to 100)			hud_healths.icon_state = "health1"
							if(60 to 80)			hud_healths.icon_state = "health2"
							if(40 to 60)			hud_healths.icon_state = "health3"
							if(20 to 40)			hud_healths.icon_state = "health4"
							if(0 to 20)				hud_healths.icon_state = "health5"
							else					hud_healths.icon_state = "health6"

/*			if(hud_nutrition_icon)
				switch(nutrition)
					if(450 to INFINITY)				hud_nutrition_icon.icon_state = "nutrition0"
					if(350 to 450)					hud_nutrition_icon.icon_state = "nutrition1"
					if(250 to 350)					hud_nutrition_icon.icon_state = "nutrition2"
					if(150 to 250)					hud_nutrition_icon.icon_state = "nutrition3"
					else							hud_nutrition_icon.icon_state = "nutrition4"
*/
/*			if(hud_pressure)
				if(istype(wear_suit, /obj/item/clothing/suit/space)||istype(wear_suit, /obj/item/clothing/suit/armor/captain))
					hud_pressure.icon_state = "pressure0"
				else
					var/datum/gas_mixture/environment = loc.return_air()
					if(environment)
						switch(environment.return_pressure())
							if(HAZARD_HIGH_PRESSURE to INFINITY)				hud_pressure.icon_state = "pressure2"
							if(WARNING_HIGH_PRESSURE to HAZARD_HIGH_PRESSURE)	hud_pressure.icon_state = "pressure1"
							if(WARNING_LOW_PRESSURE to WARNING_HIGH_PRESSURE)	hud_pressure.icon_state = "pressure0"
							if(HAZARD_LOW_PRESSURE to WARNING_LOW_PRESSURE)		hud_pressure.icon_state = "pressure-1"
							else												hud_pressure.icon_state = "pressure-2"
*/
			if(hud_pullin)
				if(pulling)								hud_pullin.icon_state = "pull1"
				else									hud_pullin.icon_state = "pull0"
//			if(rest)	//Not used with new UI
//				if(resting || lying || sleeping)		rest.icon_state = "rest1"
//				else									rest.icon_state = "rest0"
//			if(hud_toxin)
//				if(hal_screwyhud == 4 || toxins_alert)	hud_toxin.icon_state = "tox1"
//				else									hud_toxin.icon_state = "tox0"
//			if(hud_oxygen)
//				if(hal_screwyhud == 3 || oxygen_alert)	hud_oxygen.icon_state = "oxy1"
//				else									hud_oxygen.icon_state = "oxy0"
//			if(hud_fire)
//				if(fire_alert)							hud_fire.icon_state = "fire[fire_alert]" //fire_alert is either 0 if no alert, 1 for cold and 2 for heat.
//				else									hud_fire.icon_state = "fire0"

/*			if(hud_bodytemp)
				switch(bodytemperature) //310.055 optimal body temp
					if(370 to INFINITY)		hud_bodytemp.icon_state = "temp4"
					if(350 to 370)			hud_bodytemp.icon_state = "temp3"
					if(335 to 350)			hud_bodytemp.icon_state = "temp2"
					if(320 to 335)			hud_bodytemp.icon_state = "temp1"
					if(300 to 320)			hud_bodytemp.icon_state = "temp0"
					if(295 to 300)			hud_bodytemp.icon_state = "temp-1"
					if(280 to 295)			hud_bodytemp.icon_state = "temp-2"
					if(260 to 280)			hud_bodytemp.icon_state = "temp-3"
					else					hud_bodytemp.icon_state = "temp-4"
*/
			if(hud_blind)
				if(blinded)		hud_blind.layer = 18
				else			hud_blind.layer = 0

			if(has_cyborg_head && !eye_blurry && !eye_blind)
				disabilities &= ~NEARSIGHTED

			if( disabilities&NEARSIGHTED && !istype(glasses, /obj/item/clothing/glasses/regular) )
				client.screen += global_hud.vimpaired
			else if( !(disabilities&NEARSIGHTED) && istype(glasses, /obj/item/clothing/glasses/regular) )
				client.screen += global_hud.vimpaired
			if(eye_blurry)			client.screen += global_hud.blurry
			if(druggy)				client.screen += global_hud.druggy

			if( istype(src.head, /obj/item/clothing/head/welding) )
				var/obj/item/clothing/head/welding/O = head
				if(!O.up && tinted_weldhelh)
					client.screen |= global_hud.darkMask
			if(istype(src.glasses, /obj/item/clothing/glasses/welding) )
				var/obj/item/clothing/glasses/welding/O = glasses
				if(!O.up && tinted_weldhelh)
					client.screen |= global_hud.darkMask
			if(src.wear_mask && src.wear_mask.type==/obj/item/clothing/mask/gas)
				client.screen |= global_hud.darkMask

			if(eye_stat > 20)
				if(eye_stat > 30)	client.screen |= global_hud.darkMask
				else				client.screen += global_hud.vimpaired

			var/isRemoteObserve = 0
			if(machine)
				if(machine.check_eye(src)) isRemoteObserve = 1
			if((mRemote in mutations) && remoteview_target)
				if(remoteview_target.stat==CONSCIOUS)
					if(!src.head || !istype(src.head,/obj/item/clothing/head/foil))
						if(!istype(remoteview_target,/mob/living/carbon/human))
							isRemoteObserve = 1
						else if(!remoteview_target:head || !istype(remoteview_target:head,/obj/item/clothing/head/foil))
							isRemoteObserve = 1
						else
							src << "Something blocks another's vision from you"
					else
						src << "Something blocks your powers"
				else
					src << "Brainwaves not strong enough anymore"
			if(!isRemoteObserve && client && !client.adminobs)
				remoteview_target = null
				reset_view(null)
		return 1

	proc/handle_random_events()
		// Puke if toxloss is too high
		if(!stat)
			if (getToxLoss() >= 45 && nutrition > 20)
				vomit()

		//0.1% chance of playing a scary sound to someone who's in complete darkness
		if(isturf(loc) && rand(1,1000) == 1)
			var/turf/currentTurf = loc
			if(!currentTurf.lighting_lumcount)
				playsound_local(src,pick(scarySounds),50, 1, -1)

	proc/handle_virus_updates()
//		if(bodytemperature > 406)
//			for(var/datum/disease2/disease/D in viruses)
//				if(!D.antivirus && !D.surgical_heal)
//					D.cure(src)
//		else
//			for(var/datum/disease2/disease/D in viruses)
//				D.activate(src)

		/* Must be totally redone
		if(!virus2)
			for(var/obj/effect/decal/cleanable/blood/B in view(1,src))
				if(B.virus2 && get_infection_chance())
					infect_virus2(src,B.virus2)
			for(var/obj/effect/decal/cleanable/mucus/M in view(1,src))
				if(M.virus2 && get_infection_chance())
					infect_virus2(src,M.virus2)
		else
			if(isnull(virus2)) // Trying to figure out a runtime error that keeps repeating
				CRASH("virus2 nulled before calling activate()")
			else
				virus2.activate(src)

			// activate may have deleted the virus
			if(!virus2) return

			// check if we're immune
			if(virus2.antigen & src.antibodies) virus2.dead = 1

		*/
		return

	proc/handle_stomach()
		var/stomach_surgery_state = 0
		if(op_stage.stomach)
			stomach_surgery_state = 1
			var/datum/organ/external/Chest = get_organ("chest")
			if(Chest.open)
				stomach_surgery_state = 2
		if(stomach_surgery_state==1)
			adjustToxLoss(1)
		spawn(0)
			for(var/atom/movable/M in stomach_contents)
				if(stomach_surgery_state==2)
					M.loc = src.loc
				if(M.loc != src)
					stomach_contents.Remove(M)
					continue
				if(stat == 2) continue
				if(istype(M, /obj/item/weapon/reagent_containers))
					if(M.reagents && M.reagents.total_volume>0)
						M.reagents.trans_to(src,REAGENTS_METABOLISM)
					else
						for(var/atom/movable/A in M.contents)
							A.loc = src
							stomach_contents.Add(A)
						stomach_contents.Remove(M)
						del(M)
				else if(istype(M, /mob/living/carbon))
					if(air_master.current_cycle%3==1)
						if(!M:nodamage)
							M:adjustBruteLoss(5)
					var/amount = min(10,M:nutrition)
					M:nutrition -= amount
					nutrition += amount
					if(M:stat == 2 && M:nutrition<10)
						for(var/obj/item/I in M.contents)
							M:drop_from_inventory(I)
							I.loc = src
							stomach_contents.Add(I)
						M:death(0)
						stomach_contents.Remove(M)
						del(M)
						continue
				else if(istype(M,/mob/living/simple_animal))
					if(M:health>0)
						M:health-=M:maxHealth/25
						nutrition += 10
					else
						M:death(0)
						stomach_contents.Remove(M)
						del(M)
			if(prob(5))
				var/dmg = 0
				for(var/obj/item/O in stomach_contents)
					if(O.sharp && O.force)
						var/d = rand(round(O.force / 4), O.force)
						dmg += d
				if(dmg)
					var/datum/organ/external/organ = src.get_organ("chest")
					if (istype(organ))
						if(organ:take_damage(dmg, 0))
							UpdateDamageIcon()
					updatehealth()


	proc/handle_changeling()
		if(mind && mind.changeling)
			mind.changeling.regenerate()

	handle_shock()
		..()

		if(stat == DEAD) return //no pain for dead, let them rest in pieces

		if((mPain in mutations) && mpain_counter<2000)
			mpain_counter+=0.75
			if(mpain_counter>100)
				mpain_counter+=0.10
			if(mpain_counter>200)
				mpain_counter+=0.20
			if(mpain_counter>300)
				mpain_counter+=0.30
			if(mpain_counter>400)
				mpain_counter+=0.40
			if(mpain_counter>500)
				mpain_counter+=0.50
		else
			mpain_counter = max(0,mpain_counter-5)

		if(druggy)
			mpain_counter = max(0,mpain_counter-1)
			shock_stage = max(shock_stage-0.1, 0)
		if(reagents.has_reagent("alkysine"))
			mpain_counter = max(0,mpain_counter-2)
			shock_stage = max(shock_stage-0.2, 0)
		if(reagents.has_reagent("inaprovaline"))
			mpain_counter = max(0,mpain_counter-10)
			shock_stage = max(shock_stage-0.1, 0)
		if(reagents.has_reagent("space_drugs"))
			mpain_counter = max(0,mpain_counter-15)
			shock_stage = max(shock_stage-1.5, 0)
		if(reagents.has_reagent("tramadol"))
			mpain_counter = max(0,mpain_counter-30)
			shock_stage = max(shock_stage-3, 0)
		if(reagents.has_reagent("oxycodone"))
			mpain_counter = max(0,mpain_counter-50)
			shock_stage = max(shock_stage-5, 0)

		if(analgesic) return // analgesic avoids all traumatic shock temporarily

		if(health < 0)// health 0 makes you immediately collapse
			shock_stage = max(shock_stage, 61)

		if(traumatic_shock >= 80)
			shock_stage += 0.1
		else
			if(traumatic_shock-10 > shock_stage)
				shock_stage = min(shock_stage+1, traumatic_shock-10)
			else
				shock_stage = max(shock_stage-0.5, 0)

		var/active_shock = shock_stage + round(mpain_counter/10,1)
//		src << "shock: [active_shock]"

		if(active_shock >= 10)
			if(shock_stage_announced<1 || prob(5))
				src << "<font color='red'><b>"+pick("It hurts so much!", "You really need some painkillers..", "Dear god, the pain!")
				shock_stage_announced = 1
		else
			if(shock_stage_announced>0)
				src << "Pain fades"
			shock_stage_announced = 0

		if(active_shock >= 30)
			if(shock_stage_announced<3 || prob(10))
				emote("me",1,"is having trouble keeping their eyes open.")
				shock_stage_announced = 3
			eye_blurry = max(2, eye_blurry)
			stuttering = max(stuttering, 5)
		else
			if(shock_stage_announced>1)
				src << "Pain fades"
			shock_stage_announced = min(1,shock_stage_announced)

		if(active_shock >= 40)
			if(shock_stage_announced<4 || prob(10))
				src << "<font color='red'><b>"+pick("The pain is excrutiating!", "Please, just end the pain!", "Your whole body is going numb!")
				shock_stage_announced = 4
		else
			if(shock_stage_announced>3)
				src << "Pain fades"
			shock_stage_announced = min(3,shock_stage_announced)

		if(active_shock >= 60)
			if(shock_stage_announced<6 || prob(5))
				emote("me",1,"'s body becomes limp.")
				shock_stage_announced = 6
			if (prob(5))
				Stun(20)
				lying = 1
		else
			if(shock_stage_announced>4)
				src << "Pain fades"
			shock_stage_announced = min(4,shock_stage_announced)

		if(active_shock >= 80)
			if(shock_stage_announced<8 || prob(10))
				src << "<font color='red'><b>"+pick("You see a light at the end of the tunnel!", "You feel like you could die any moment now.", "You're about to lose consciousness.")
				shock_stage_announced = 8
		else
			if(shock_stage_announced>6)
				src << "Pain fades"
			shock_stage_announced = min(6,shock_stage_announced)

		if (active_shock > 80)
			Paralyse(rand(15,28))

		if(shock_stage)
			sleeping = max(0,sleeping-1)

	proc/update_stamina_hud()
		if(hud_stamina)
			switch(stamina)
				if(100 to INFINITY)		hud_stamina.icon_state = "stamina10"
				if(90 to 100)			hud_stamina.icon_state = "stamina9"
				if(80 to 90)			hud_stamina.icon_state = "stamina8"
				if(70 to 80)			hud_stamina.icon_state = "stamina7"
				if(60 to 70)			hud_stamina.icon_state = "stamina6"
				if(50 to 60)			hud_stamina.icon_state = "stamina5"
				if(40 to 50)			hud_stamina.icon_state = "stamina4"
				if(30 to 40)			hud_stamina.icon_state = "stamina3"
				if(20 to 30)			hud_stamina.icon_state = "stamina2"
				if(10 to 20)			hud_stamina.icon_state = "stamina1"
				else				hud_stamina.icon_state = "stamina0"

	proc/makeTrippyFakes(var/chance)
		var/list/nfakelist = list()
		var/list/nfakeignore = list()
		if(fake_visions_remember && !islist(fake_visions_remember))
			fake_visions_remember = list()
		var/list/nview = range(client.view+2, src)
		if(client.eye != src)
			nview |= range(client.view+2, client.eye)
		for(var/mob/living/H in nview)
			if(H==src) continue
			if(fake_visions[H])
				var/list/D = fake_visions[H]
				if(islist(fake_visions_remember) && !fake_visions_remember[H])
					fake_visions_remember[H] = D["path"]
				nfakelist[H] = D
				if(D["stat"]!=H.stat || D["lying"]!=H.lying)
					var/path = D["path"]
					var/mob/Dummy = new path
					var/image/I = D["img"]
					Dummy.lying = H.lying
					if(H.stat)
						if(hascall(Dummy,"Die"))
							Dummy:Die()
						Dummy.stat = H.stat
					if(hascall(Dummy,"update_icons"))
						Dummy:update_icons()
					I.icon = Dummy.icon
					I.icon_state = Dummy.icon_state
					D["img"] = I
					del(Dummy)
					D["stat"]=H.stat
					D["lying"]=H.lying
			else if(chance>=100 || prob(chance) && (!(H in fake_visions_ignore) || prob(chance)))
				var/list/paths = list(/mob/living/carbon/monkey,
							/mob/living/carbon/human,
							/mob/living/carbon/slime,
							/mob/living/carbon/slime/adult,
							/mob/living/silicon/robot,
							/mob/living/simple_animal/parrot,
							/mob/living/simple_animal/clown,
							/mob/living/simple_animal/cat,
							/mob/living/simple_animal/corgi,
							/mob/living/simple_animal/crab,
							/mob/living/simple_animal/chicken,
							/mob/living/simple_animal/cow,
							/mob/living/simple_animal/mushroom,
							/mob/living/simple_animal/mouse,
							/mob/living/simple_animal/hostile/retaliate/goat,
							/mob/living/simple_animal/hostile/giant_spider,
							/mob/living/simple_animal/hostile/giant_spider/nurse,
							/mob/living/simple_animal/hostile/giant_spider/hunter,
							/mob/living/simple_animal/hostile/pirate,
							/mob/living/simple_animal/hostile/zombie,
							/mob/living/simple_animal/hostile/carp,
							/mob/living/simple_animal/hostile/bear,
							/mob/living/simple_animal/hostile/alien,
							/mob/living/simple_animal/hostile/alien/drone,
							/mob/living/simple_animal/hostile/alien/sentinel,
							/mob/living/simple_animal/hostile/alien/queen,
							/mob/living/simple_animal/hostile/tomato,
							/mob/living/simple_animal/hostile/russian
							)

				var/path
				if(islist(fake_visions_remember) && fake_visions_remember[H])
					path = fake_visions_remember[H]
				else
					path = pick(paths)
					if(islist(fake_visions_remember))
						fake_visions_remember[H] = path

				var/mob/Dummy = new path
				Dummy.lying = H.lying
				if(H.stat)
					if(hascall(Dummy,"Die"))
						Dummy:Die()
					Dummy.stat = H.stat
				if(hascall(Dummy,"update_icons"))
					Dummy:update_icons()
				var/who = Dummy.name
				var/image/fake = image(Dummy.icon, H, Dummy.icon_state)
				del(Dummy)
				fake.override = 1
				src << fake
				nfakelist[H] = list("name" = who, "img" = fake, "path" = path, "stat" = H.stat, "lying" = H.lying)
			else
				nfakeignore += H
		for(var/mob/living/H in fake_visions)
			if(!nfakelist[H])
				var/list/Data = fake_visions[H]
				var/image/I = Data["img"]
				I.override = 0
				if(client)
					client.images -= I
				del(I)
		fake_visions = nfakelist
		fake_visions_ignore = nfakeignore

#undef HUMAN_MAX_OXYLOSS
#undef HUMAN_CRIT_MAX_OXYLOSS
