//
/mob/living/carbon/human/say(var/message as text)
	if (copytext(message, 1, 2) == "*" && !stat)
		return emote(copytext(message, 2))

	var/datum/organ/external/head/OHead = get_organ("head")
	if(OHead && (OHead.status & ORGAN_DESTROYED))
		if(OHead.headless)
			if(!istype(src.head,/obj/item/weapon/organ/head))
				emote("groan")
				return
		else
			return

	if (silent)
		return

	//Mimes dont speak! Changeling hivemind and emotes are allowed.
	if(miming)
		if(length(message) >= 2)
			if(mind && mind.changeling)
				if(copytext(message, 1, 2) != "*" && copytext(message, 1, 3) != ":g" && copytext(message, 1, 3) != ":G" && copytext(message, 1, 3) != ":�")
					return
				else
					return ..(message)
			if(stat == DEAD)
				return ..(message)

		if(length(message) >= 1) //In case people forget the '*help' command, this will slow them the message and prevent people from saying one letter at a time
			if (copytext(message, 1, 2) != "*")
				return

	if(stat != DEAD)
		if(src.disease_mods["honk"])
			var/list/temp_message = dd_text2list(message, " ") //List each word in the message
			var/list/pick_list = list()
			for(var/i = 1, i <= temp_message.len, i++) //Create a second list for excluding words down the line
				pick_list += i
			for(var/i=1, ((i <= src.disease_mods["honk"]/10) && (i <= temp_message.len)), i++) //Loop for each stage of the disease or until we run out of words
				if(prob(src.disease_mods["honk"])) //Stage 1: 3% Stage 2: 6% Stage 3: 9% Stage 4: 12%
					var/H = pick(pick_list)
					if(findtext(temp_message[H], "*") || findtext(temp_message[H], ";") || findtext(temp_message[H], ":")) continue
					temp_message[H] = "HONK"
					pick_list -= H //Make sure that you dont HONK the same word twice
				message = dd_list2text(temp_message, " ")

	if(istype(src.wear_mask, /obj/item/clothing/mask/luchador))
		if(copytext(message, 1, 2) != "*")
			message = dd_replacetext(message, "captain", "CAPIT�N")
			message = dd_replacetext(message, "station", "ESTACI�N")
			message = dd_replacetext(message, "sir", "SE�OR")
			message = dd_replacetext(message, "the ", "el ")
			message = dd_replacetext(message, "my ", "mi ")
			message = dd_replacetext(message, "is ", "es ")
			message = dd_replacetext(message, "it's", "es")
			message = dd_replacetext(message, "friend", "amigo")
			message = dd_replacetext(message, "buddy", "amigo")
			message = dd_replacetext(message, "hello", "hola")
			message = dd_replacetext(message, " hot", " caliente")
			message = dd_replacetext(message, " very ", " muy ")
			message = dd_replacetext(message, "sword", "espada")
			message = dd_replacetext(message, "library", "biblioteca")
			message = dd_replacetext(message, "traitor", "traidor")
			message = dd_replacetext(message, "wizard", "mago")
			message = uppertext(message) //Things end up looking better this way (no mixed cases), and it fits the macho wrestler image.
			if(prob(25))
				message += " OLE!"

	if (src.slurring)
		message = slur(message)
	..(message)

/mob/living/carbon/human/say_understands(var/lang)
	if(..(lang)) return 1
	if(druggy>5)
		if(lang in list("Monkey","Cat","Dog")) return 1
	return 0

/*
/mob/living/carbon/human/say_understands(var/other)
	if (istype(other, /mob/living/silicon/ai))
		return 1
	if (istype(other, /mob/living/silicon/decoy))
		return 1
	if (istype(other, /mob/living/silicon/pai))
		return 1
	if (istype(other, /mob/living/silicon/robot))
		return 1
	if (istype(other, /mob/living/carbon/brain))
		return 1
	if (istype(other, /mob/living/carbon/slime))
		return 1
	return ..()
*/

/mob/living/carbon/human/GetVoice()
	if(mind && mind.changeling && mind.changeling.mimicing)
		return "Unknown ([copytext(mind.changeling.mimicing, 1, 6)])"

	var/obj/item/weapon/implant/modulator/Modulator = locate() in src
	if(!isnull(Modulator))
		var/obj/item/clothing/mask/gas/voice/V = Modulator.DummyMask
		if(istype(V) && V.vchange && V.voice["hash"])
			return "Unknown ([copytext(V.voice["hash"], 1, 6)])"

	if(istype(src.wear_mask, /obj/item/clothing/mask/gas/voice))
		var/obj/item/clothing/mask/gas/voice/V = src.wear_mask
		if(istype(V) && V.vchange && V.voice["hash"])
			return "Unknown ([copytext(V.voice["hash"], 1, 6)])"

	return "Unknown ([copytext(dna.get_unique_enzymes(), 1, 6)])"
//	return real_name

/mob/living/carbon/human/proc/IsFakeVoice()
	if(istype(src.wear_mask, /obj/item/clothing/mask/gas/voice))
		var/obj/item/clothing/mask/gas/voice/V = src.wear_mask
		if(V.vchange)
			return 1
	return 0
