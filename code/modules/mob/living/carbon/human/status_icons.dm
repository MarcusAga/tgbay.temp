//
/mob/living/carbon/human/var/list/status_icons
/mob/living/carbon/human/proc/build_status_icons()
	if(status_icons) return
	status_icons = list()
	for(var/type in (typesof(/obj/screen/status_icon/human) - /obj/screen/status_icon/human))
		status_icons += new type(null)

/mob/living/carbon/human/proc/update_status_icons()
	var/pos = 1
	if(!src.hud_used) return
	if(!src.client) return

	if(!hud_used.hud_shown)	//Hud toggled to minimal
		return

	var/changed = 0
	for(var/obj/screen/status_icon/I in status_icons)
		changed += I.update(src)
	if(!changed && src.hud_used.status_icons_list)
		src.client.screen |= src.hud_used.status_icons_list
		return

	src.client.screen -= src.hud_used.status_icons_list
	hud_used.status_icons_list = list()
	var/list/ordered = list()

	for(var/obj/screen/status_icon/I in status_icons)
		if(I.value==0) continue
		var/idx = 1
		for(var/obj/screen/status_icon/I2 in ordered)
			if(I.priority>I2.priority)
				ordered.Insert(idx, I)
				break
			idx++
		if(idx > ordered.len)
			ordered.Insert(idx, I)

	for(var/obj/screen/status_icon/I in ordered)
		hud_used.status_icons_list += I
		I.screen_loc = ui_status_slots
		if(pos>=12) break
		pos++

	src.client.screen |= src.hud_used.status_icons_list

/obj/screen/status_icon
	icon = 'icons/mob/status_icons.dmi'
	var/value = 0
	var/oldvalue = 0
	var/priority = 0

	proc/getBkg()
		return 1
	proc/getR()
		return 0
	proc/getG()
		return 0
	proc/getB()
		return 0

	proc/update(var/mob/M)
		if(value!=oldvalue)
			oldvalue = value
			update_icon()
			update_priority()
			return 1
		return 0
	update_icon()
		if(getBkg())
			var/icon/J = new('icons/mob/status_icons.dmi', icon_state = "background")
			J.MapColors(getR(),0,0, 0,getG(),0, 0,0,getB(), 0,0,0)
//			J.MapColors(getR(),0,0,0, 0,getG(),0,0, 0,0,getB(),0, 0,0,0,0.5, 0,0,0,0)
			underlays = list(J)
		else
			underlays.Cut()
	proc/update_priority()
		priority = value

	human
		high
			name = "You feel happy"
			icon_state = "high"

			update(var/mob/living/carbon/human/H)
				value = min(100,max(0,H.iamhigh))
				var/amnt = round(value/10)
				if(amnt>1)
					name = "You feel happy! x[amnt]"
				else
					name = initial(name)
				return ..()
			getG()
				return round(value/10)/10
		bleeding
			name = "You are bleeding"
			icon_state = "bleed"

			update(var/mob/living/carbon/human/H)
				value = 0
				for(var/datum/organ/external/temp in H.organs)
					if(temp.parent && (temp.parent.status & (ORGAN_ROBOT|ORGAN_DESTROYED)))
						continue
					if(!(temp.status & ORGAN_BLEEDING) || (temp.status & ORGAN_ROBOT))
						continue
					for(var/datum/wound/W in temp.wounds) if(W.bleeding())
						value += W.damage
					if(temp.status & ORGAN_DESTROYED && !(temp.status & ORGAN_GAUZED))
						value += 20 //Yer missing a fucking limb.
					if(value>100)
						value = 100
				return ..()
			getR()
				return round(value/10)/10

		brain
			name = "You are stupid."
			icon_state = "brain"

			update(var/mob/living/carbon/human/H)
				value = min(100,max(0,H.brainloss))
				var/amnt = round(value/10)
				if(amnt>1)
					name = "You are stupid. x[amnt]"
				else
					name = initial(name)
				return ..()
			getG()
				return 1-round(value/10)/10
			getR()
				return 1

		suffocation
			name = "You are suffocating."
			icon_state = "suffocation"

			update(var/mob/living/carbon/human/H)
				value = min(100,max(0,H.oxyloss+25*H.failed_last_breath))
				if(H.hal_screwyhud == 3)
					value = 100
				var/amnt = round(value/10)
				if(amnt>1)
					name = "You are suffocating. x[amnt]"
				else
					name = initial(name)
				return ..()
			getG()
				return 1-round(value/10)/10
			getR()
				return 1

		pain
			name = "It hurts!"
			icon_state = "pain"

			update(var/mob/living/carbon/human/H)
				value = H.shock_stage + round(H.mpain_counter/10,1)
				if(H.shock_stage_announced)
					name = "It hurts! x[H.shock_stage_announced+1]"
				else
					name = initial(name)
				return ..()
			update_priority()
				priority = max(0,min(100,value))
			getG()
				return 1-round(value/10)/10
			getR()
				return 1

		body_temperature
			name = "Normal temperature"
			icon_state = ""

			update(var/mob/living/carbon/human/H)
				switch(H.bodytemperature)
					if(370 to INFINITY)		value = 4
					if(350 to 370)			value = 3
					if(335 to 350)			value = 2
					if(320 to 335)			value = 1
					if(300 to 320)			value = 0
					if(295 to 300)			value = -1
					if(280 to 295)			value = -2
					if(260 to 280)			value = -3
					else				value = -4
				if(value==oldvalue) return 0
				oldvalue=value
				if(value>0)
					icon_state="hot"
					name = "You are hot! x[value]"
				else if(value<0)
					icon_state="cool"
					name = "You are cold! x[-value]"
				else
					name = initial(name)
					icon_state = initial(icon_state)
				update_priority()
				update_icon()
				return 1
			update_priority()
				priority = (value>0?1:-1)*value*25
			getB()
				if(value<0)
					return -value*0.25
				else
					return 0
			getR()
				if(value<0)
					return 0
				else
					return value*0.25

		fire_alert
			name = "Normal"
			icon_state = ""

			update(var/mob/living/carbon/human/H)
				value = H.fire_alert
				if(value==oldvalue) return 0
				oldvalue=value
				switch(value)
					if(0)
						name = initial(name)
						icon_state = initial(icon_state)
					if(1)
						icon_state="freezing"
						name = "You are freezing!"
					if(2)
						icon_state="burning"
						name = "You are burning!"
				update_priority()
				update_icon()
				return 1
			update_priority()
				priority = (value>0?80:-80)
			getB()
				if(value==1)
					return 1
				else
					return 0
			getR()
				if(value==2)
					return 1
				else
					return 0

		toxin_alert
			name = "Toxins!"
			icon_state = "plasma"

			update(var/mob/living/carbon/human/H)
				value = (H.hal_screwyhud == 4 || H.toxins_alert)?1:0
				return ..()
			update_priority()
				priority = (value>0?100:0)
			getG()
				return 1
			getR()
				return 1

		pressure
			name = "Pressure"
			icon_state = "pressure"

			update(var/mob/living/carbon/human/H)
				if(istype(H.wear_suit, /obj/item/clothing/suit/space)||istype(H.wear_suit, /obj/item/clothing/suit/armor/captain))
					value = 0
				else
					var/datum/gas_mixture/environment = H.loc.return_air()
					if(environment)
						switch(environment.return_pressure())
							if(HAZARD_HIGH_PRESSURE to INFINITY)			value = 2
							if(WARNING_HIGH_PRESSURE to HAZARD_HIGH_PRESSURE)	value = 1
							if(WARNING_LOW_PRESSURE to WARNING_HIGH_PRESSURE)	value = 0
							if(HAZARD_LOW_PRESSURE to WARNING_LOW_PRESSURE)		value = -1
							else							value = -2
				if(value==oldvalue) return 0
				oldvalue=value
				if(value>0)
					name = "You are pressurized! x[value]"
				else if(value<0)
					name = "You are depressurized! x[-value]"
				update_priority()
				update_icon()
				return 1
			update_priority()
				priority = (value>0?1:-1)*value*50
			getR()
				if(value>0)
					return 1
				else
					return 0
			getG()
				if(value>0)
					return 1-value*0.5
				else
					return 0
			getB()
				if(value>0)
					return 0
				else
					return 1

		hunger
			name = "Hunger"
			icon_state = "nutrition"

			update(var/mob/living/carbon/human/H)
				switch(H.nutrition)
					if(450 to INFINITY)	value = -1
					if(300 to 450)		value = 0
					if(200 to 300)		value = 1
					if(150 to 200)		value = 2
					if(100 to 150)		value = 3
					if(50 to 100)		value = 4
					else			value = 5
				if(value==oldvalue) return 0
				oldvalue = value
				if(value>0)
					name = "You are hungry! x[value]"
				else if(value<0)
					name = "You ate too much."
				update_priority()
				update_icon()
				return 1
			update_priority()
				if(value>0)
					priority = value*20
				else
					priority = 25
			getR()
				if(value>0)
					return 1
				else
					return 0
			getG()
				if(value>0)
					return 1-value*0.2
				else
					return 0
			getB()
				if(value>0)
					return 0
				else
					return 1

		bloodloss
			name = "Bloodloss"
			icon_state = "noblood"
			update(var/mob/living/carbon/human/H)
				if(H.isCyborg())
					value = 0
					priority = 0
					return ..()
				var/blood_volume = round(H.vessel.get_reagent_amount("blood"))
				switch(blood_volume)
					if(BLOOD_VOLUME_SAFE to 10000)			value = 0
					if(BLOOD_VOLUME_OKAY to BLOOD_VOLUME_SAFE)	value = 1
					if(BLOOD_VOLUME_BAD to BLOOD_VOLUME_OKAY)	value = 2
					if(BLOOD_VOLUME_SURVIVE to BLOOD_VOLUME_BAD)	value = 3
					if(0 to BLOOD_VOLUME_SURVIVE)			value = 4
				if(value==oldvalue) return 0
				oldvalue=value
				if(value>0)
					name = "Bloodloss! x[value]"
				else
					name = "Bloodloss!"
				update_priority()
				update_icon()
				return 1
			update_priority()
				priority = value*25
			getG()
				return 1-value*0.25
			getR()
				return 1

		sleepy
			name = "You want to sleep."
			icon_state = "sleepy"

			update(var/mob/living/carbon/human/H)
				value = min(100,max(0,H.drowsyness))
				var/amnt = round(value/10)
				if(amnt>1)
					name = "You want to sleep. x[amnt]"
				else
					name = initial(name)
				return ..()
			getG()
				return 1-round(value/10)/10
			getR()
				return 1

#ifndef CSServer
		overweight
			name = "You carry too much."
			icon_state = "overweight"

			update(var/mob/living/carbon/human/H)
				var/amnt = round(H.overweight/4)
				value = min(100,max(0,amnt*40))
				if(amnt>1)
					name = "You carry too much. x[amnt]"
				else
					name = initial(name)
				return ..()
			getG()
				return 1-round(value/10)/10
			getR()
				return 1
#endif

		eardeaf
			name = "Deafness."
			icon_state = "eardeaf"

			update(var/mob/living/carbon/human/H)
				if(H.ear_deaf>0)
					value = -1
				else
					value = min(100,max(0,H.ear_damage*2))
				return ..()
			getG()
				if(value<0)
					return 0
				else
					return 1
			getR()
				if(value<0)
					return 1
				else
					return round(value/10)/10

		blind
			name = "Blindness."
			icon_state = "blind"

			update(var/mob/living/carbon/human/H)
				if((H.sdisabilities & BLIND) || H.eye_blind>0)
					value = -1
				else
					value = min(100,max(0,H.eye_stat*2-20))
				return ..()
			getG()
				if(value<0)
					return 0
				else
					return 1
			getR()
				if(value<0)
					return 1
				else
					return round(value/10)/10

		intoxication
			name = "Intoxication."
			icon_state = "intoxication"

			update(var/mob/living/carbon/human/H)
				value = min(100,max(0,H.toxloss))
				var/amnt = round(value/10)
				if(amnt>1)
					name = "Intoxication! x[amnt]"
				else
					name = initial(name)
				return ..()
			getR()
				return 1
			getB()
				return 1-round(value/10)/10

		stunned
			name = "Stunned."
			icon_state = "stunned"

			update(var/mob/living/carbon/human/H)
				value = min(100,max(0,H.stunned))
				var/amnt = round(value/10)
				if(amnt>1)
					name = "Stunned! x[amnt]"
				else
					name = initial(name)
				return ..()
			getR()
				return 1
			getB()
				return 1-round(value/10)/10

		anonymous
			name = "Anonymous"
			icon_state = "anonymous"

			update(var/mob/living/carbon/human/H)
				value = 0
				var/datum/organ/external/head/ohead = H.get_organ("head")
				if( !ohead || ohead.disfigured || (ohead.status & (ORGAN_DESTROYED|ORGAN_HUSKED)) || H.status_flags&DISFIGURED)
					value = -1
				else
					if( H.wear_mask && (H.wear_mask.flags_inv&HIDEFACE) )
						value = 1
					if( H.head && (H.head.flags_inv&HIDEFACE) )
						value = 1
				return ..()
			update_priority()
				priority = value?100:0
			getG()
				if(value>0)
					return 1
				return 0
			getR()
				if(value<0)
					return 1
				return 0

		evil_presence
			name = "You feel evil presence watching over you"
			icon_state = "eye"

			update(var/mob/living/carbon/human/H)
				var/level = 0
				var/turf/Pos = get_turf(H)
				for(var/client/C in admin_list)
					if(C.holder && C.holder.evil_presence && C.mob && isadminobserver(C.mob))
						var/turf/T = get_turf(C.mob)
						if(T.z==Pos.z && get_dist(Pos, T)<C.view)
							var/l = C.view-get_dist(Pos, T)
							if(l>level)
								level = l
				value = min(100,max(0,100*level/world.view))
				return ..()
			getR()
				return round(value/10)/10
