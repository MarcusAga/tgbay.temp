	///////////////////////
	//UPDATE_ICONS SYSTEM//
	///////////////////////
/*
Calling this  a system is perhaps a bit trumped up. It is essentially update_clothing dismantled into its
core parts. The key difference is that when we generate overlays we do not generate either lying or standing
versions. Instead, we generate both and store them in two fixed-length lists, both using the same list-index
(The indexes are in update_icons.dm): Each list for humans is (at the time of writing) of length 19.
This will hopefully be reduced as the system is refined.

	var/overlays_lying[19]			//For the lying down stance
	var/overlays_standing[19]		//For the standing stance

When we call update_icons, the 'lying' variable is checked and then the appropriate list is assigned to our overlays!
That in itself uses a tiny bit more memory (no more than all the ridiculous lists the game has already mind you).

On the other-hand, it should be very CPU cheap in comparison to the old system.
In the old system, we updated all our overlays every life() call, even if we were standing still inside a crate!
or dead!. 25ish overlays, all generated from scratch every second for every xeno/human/monkey and then applied.
More often than not update_clothing was being called a few times in addition to that! CPU was not the only issue,
all those icons had to be sent to every client. So really the cost was extremely cumulative. To the point where
update_clothing would frequently appear in the top 10 most CPU intensive procs during profiling.

Another feature of this new system is that our lists are indexed. This means we can update specific overlays!
So we only regenerate icons when we need them to be updated! This is the main saving for this system.

In practice this means that:
	everytime you fall over, we just switch between precompiled lists. Which is fast and cheap.
	Everytime you do something minor like take a pen out of your pocket, we only update the in-hand overlay
	etc...


There are several things that need to be remembered:

>	Whenever we do something that should cause an overlay to update (which doesn't use standard procs
	( i.e. you do something like l_hand = /obj/item/something new(src) )
	You will need to call the relevant update_inv_* proc:
		update_inv_head()
		update_inv_wear_suit()
		update_inv_gloves()
		update_inv_shoes()
		update_inv_w_uniform()
		update_inv_glasse()
		update_inv_l_hand()
		update_inv_r_hand()
		update_inv_belt()
		update_inv_wear_id()
		update_inv_ears()
		update_inv_s_store()
		update_inv_pockets()
		update_inv_back()
		update_inv_handcuffed()
		update_inv_wear_mask()

	All of these are named after the variable they update from. They are defined at the mob/ level like
	update_clothing was, so you won't cause undefined proc runtimes with usr.update_inv_wear_id() if the usr is a
	metroid etc. Instead, it'll just return without doing any work. So no harm in calling it for metroids and such.


>	There are also these special cases:
		update_mutations()	//handles updating your appearance for certain mutations.  e.g TK head-glows
		update_mutantrace()	//handles updating your appearance after setting the mutantrace var
		UpdateDamageIcon()	//handles damage overlays for brute/burn damage //(will rename this when I geta round to it)
		update_body()	//Handles updating your mob's icon to reflect their gender/race/complexion etc
		update_hair()	//Handles updating your hair overlay (used to be update_face, but mouth and
																			...eyes were merged into update_body)

>	All of these procs update our overlays_lying and overlays_standing, and then call update_icons() by default.
	If you wish to update several overlays at once, you can set the argument to 0 to disable the update and call
	it manually:
		e.g.
		update_inv_head(0)
		update_inv_l_hand(0)
		update_inv_r_hand()		//<---calls update_icons()

	or equivillantly:
		update_inv_head(0)
		update_inv_l_hand(0)
		update_inv_r_hand(0)
		update_icons()

>	If you need to update all overlays you can use regenerate_icons(). it works exactly like update_clothing used to.

>	I reimplimented an old unused variable which was in the code called (coincidentally) var/update_icon
	It can be used as another method of triggering regenerate_icons(). It's basically a flag that when set to non-zero
	will call regenerate_icons() at the next life() call and then reset itself to 0.
	The idea behind it is icons are regenerated only once, even if multiple events requested it.

This system is confusing and is still a WIP. It's primary goal is speeding up the controls of the game whilst
reducing processing costs. So please bear with me while I iron out the kinks. It will be worth it, I promise.
If I can eventually free var/lying stuff from the life() process altogether, stuns/death/status stuff
will become less affected by lag-spikes and will be instantaneous! :3

If you have any questions/constructive-comments/bugs-to-report/or have a massivly devestated butt...
Please contact me on #coderbus IRC. ~Carn x
*/

//Human Overlays Indexes/////////
#define MUTATIONS_LAYER			1
#define DAMAGE_LAYER			2
#define UNIFORM_LAYER			3
#define ID_LAYER			4
#define SHOES_LAYER			5
#define GLOVES_LAYER			6
#define EARS_LAYER			7
#define SUIT_LAYER			8
#define GLASSES_LAYER			9
#define BELT_LAYER			10		//Possible make this an overlay of somethign required to wear a belt?
#define HANDS_LAYER			11
#define SUIT_STORE_LAYER		12
#define BACK_LAYER			13
#define HAIR_LAYER			14		//TODO: make part of head layer?
#define FACEMASK_LAYER			15
#define HEAD_LAYER			16
#define HANDCUFF_LAYER			17
#define LEGCUFF_LAYER			18
#define L_HAND_LAYER			19
#define R_HAND_LAYER			20
#define TAIL_LAYER			21		//bs12 specific. this hack is probably gonna come back to haunt me
#define TOTAL_LAYERS			21
//////////////////////////////////

/mob/living/carbon/human
	var/list/overlays_lying[TOTAL_LAYERS]
	var/list/overlays_standing[TOTAL_LAYERS]
	var/previous_damage_appearance // store what the body last looked like, so we only have to update it if something changed


//UPDATES OVERLAYS FROM OVERLAYS_LYING/OVERLAYS_STANDING
//this proc is messy as I was forced to include some old laggy cloaking code to it so that I don't break cloakers
//I'll work on removing that stuff by rewriting some of the cloaking stuff at a later date.
/mob/living/carbon/human/update_icons()

	lying_prev = lying	//so we don't update overlays for lying/standing unless our stance changes again
	update_hud()		//TODO: remove the need for this
	overlays = null

//	if(lying)		//can't be cloaked when lying. (for now)
//		icon = lying_icon
//		for(var/image/I in overlays_lying)
//			overlays += I
//	else
	var/stealth = 0
	if(istype(wear_suit, /obj/item/clothing/suit/space/space_ninja) && wear_suit:s_active)
		stealth = 1
	else
		//cloaking devices. //TODO: get rid of this :<
		for(var/obj/item/weapon/cloaking_device/S in list(l_hand,r_hand,belt,l_store,r_store))
			if(S.active)
				stealth = 1
				break

	alpha = stealth ? 20 : 255

	if(lying)
		if(icon != lying_icon)
			icon = lying_icon
	else
		if(icon != stand_icon)
			icon = stand_icon

	var/show_hands = 1
	if(w_uniform && (w_uniform.flags&CLOTHINGCOVERSHANDS))
		show_hands = 0
	if(wear_suit && (wear_suit.flags&CLOTHINGCOVERSHANDS))
		show_hands = 0
	if(gloves)
		show_hands = 0

	if(lying)
		for(var/image/I in overlays_lying)
			if(overlays_lying[HANDS_LAYER] && overlays_lying[HANDS_LAYER] == I && !show_hands) continue
			if(istype(I))	overlays += I
	else
		for(var/image/I in overlays_standing)
			if(overlays_standing[HANDS_LAYER] && overlays_standing[HANDS_LAYER] == I && !show_hands) continue
			if(istype(I))	overlays += I

	for(var/obj/item/device/holopad/H in contents)
		H.update_holo()

var/global/list/damage_icon_parts = list()
proc/get_damage_icon_part(damage_state, body_part, robot)
	if(damage_icon_parts["[damage_state]/[body_part]/[robot]"] == null)
		var/icon/DI = new /icon('icons/mob/dam_human.dmi', damage_state)			// the damage icon for whole human
		if(robot)
			DI.GrayScale()
		DI.Blend(new /icon('dam_mask.dmi', body_part), ICON_MULTIPLY)		// mask with this organ's pixels
		damage_icon_parts["[damage_state]/[body_part]/[robot]"] = DI
		return DI
	else
		return damage_icon_parts["[damage_state]/[body_part]/[robot]"]

//DAMAGE OVERLAYS
//constructs damage icon for each organ from mask * damage field and saves it in our overlays_ lists
/mob/living/carbon/human/UpdateDamageIcon(var/update_icons=1)
	// first check whether something actually changed about damage appearance
	var/damage_appearance = ""

	for(var/datum/organ/external/O in organs)
		if(O.status & ORGAN_DESTROYED) damage_appearance += "d"
		else
			O.update_icon(1)
			damage_appearance += O.damage_state
//	world << "[damage_appearance] and was [previous_damage_appearance]"

	if(damage_appearance == previous_damage_appearance)
		// nothing to do here
		return

	previous_damage_appearance = damage_appearance

	var/icon/standing = new /icon('dam_human.dmi', "00")

	// blend the individual damage states with our icons
	var/skeleton = (SKELETON in src.mutations)
	if(!skeleton)
		var/needs_body_redraw = 0
		for(var/datum/organ/external/O in organs)
			if(!(O.status & ORGAN_DESTROYED))
				if(O.status & ORGAN_HUSKED)
					needs_body_redraw = 1
				else
					var/icon/DI
					DI = get_damage_icon_part(O.damage_state, O.icon_name, O.status&ORGAN_ROBOT)
					standing.Blend(DI, ICON_OVERLAY)
		if(needs_body_redraw)
			update_body(0)

	overlays_standing[DAMAGE_LAYER]	= new/image(standing)
	overlays_lying[DAMAGE_LAYER]	= new/image(standing.MakeLying())

	if(update_icons)   update_icons()

/proc/get_human_species_icon_base(var/species)
	var/icon/icobase = 'icons/mob/human_races/r_human.dmi'
	switch(species)
		if("Tajaran")
			icobase = 'icons/mob/human_races/r_tajaran.dmi'
		if("Soghun")
			icobase = 'icons/mob/human_races/r_lizard.dmi'
		if("Skrell")
			icobase = 'icons/mob/human_races/r_skrell.dmi'
		if("Dragon")
			icobase = 'icons/mob/human_races/r_dragon.dmi'
		if("Slime")
			icobase = 'icons/mob/human_races/r_slimer.dmi'
	return icobase

//BASE MOB SPRITE
/mob/living/carbon/human/proc/update_body(var/update_icons=1)
	if(stand_icon)	del(stand_icon)
	if(lying_icon)	del(lying_icon)

//	var/borg_color_mod = rgb(150,150,180)
	var/husk_color_mod = rgb(66,58,50)
	var/hulk_color_mod = rgb(48,224,40)
	var/plant_color_mod = rgb(144,224,144)

	var/husk = (HUSK in src.mutations)
	var/fat = (FAT in src.mutations)
	var/hulk = (HULK in src.mutations)
	var/skeleton = (SKELETON in src.mutations)
	var/plant = (PLANT in src.mutations)

	var/cur_species = get_species()

	var/g = "m"
	if(gender == FEMALE)	g = "f"

	var/icon/icobase
	if(skeleton)
		icobase = 'icons/mob/human_races/r_skeleton.dmi'
	else
		icobase = get_human_species_icon_base(cur_species)

	stand_icon = new /icon('icons/mob/mob.dmi', "blank")
/*	if(!skeleton)
		stand_icon = new /icon(icobase, "torso_[g][fat?"_fat":""]")
		if(husk)
			stand_icon.ColorTone(husk_color_mod)
		else if(hulk)
//			stand_icon.ColorTone(hulk_color_mod)
			var/list/TONE = ReadRGB(hulk_color_mod)
			stand_icon.MapColors(rgb(TONE[1],0,0),rgb(0,TONE[2],0),rgb(0,0,TONE[3]))
		else if(plant)
			stand_icon.ColorTone(plant_color_mod)
	else
		stand_icon = new /icon(icobase, "torso")
*/
	var/datum/organ/external/head/OrgHead = get_organ("head")
	var/cyborg_head = 0
	var/has_head = 0
	if(OrgHead && !(OrgHead.status & ORGAN_DESTROYED))
		has_head = 1
		if((OrgHead.status&ORGAN_ROBOT) && !(OrgHead.status&ORGAN_ROBOT_MASKED))
			cyborg_head = 1
	if(OrgHead && (OrgHead.status & ORGAN_DESTROYED) && OrgHead.headless && istype(src.head,/obj/item/weapon/organ/head))
		has_head = 2

	//Skin tone
	if(!skeleton && !husk && !hulk && !plant)
		if(cur_species == "Human")
			if(s_tone >= 0)
				stand_icon.Blend(rgb(s_tone, s_tone, s_tone), ICON_ADD)
			else
				stand_icon.Blend(rgb(-s_tone,  -s_tone,  -s_tone), ICON_SUBTRACT)
		else if(cur_species=="Dragon")
			stand_icon.MapColors(rgb(255,0,0),rgb(r_hair,g_hair,b_hair),rgb(0,0,255))
		else if(cur_species=="Tajaran")
			stand_icon.MapColors(rgb(255,0,0),rgb(r_facial,g_facial,b_facial),rgb(r_hair,g_hair,b_hair))
		else if(cur_species=="Slime")
			stand_icon.MapColors(rgb(r_hair,g_hair,b_hair),rgb(0,255,0),rgb(0,0,255))

	var/icon/hands_standing = new('icons/mob/human.dmi',"blank")

	for(var/datum/organ/external/part in organs)
		if(!(part.status & ORGAN_DESTROYED))
			var/icon/temp
			if(istype(part, /datum/organ/external/chest))
				if(part.status&ORGAN_ROBOT)
					temp = new /icon(icobase, "torso_[g]")
				else
					temp = new /icon(icobase, "torso_[g][fat?"_fat":""]")
				if(!(part.status&ORGAN_HUSKED) && cur_species == "Dragon")
					var/icon/I = icon('icons/mob/human_races/r_dragon.dmi', "wings")
					temp.Blend(I,ICON_UNDERLAY)
			else if(istype(part, /datum/organ/external/groin))
				if(skeleton)
					temp = new /icon(icobase, "groin")
				else
					temp = new /icon(icobase, "groin_[g]")
			else if(istype(part, /datum/organ/external/head))
				if(skeleton)
					temp = new /icon(icobase, "head")
				else if(cyborg_head)
					temp = new /icon('icons/mob/human.dmi', "cyborg_head")
				else
					temp = new /icon(icobase, "head_[g]")
			else
				temp = new /icon(icobase, "[part.icon_name]")
			if((part.status&ORGAN_ROBOT) && !(part.status&ORGAN_ROBOT_MASKED))
				if(part.body_part != HEAD)
					temp.GrayScale()
//					temp.ColorTone(borg_color_mod)
			else if(!skeleton)
				if(husk || part.status&ORGAN_HUSKED)
					temp.ColorTone(husk_color_mod)
				else if(hulk)
//					temp.ColorTone(hulk_color_mod)
					var/list/TONE = ReadRGB(hulk_color_mod)
					temp.MapColors(rgb(TONE[1],0,0),rgb(0,TONE[2],0),rgb(0,0,TONE[3]))
				else if(plant)
					temp.ColorTone(plant_color_mod)
				else
					if(cur_species == "Human")
						if(s_tone >= 0)
							temp.Blend(rgb(s_tone, s_tone, s_tone), ICON_ADD)
						else
							temp.Blend(rgb(-s_tone,  -s_tone,  -s_tone), ICON_SUBTRACT)
					else if(cur_species=="Dragon")
						temp.MapColors(rgb(255,0,0),rgb(r_hair,g_hair,b_hair),rgb(0,0,255))
					else if(cur_species=="Tajaran")
						temp.MapColors(rgb(255,0,0),rgb(r_facial,g_facial,b_facial),rgb(r_hair,g_hair,b_hair))
					else if(cur_species=="Slime")
						temp.MapColors(rgb(r_hair,g_hair,b_hair),rgb(0,255,0),rgb(0,0,255))

			//That part makes left and right legs drawn topmost and lowermost when human looks WEST or EAST
			//And no change in rendering for other parts (they icon_position is 0, so goes to 'else' part)
			if(part.icon_position&(LEFT|RIGHT))
				var/icon/temp2 = new('icons/mob/human.dmi',"blank")
				temp2.Insert(new/icon(temp,dir=NORTH),dir=NORTH)
				temp2.Insert(new/icon(temp,dir=SOUTH),dir=SOUTH)
				if(!(part.icon_position & LEFT))
					temp2.Insert(new/icon(temp,dir=EAST),dir=EAST)
				if(!(part.icon_position & RIGHT))
					temp2.Insert(new/icon(temp,dir=WEST),dir=WEST)
				if(part.body_part == HAND_RIGHT || part.body_part == HAND_LEFT)
					hands_standing.Blend(temp2, ICON_OVERLAY)
					stand_icon.Blend(temp2, ICON_OVERLAY)
				else
					stand_icon.Blend(temp2, ICON_OVERLAY)
				temp2 = new('icons/mob/human.dmi',"blank")
				if(part.icon_position & LEFT)
					temp2.Insert(new/icon(temp,dir=EAST),dir=EAST)
				if(part.icon_position & RIGHT)
					temp2.Insert(new/icon(temp,dir=WEST),dir=WEST)
				stand_icon.Blend(temp2, ICON_UNDERLAY)
			else
				stand_icon.Blend(temp, ICON_OVERLAY)
		else
			if(has_head==2 && istype(part, /datum/organ/external/head))
				var/g2 = "m"
				if(src.head:unique_voice && src.head:unique_voice["gender"]==FEMALE)
					g2 = "m"
				var/icon/temp
				temp = new /icon(get_human_species_icon_base(head:species), "head_[g2]")
				var/t_s_tone = head:facials["s_tone"]
				var/t_r_facial = head:facials["r_facial"]
				var/t_g_facial = head:facials["g_facial"]
				var/t_b_facial = head:facials["b_facial"]
				var/t_r_hair = head:facials["r_hair"]
				var/t_g_hair = head:facials["g_hair"]
				var/t_b_hair = head:facials["b_hair"]
				if(head:species == "Human")
					if(s_tone >= 0)
						temp.Blend(rgb(t_s_tone, t_s_tone, t_s_tone), ICON_ADD)
					else
						temp.Blend(rgb(-t_s_tone,  -t_s_tone,  -t_s_tone), ICON_SUBTRACT)
				else if(head:species=="Dragon")
					temp.MapColors(rgb(255,0,0),rgb(t_r_hair,t_g_hair,t_b_hair),rgb(0,0,255))
				else if(head:species=="Tajaran")
					temp.MapColors(rgb(255,0,0),rgb(t_r_facial,t_g_facial,t_b_facial),rgb(t_r_hair,t_g_hair,t_b_hair))
				else if(cur_species=="Slime")
					temp.MapColors(rgb(r_hair,g_hair,b_hair),rgb(0,255,0),rgb(0,0,255))
				stand_icon.Blend(temp, ICON_OVERLAY)

	if(husk)
		var/icon/mask = new(stand_icon)
		var/icon/husk_over = new(icobase,"overlay_husk")
		mask.MapColors(0,0,0,1, 0,0,0,1, 0,0,0,1, 0,0,0,1, 0,0,0,0)
		husk_over.Blend(mask, ICON_ADD)
		stand_icon.Blend(husk_over, ICON_OVERLAY)

	if(has_head)
		if(has_head==2 && istype(src.head,/obj/item/weapon/organ/head))
			var/obj/item/weapon/organ/head/fakehead = src.head
			r_eyes = fakehead.facials["r_eyes"]
			g_eyes = fakehead.facials["g_eyes"]
			b_eyes = fakehead.facials["b_eyes"]
		//Eyes
		if(!skeleton && !cyborg_head)
			var/icon/eyes_s = new/icon('icons/mob/human_face.dmi', "eyes_s")
			eyes_s.Blend(rgb(r_eyes, g_eyes, b_eyes), ICON_ADD)
			stand_icon.Blend(eyes_s, ICON_OVERLAY)

	//Mouth	(lipstick!)
	if(!cyborg_head && lip_style)	//skeletons are allowed to wear lipstick no matter what you think, agouri.
		stand_icon.Blend(new/icon('icons/mob/human_face.dmi', "lips_[lip_style]_s"), ICON_OVERLAY)

	//Underwear
	if(underwear >0 && underwear < 12)
		if(!fat && !skeleton)
			stand_icon.Blend(new /icon('icons/mob/human.dmi', "underwear[underwear]_[g]_s"), ICON_OVERLAY)

	lying_icon = stand_icon.MakeLying()

	overlays_standing[HANDS_LAYER]	= image(hands_standing)
	overlays_lying[HANDS_LAYER]	= image(hands_standing.MakeLying())

	//tail
	update_tail_showing(0)

	if(update_icons)
		update_icons()

//HAIR OVERLAY
/mob/living/carbon/human/proc/update_hair(var/update_icons=1)
	//Reset our hair
	overlays_standing[HAIR_LAYER]	= null
	overlays_lying[HAIR_LAYER]	= null

	var/datum/organ/external/head/head_organ = get_organ("head")
	var/have_fake_head = 0
	var/t_s_tone = s_tone
	var/t_r_facial = r_facial
	var/t_g_facial = g_facial
	var/t_b_facial = b_facial
	var/t_r_hair = r_hair
	var/t_g_hair = g_hair
	var/t_b_hair = b_hair

	if(head_organ && head_organ.headless && head_organ.status&ORGAN_DESTROYED)
		if(istype(src.head,/obj/item/weapon/organ/head))
			var/obj/item/weapon/organ/head/fakehead = src.head
			have_fake_head = 1
			f_style = fakehead.facials["f_style"]
			t_r_facial = fakehead.facials["r_facial"]
			t_g_facial = fakehead.facials["g_facial"]
			t_b_facial = fakehead.facials["b_facial"]
			h_style = fakehead.facials["h_style"]
			t_r_hair = fakehead.facials["r_hair"]
			t_g_hair = fakehead.facials["g_hair"]
			t_b_hair = fakehead.facials["b_hair"]
			t_s_tone = fakehead.facials["s_tone"]

	if(!have_fake_head)
		if( !head_organ || (head_organ.status&(ORGAN_DESTROYED|ORGAN_HUSKED)) || ((head_organ.status&ORGAN_ROBOT) && !(head_organ.status&ORGAN_ROBOT_MASKED)) )
			if(update_icons)   update_icons()
			return

	//masks and helmets can obscure our hair.
	if( (head && (head.flags & BLOCKHAIR)) || (wear_mask && (wear_mask.flags & BLOCKHAIR)))
		if(update_icons)   update_icons()
		return

	//base icons
	var/icon/face_standing	= new /icon('icons/mob/human_face.dmi',"bald_s")

	if(f_style)
		var/datum/sprite_accessory/facial_hair_style = facial_hair_styles_list[f_style]
		if(facial_hair_style)
			var/icon/facial_s = new/icon("icon" = facial_hair_style.icon, "icon_state" = "[facial_hair_style.icon_state]_s")
			facial_s.Blend(rgb(t_r_facial, t_g_facial, t_b_facial), ICON_ADD)
			face_standing.Blend(facial_s, ICON_OVERLAY)

	if(h_style)
		var/datum/sprite_accessory/hair_style = hair_styles_list[h_style]
		if(hair_style)
			var/icon/hair_s = new/icon("icon" = hair_style.icon, "icon_state" = "[hair_style.icon_state]_s")
			if( get_species() == "Dragon")
				hair_s.MapColors(rgb(220+t_s_tone,0,0),rgb(0,220+t_s_tone,0),rgb(0,0,220+t_s_tone))
			else if( get_species() == "Tajaran")
				hair_s.MapColors(rgb(255,0,0),rgb(t_r_facial,t_g_facial,t_b_facial),rgb(t_r_hair,t_g_hair,t_b_hair))
			else
				hair_s.Blend(rgb(t_r_hair, t_g_hair, t_b_hair), ICON_ADD)
			face_standing.Blend(hair_s, ICON_OVERLAY)

	overlays_standing[HAIR_LAYER]	= image(face_standing)
	overlays_lying[HAIR_LAYER]	= image(face_standing.MakeLying())

	if(update_icons)   update_icons()

/mob/living/carbon/human/update_mutations(var/update_icons=1)
	var/fat = ""
	if(FAT in mutations)
		fat = "fat"

	var/icon/base = new('icons/effects/genetics.dmi')
	var/icon/standing = new(base, "")
	var/add_image = 0
//	var/g = "m"
//	if(gender == FEMALE)	g = "f"
	if(stat != DEAD)
		for(var/mut in mutations)
			switch(mut)
//				if(HULK)
//					if(fat)
//						standing.Blend(new/icon(base,"hulk_[fat]_s"),ICON_UNDERLAY)
//					else
//						standing.Blend(new/icon(base,"hulk_[g]_s"),ICON_UNDERLAY)
//					add_image = 1
				if(COLD_RESISTANCE)
					standing.Blend(new/icon(base,"fire[fat]_s"),ICON_OVERLAY)
					add_image = 1
				if(TK)
					var/list/jediSkills = mutations & list(JediReflect,JediGrab,JediLightning,JediPush)
					if(jediSkills.len) continue
					standing.Blend(new/icon(base,"telekinesishead[fat]_s"),ICON_OVERLAY)
					add_image = 1
				if(LASER)
					standing.Blend(new/icon(base,"lasereyes_s"),ICON_OVERLAY)
					add_image = 1
	if(add_image)
		overlays_standing[MUTATIONS_LAYER]	= image(standing)
		overlays_lying[MUTATIONS_LAYER]	= image(standing.MakeLying())
	else
		overlays_lying[MUTATIONS_LAYER]		= null
		overlays_standing[MUTATIONS_LAYER]	= null
	if(update_icons)   update_icons()


/mob/living/carbon/human/proc/update_mutantrace(var/update_icons=1)
	update_body(0)
	update_hair(0)
	if(update_icons)   update_icons()

/* --------------------------------------- */
//For legacy support.
/mob/living/carbon/human/regenerate_icons()
	..()
	if(monkeyizing)		return
	update_mutations(0)
	update_mutantrace(0)
	update_inv_w_uniform(0)
	update_inv_wear_id(0)
	update_inv_gloves(0)
	update_inv_glasses(0)
	update_inv_ears(0)
	update_inv_shoes(0)
	update_inv_s_store(0)
	update_inv_wear_mask(0)
	update_inv_head(0)
	update_inv_belt(0)
	update_inv_back(0)
	update_inv_wear_suit(0)
	update_inv_r_hand(0)
	update_inv_l_hand(0)
	update_inv_handcuffed(0)
	update_inv_legcuffed(0)
	update_inv_pockets(0)
	UpdateDamageIcon(0)
	update_icons()
	//Hud Stuff
	update_hud()

/* --------------------------------------- */
//vvvvvv UPDATE_INV PROCS vvvvvv

/mob/living/carbon/human/update_inv_w_uniform(var/update_icons=1)
	if(w_uniform && istype(w_uniform, /obj/item/clothing/under) )
		w_uniform.screen_loc = ui_iclothing
		var/t_color = w_uniform._color
		if(!t_color)		t_color = icon_state
		var/icon/standing = new('icons/mob/uniform.dmi',"[t_color]_s")
		if((FAT in src.mutations) && ("[t_color]_s" in icon_states('icons/mob/uniform_fat.dmi')))
			standing = new('icons/mob/uniform_fat.dmi',"[t_color]_s")

		if(w_uniform.blood_DNA)
			standing.Blend(icon('icons/effects/blood.dmi', "uniformblood"),ICON_OVERLAY)

		if(w_uniform:hastie)	//WE CHECKED THE TYPE ABOVE. THIS REALLY SHOULD BE FINE.
			var/tie_color = w_uniform:hastie._color
			if(!tie_color) tie_color = w_uniform:hastie.icon_state
			standing.Blend(icon('icons/mob/ties.dmi', "[tie_color]"),ICON_OVERLAY)

		overlays_standing[UNIFORM_LAYER]	= image(standing)
		overlays_lying[UNIFORM_LAYER]		= image(standing.MakeLying())
	else
		overlays_lying[UNIFORM_LAYER]		= null
		overlays_standing[UNIFORM_LAYER]	= null
		// Automatically drop anything in store / id / belt if you're not wearing a uniform.	//CHECK IF NECESARRY
		for( var/obj/item/thing in list(r_store, l_store, wear_id, belt) )
			if(thing)
				if(thing==belt && istype(belt,/obj/item/weapon/storage/belt)) continue
				u_equip(thing)
				if (client)
					client.screen -= thing
																								//
				if (thing)
					thing.loc = loc
					thing.dropped(src)
					thing.layer = initial(thing.layer)
	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_wear_id(var/update_icons=1)
	if(wear_id)
		var/icon/I = new('icons/mob/mob.dmi',"id")
		overlays_standing[ID_LAYER]	= image(I)
		overlays_lying[ID_LAYER]	= image(I.MakeLying())
		wear_id.screen_loc = ui_id	//TODO
	else
		overlays_lying[ID_LAYER]	= null
		overlays_standing[ID_LAYER]	= null
	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_gloves(var/update_icons=1)
	if(gloves)
		var/t_state = gloves.item_state
		if(!t_state) t_state = gloves.icon_state
		var/icofile = 'icons/mob/hands.dmi'
		if(gloves.item_icon)
			icofile = gloves.item_icon
			t_state = t_state+"_gloves"
		var/icon/standing = new(icofile, "[t_state]")
		if(gloves.blood_DNA)
			standing.Blend(icon('icons/effects/blood.dmi', "bloodyhands"),ICON_OVERLAY)
		gloves.screen_loc = ui_gloves
		overlays_standing[GLOVES_LAYER]	= image(standing)
		overlays_lying[GLOVES_LAYER]	= image(standing.MakeLying())
	else
		if(blood_DNA)
			var/icon/I = new('icons/effects/blood.dmi', "bloodyhands")
			overlays_standing[GLOVES_LAYER]	= image(I)
			overlays_lying[GLOVES_LAYER]	= image(I.MakeLying())
		else
			overlays_lying[GLOVES_LAYER]	= null
			overlays_standing[GLOVES_LAYER]	= null
	if(update_icons)   update_icons()


/mob/living/carbon/human/update_inv_glasses(var/update_icons=1)
	if(glasses)
		var/icofile = 'icons/mob/eyes.dmi'
		if(glasses.item_icon) icofile = glasses.item_icon
		var/icon/I = new(icofile, glasses.item_state?"[glasses.item_state]":"[glasses.icon_state]")
		overlays_standing[GLASSES_LAYER]	= image(I)
		overlays_lying[GLASSES_LAYER]		= image(I.MakeLying())
	else
		overlays_lying[GLASSES_LAYER]		= null
		overlays_standing[GLASSES_LAYER]	= null
	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_ears(var/update_icons=1)
	if(ears)
		var/icofile = 'icons/mob/ears.dmi'
		if(ears.item_icon) icofile = ears.item_icon
		var/icon/I = new(icofile, ears.item_state?"[ears.item_state]":"[ears.icon_state]")
		overlays_standing[EARS_LAYER] = image(I)
		overlays_lying[EARS_LAYER] = image(I.MakeLying())
	else
		overlays_lying[EARS_LAYER]		= null
		overlays_standing[EARS_LAYER]	= null
	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_shoes(var/update_icons=1)
	if(shoes)
		var/icofile = 'icons/mob/feet.dmi'
		var/t_state = shoes.icon_state
		if(shoes.item_icon)
			icofile = shoes.item_icon
			t_state = t_state+"_shoes"
		var/icon/standing = new(icofile, "[t_state]")
		if(shoes.blood_DNA)
			standing.Blend(icon('icons/effects/blood.dmi', "shoeblood"),ICON_OVERLAY)
		overlays_standing[SHOES_LAYER]	= image(standing)
		overlays_lying[SHOES_LAYER]	= image(standing.MakeLying())
	else
		overlays_lying[SHOES_LAYER]			= null
		overlays_standing[SHOES_LAYER]		= null
	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_s_store(var/update_icons=1)
	if(s_store)
		var/t_state = s_store.item_state
		if(!t_state)	t_state = s_store.icon_state
		var/icon/I = new('icons/mob/belt_mirror.dmi', "[t_state]")
		overlays_standing[SUIT_STORE_LAYER]	= image(I)
		overlays_lying[SUIT_STORE_LAYER]	= image(I.MakeLying())
		s_store.screen_loc = ui_sstore1		//TODO
	else
		overlays_lying[SUIT_STORE_LAYER]	= null
		overlays_standing[SUIT_STORE_LAYER]	= null
	if(update_icons)   update_icons()


/mob/living/carbon/human/update_inv_head(var/update_icons=1)
	if(head)
		head.screen_loc = ui_head		//TODO
		var/icon/standing
		if(istype(head,/obj/item/clothing/head/foil))
			if(istype(r_hand,/obj/item/tk_grab))
				del(r_hand)
			if(istype(l_hand,/obj/item/tk_grab))
				del(l_hand)
		if(istype(head,/obj/item/clothing/head/kitty))
			standing = new("icon" = head:mob)
		else
			var/icofile = 'icons/mob/head.dmi'
			var/t_state = head.icon_state
			if(head.item_icon)
				icofile = head.item_icon
				t_state = t_state+"_head"
			standing = icon(icofile, "[t_state]")
		if(head.blood_DNA)
			standing.Blend(icon('icons/effects/blood.dmi', "helmetblood"),ICON_OVERLAY)
		overlays_standing[HEAD_LAYER]	= image(standing)
		overlays_lying[HEAD_LAYER]	= image(standing.MakeLying())
	else
		overlays_lying[HEAD_LAYER]		= null
		overlays_standing[HEAD_LAYER]	= null
	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_belt(var/update_icons=1)
	if(belt)
		belt.screen_loc = ui_belt	//TODO
		var/t_state = belt.item_state
		if(!t_state)	t_state = belt.icon_state
		var/icon/I = new('icons/mob/belt.dmi', "[t_state]")
		overlays_standing[BELT_LAYER]	= image(I)
		overlays_lying[BELT_LAYER]	= image(I.MakeLying())
	else
		overlays_lying[BELT_LAYER]		= null
		overlays_standing[BELT_LAYER]	= null
	if(update_icons)   update_icons()


/mob/living/carbon/human/update_inv_wear_suit(var/update_icons=1)
	if( wear_suit && istype(wear_suit, /obj/item/clothing/suit) )	//TODO check this
		wear_suit.screen_loc = ui_oclothing	//TODO
		var/icofile = 'icons/mob/suit.dmi'
		var/t_state = wear_suit.icon_state
		if(wear_suit.item_icon)
			icofile = wear_suit.item_icon
			t_state = t_state + "_suit"
		var/icon/standing = new(icofile, "[t_state]")

		if( istype(wear_suit, /obj/item/clothing/suit/straight_jacket) )
			drop_from_inventory(handcuffed)
			drop_l_hand()
			drop_r_hand()

		if(wear_suit.blood_DNA)
			if( istype(wear_suit, /obj/item/clothing/suit/armor/vest || /obj/item/clothing/suit/wcoat) )
				t_state = "armor"
			else if( istype(wear_suit, /obj/item/clothing/suit/det_suit || /obj/item/clothing/suit/labcoat) )
				t_state = "coat"
			else
				t_state = "suit"
			standing.Blend(icon('icons/effects/blood.dmi', "[t_state]blood"), ICON_OVERLAY)

		overlays_standing[SUIT_LAYER]	= image(standing)
		overlays_lying[SUIT_LAYER]	= image(standing.MakeLying())

		update_tail_showing(0)

	else
		overlays_lying[SUIT_LAYER]		= null
		overlays_standing[SUIT_LAYER]	= null

		update_tail_showing(0)

	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_pockets(var/update_icons=1)
	if(l_store)			l_store.screen_loc = ui_storage1	//TODO
	if(r_store)			r_store.screen_loc = ui_storage2	//TODO
	if(update_icons)	update_icons()


/mob/living/carbon/human/update_inv_wear_mask(var/update_icons=1)
	if( wear_mask && istype(wear_mask, /obj/item/clothing/mask) )
		wear_mask.screen_loc = ui_mask	//TODO
		var/icofile = 'icons/mob/mask.dmi'
		var/t_state = wear_mask.icon_state
		if(wear_mask.item_icon)
			icofile = wear_mask.item_icon
			t_state += "_mask"
		var/icon/standing = icon(icofile, "[t_state]")
		if( !istype(wear_mask, /obj/item/clothing/mask/cigarette) && wear_mask.blood_DNA )
			standing.Blend(icon('icons/effects/blood.dmi', "maskblood"),ICON_OVERLAY)
		overlays_standing[FACEMASK_LAYER]	= image(standing)
		overlays_lying[FACEMASK_LAYER]		= image(standing.MakeLying())
	else
		overlays_lying[FACEMASK_LAYER]		= null
		overlays_standing[FACEMASK_LAYER]	= null
	if(update_icons)   update_icons()


/mob/living/carbon/human/update_inv_back(var/update_icons=1)
	if(back)
		back.screen_loc = ui_back	//TODO
		var/icofile = 'icons/mob/back.dmi'
		var/t_state = back.icon_state
		if(back.item_icon)
			icofile = back.item_icon
			t_state += "_back"
		var/icon/I = new(icofile, "[t_state]")
		overlays_standing[BACK_LAYER]	= image(I)
		overlays_lying[BACK_LAYER]	= image(I.MakeLying())
	else
		overlays_lying[BACK_LAYER]	= null
		overlays_standing[BACK_LAYER]	= null
	if(update_icons)   update_icons()


/mob/living/carbon/human/update_hud()	//TODO: do away with this if possible
	if(client)
		client.screen |= contents
		if(hud_used)
			hud_used.hidden_inventory_update() 	//Updates the screenloc of the items on the 'other' inventory bar


/mob/living/carbon/human/update_inv_handcuffed(var/update_icons=1)
	if(handcuffed)
		drop_r_hand()
		drop_l_hand()
		stop_pulling()	//TODO: should be handled elsewhere
		var/icon/I = new("icon" = 'icons/mob/mob.dmi', "icon_state" = "handcuff1")
		overlays_standing[HANDCUFF_LAYER]	= image(I)
		overlays_lying[HANDCUFF_LAYER]		= image(I.MakeLying())
	else
		overlays_lying[HANDCUFF_LAYER]		= null
		overlays_standing[HANDCUFF_LAYER]	= null
	if(update_icons)   update_icons()

/mob/living/carbon/human/update_inv_legcuffed(var/update_icons=1)
	if(legcuffed)
		var/icon/I = new("icon" = 'icons/mob/mob.dmi', "icon_state" = "legcuff1")
		overlays_standing[LEGCUFF_LAYER]	= image(I)
		overlays_lying[LEGCUFF_LAYER]		= image(I.MakeLying())
		if(src.m_intent != "walk")
			src.m_intent = "walk"
			if(src.hud_used && src.hud_used.move_intent)
				src.hud_used.move_intent.icon_state = "walking"

	else
		overlays_lying[LEGCUFF_LAYER]		= null
		overlays_standing[LEGCUFF_LAYER]	= null
	if(update_icons)   update_icons()


/mob/living/carbon/human/update_inv_r_hand(var/update_icons=1)
	if(r_hand)
		r_hand.screen_loc = ui_rhand	//TODO
		var/t_state = r_hand.item_state
		if(!t_state)	t_state = r_hand.icon_state
		var/iconfile = r_hand.item_icon
		if(!iconfile)
			iconfile = 'icons/mob/items_righthand.dmi'
			if(!t_state || t_state=="")
				t_state="nothing"
		else
			t_state = t_state + "_r"
		var/icon/I = new("icon" = iconfile, "icon_state" = "[t_state]")
		overlays_standing[R_HAND_LAYER] = image(I)
		overlays_lying[R_HAND_LAYER] = image(I.MakeLying())
		if (handcuffed) drop_r_hand()
	else
		overlays_standing[R_HAND_LAYER] = null
		overlays_lying[R_HAND_LAYER] = null
	if(update_icons)   update_icons()


/mob/living/carbon/human/update_inv_l_hand(var/update_icons=1)
	if(l_hand)
		l_hand.screen_loc = ui_lhand	//TODO
		var/t_state = l_hand.item_state
		if(!t_state)	t_state = l_hand.icon_state
		var/iconfile = l_hand.item_icon
		if(!iconfile)
			iconfile = 'icons/mob/items_lefthand.dmi'
			if(!t_state || t_state=="")
				t_state="nothing"
		else
			t_state = t_state + "_l"
		var/icon/I = new("icon" = iconfile, "icon_state" = "[t_state]")
		overlays_standing[L_HAND_LAYER] = image(I)
		overlays_lying[L_HAND_LAYER] = image(I.MakeLying())
		if (handcuffed) drop_l_hand()
	else
		overlays_standing[L_HAND_LAYER] = null
		overlays_lying[L_HAND_LAYER] = null
	if(update_icons)   update_icons()

/mob/living/carbon/human/proc/update_tail_showing(var/update_icons=1)
	overlays_lying[TAIL_LAYER] = null
	overlays_standing[TAIL_LAYER] = null
	var/cur_species = get_species()
	var/icon/I = null
	var/datum/organ/external/groin/OrgGroin = get_organ("groin")
	if(OrgGroin && !(OrgGroin.status & ORGAN_HUSKED))
		if( cur_species == "Tajaran")
			if(!wear_suit || !(wear_suit.flags_inv & HIDEJUMPSUIT) && !istype(wear_suit, /obj/item/clothing/suit/space))
				I = icon('icons/mob/human_races/r_tajaran.dmi', "tajtail_s")
				I.MapColors(rgb(255,0,0),rgb(0,255,0),rgb(r_hair,g_hair,b_hair))
		else if( cur_species == "Soghun")
			if(!wear_suit || !(wear_suit.flags_inv & HIDEJUMPSUIT) && !istype(wear_suit, /obj/item/clothing/suit/space))
				I = icon('icons/effects/species.dmi', "sogtail_s")
		else if( cur_species == "Dragon")
			var/icon/Iwings = icon('icons/mob/human_races/r_dragon.dmi', "wings")
			Iwings.MapColors(rgb(255,0,0),rgb(r_hair,g_hair,b_hair),rgb(0,0,255))
			I = new('icons/mob/human.dmi',"blank")
			I.Insert(new/icon(Iwings,dir=NORTH),dir=NORTH)

	if(PLANT in src.mutations)
		I.ColorTone(rgb(144,224,144))

	if(I)
		overlays_standing[TAIL_LAYER] = image(I)
		overlays_lying[TAIL_LAYER] = image(I.MakeLying())

	if(update_icons)
		update_icons()

// Used mostly for creating head items
/mob/living/carbon/human/proc/generate_head_icon()
//gender no longer matters for the mouth, although there should probably be seperate base head icons.
//	var/g = "m"
//	if (gender == FEMALE)	g = "f"

	//base icons
	var/icon/face_lying		= new /icon('icons/mob/human_face.dmi',"bald_s")

	if(f_style)
		var/datum/sprite_accessory/facial_hair_style = facial_hair_styles_list[f_style]
		if(facial_hair_style)
			var/icon/facial_l = new/icon("icon" = facial_hair_style.icon, "icon_state" = "[facial_hair_style.icon_state]_s")
			facial_l.Blend(rgb(r_facial, g_facial, b_facial), ICON_ADD)
			face_lying.Blend(facial_l, ICON_OVERLAY)

	if(h_style)
		var/datum/sprite_accessory/hair_style = hair_styles_list[h_style]
		if(hair_style)
			var/icon/hair_s = new/icon("icon" = hair_style.icon, "icon_state" = "[hair_style.icon_state]_s")
			if( get_species() == "Dragon")
				hair_s.MapColors(rgb(220+s_tone,0,0),rgb(0,220+s_tone,0),rgb(0,0,220+s_tone))
			else if( get_species() == "Tajaran")
				hair_s.MapColors(rgb(255,0,0),rgb(r_facial,g_facial,b_facial),rgb(r_hair,g_hair,b_hair))
			else
				hair_s.Blend(rgb(r_hair, g_hair, b_hair), ICON_ADD)
			face_lying.Blend(hair_s, ICON_OVERLAY)

	//Eyes
	// Note: These used to be in update_face(), and the fact they're here will make it difficult to create a disembodied head
	var/icon/eyes_l = new/icon('icons/mob/human_face.dmi', "eyes_s")
	eyes_l.Blend(rgb(r_eyes, g_eyes, b_eyes), ICON_ADD)
	face_lying.Blend(eyes_l, ICON_OVERLAY)

	if(lip_style)
		face_lying.Blend(new/icon('icons/mob/human_face.dmi', "lips_[lip_style]_s"), ICON_OVERLAY)

	var/image/face_lying_image = new /image(icon = face_lying.MakeLying())
	return face_lying_image

//Human Overlays Indexes/////////
#undef MUTANTRACE_LAYER
#undef MUTATIONS_LAYER
#undef DAMAGE_LAYER
#undef UNIFORM_LAYER
#undef ID_LAYER
#undef SHOES_LAYER
#undef GLOVES_LAYER
#undef EARS_LAYER
#undef SUIT_LAYER
#undef GLASSES_LAYER
#undef FACEMASK_LAYER
#undef BELT_LAYER
#undef SUIT_STORE_LAYER
#undef BACK_LAYER
#undef HAIR_LAYER
#undef HEAD_LAYER
#undef HANDCUFF_LAYER
#undef LEGCUFF_LAYER
#undef L_HAND_LAYER
#undef R_HAND_LAYER
#undef TAIL_LAYER
#undef TOTAL_LAYERS
