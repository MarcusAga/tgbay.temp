/mob/living/carbon/monkey/gib()
	death(1)
	var/atom/movable/overlay/animation = null
	monkeyizing = 1
	canmove = 0
	icon = null
	invisibility = 101

	animation = new(loc)
	animation.icon_state = "blank"
	animation.icon = 'icons/mob/mob.dmi'
	animation.master = src

	flick("gibbed-m", animation)
	gibs(loc, viruses, dna)

	spawn(15)
		if(animation)	del(animation)
		if(src)			del(src)

/mob/living/carbon/monkey/dust()
	death(1)
	var/atom/movable/overlay/animation = null
	monkeyizing = 1
	canmove = 0
	icon = null
	invisibility = 101

	animation = new(loc)
	animation.icon_state = "blank"
	animation.icon = 'icons/mob/mob.dmi'
	animation.master = src

	flick("dust-m", animation)
	var/obj/effect/decal/cleanable/ash/A = new(loc)
	A.amount = 60

	spawn(15)
		if(animation)	del(animation)
		if(src)			del(src)


/mob/living/carbon/monkey/death(gibbed)
	if(stat == DEAD)	return
	if(hud_healths)			hud_healths.icon_state = "health5"
	stat = DEAD

	if(!gibbed)
		emote("deathgasp") //let the world KNOW WE ARE DEAD

	update_canmove()
	if(hud_blind)	hud_blind.layer = 0

	ticker.mode.check_win()

	return ..(gibbed)