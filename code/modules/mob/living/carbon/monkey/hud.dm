/datum/hud/proc/monkey_hud(var/ui_style='icons/mob/screen1_old.dmi')

	src.adding = list()
	src.other = list()

	var/obj/screen/using
	var/obj/screen/inventory/inv_box

	using = new /obj/screen()
	using.name = "act_intent"
	using.dir = SOUTHWEST
	using.icon = ui_style
	using.icon_state = (mymob.a_intent == "hurt" ? "harm" : mymob.a_intent)
	using.screen_loc = ui_acti
	using.layer = 20
	src.adding += using
	action_intent = using

//intent small hud objects
	var/icon/ico

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),1,ico.Height()/2,ico.Width()/2,ico.Height())
	using = new /obj/screen( src )
	using.name = "help"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	help_intent = using

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),ico.Width()/2,ico.Height()/2,ico.Width(),ico.Height())
	using = new /obj/screen( src )
	using.name = "disarm"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	disarm_intent = using

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),ico.Width()/2,1,ico.Width(),ico.Height()/2)
	using = new /obj/screen( src )
	using.name = "grab"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	grab_intent = using

	ico = new(ui_style, "black")
	ico.MapColors(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, -1,-1,-1,-1)
	ico.DrawBox(rgb(255,255,255,1),1,1,ico.Width()/2,ico.Height()/2)
	using = new /obj/screen( src )
	using.name = "harm"
	using.icon = ico
	using.screen_loc = ui_acti
	using.layer = 21
	src.adding += using
	hurt_intent = using

//end intent small hud objects

	using = new /obj/screen()
	using.name = "mov_intent"
	using.dir = SOUTHWEST
	using.icon = ui_style
	using.icon_state = (mymob.m_intent == "run" ? "running" : "walking")
	using.screen_loc = ui_movi
	using.layer = 20
	src.adding += using
	move_intent = using

	using = new /obj/screen()
	using.name = "drop"
	using.icon = ui_style
	using.icon_state = "act_drop"
	using.screen_loc = ui_drop_throw
	using.layer = 19
	src.adding += using

	inv_box = new /obj/screen/inventory()
	inv_box.name = "r_hand"
	inv_box.dir = WEST
	inv_box.icon = ui_style
	inv_box.icon_state = "hand_inactive"
	if(mymob && !mymob.hand)	//This being 0 or null means the right hand is in use
		inv_box.icon_state = "hand_active"
	inv_box.screen_loc = ui_rhand
	inv_box.slot_id = slot_r_hand
	inv_box.layer = 19
	src.r_hand_hud_object = inv_box
	src.adding += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "l_hand"
	inv_box.dir = EAST
	inv_box.icon = ui_style
	inv_box.icon_state = "hand_inactive"
	if(mymob && mymob.hand)	//This being 1 means the left hand is in use
		inv_box.icon_state = "hand_active"
	inv_box.screen_loc = ui_lhand
	inv_box.slot_id = slot_l_hand
	inv_box.layer = 19
	src.l_hand_hud_object = inv_box
	src.adding += inv_box

	using = new /obj/screen()
	using.name = "hand"
	using.dir = SOUTH
	using.icon = ui_style
	using.icon_state = "hand1"
	using.screen_loc = ui_swaphand1
	using.layer = 19
	src.adding += using

	using = new /obj/screen()
	using.name = "hand"
	using.dir = SOUTH
	using.icon = ui_style
	using.icon_state = "hand2"
	using.screen_loc = ui_swaphand2
	using.layer = 19
	src.adding += using

	inv_box = new /obj/screen/inventory()
	inv_box.name = "mask"
	inv_box.dir = NORTH
	inv_box.icon = ui_style
	inv_box.icon_state = "equip"
	inv_box.screen_loc = ui_monkey_mask
	inv_box.slot_id = slot_wear_mask
	inv_box.layer = 19
	src.adding += inv_box

	inv_box = new /obj/screen/inventory()
	inv_box.name = "back"
	inv_box.dir = NORTHEAST
	inv_box.icon = ui_style
	inv_box.icon_state = "equip"
	inv_box.screen_loc = ui_back
	inv_box.slot_id = slot_back
	inv_box.layer = 19
	src.adding += inv_box

	mymob.hud_throw_icon = new /obj/screen()
	mymob.hud_throw_icon.icon = ui_style
	mymob.hud_throw_icon.icon_state = "act_throw_off"
	mymob.hud_throw_icon.name = "throw"
	mymob.hud_throw_icon.screen_loc = ui_drop_throw

	mymob.hud_oxygen = new /obj/screen()
	mymob.hud_oxygen.icon = ui_style
	mymob.hud_oxygen.icon_state = "oxy0"
	mymob.hud_oxygen.name = "oxygen"
	mymob.hud_oxygen.screen_loc = ui_oxygen

	mymob.hud_pressure = new /obj/screen()
	mymob.hud_pressure.icon = ui_style
	mymob.hud_pressure.icon_state = "pressure0"
	mymob.hud_pressure.name = "pressure"
	mymob.hud_pressure.screen_loc = ui_pressure

	mymob.hud_toxin = new /obj/screen()
	mymob.hud_toxin.icon = ui_style
	mymob.hud_toxin.icon_state = "tox0"
	mymob.hud_toxin.name = "toxin"
	mymob.hud_toxin.screen_loc = ui_toxin

	mymob.hud_internals = new /obj/screen()
	mymob.hud_internals.icon = ui_style
	mymob.hud_internals.icon_state = "internal0"
	mymob.hud_internals.name = "internal"
	mymob.hud_internals.screen_loc = ui_internal

	mymob.hud_fire = new /obj/screen()
	mymob.hud_fire.icon = ui_style
	mymob.hud_fire.icon_state = "fire0"
	mymob.hud_fire.name = "fire"
	mymob.hud_fire.screen_loc = ui_fire

	mymob.hud_bodytemp = new /obj/screen()
	mymob.hud_bodytemp.icon = ui_style
	mymob.hud_bodytemp.icon_state = "temp1"
	mymob.hud_bodytemp.name = "body temperature"
	mymob.hud_bodytemp.screen_loc = ui_temp

	mymob.hud_healths = new /obj/screen()
	mymob.hud_healths.icon = ui_style
	mymob.hud_healths.icon_state = "health0"
	mymob.hud_healths.name = "health"
	mymob.hud_healths.screen_loc = ui_health

	mymob.hud_pullin = new /obj/screen()
	mymob.hud_pullin.icon = ui_style
	mymob.hud_pullin.icon_state = "pull0"
	mymob.hud_pullin.name = "pull"
	mymob.hud_pullin.screen_loc = ui_pull_resist

	mymob.hud_blind = new /obj/screen()
	mymob.hud_blind.icon = 'icons/mob/screen1_full.dmi'
	mymob.hud_blind.icon_state = "blackimageoverlay"
	mymob.hud_blind.name = " "
	mymob.hud_blind.screen_loc = "1,1"
	mymob.hud_blind.layer = 0

	mymob.hud_flash = new /obj/screen()
	mymob.hud_flash.icon = ui_style
	mymob.hud_flash.icon_state = "blank"
	mymob.hud_flash.name = "flash"
	mymob.hud_flash.screen_loc = "1,1 to 15,15"
	mymob.hud_flash.layer = 17

	mymob.zone_sel = new /obj/screen/zone_sel()
	ico = new/icon(ui_style, "zone_sel")
	ico.Scale(128,128)
	mymob.zone_sel.icon = ico
	mymob.zone_sel.overlays = null
	mymob.zone_sel.overlays += image('icons/mob/zone_sel.dmi', "[mymob.zone_sel.selecting]")

	mymob.client.screen = null

	mymob.client.screen += list( mymob.hud_throw_icon, mymob.zone_sel, mymob.hud_oxygen, mymob.hud_pressure, mymob.hud_toxin, mymob.hud_bodytemp, mymob.hud_internals, mymob.hud_fire, mymob.hud_healths, mymob.hud_pullin, mymob.hud_blind, mymob.hud_flash) //, mymob.hands, mymob.rest, mymob.sleep, mymob.mach )
	mymob.client.screen += src.adding + src.other

	return

