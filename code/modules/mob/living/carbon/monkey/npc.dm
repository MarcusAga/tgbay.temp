/mob/living/carbon/monkey/var
	mob/npc_target = null // the NPC this monkey is attacking
	mob/npc_fleeing = null // the monkey is scared of this mob
	mob/hiding_behind = null
	hid_behind = 0

	var/list/hostiles = list()

	fleeing_duration = 0

/mob/living/carbon/monkey/proc/npc_act()
	if(being_strategy_controlled) return
	if(!client && !stat)
		if(npc_fleeing && canmove)
			var/prevloc = loc
			if(!hiding_behind)
				for(var/mob/living/carbon/human/H in view(7, src))
					if(!hostiles.Find(H))
						hiding_behind = H

			if(hiding_behind)
				if(get_dist(src, hiding_behind) == 1)
					if(!hid_behind)
						direct_emote(1, "hides behind [hiding_behind]!")
						hid_behind = 1
					step_to(src, get_step(hiding_behind, get_dir(npc_fleeing, hiding_behind)))
				else
					if(!step_to(src, hiding_behind, 1))
						hiding_behind = null
			else
				step_away(src, npc_fleeing, 7)

			if(prob(7))
				if(prob(50) && (npc_fleeing in view(8,src)))
					switch(rand(1,3))
						if(1)
							direct_emote(1, "shows [npc_fleeing] its fangs!")
						if(2)
							direct_emote(2, "gnarls at [npc_fleeing].")
						if(3)
							direct_emote(2, "eyes [npc_fleeing] fearfully.")
				else
					switch(rand(1,3))
						if(1)
							emote("whimper")
						if(2)
							direct_emote(1, "trembles heavily.")
						if(3)
							direct_emote(2, "chimpers nervously.")

			fleeing_duration--
			if(fleeing_duration <= 0)
				npc_fleeing = null
				hiding_behind = null
				hid_behind = 0

			if(loc == prevloc) dir = get_dir(src, npc_fleeing)
		else
			var/list/inview = view(7, src)
			var/obj/item/weapon/reagent_containers/food/snacks/grown/banana/banana = null
			for(var/atom/A in inview)
				if(banana && prob(50)) continue
				if(istype(A,/obj/item/weapon/reagent_containers/food/snacks/grown/banana))
					banana=A
				else if(istype(A,/mob/living/carbon/human))
					var/mob/living/carbon/human/H = A
					if(istype(H.l_hand,/obj/item/weapon/reagent_containers/food/snacks/grown/banana))
						banana=H.l_hand
					else if(istype(H.r_hand,/obj/item/weapon/reagent_containers/food/snacks/grown/banana))
						banana=H.r_hand
			if(banana)
				if(isturf(banana.loc) && get_dist(banana.loc,src.loc)<=1)
					banana.attack(src,src,"head")
				else if(prob(10))
					var/place=""
					if(ishuman(banana.loc))
						place=" in the [banana.loc]'s hand"
					for(var/mob/O in viewers(src, null))
						O.show_message("[src] stares hungrily at the [banana][place]", 1)
			if(canmove && isturf(loc))
				if(banana)
					step_to(src, get_turf(banana))
				else if(prob(33))
					step(src, pick(cardinal))
			if(prob(1))
				if(health < 70)
					switch(rand(1,3))
						if(1)
							direct_emote(1, "cowers on the floor, writhing in pain.")
						if(2)
							direct_emote(1, "trembles visibly, it seems to be in pain.")
						if(3)
							direct_emote(1, "wraps its arms around its knees, breathing heavily.")
				else
					emote(pick("scratch","jump","roll","tail"))

/mob/living/carbon/monkey/proc/react_to_attack(mob/M)
	if(npc_fleeing == M)
		fleeing_duration += 30
		return

	if(!hostiles.Find(M)) hostiles += M

	spawn(5)
		switch(rand(1,3))
			if(1)
				direct_emote(1, "flails about wildly!")
			if(2)
				direct_emote(2, "screams loudly[M?" at [M]":""]!")
			if(3)
				direct_emote(2, "whimpers fearfully!")

		npc_fleeing = M
		fleeing_duration = 30


/mob/living/carbon/monkey/adjustBruteLoss(var/damage)
	if(!client && !stat && damage>0)
		react_to_attack(usr)
		if(damage > 10)
			if(prob(40) || health == 100)
				direct_emote(2, pick("screams loudly!", "whimpers in pain!"))
		else if(health == 100 || (damage > 0 && prob(10)))
			direct_emote(1, pick("flails about wildly!", "cringes visibly!", "chimpers nervously."))
	return ..()

/mob/living/carbon/monkey/proc/direct_emote(var/m_type,var/str)
	var/message="[src] [str]"
	if ((str && src.stat == 0))
		if (m_type & 1)
			for(var/mob/O in viewers(src, null))
				O.show_message(message, m_type)
				//Foreach goto(703)
		else
			for(var/mob/O in hearers(src, null))
				O.show_message(message, m_type)
				//Foreach goto(746)
