#define SAY_MINIMUM_PRESSURE 10
var/list/department_radio_keys = list(
	  ":r" = "right hand",
	  ":l" = "left hand",
	  ":i" = "intercom",
	  ":h" = "department",
	  ":c" = "Command",
	  ":n" = "Science",
	  ":m" = "Medical",
	  ":e" = "Engineering",
	  ":s" = "Security",
	  ":w" = "whisper",
	  ":b" = "binary",
	  ":a" = "alientalk",
	  ":t" = "Syndicate",
	  ":d" = "Mining",
	  ":q" = "Cargo",
	  ":g" = "changeling",

	  ":R" = "right hand",
	  ":L" = "left hand",
	  ":I" = "intercom",
	  ":H" = "department",
	  ":C" = "Command",
	  ":N" = "Science",
	  ":M" = "Medical",
	  ":E" = "Engineering",
	  ":S" = "Security",
	  ":W" = "whisper",
	  ":B" = "binary",
	  ":A" = "alientalk",
	  ":T" = "Syndicate",
	  ":D" = "Mining",
	  ":Q" = "Cargo",
	  ":G" = "changeling",

	  //kinda localization -- rastaf0
	  //same keys as above, but on russian keyboard layout. This file uses cp1251 as encoding.
	  ":�" = "right hand",
	  ":�" = "left hand",
	  ":�" = "intercom",
	  ":�" = "department",
	  ":�" = "Command",
	  ":�" = "Science",
	  ":�" = "Medical",
	  ":�" = "Engineering",
	  ":�" = "Security",
	  ":�" = "whisper",
	  ":�" = "binary",
	  ":�" = "alientalk",
	  ":�" = "Syndicate",
	  ":�" = "Mining",
	  ":�" = "Cargo",
	  ":�" = "changeling",

	  ":�" = "right hand",
	  ":�" = "left hand",
	  ":�" = "intercom",
	  ":�" = "department",
	  ":�" = "Command",
	  ":�" = "Science",
	  ":�" = "Medical",
	  ":�" = "Engineering",
	  ":�" = "Security",
	  ":�" = "whisper",
	  ":�" = "binary",
	  ":�" = "alientalk",
	  ":�" = "Syndicate",
	  ":�" = "Mining",
	  ":�" = "Cargo",
	  ":�" = "changeling"
)

/mob/living/proc/binarycheck()
	if(say_can_use_language("Robot")) return 1
	if (!ishuman(src))
		return 0
	var/mob/living/carbon/human/H = src
	if (H.ears)
		var/obj/item/device/radio/headset/dongle = H.ears
		if(!istype(dongle)) return 0
		if(dongle.translate_binary) return 1

/mob/living/proc/hivecheck()
	if(say_can_use_language("Xenomorph")) return 1
	if (!ishuman(src)) return
	var/mob/living/carbon/human/H = src
	if (H.ears)
		var/obj/item/device/radio/headset/dongle = H.ears
		if(!istype(dongle)) return
		if(dongle.translate_hive) return 1

/mob/living/say(var/message as text)
	var/original_message = message
	message = trim(copytext(sanitize(message), 1, MAX_MESSAGE_LEN))
	message = capitalize(message)

	if (!message)
		return

	if(!speech_allowed && usr == src)
		usr << "\red You can't speak."
		return

	if (stat == 2)
		return say_dead(message)

	if (src.client)
		if(client.muted & MUTE_IC)
			src << "\red You cannot speak in IC (muted)."
			return
		if (src.client.handle_spam_prevention(message,MUTE_IC))
			return

	// stat == 2 is handled above, so this stops transmission of uncontious messages
	if (stat)
		return

	// Mute disability
	if (sdisabilities & MUTE)
		return

	if (istype(wear_mask, /obj/item/clothing/mask/muzzle))
		return

	if(reagents)
		var/datum/reagent/R
		for(var/datum/reagent/R1 in reagents.reagent_list)
			if(R1.id == "kurare")
				R=R1
				break

		if(R && R.data>10)
			whisper(message)
			return

	// emotes
	if (copytext(message, 1, 2) == "*" && !stat)
		return emote(copytext(message, 2))

	var/alt_name = ""
//	if (istype(src, /mob/living/carbon/human) && name != GetVoice())
//		var/mob/living/carbon/human/H = src
//		alt_name = " (as [H.get_id_name("Unknown")])"
	var/italics = 0
	var/message_range = 8
	var/message_mode = null

	var/channel_prefix = null
	if (getBrainLoss() >= 60 && prob(50))
		if (ishuman(src))
			message_mode = "headset"
	// Special message handling
	else if (copytext(message, 1, 2) == ";" || copytext(message, 1, 2) == ".")
		if (ishuman(src))
			message_mode = "headset"
		else if(ispAI(src) || isrobot(src))
			message_mode = "pAI"
		message = copytext(message, 2)

	else if (length(message) >= 2)
		channel_prefix = copytext(message, 1, 3)

		message_mode = department_radio_keys[channel_prefix]
		//world << "channel_prefix=[channel_prefix]; message_mode=[message_mode]"
		if (message_mode)
			message = trim(copytext(message, 3))
			if (!(ishuman(src) || isanimal(src) || isrobot(src) && (message_mode=="department" || (message_mode in radiochannels))))
				message_mode = null //only humans can use headsets
			// Check removed so parrots can use headsets!
			// And borgs -Sieve

	if (!message)
		return

	//work out if we're speaking skrell or not
	var/language = default_language
	var/list/all_known_languages = (islist(known_languages)?(known_languages.Copy()):(list())) | (src.mind?(src.mind.known_languages):(list()))
	for(var/lang in LANGUAGES)
		var/list/keys = LANGUAGES[lang]
		if(copytext(message, 1, 3) in keys)
			message = copytext(message, 3)
			message = trim(message, 1, MAX_MESSAGE_LEN)
			if(default_language=="Universal" || (lang in all_known_languages))
				language = lang
			else
				usr << "You can't speak '[lang]' language"

	if(usr && usr.client)
		if(winget(usr, "saybutton", "is-checked"))
			var/cmd = ""
			if(message_mode && channel_prefix)
				cmd += channel_prefix
			if(language != default_language)
				var/list/keys = LANGUAGES[language]
				cmd += keys[1]
			var/list/params = list("command"="say","text"=cmd)
			winset(usr, "input", list2params(params))
	
	// :downs:
	if (getBrainLoss() >= 60)
		message = dd_replacetext(message, " am ", " ")
		message = dd_replacetext(message, " is ", " ")
		message = dd_replacetext(message, " are ", " ")
		message = dd_replacetext(message, "you", "u")
		message = dd_replacetext(message, "help", "halp")
		message = dd_replacetext(message, "grief", "grife")
		message = dd_replacetext(message, "space", "spess")
		message = dd_replacetext(message, "carp", "crap")
		message = dd_replacetext(message, "reason", "raisin")

		message = dd_replacetext(message, "�������", "�������")
		message = dd_replacetext(message, "���", "���")
		message = dd_replacetext(message, "�����", "�����")
		message = dd_replacetext(message, "����", "�����")
		message = dd_replacetext(message, "���", "����")
		message = dd_replacetext(message, "���", "����")
		message = dd_replacetext(message, "���", "����")
		message = dd_replacetext(message, "������", "������")
		if(prob(50))
			message = uppertext(message)
			message += "[stutter(pick("!", "!!", "!!!"))]"
		if(!stuttering && prob(15))
			message = stutter(message)

	if (stuttering)
		message = stutter(message)

/* //qw do not have beesease atm.
	if(virus)
		if(virus.name=="beesease" && virus.stage>=2)
			if(prob(virus.stage*10))
				var/bzz = length(message)
				message = "B"
				for(var/i=0,i<bzz,i++)
					message += "Z"
*/
	var/list/obj/item/used_radios = new

	var/turf/T = get_turf(src)

	var/message_a = say_quote(message,language)
	var/message_b

	if (voice_message)
		message_b = voice_message
	else
		message_b = stars(message)
		if(language=="Robot")
			message_b = Gibberish(message_b,100)
		message_b = say_quote(message_b,language)
	var/list/talkinto_vars = list("msg"=message_a,"rmsg"=message,"vmsg"=message_b,"lang"=language,"voice"=GetVoice(),"gender"=src.gender)
	var/voice_hash = null
	var/voice_gender = gender
	if(ishuman(src))
		if(src.dna && src.dna.get_unique_enzymes())
			if(mind && mind.changeling && mind.changeling.mimicing)
				voice_hash = mind.changeling.mimicing
			else
				voice_hash = src.dna.get_unique_enzymes()
		if(ishuman(src))
			var/mob/living/carbon/human/Me = src
			var/datum/organ/external/head/OHead = Me.get_organ("head")
			if(OHead && (OHead.status & ORGAN_DESTROYED) && OHead.headless)
				var/obj/item/helmet = Me.head
				if(istype(helmet,/obj/item/weapon/organ/head) && helmet:unique_voice)
					voice_hash = helmet:unique_voice["voice"]
					voice_gender = helmet:unique_voice["gender"]

		var/obj/item/clothing/mask/gas/voice/Mask = src.wear_mask
		var/obj/item/weapon/implant/modulator/Modulator = locate() in src
		if(!isnull(Modulator))
			Mask = Modulator.DummyMask
		if(istype(Mask))
			if(Mask.vchange && Mask.voice["hash"])
				voice_hash = Mask.voice["hash"]
				voice_gender = Mask.voice["sex"]
		talkinto_vars["uni_voice"] = voice_hash
		talkinto_vars["gender"] = voice_gender

	if(src:mind && (!src:mind.known_voices[src.dna.get_unique_enzymes()] || src:mind.known_voices[src.dna.get_unique_enzymes()] == "Unknown"))
		src:mind.known_voices[src.dna.get_unique_enzymes()] = "Yourself"

	switch (message_mode)
		if ("headset")
			if (src:ears)
				src:ears.talk_into(src,talkinto_vars)
				used_radios += src:ears

			message_range = 1
			italics = 1

		if ("right hand")
			if (r_hand)
				r_hand.talk_into(src,talkinto_vars)
				used_radios += src:r_hand

			message_range = 1
			italics = 1

		if ("left hand")
			if (l_hand)
				l_hand.talk_into(src,talkinto_vars)
				used_radios += src:l_hand

			message_range = 1
			italics = 1

		if ("intercom")
			for (var/obj/item/device/radio/intercom/I in view(1, null))
				I.talk_into(src,talkinto_vars)
				used_radios += I

			message_range = 1
			italics = 1

		//I see no reason to restrict such way of whispering
		if ("whisper")
			whisper(message)
			return

		if ("binary")
			if(binarycheck())
			//message = trim(copytext(sanitize(message), 1, MAX_MESSAGE_LEN)) //seems redundant
				robot_talk(message)
			return

		if ("alientalk")
			if(hivecheck())
			//message = trim(copytext(sanitize(message), 1, MAX_MESSAGE_LEN)) //seems redundant
				alien_talk(message)
			return

		if ("department")
			if(istype(src, /mob/living/carbon))
				if (src:ears)
					src:ears.talk_into(src,talkinto_vars, message_mode)
					used_radios += src:ears
			else if(istype(src, /mob/living/silicon/robot))
				if (src:radio)
					src:radio.talk_into(src,talkinto_vars, message_mode)
					used_radios += src:radio
			message_range = 1
			italics = 1

		if ("pAI")
			if (src:radio)
				src:radio.talk_into(src,talkinto_vars)
				used_radios += src:radio
			message_range = 1
			italics = 1

		if("changeling")
			if(mind && mind.changeling)
				for(var/mob/Changeling in mob_list)
					if((Changeling.mind && Changeling.mind.changeling) || istype(Changeling, /mob/dead/observer))
						Changeling << "<i><font color=#800080><b>[mind.changeling.changelingID]:</b> [message]</font></i>"
			return
////SPECIAL HEADSETS START
		else
			//world << "SPECIAL HEADSETS"
			if (message_mode in radiochannels)
				if(isrobot(src))//Seperates robots to prevent runtimes from the ear stuff
					var/mob/living/silicon/robot/R = src
					if(R.radio)//Sanityyyy
						R.radio.talk_into(src,talkinto_vars, message_mode)
						used_radios += R.radio
				else
					if (src:ears)
						src:ears.talk_into(src,talkinto_vars, message_mode)
						used_radios += src:ears
				message_range = 1
				italics = 1
/////SPECIAL HEADSETS END

	var/datum/gas_mixture/environment = T.return_air()
	if(environment)
		var/pressure = environment.return_pressure()
		if (pressure < SAY_MINIMUM_PRESSURE)	//in space no one can hear you scream
			italics = 1
			message_range = 1

	var/list/listening = get_mobs_in_hear(message_range, src)

	for(var/mob/M in player_list)
		if (!M.client)
			continue //skip monkeys and leavers
		if (istype(M, /mob/new_player))
			continue
		if((M.stat == 2 || isadminobserver(M)) && M.client.ghost_ears)
			listening|=M

	var/list/W = hear(message_range, T)

	for (var/obj/O in ((W | contents)-used_radios))
		W |= O

	for (var/mob/M in W)
		W |= M.contents

	for (var/atom/A in W)
		if(istype(A, /mob/living/simple_animal/parrot)) //Parrot speech mimickry
			if(A == src)
				continue //Dont imitate ourselves

			var/mob/living/simple_animal/parrot/P = A
			if(P.speech_buffer.len >= 10)
				P.speech_buffer.Remove(pick(P.speech_buffer))
			P.speech_buffer.Add(message)

		if(istype(A, /obj/)) //radio in pocket could work, radio in backpack wouldn't --rastaf0
			var/obj/O = A
			spawn (0)
				if(O && !istype(O.loc, /obj/item/weapon/storage))
					O.hear_talk(src, talkinto_vars)


/*			Commented out as replaced by code above from BS12
	for (var/obj/O in ((V | contents)-used_radios)) //radio in pocket could work, radio in backpack wouldn't --rastaf0
		spawn (0)
			if (O)
				O.hear_talk(src, message)
*/

/*	if(isbrain(src))//For brains to properly talk if they are in an MMI..or in a brain. Could be extended to other mobs I guess.
		for(var/obj/O in loc)//Kinda ugly but whatever.
			if(O)
				spawn(0)
					O.hear_talk(src, message)
*/


	var/list/heard_a = list() // understood us
	var/list/heard_b = list() // didn't understand us

	for (var/M in listening)
		if(hascall(M,"say_understands"))
			if (M:say_understands(language))
				heard_a += M
			else
				heard_b += M

	var/speech_bubble_test = say_test(message)
	var/robot = ""
	if(issilicon(src))
		robot = "R"
	var/image/speech_bubble = image('icons/mob/talk.dmi',src,"h[robot][speech_bubble_test]")
	spawn(30) del(speech_bubble)

	var/rendered = null
	if (length(heard_a))
		var/unknown_voice = voice_name
		switch(voice_gender)
			if(MALE)
				unknown_voice = "[unknown_voice] male"
			if(FEMALE)
				unknown_voice = "[unknown_voice] female"
		if(voice_hash)
			unknown_voice = "[unknown_voice] ([copytext(voice_hash, 1, 6)])"
		var/visible_name = null
		if(ishuman(src))
			var/mob/living/carbon/human/Me = src
			if(istype(Me.wear_id, /obj/item/device/pda))
				var/obj/item/device/pda/PDA = Me.wear_id
				visible_name = "[PDA.owner] - [PDA.ownjob]"
			else if(istype(Me.wear_id, /obj/item/weapon/card/id))
				var/obj/item/weapon/card/id/ID = Me.wear_id
				visible_name = "[ID.registered_name] - [ID.assignment]"

		if (italics)
			message_a = "<i>[message_a]</i>"

		rendered = "<span class='game say'><span class='name'>[unknown_voice]</span>[alt_name] <span class='message'>[message_a]</span></span>"

		for (var/M in heard_a)
			if(ishuman(M) && M:fake_visions[src])
				var/list/D = M:fake_visions[src]
				var/cstm_rendered = "<span class='game say'><span class='name'>[D["name"]]</span>[alt_name] <span class='message'>[message_a]</span></span>"
				M:show_message(cstm_rendered, 2)
				M << speech_bubble
			else if(ishuman(src) && (isliving(M) && !M:mind || isadminobserver(M)))
				var/cstm_rendered = "<span class='game say'><span class='name'>[src.real_name]</span>[alt_name] <span class='message'>[message_a]</span></span>"
				M:show_message(cstm_rendered, 2)
				M << speech_bubble
			else if(ishuman(src) && voice_hash && M:mind)
				if(src == M)
					if(!M:mind.known_voices[voice_hash])
						M:mind.known_voices[voice_hash] = "Me"
				else if(visible_name && M:stat==0 && (!M:mind.known_voices[voice_hash] || M:mind.known_voices[voice_hash] == unknown_voice))
					var/mob/living/carbon/human/He = M
					var/id_radius = 7
					if( istype(He) && He.disabilities & NEARSIGHTED && !istype(He.glasses, /obj/item/clothing/glasses/regular) )
						id_radius = 3
					if(src in view(id_radius,M))
						M:mind.known_voices[voice_hash] = visible_name
				if(!M:mind.known_voices[voice_hash])
					M:mind.known_voices[voice_hash] = unknown_voice
				var/cstm_rendered = "<span class='game say'><span class='name'><a href='?src=\ref[M:mind];voice=[voice_hash]'>[sanitize_simple(M:mind.known_voices[voice_hash])]</a></span>[alt_name] <span class='message'>[message_a]</span></span>"
				M:show_message(cstm_rendered, 2)
				M << speech_bubble
			else if(hascall(M,"show_message"))
				M:show_message(rendered, 2)
				M << speech_bubble

	if (length(heard_b))
		if (italics)
			message_b = "<i>[message_b]</i>"

		rendered = "<span class='game say'><span class='name'>[voice_name]</span> <span class='message'>[message_b]</span></span>"

		for (var/M in heard_b)
			if(hascall(M,"show_message"))
				M:show_message(rendered, 2)
				M << speech_bubble

	//talking crystals
	for(var/obj/item/weapon/talkingcrystal/O in view(3,src))
		O.catchMessage(message,src)

	log_say("[name]/[key] : [original_message]")

/obj/effect/speech_bubble
	var/mob/parent

/mob/living/proc/GetVoice()
	return name
