// AI EYE
//
// An invisible (no icon) mob that the AI controls to look around the station with.
// It streams chunks as it moves around, which will show it what the AI can and cannot see.

/mob/aiEye
	name = "Inactive AI Eye"
	icon = 'icons/obj/status_display.dmi' // For AI friend secret shh :o
	var/list/visibleCameraChunks = list()
	var/mob/living/silicon/ai/ai = null
	density = 0
	nodamage = 1 // You can't damage it.
	mouse_opacity = 0
	invisibility = 60

// Movement code. Returns 0 to stop air movement from moving it.
/mob/aiEye/Move()
	return 0

// Hide popout menu verbs
/mob/aiEye/examine()
	set src in world
	..()

/mob/aiEye/pull()
	set popup_menu = 0
	set src = usr.contents
	return 0

/mob/aiEye/point()
	set popup_menu = 0
	set src = usr.contents
	return 0

/mob/living/silicon/ai/var/obj/item/weapon/photo/Photo = null
/mob/living/silicon/ai/verb/verbPhoto(var/turf/target in world)
	set name = "Take a Photo"
	set category = null
	makePhoto(target)

/mob/living/silicon/ai/proc/makePhoto(var/turf/target)
	if(Photo)
		for(var/obj/machinery/ai_status_display/O in world) //change status
			if(O.Picture)
				O.Picture = null
				O.emotion = src.emotion

	var/x_c = target.x - 1
	var/y_c = target.y + 1
	var/z_c	= target.z

	var/icon/temp = icon('96x96.dmi',"")
	var/icon/black = icon('space.dmi', "black")
	var/mobs = ""
	var/list/faces = list()
	for (var/i = 1; i <= 3; i++)
		for (var/j = 1; j <= 3; j++)
			var/turf/T = locate(x_c,y_c,z_c)
			var/datum/camerachunk/chunk = cameranet.getCameraChunk(T.x, T.y, T.z)
			var/visible = 0
			if(chunk)
				if(chunk.changed)
					chunk.hasChanged(1) // Update now, no matter if it's visible or not.
				if(T in chunk.visibleTurfs)
					visible = 1
			if(visible)
				temp.Blend(call(/obj/item/weapon/camera_test/proc/get_icon)(T),ICON_OVERLAY,64*(j-1-1),64 - 64*(i-1))
			else
				temp.Blend(black,ICON_OVERLAY,64*(j-1),64 - 64*(i-1-1))
			mobs = call(/obj/item/weapon/camera_test/proc/get_mobs)(T,mobs,i*3+j-3)
			faces += call(/obj/item/weapon/camera_test/proc/get_faces)(T)
			x_c++
		y_c--
		x_c = x_c - 3

	if(!Photo)
		Photo = new/obj/item/weapon/photo(src)
	var/icon/small_img = icon(temp)
	var/icon/ic = icon('items.dmi',"photo")
	small_img.Scale(16,16)
	ic.Blend(small_img,ICON_OVERLAY,19,25)
	Photo.layer = 18
	Photo.icon = ic
	Photo.img = temp
	Photo.desc = mobs
	Photo.alt_desc = mobs
	Photo.faces = faces
	Photo.pixel_x = 0
	Photo.pixel_y = 0
	Photo.screen_loc = "1,NORTH"
	client.screen |= Photo

// Use this when setting the aiEye's location.
// It will also stream the chunk that the new loc is in.

/mob/aiEye/proc/setLoc(var/T)
	if(ai && ishuman(ai.loc) && ai.loc:isCyborg() && ai.loc:fakeAI==ai)
		T = get_turf(ai.loc)
	else
		T = get_turf(T)
	loc = T
	cameranet.visibility(src)
	if(ai)
		if(ai.client)
			ai.client.eye = src
		//Holopad
		if(istype(ai.current_holopad, /obj/machinery/hologram/holopad))
			var/obj/machinery/hologram/holopad/H = ai.current_holopad
			H.move_hologram()
		else if(istype(ai.current_holopad, /obj/effect/overlay))
			step_to(ai.current_holopad, src) // So it turns.
			ai.current_holopad.loc = get_turf(src)


// AI MOVEMENT

// The AI's "eye". Described on the top of the page.

/mob/living/silicon/ai
	var/mob/aiEye/eyeobj = new()
	var/sprint = 10
	var/cooldown = 0
	var/acceleration = 1


// Intiliaze the eye by assigning it's "ai" variable to us. Then set it's loc to us.
/mob/living/silicon/ai/New()
	..()
	eyeobj.ai = src
	eyeobj.name = "[src.name] (AI Eye)" // Give it a name
	spawn(5)
		eyeobj.loc = src.loc

/mob/living/silicon/ai/Del()
	eyeobj.ai = null
	del(eyeobj) // No AI, no Eye
	..()

/*
/atom/proc/move_camera_by_click()
	if(istype(usr, /mob/living/silicon/ai))
		var/mob/living/silicon/ai/AI = usr
		if(AI.eyeobj && AI.client.eye == AI.eyeobj)
			AI.cameraFollow = null
			AI.eyeobj.setLoc(src)
*/

/mob/living/DblClick(location,control,params)
	var/list/parameters = list()
	if(params)
		parameters = params2list(params)

	if(isAI(usr) && usr != src && !parameters["middle"])
		var/mob/living/silicon/ai/A = usr
		A.ai_actual_track(src)
		return
	..()

// This will move the AIEye. It will also cause lights near the eye to light up, if toggled.
// This is handled in the proc below this one.

/client/proc/AIMove(n, direct, var/mob/living/silicon/ai/user)
	if(ishuman(user.loc) && user.loc:isCyborg() && user.loc:fakeAI==user)
		var/mob/living/carbon/human/H = user.loc
		if(H.isCyborg() && H.fakeAI==user)
			mob = H
			return
		return

	var/initial = initial(user.sprint)
	var/max_sprint = 50

	if(user.cooldown && user.cooldown < world.timeofday) // 3 seconds
		user.sprint = initial

	for(var/i = 0; i < max(user.sprint, initial); i += 20)
		var/turf/step = get_turf(get_step(user.eyeobj, direct))
		if(step)
			user.eyeobj.setLoc(step)

	user.cooldown = world.timeofday + 5
	if(user.acceleration)
		user.sprint = min(user.sprint + 0.5, max_sprint)
	else
		user.sprint = initial

	user.cameraFollow = null
	src.eye = user.eyeobj

	//user.machine = null //Uncomment this if it causes problems.
	user.lightNearbyCamera()


// Return to the Core.

/mob/living/silicon/ai/verb/core()
	set category = "AI Commands"
	set name = "AI Core"

	view_core()


/mob/living/silicon/ai/proc/view_core()

	current = null
	cameraFollow = null
	machine = null

	if(src.eyeobj && src.loc)
		src.eyeobj.loc = src.loc
	else
		src << "ERROR: Eyeobj not found. Please report this to Giacom. Creating new eye..."
		src.eyeobj = new(src.loc)

	if(client && client.eye)
		client.eye = src
	for(var/datum/camerachunk/c in eyeobj.visibleCameraChunks)
		c.remove(eyeobj)

/mob/living/silicon/ai/verb/toggle_acceleration()
	set category = "AI Commands"
	set name = "Toggle Camera Acceleration"

	acceleration = !acceleration
	usr << "Camera acceleration has been toggled [acceleration ? "on" : "off"]."
