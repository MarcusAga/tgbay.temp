
/datum/hud/proc/robot_hud()

	src.adding = list()
	src.other = list()

	var/obj/screen/using


//Radio
	using = new /obj/screen()
	using.name = "radio"
	using.dir = SOUTHWEST
	using.icon = 'icons/mob/screen1_robot.dmi'
	using.icon_state = "radio"
	using.screen_loc = ui_movi
	using.layer = 20
	src.adding += using

//Module select

	using = new /obj/screen()
	using.name = "module1"
	using.dir = SOUTHWEST
	using.icon = 'icons/mob/screen1_robot.dmi'
	using.icon_state = "inv1"
	using.screen_loc = ui_inv1
	using.layer = 20
	src.adding += using
	mymob:hud_inv1 = using

	using = new /obj/screen()
	using.name = "module2"
	using.dir = SOUTHWEST
	using.icon = 'icons/mob/screen1_robot.dmi'
	using.icon_state = "inv2"
	using.screen_loc = ui_inv2
	using.layer = 20
	src.adding += using
	mymob:hud_inv2 = using

	using = new /obj/screen()
	using.name = "module3"
	using.dir = SOUTHWEST
	using.icon = 'icons/mob/screen1_robot.dmi'
	using.icon_state = "inv3"
	using.screen_loc = ui_inv3
	using.layer = 20
	src.adding += using
	mymob:hud_inv3 = using

//End of module select

//Intent
	using = new /obj/screen()
	using.name = "act_intent"
	using.dir = SOUTHWEST
	using.icon = 'icons/mob/screen1_robot.dmi'
	using.icon_state = (mymob.a_intent == "hurt" ? "harm" : mymob.a_intent)
	using.screen_loc = ui_acti
	using.layer = 20
	src.adding += using
	action_intent = using

//Cell
	mymob:hud_cells = new /obj/screen()
	mymob:hud_cells.icon = 'icons/mob/screen1_robot.dmi'
	mymob:hud_cells.icon_state = "charge-empty"
	mymob:hud_cells.name = "cell"
	mymob:hud_cells.screen_loc = ui_toxin

//Health
	mymob.hud_healths = new /obj/screen()
	mymob.hud_healths.icon = 'icons/mob/screen1_robot.dmi'
	mymob.hud_healths.icon_state = "health0"
	mymob.hud_healths.name = "health"
	mymob.hud_healths.screen_loc = ui_borg_health

//Installed Module
	mymob.hud_hands = new /obj/screen()
	mymob.hud_hands.icon = 'icons/mob/screen1_robot.dmi'
	mymob.hud_hands.icon_state = "nomod"
	mymob.hud_hands.name = "module"
	mymob.hud_hands.screen_loc = ui_borg_module

//Module Panel
	using = new /obj/screen()
	using.name = "panel"
	using.icon = 'icons/mob/screen1_robot.dmi'
	using.icon_state = "panel"
	using.screen_loc = ui_borg_panel
	using.layer = 19
	src.adding += using

//Store
	mymob.hud_throw_icon = new /obj/screen()
	mymob.hud_throw_icon.icon = 'icons/mob/screen1_robot.dmi'
	mymob.hud_throw_icon.icon_state = "store"
	mymob.hud_throw_icon.name = "store"
	mymob.hud_throw_icon.screen_loc = ui_borg_store

//Temp
	mymob.hud_bodytemp = new /obj/screen()
	mymob.hud_bodytemp.icon_state = "temp0"
	mymob.hud_bodytemp.name = "body temperature"
	mymob.hud_bodytemp.screen_loc = ui_temp


	mymob.hud_oxygen = new /obj/screen()
	mymob.hud_oxygen.icon = 'icons/mob/screen1_robot.dmi'
	mymob.hud_oxygen.icon_state = "oxy0"
	mymob.hud_oxygen.name = "oxygen"
	mymob.hud_oxygen.screen_loc = ui_oxygen

	mymob.hud_fire = new /obj/screen()
	mymob.hud_fire.icon = 'icons/mob/screen1_robot.dmi'
	mymob.hud_fire.icon_state = "fire0"
	mymob.hud_fire.name = "fire"
	mymob.hud_fire.screen_loc = ui_fire

	mymob.hud_pullin = new /obj/screen()
	mymob.hud_pullin.icon = 'icons/mob/screen1_robot.dmi'
	mymob.hud_pullin.icon_state = "pull0"
	mymob.hud_pullin.name = "pull"
	mymob.hud_pullin.screen_loc = ui_borg_pull

	mymob.hud_blind = new /obj/screen()
	mymob.hud_blind.icon = 'icons/mob/screen1_full.dmi'
	mymob.hud_blind.icon_state = "blackimageoverlay"
	mymob.hud_blind.name = " "
	mymob.hud_blind.screen_loc = "1,1"
	mymob.hud_blind.layer = 0

	mymob.hud_flash = new /obj/screen()
	mymob.hud_flash.icon = 'icons/mob/screen1_robot.dmi'
	mymob.hud_flash.icon_state = "blank"
	mymob.hud_flash.name = "flash"
	mymob.hud_flash.screen_loc = "1,1 to 15,15"
	mymob.hud_flash.layer = 17

	mymob.zone_sel = new /obj/screen/zone_sel()
	var/icon/ico = new('icons/mob/screen1_robot.dmi', "zone_sel")
	ico.Scale(128,128)
	mymob.zone_sel.icon = ico
	mymob.zone_sel.overlays = null
	mymob.zone_sel.overlays += image('icons/mob/zone_sel.dmi', "[mymob.zone_sel.selecting]")

	mymob.client.screen = null

	mymob.client.screen += list( mymob.hud_throw_icon, mymob.zone_sel, mymob.hud_oxygen, mymob.hud_fire, mymob.hud_hands, mymob.hud_healths, mymob:hud_cells, mymob.hud_pullin, mymob.hud_blind, mymob.hud_flash) //, mymob.rest, mymob.sleep, mymob.mach )
	mymob.client.screen += src.adding + src.other

	return
