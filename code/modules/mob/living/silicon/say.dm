//
/mob/living/silicon/say(var/message as text)
	if (!message)
		return

	if (src.client)
		if(client.muted & MUTE_IC)
			src << "You cannot send IC messages (muted)."
			return
		if (src.client.handle_spam_prevention(message,MUTE_IC))
			return

	message = sanitize(message)

	if (stat == 2)
		message = trim(copytext(message, 1, MAX_MESSAGE_LEN))
		return say_dead(message)

	//Must be concious to speak
	if (stat)
		return

	if (length(message) >= 2)
		var/intercomdir = 0
		if ((copytext(message, 1, 3) == ":b") || (copytext(message, 1, 3) == ":B") || (copytext(message, 1, 3) == ":�") || (copytext(message, 1, 3) == ":�"))
			if(istype(src, /mob/living/silicon/pai))
				return ..(message)
			message = copytext(message, 3)
			message = trim(message, 1, MAX_MESSAGE_LEN)
			robot_talk(message)
			if(usr && usr.client)
				if(winget(usr, "saybutton", "is-checked"))
					var/list/params = list("command"="say","text"=":b")
					winset(usr, "input", list2params(params))
			return
		else if(copytext(message, 1, 3) == ":l" || copytext(message, 1, 3) == ":L" || copytext(message, 1, 3) == ":�" || copytext(message, 1, 3) == ":�")
			intercomdir = 1
			message = copytext(message, 3)
			message = trim(message, 1, MAX_MESSAGE_LEN)
			if(usr && usr.client)
				if(winget(usr, "saybutton", "is-checked"))
					var/list/params = list("command"="say","text"=":l")
					winset(usr, "input", list2params(params))
		else if(copytext(message, 1, 3) == ":c" || copytext(message, 1, 3) == ":C" || copytext(message, 1, 3) == ":�" || copytext(message, 1, 3) == ":�")
			intercomdir = 2
			message = copytext(message, 3)
			message = trim(message, 1, MAX_MESSAGE_LEN)
			if(usr && usr.client)
				if(winget(usr, "saybutton", "is-checked"))
					var/list/params = list("command"="say","text"=":c")
					winset(usr, "input", list2params(params))
		else if(copytext(message, 1, 3) == ":r" || copytext(message, 1, 3) == ":R" || copytext(message, 1, 3) == ":�" || copytext(message, 1, 3) == ":�")
			intercomdir = 3
			message = copytext(message, 3)
			message = trim(message, 1, MAX_MESSAGE_LEN)
			if(usr && usr.client)
				if(winget(usr, "saybutton", "is-checked"))
					var/list/params = list("command"="say","text"=":r")
					winset(usr, "input", list2params(params))
		else if (isAI(src) && ((copytext(message, 1, 3) == ":h") || (copytext(message, 1, 3) == ":H") || (copytext(message, 1, 3) == ":�") || (copytext(message, 1, 3) == ":�")))
			if(client)//For patching directly into AI holopads.
				var/mob/living/silicon/ai/U = src
				message = copytext(message, 3)
				message = trim(copytext(sanitize(message), 1, MAX_MESSAGE_LEN))
				U.holopad_talk(message)
				if(usr && usr.client)
					if(winget(usr, "saybutton", "is-checked"))
						var/list/params = list("command"="say","text"=":h")
						winset(usr, "input", list2params(params))
				return
			else//Will not allow anyone by an active AI to use this function.
				src << "This function is not available to you."
				return
		if(!isAI(src) || !intercomdir) return ..(message)
		else
			var/language = default_language
			var/list/all_known_languages = (islist(known_languages)?(known_languages):(list("-Universal"))) | (src.mind?(src.mind.known_languages):(list()))
			for(var/lang in LANGUAGES)
				var/list/keys = LANGUAGES[lang]
				if(copytext(message, 1, 3) in keys)
					message = copytext(message, 3)
					message = trim(message, 1, MAX_MESSAGE_LEN)
					if(default_language=="Universal" || (lang in all_known_languages))
						language = lang
					else
						usr << "You can't speak '[lang]' language"
			var/list/msg_packet = list("rmsg"=message,"msg"=say_quote(message,language),"lang"=language,"vmsg"=say_quote(stars(message),language),"voice"=GetVoice())
			for(var/obj/item/device/radio/intercom/I in range(src,1))
				if(I.aidir == intercomdir)
					I.talk_into(src,msg_packet)
	else
		return ..(message)

//For holopads only. Usable by AI.
/mob/living/silicon/ai/proc/holopad_talk(var/message)

	log_say("[key_name(src)] : [message]")

	message = trim(message)

	if (!message)
		return

	var/obj/machinery/hologram/holopad/T = src.current_holopad
	if(istype(T) && T.hologram && T.master == src)//If there is a hologram and its master is the user.
		var/message_a = say_quote(message)

		//Human-like, sorta, heard by those who understand humans.
		var/rendered_a = "<span class='game say'><span class='name'>[name]</span> <span class='message'>[message_a]</span></span>"

		//Speach distorted, heard by those who do not understand AIs.
		message = stars(message)
		var/message_b = say_quote(message)
		var/rendered_b = "<span class='game say'><span class='name'>[voice_name]</span> <span class='message'>[message_b]</span></span>"

		var/language = default_language
		for(var/lang in LANGUAGES)
			var/list/keys = LANGUAGES[lang]
			if(copytext(message, 1, 3) in keys)
				message = copytext(message, 3)
				message = trim(message, 1, MAX_MESSAGE_LEN)
				if(default_language=="Universal" || (lang in src.known_languages))
					language = lang
		src << "<i><span class='game say'>Holopad transmitted, <span class='name'>[real_name]</span> <span class='message'>[message_a]</span></span></i>"//The AI can "hear" its own message.
		for(var/mob/M in hearers(T.loc))//The location is the object, default distance.
			if(M.say_understands(language))//If they understand AI speak. Humans and the like will be able to.
				M.show_message(rendered_a, 2)
			else//If they do not.
				M.show_message(rendered_b, 2)
		/*Radios "filter out" this conversation channel so we don't need to account for them.
		This is another way of saying that we won't bother dealing with them.*/
	else
		src << "No holopad connected."
	return

/mob/living/proc/robot_talk(var/message)
	log_say("[key_name(src)] : [message]")

	message = trim(message)

	if (!message)
		return

	var/message_a = say_quote(message)
	var/rname = name
	if(ishuman(src))
		if(src:fakeAI)
			var/mob/living/silicon/ai/fai = src:fakeAI
			rname = fai.name
	var/rendered = "<i><span class='game say'>Robotic Talk, <span class='name'>[rname]</span> <span class='message'>[message_a]</span></span></i>"

	for (var/mob/living/S in living_mob_list)
		if (S.binarycheck())
			if(istype(S , /mob/living/silicon/ai))
				var/renderedAI = "<i><span class='game say'>Robotic Talk, <a href='byond://?src=\ref[S];track2=\ref[S];track=\ref[src]'><span class='name'>[rname]</span></a> <span class='message'>[message_a]</span></span></i>"
				S.show_message(renderedAI, 2)
			else
				S.show_message(rendered, 2)

	var/list/listening = hearers(1, src)
	listening |= src

	var/list/heard = list()
	for (var/mob/living/M in listening)
		if(!M.binarycheck())
			heard += M

	if (length(heard))
		var/message_b

//		message_b = "beep beep beep"
		message_b = Gibberish(message,100)
		message_b = say_quote(message_b)
		message_b = "<i>[message_b]</i>"

		rendered = "<i><span class='game say'><span class='name'>[voice_name]</span> <span class='message'>[message_b]</span></span></i>"

		for (var/mob/M in heard)
			M.show_message(rendered, 2)

	message = say_quote(message)

	rendered = "<i><span class='game say'>Robotic Talk, <span class='name'>[rname]</span> <span class='message'>[message_a]</span></span></i>"

	for (var/mob/M in dead_mob_list)
		if(!istype(M,/mob/new_player) && !(istype(M,/mob/living/carbon/brain)))//No meta-evesdropping
			M.show_message(rendered, 2)
