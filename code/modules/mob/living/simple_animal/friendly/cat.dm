//Cat
/mob/living/simple_animal/cat
	name = "cat"
	desc = "A domesticated, feline pet. Has a tendency to adopt crewmembers."
	voice_name = "cat"
	icon_state = "cat"
	icon_living = "cat"
	icon_dead = "cat_dead"
	speak = list("Meow!","Esp!","Purr!","HSSSSS")
	speak_emote = list("purrs", "meows")
	emote_hear = list("meows","mews")
	emote_see = list("shakes its head", "shivers")
	speak_chance = 1
	turns_per_move = 5
	see_in_dark = 6
	meat_type = /obj/item/weapon/reagent_containers/food/snacks/meat
	meat_amount = 3
	response_help  = "pets the"
	response_disarm = "gently pushes aside the"
	response_harm   = "kicks the"
	var/turns_since_scan = 0
	var/mob/living/simple_animal/mouse/movement_target
	min_oxy = 16 //Require atleast 16kPA oxygen
	minbodytemp = 223		//Below -50 Degrees Celcius
	maxbodytemp = 323	//Above 50 Degrees Celcius
	pass_flags = PASSTABLE
	var/sitting = 0
	gender = MALE

	default_language = "Cat"
	known_languages = list("-Universal","-Human","Cat","-Tajaran")

	can_pull = 3
	melee_damage_lower = 1
	melee_damage_upper = 2

/mob/living/simple_animal/cat/Move()
	..()
	if(locate(/obj/structure/table, src.loc) || locate(/obj/structure/stool, src.loc))
		pixel_y=16
	else
		pixel_y=0

/mob/living/simple_animal/cat/update_canmove()
	..()
	if(sitting || resting)
		canmove = 0
	if(resting)
		lying = 1

/mob/living/simple_animal/cat/Life()
	//MICE!
	if((src.loc) && isturf(src.loc))
		if(!stat && !resting && !buckled)
			for(var/mob/living/simple_animal/mouse/M in view(1,src))
				if(!M.stat)
					M.splat()
					emote(pick("\red splats the [M]!","\red toys with the [M]","worries the [M]"))
					movement_target = null
					stop_automated_movement = 0
					break

	..()

	if(stat!=DEAD)
		if(lying)
			icon_state = "cat_rest"
		else if(sitting)
			icon_state = "cat_sit"
		else
			icon_state = "cat"

	if(!ckey) return

	for(var/mob/living/simple_animal/mouse/snack in oview(src, 3))
		if(prob(15))
			emote(pick("hisses and spits!","mrowls fiercely!","eyes [snack] hungrily."))
		break

	if(!stat && !resting && !buckled)
		turns_since_scan++
		if(turns_since_scan > 5)
			walk_to(src,0)
			turns_since_scan = 0
			if((movement_target) && !(isturf(movement_target.loc) || ishuman(movement_target.loc) ))
				movement_target = null
				stop_automated_movement = 0
			if( !movement_target || !(movement_target.loc in oview(src, 3)) )
				movement_target = null
				stop_automated_movement = 0
				for(var/mob/living/simple_animal/mouse/snack in oview(src,3))
					if(isturf(snack.loc) && !snack.stat)
						movement_target = snack
						break
			if(movement_target)
				stop_automated_movement = 1
				walk_to(src,movement_target,0,5)

/mob/living/simple_animal/cat/verb/sit_down()
	set name = "Sit"
	set category = "IC"

	sitting = !sitting
	src << "\blue You are now [sitting ? "sitting" : "standing"]"

//RUNTIME IS ALIVE! SQUEEEEEEEE~
/mob/living/simple_animal/cat/Runtime
	name = "Runtime"
	desc = "Its fur has the look and feel of velvet, and it's tail quivers occasionally."
	
/mob/living/simple_animal/cat/ghost
	meat_type = /obj/item/weapon/ectoplasm
	meat_amount = 1
	unsuitable_atoms_damage = 0
	layer = 5
	New()
		..()
		spawn(1)
			var/icon/i = new/icon(icon)
			i.ColorTone(rgb(96,96,96))
			i.ChangeOpacity(0.75)
			icon = i
	Die()
		gib()
	gib()
		if(meat_amount && meat_type)
			for(var/i = 0; i < meat_amount; i++)
				new meat_type(src.loc)
		del(src)
