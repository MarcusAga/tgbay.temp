

/mob/living/simple_animal/hostile/carp
	name = "space carp"
	voice_name = "Carp"
	desc = "A ferocious, fang-bearing creature that resembles a fish."
	icon_state = "carp"
	icon_living = "carp"
	icon_dead = "carp_dead"
	icon_gib = "carp_gib"
	speak_chance = 0
	turns_per_move = 5
	meat_type = /obj/item/weapon/reagent_containers/food/snacks/carpmeat
	meat_amount = 2
	response_help = "pets the"
	response_disarm = "gently pushes aside the"
	response_harm = "hits the"
	speed = -1
	stop_automated_movement_when_pulled = 0
	maxHealth = 25
	health = 25

	harm_intent_damage = 8
	melee_damage_lower = 15
	melee_damage_upper = 15
	attacktext = "bites"
	attack_sound = 'sound/weapons/bite.ogg'

	//Space carp aren't affected by atmos.
	min_oxy = 0
	max_oxy = 0
	min_tox = 0
	max_tox = 0
	min_co2 = 0
	max_co2 = 0
	min_n2 = 0
	max_n2 = 0
	minbodytemp = 0

/mob/living/simple_animal/hostile/carp/Process_Spacemove(var/check_drift = 0)
	return 1	//No drifting in space for space carp!	//original comments do not steal

/mob/living/simple_animal/hostile/carp/FindTarget()
	. = ..()
	if(.)
		emote("nashes at [.]")

/mob/living/simple_animal/hostile/carp/AttackingTarget()
	. =..()
	var/mob/living/L = .
	if(istype(L))
		if(prob(15))
			L.Weaken(3)
			L.visible_message("<span class='danger'>\the [src] knocks down %knownface:1%!</span>",actors=list(L))

/mob/living/simple_animal/hostile/carp/holo
//	name = "holografic space carp"
	meat_amount = 0
	melee_damage_lower = 0
	melee_damage_upper = 0

	AttackingTarget()
		var/mob/living/L = target_mob
		if(istype(L))
			if(attack_sound)
				playsound(loc, attack_sound, 50, 1, 1)
			L.visible_message("\red <B>[src]</B> [attacktext] %knownface:1%!", "\red <B>[src]</B> [attacktext] you!",actors=list(L))
			var/damage = rand(melee_damage_lower, melee_damage_upper)
			var/def_zone = ran_zone("chest",10)
			var/armor = L.run_armor_check(def_zone, "melee")
			L.apply_damage(damage/(armor+1), HALLOSS, def_zone, armor)
			if(prob(15))
				L.Weaken(3)
				L.visible_message("<span class='danger'>\the [src] knocks down %knownface:1%!</span>",actors=list(L))
		return L

	Die()
		..()
		src.visible_message("<B>[src]</B> vanishes!")
		del(src)
