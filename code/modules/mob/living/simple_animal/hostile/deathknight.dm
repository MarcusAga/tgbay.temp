//Space deathknights
/mob/living/simple_animal/hostile/deathknight
	name = "Death Knight"
	real_name = "Death Knight"
	voice_name = "Death Knight"
	desc = "Knight of death itself..."
	icon = 'icons/mob/deathknight.dmi'
	icon_state = "s"
	icon_living = "s"
	icon_dead = "d"
	icon_gib = "d"
	speak_chance = 0
	turns_per_move = 1
	see_in_dark = 6
	meat_amount = 0
	response_help   = "pets the"
	response_disarm = "gently pushes aside the"
	response_harm   = "pokes the"
	stop_automated_movement_when_pulled = 0
	maxHealth = 200
	health = 200
	speed = 2
	melee_damage_lower = 15
	melee_damage_upper = 25
	wall_smash = 1

	pixel_x=-32
	pixel_y=-16
	a_intent="hurt"

	default_language = "Universal"
	known_languages = list("Universal","-Human")

	//Space deathknights are uber and don't breath
	min_oxy = 0
	max_oxy = 0
	min_tox = 0
	max_tox = 0
	min_co2 = 0
	max_co2 = 0
	min_n2 = 0
	max_n2 = 0
	minbodytemp = 0

	var/stance_step = 0

/mob/living/simple_animal/hostile/deathknight/Life()
	. =..()
	if(!.)
		return

	if(!stat)
		icon_state = icon_living

	switch(stance)

		if(HOSTILE_STANCE_TIRED)
			stop_automated_movement = 1
			stance_step++
			if(stance_step >= 10) //rests for 10 ticks
				if(target_mob && target_mob in ListTargets())
					stance = HOSTILE_STANCE_ATTACK //If the mob he was chasing is still nearby, resume the attack, otherwise go idle.
				else
					stance = HOSTILE_STANCE_IDLE

		if(HOSTILE_STANCE_ALERT)
			stop_automated_movement = 1
			var/found_mob = 0
			if(target_mob && target_mob in ListTargets())
				if(!(SA_attackable(target_mob)))
					stance_step = max(0, stance_step) //If we have not seen a mob in a while, the stance_step will be negative, we need to reset it to 0 as soon as we see a mob again.
					stance_step++
					found_mob = 1
					src.dir = get_dir(src,target_mob)	//Keep staring at the mob

					if(stance_step in list(1,4,7)) //every 3 ticks
						var/action = pick( list("stares angrily at [target_mob]", "prepares to attack [target_mob]") )
						if(action)
							emote(action)
			if(!found_mob)
				stance_step--

			if(stance_step <= -20) //If we have not found a mob for 20-ish ticks, revert to idle mode
				stance = HOSTILE_STANCE_IDLE
			if(stance_step >= 1)   //If we have been staring at a mob for 7 ticks,
				stance = HOSTILE_STANCE_ATTACK

		if(HOSTILE_STANCE_ATTACKING)
			if(stance_step >= 20)	//attacks for 20 ticks, then it gets tired and needs to rest
				emote( "slows down" )
				stance = HOSTILE_STANCE_TIRED
				stance_step = 0
				walk(src, 0) //This stops the bear's walking
				return



/mob/living/simple_animal/hostile/deathknight/attackby(var/obj/item/O as obj, var/mob/user as mob)
	if(stance)
		if(stance != HOSTILE_STANCE_ATTACK && stance != HOSTILE_STANCE_ATTACKING)
			stance = HOSTILE_STANCE_ALERT
			stance_step = 6
			target_mob = user
	..()

/mob/living/simple_animal/hostile/deathknight/attack_hand(mob/living/carbon/human/M as mob)
	if(stance)
		if(stance != HOSTILE_STANCE_ATTACK && stance != HOSTILE_STANCE_ATTACKING)
			stance = HOSTILE_STANCE_ALERT
			stance_step = 6
			target_mob = M
	..()

/mob/living/simple_animal/hostile/deathknight/FindTarget()
	. = ..()
	if(.)
		emote("stares evilly at [.]")
		stance = HOSTILE_STANCE_ALERT

/mob/living/simple_animal/hostile/deathknight/LoseTarget()
	..(5)

/mob/living/simple_animal/hostile/deathknight/AttackingTarget()
	attacktext = pick( list("bashes at", "stabs") )
	flick("a",src)

	if(isliving(target_mob))
		var/mob/living/L = target_mob
		L.attack_animal(src)
		return L
	if(istype(target_mob,/obj/mecha))
		var/obj/mecha/M = target_mob
		M.attack_animal(src)
		return M

/mob/living/simple_animal/hostile/deathknight/bullet_act(var/obj/item/projectile/Proj)
	if(!Proj || istype(Proj,/obj/item/projectile/beam))	return
	src.health -= Proj.damage
	return 0

/mob/living/simple_animal/hostile/deathknight/MoveToTarget(var/step = 5)
	stop_automated_movement = 1
	if(!target_mob || SA_attackable(target_mob))
		stance = HOSTILE_STANCE_IDLE
		stance_step = step //Make it very alert, so it quickly attacks again if a mob returns
	if(target_mob in ListTargets())
		walk_to(src, target_mob, 1, 3)
		stance = HOSTILE_STANCE_ATTACKING
		stance_step = 0

/mob/living/simple_animal/hostile/deathknight/ListTargets()
	var/list/targets = list()
	for(var/mob/living/L in view(src, 7))
		if(!istype(L,/mob/living/simple_animal/hostile))
			targets+=L
	return targets
