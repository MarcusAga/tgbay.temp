//
/mob/living/simple_animal/hostile/suicidebomber
	name = "Sheep"
	voice_name = "Sheep"
	desc = "A strange creature."
	icon = 'icons/mob/baran.dmi'
	icon_state = "alive"
	icon_living = "alive"
	icon_dead = "dead"
	icon_gib = "gib"
	speak_chance = 0
	turns_per_move = 0
	meat_type = null
	meat_amount = 0
	response_help = "pets the"
	response_disarm = "gently pushes aside the"
	response_harm = "hits the"
	speed = -1
	stop_automated_movement_when_pulled = 0
	maxHealth = 25
	health = 25

	harm_intent_damage = 0
	melee_damage_lower = 0
	melee_damage_upper = 0
	attacktext = "bites"

	//Space carp aren't affected by atmos.
	min_oxy = 0
	max_oxy = 0
	min_tox = 0
	max_tox = 0
	min_co2 = 0
	max_co2 = 0
	min_n2 = 0
	max_n2 = 0
	minbodytemp = 0

	var/exp_d = 1
	var/exp_m = 2
	var/exp_l = 4
	var/exp_f = 5

	var/stance_step = 0

	Process_Spacemove(var/check_drift = 0)
		return 1	//No drifting in space for space carp!	//original comments do not steal

	FindTarget()
		. = ..()
		if(.)
			emote("runs at [.]")

	AttackingTarget()
		explosion(get_turf(src),exp_d,exp_m,exp_l,exp_f)
		del(src)

	MoveToTarget(var/step = 5)
		stop_automated_movement = 1
		if(!target_mob || SA_attackable(target_mob))
			stance = HOSTILE_STANCE_IDLE
			stance_step = step //Make it very alert, so it quickly attacks again if a mob returns
		if(target_mob in ListTargets())
			walk_to(src, target_mob, 1, 3)
			stance = HOSTILE_STANCE_ATTACKING
			stance_step = 0

	ListTargets()
		var/list/targets = list()
		for(var/mob/living/L in view(src, 7))
			if(!istype(L,/mob/living/simple_animal/hostile))
				targets+=L
		return targets
