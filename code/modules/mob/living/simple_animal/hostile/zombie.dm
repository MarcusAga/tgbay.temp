//Space zombies
/mob/living/simple_animal/hostile/zombie
	name = "zombie"
	real_name = "zombie"
	voice_name = "zombie"
	desc = "Brrraaaaains..."
	icon = 'icons/mob/ZombieX.dmi'
	icon_state = "s"
	icon_living = "s"
	icon_dead = "d"
	icon_gib = "d"
	speak = list("Braains!","Brainz...","GRR!","Ghhhhhhh!")
	speak_emote = list("growls", "roars")
	emote_see = list("stares ferociously", "drools")
	speak_chance = 1
	turns_per_move = 2
	see_in_dark = 6
	meat_type = /obj/item/weapon/reagent_containers/food/snacks/meat
	meat_amount = 1
	response_help  = "pets the"
	response_disarm = "gently pushes aside the"
	response_harm   = "pokes the"
	stop_automated_movement_when_pulled = 0
	maxHealth = 150
	health = 150
	speed = 4
	melee_damage_lower = 5
	melee_damage_upper = 10

	a_intent="hurt"

	//Space zombies aredead and do not breathe
	min_oxy = 0
	max_oxy = 0
	min_tox = 0
	max_tox = 0
	min_co2 = 0
	max_co2 = 0
	min_n2 = 0
	max_n2 = 0
	minbodytemp = 0

	default_language = "Zombie"
	known_languages = list("Zombie")

	var/stance_step = 0

/mob/living/simple_animal/hostile/zombie/Life()
	. =..()
	if(!.)
		return

	if(!stat)
		icon_state = icon_living
		if(health<maxHealth && prob(25))
			health+=1

	switch(stance)

		if(HOSTILE_STANCE_TIRED)
			stop_automated_movement = 1
			stance_step++
			if(stance_step >= 5) //rests for 10 ticks
				if(target_mob && target_mob in ListTargets())
					stance = HOSTILE_STANCE_ATTACK //If the mob he was chasing is still nearby, resume the attack, otherwise go idle.
				else
					stance = HOSTILE_STANCE_IDLE

		if(HOSTILE_STANCE_ALERT)
			stop_automated_movement = 1
			var/found_mob = 0
			if(target_mob && target_mob in ListTargets())
				if(!(SA_attackable(target_mob)))
					stance_step = max(0, stance_step) //If we have not seen a mob in a while, the stance_step will be negative, we need to reset it to 0 as soon as we see a mob again.
					stance_step++
					found_mob = 1
					src.dir = get_dir(src,target_mob)	//Keep staring at the mob

					if(stance_step in list(1,4,7)) //every 3 ticks
						var/action = pick( list( "growls at [target_mob]", "stares angrily at [target_mob]", "prepares to attack [target_mob]", "drools, loking at [target_mob]" ) )
						if(action)
							emote(action)
			if(!found_mob)
				stance_step--

			if(stance_step <= -20) //If we have not found a mob for 20-ish ticks, revert to idle mode
				stance = HOSTILE_STANCE_IDLE
			if(stance_step >= 1)   //If we have been staring at a mob for 7 ticks,
				stance = HOSTILE_STANCE_ATTACK

		if(HOSTILE_STANCE_ATTACKING)
			if(stance_step >= 32)	//attacks for 20 ticks, then it gets tired and needs to rest
				emote( "slows down" )
				stance = HOSTILE_STANCE_TIRED
				stance_step = 0
				walk(src, 0) //This stops the bear's walking
				return

	if(stance == HOSTILE_STANCE_ATTACKING || stance == HOSTILE_STANCE_ATTACK)
		var/obj/structure/obstacle = locate(/obj/structure, get_step(src, dir))
		if(istype(obstacle, /obj/structure/window) && (obstacle.dir==dir || obstacle.dir==turn(dir,180)))
			obstacle.attack_animal(src)
		if(istype(obstacle, /obj/structure/closet) || istype(obstacle, /obj/structure/table) || istype(obstacle, /obj/structure/grille))
			obstacle.attack_animal(src)
		obstacle = locate(/obj/structure/window, get_turf(loc))
		if(istype(obstacle) && obstacle.dir==dir)
			obstacle.attack_animal(src)

/mob/living/simple_animal/hostile/zombie/attackby(var/obj/item/O as obj, var/mob/user as mob)
	if(stance)
		if(stance != HOSTILE_STANCE_ATTACK && stance != HOSTILE_STANCE_ATTACKING)
			stance = HOSTILE_STANCE_ALERT
			stance_step = 6
			target_mob = user
	..()

/mob/living/simple_animal/hostile/zombie/attack_hand(mob/living/carbon/human/M as mob)
	if(stance)
		if(stance != HOSTILE_STANCE_ATTACK && stance != HOSTILE_STANCE_ATTACKING)
			stance = HOSTILE_STANCE_ALERT
			stance_step = 6
			target_mob = M
	..()

/mob/living/simple_animal/hostile/zombie/FindTarget()
	. = ..()
	if(.)
//		emote("stares alertly at [.]")
		stance = HOSTILE_STANCE_ALERT

/mob/living/simple_animal/hostile/zombie/LoseTarget()
	..(5)

/mob/living/simple_animal/hostile/zombie/AttackingTarget()
	if(!CanReachThrough(get_turf(src),get_turf(target_mob),target_mob)) return

	attacktext = pick( list("slashes at", "bites") )

	if(isliving(target_mob))
		var/mob/living/L = target_mob
		L.attack_animal(src)
		return L
	if(istype(target_mob,/obj/mecha))
		var/obj/mecha/M = target_mob
		M.attack_animal(src)
		return M

/mob/living/simple_animal/hostile/zombie/MoveToTarget(var/step = 5)
	stop_automated_movement = 1
	if(!target_mob || SA_attackable(target_mob))
		stance = HOSTILE_STANCE_IDLE
		stance_step = step //Make it very alert, so it quickly attacks again if a mob returns
	if(target_mob in ListTargets())
		walk_to(src, target_mob, 1, 8)
		stance = HOSTILE_STANCE_ATTACKING
		stance_step = 0

/mob/living/simple_animal/hostile/zombie/ListTargets()
	var/list/targets = list()
	for(var/mob/living/L in view(src, 7))
		if(ishuman(L) || ismonkey(L))
			targets+=L
	return targets
