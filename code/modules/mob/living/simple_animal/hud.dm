//
/datum/hud/proc/animal_hud(var/ui_style='icons/mob/screen1_old.dmi')
	src.adding = list()

	var/obj/screen/using

	using = new /obj/screen()
	using.name = "act_intent"
	using.dir = SOUTHWEST
	using.icon = ui_style
	using.icon_state = (mymob.a_intent == "hurt" ? "harm" : mymob.a_intent)
	using.screen_loc = ui_acti
	using.layer = 20
	src.adding += using
	action_intent = using

	mymob.hud_oxygen = new /obj/screen()
	mymob.hud_oxygen.icon = ui_style
	mymob.hud_oxygen.icon_state = "oxy0"
	mymob.hud_oxygen.name = "oxygen"
	mymob.hud_oxygen.screen_loc = ui_alien_oxygen

	mymob.hud_toxin = new /obj/screen()
	mymob.hud_toxin.icon = ui_style
	mymob.hud_toxin.icon_state = "tox0"
	mymob.hud_toxin.name = "toxin"
	mymob.hud_toxin.screen_loc = ui_alien_toxin

	mymob.hud_healths = new /obj/screen()
	mymob.hud_healths.icon = ui_style
	mymob.hud_healths.icon_state = "health0"
	mymob.hud_healths.name = "health"
	mymob.hud_healths.screen_loc = ui_alien_health

	mymob.hud_bodytemp = new /obj/screen()
	mymob.hud_bodytemp.icon = ui_style
	mymob.hud_bodytemp.icon_state = "temp1"
	mymob.hud_bodytemp.name = "body temperature"
	mymob.hud_bodytemp.screen_loc = ui_temp

	mymob.hud_pullin = new /obj/screen()
	mymob.hud_pullin.icon = ui_style
	mymob.hud_pullin.icon_state = "pull0"
	mymob.hud_pullin.name = "pull"
	mymob.hud_pullin.screen_loc = ui_pull_resist

	mymob.hud_blind = new /obj/screen()
	mymob.hud_blind.icon = 'icons/mob/screen1_full.dmi'
	mymob.hud_blind.icon_state = "blackimageoverlay"
	mymob.hud_blind.name = " "
	mymob.hud_blind.screen_loc = "1,1"
	mymob.hud_blind.layer = 0

	mymob.hud_flash = new /obj/screen()
	mymob.hud_flash.icon = ui_style
	mymob.hud_flash.icon_state = "blank"
	mymob.hud_flash.name = "flash"
	mymob.hud_flash.screen_loc = "1,1 to 15,15"
	mymob.hud_flash.layer = 17

	mymob.zone_sel = new /obj/screen/zone_sel()
	var/icon/ico = new(ui_style, "zone_sel")
	ico.Scale(128,128)
	mymob.zone_sel.icon = ico
	mymob.zone_sel.overlays = null
	mymob.zone_sel.overlays += image("icon" = 'icons/mob/zone_sel.dmi', "icon_state" = text("[]", mymob.zone_sel.selecting))

	mymob.client.screen = null

	mymob.client.screen += list( mymob.zone_sel, mymob.hud_oxygen, mymob.hud_toxin, mymob.hud_healths, mymob.hud_pullin, mymob.hud_blind, mymob.hud_flash) //, mymob.rest, mymob.sleep, mymob.mach )
	mymob.client.screen += src.adding

/mob/living/simple_animal/proc/handle_regular_hud_updates()
	if (hud_healths)
		if (stat != 2)
			if(health>=maxHealth)
				hud_healths.icon_state = "health0"
			else if(health>=0.8*maxHealth)
				hud_healths.icon_state = "health1"
			else if(health>=0.6*maxHealth)
				hud_healths.icon_state = "health2"
			else if(health>=0.4*maxHealth)
				hud_healths.icon_state = "health3"
			else if(health>=0.2*maxHealth)
				hud_healths.icon_state = "health4"
			else if(health>=0)
				hud_healths.icon_state = "health5"
			else
				hud_healths.icon_state = "health6"
		else
			hud_healths.icon_state = "health7"

	if(hud_pullin)	hud_pullin.icon_state = "pull[pulling ? 1 : 0]"

	if (hud_blind && stat != 2)
		if (blinded)
			hud_blind.layer = 18
		else
			hud_blind.layer = 0

	if (hud_toxin)	hud_toxin.icon_state = "tox[toxins_alert ? 1 : 0]"
	if (hud_oxygen) hud_oxygen.icon_state = "oxy[oxygen_alert ? 1 : 0]"

	if(hud_bodytemp)
		if(bodytemperature>=maxbodytemp)
			hud_bodytemp.icon_state = "temp4"
		else if(bodytemperature<=minbodytemp)
			hud_bodytemp.icon_state = "temp-4"
		else
			hud_bodytemp.icon_state = "temp0"

	return 1
