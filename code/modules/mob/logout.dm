/mob/Logout()
	player_list -= src
//	log_access("Logout: [key_name(src)]")
	if(admins[src.ckey])
		if (ticker && ticker.current_state == GAME_STATE_PLAYING) //Only report this stuff if we are currently playing.
			message_admins("Admin logout: [key_name(src)]")
//#ifdef CSServer
//			var/admins_number = admin_list.len
//			if(admins_number == 0) //Apparently the admin logging out is no longer an admin at this point, so we have to check this towards 0 and not towards 1. Awell.
//				ooc_allowed = 0
//				world << "<B>The OOC channel has been globally disabled because no admin is moderating it!</B>"
//#endif
	..()

	return 1