//
/mob/proc/say(var/message as text)
	return

/mob/verb/whisper()
	set name = "Whisper"
	set category = "IC"
	return

/mob/verb/say_verb(var/message as text)
	set name = "Say"
	set category = "IC"
	usr.say(message)

/mob/verb/me_verb(message as text)
	set name = "Me"
	set category = "IC"

	message = trim(copytext(sanitize(message), 1, MAX_MESSAGE_LEN))

	if(ishuman(src) || isrobot(src))
		usr.emote("me",1,message)
	else
		usr.emote(message)

/mob/proc/say_dead(var/message)
	var/name = src.real_name
	var/alt_name = ""

	if(mind && mind.name)
		name = "[mind.name]"
	else
		name = real_name
	if(name != real_name)
		alt_name = " (died as [real_name])"

	message = src.say_quote(message)
	var/rendered = "<span class='game deadsay'><span class='prefix'>DEAD:</span> <span class='name'>[name]</span>[alt_name] <span class='message'>[message]</span></span>"

	var/list/viewerz = viewers()
	if(invisibility>=5)
		viewerz.Cut()
	for (var/mob/M in player_list)
		if (istype(M, /mob/new_player))
			continue
		if(M.client && M.client.holder && M.client.deadchat) //admins can toggle deadchat on and off. This is a proc in admin.dm and is only give to Administrators and above
			if(!M.client.STFU_ghosts) //Admin shut-off for ghosts chatter
				M << rendered	//Admins can hear deadchat, if they choose to, no matter if they're blind/deaf or not.
				viewerz -= M
		else if (M.stat == DEAD)
			M.show_message(rendered, 2) //Takes into account blindness and such.
			viewerz -= M

	if(!isobserver(src)) return

	rendered = "<span class='game say'>Ghost: <span class='name'>[name]</span>[alt_name] <span class='message'>[message]</span></span>"
	for (var/mob/M in viewerz)
		M << rendered
	return

/mob/proc/say_understands(var/language)
	if(!language)
		return 1
	if (src.stat == 2)
		return 1
	var/list/all_known_languages = islist(known_languages) ? (src.known_languages.Copy()) : (list("-Universal"))
	if(mind)
		all_known_languages |= mind.known_languages
	return (language in all_known_languages) || ("-[language]" in all_known_languages)
/mob/proc/say_can_use_language(var/language)
	if(!language)
		return 1
	var/list/all_known_languages = islist(known_languages) ? (src.known_languages.Copy()) : (list())
	if(mind)
		all_known_languages |= mind.known_languages
	return (language in all_known_languages)

/proc/double_symbols(text, search_symb)
	if(!text || !istext(text) || !search_symb || !istext(search_symb))
		return text
	var/list/textList = tg_text2list_case(text, search_symb)
	if(copytext(text, length(text))==search_symb)
		textList += ""
	var/total = textList.len
	if(!total)
		return text
	var/count = 2
	var/newText = "[textList[1]]"
	while(count <= total)
		var/n = pick(1,2,3)
		for(var/i=1,i<=n,i++)
			newText += search_symb
		newText += "[textList[count]]"
		count++
	return newText

/mob/proc/say_quote(var/text,var/language)
	if(!text)
		return "says, \"...\"";	//not the best solution, but it will stop a large number of runtimes. The cause is somewhere in the Tcomms code
		//tcomms code is still runtiming somewhere here
	var/ending = copytext(text, length(text))
	var/species = get_species()
	if (species == "Tajaran")
		text = double_symbols(text,"r")
		text = double_symbols(text,"R")
		text = double_symbols(text,"�")
		text = double_symbols(text,"�")
	else if (species == "Soghun")
		text = double_symbols(text,"s")
		text = double_symbols(text,"S")
		text = double_symbols(text,"�")
		text = double_symbols(text,"�")

	if (language == "Robot")
		return "beeps, \"<span class='species'>[text]</span>\"";
	if (src.stuttering)
		if(language in list("Skrell","Soghun","Tajaran","Dragon"))
			return "stammers, \"<span class='species'>[text]</span>\"";
		return "stammers, \"[text]\"";
	if (src.slurring)
		if(language in list("Skrell","Soghun","Tajaran","Dragon"))
			return "slurrs, \"<span class='species'>[text]</span>\"";
		return "slurrs, \"[text]\"";
	if (language == "Soghun")
		return "hisses, \"<span class='species'>[text]</span>\"";
	if (language == "Skrell")
		return "warbles, \"<span class='species'>[text]</span>\"";
	if (language == "Tajaran")
		return "mrowls, \"<span class='species'>[text]</span>\"";
	if (language == "Dragon")
		text = double_symbols(text,"r")
		text = double_symbols(text,"R")
		text = double_symbols(text,"�")
		text = double_symbols(text,"�")
		return "growls, \"<span class='species'>[text]</span>\"";
	if(isliving(src))
		var/mob/living/L = src
		if (L.getBrainLoss() >= 60)
			return "gibbers, \"[text]\"";

	var/sayverb = "says"
	if (ending == "?")
		sayverb = "asks";
	if (ending == "!")
		sayverb = "exclaims";
	if(ishuman(src) && language!="Human")
		return "[sayverb] in [language], \"[text]\""

	return "[sayverb], \"[text]\"";

/mob/proc/emote(var/act)
	return

/mob/proc/get_ear()
	// returns an atom representing a location on the map from which this
	// mob can hear things

	// should be overloaded for all mobs whose "ear" is separate from their "mob"

	return get_turf(src)

/proc/say_test(var/text)
	var/ending = copytext(text, length(text))
	if (ending == "?")
		return "1"
	else if (ending == "!")
		return "2"
	return "0"

/client/var/selectingLanguage = 0
/client/verb/selectLanguage()
	set name = ".selectLanguage"
	if(!mob || !isliving(mob)) return

	if(selectingLanguage) return
	selectingLanguage = 1

	var/list/available = list()
	for(var/lang in mob.known_languages)
		if(copytext(lang,1,2)=="-") continue
		available |= lang
	if(mob.mind)
		for(var/lang in mob.mind.known_languages)
			if(copytext(lang,1,2)=="-") continue
			available |= lang

	if(available)
		if(available.len>1)
			var/lang = input(usr,"Default language","Which one?") as anything in available
			if(!lang) return
			mob.default_language = lang
		else if(available.len)
			mob.default_language = available[1]
		else
			mob.default_language = "Unknown"
	updateSelectedLanguage()
	selectingLanguage = 0

/client/proc/updateSelectedLanguage()
	var/list/params = list()
	params["text"] = mob.default_language
	winset(src, "langButton", list2params(params))

/client/verb/selectChannel()
	set name = ".selectChannel"
	if(!mob || !isliving(mob)) return

	var/list/channels = list()

	if(iscarbon(mob) && hasvar(mob,"ears") && istype(mob:ears,/obj/item/device/radio))
		var/obj/item/device/radio/R = mob:ears
		for(var/chnl in R.channels)
			channels += chnl
		channels += "global"
	else if(isrobot(mob) && mob:radio)
		channels += "department"
		channels += "global"
	else if(isAI(mob))
		channels += "left intercom"
		channels += "central intercom"
		channels += "right intercom"
	if(mob.r_hand && istype(mob.r_hand, /obj/item/device/radio))
		channels += "right hand"
	if(mob.l_hand && istype(mob.l_hand, /obj/item/device/radio))
		channels += "left hand"
	if(locate(/obj/item/device/radio/intercom) in view(1, mob))
		channels += "intercom"
	channels += "whisper"
	if(mob:binarycheck())
		channels += "binary"
	if(mob:hivecheck())
		channels += "alientalk"
	if(ispAI(mob) && mob:radio)
		channels += "pAI"
	if(mob.mind && mob.mind.changeling)
		channels += "changeling"
	var/chnl = input(usr,"Channels:","Select channel") as null|anything in channels
	if(!chnl) return

	var/txt = winget(src, "input", "text")
	var/trimming = 1
	while(trimming)
		trimming = 0
		if(lentext(txt)>0 && (copytext(txt,1,2)=="." || copytext(txt,1,2)==";"))
			txt = lentext(txt)==1?"":copytext(txt,2)
			trimming = 1
		else if(lentext(txt)>1)
			var/pref = copytext(txt,1,3)
			if(department_radio_keys[pref])
				txt = lentext(txt)==2?"":copytext(txt,3)
				trimming = 1

	var/pref=""
	if(chnl=="global")
		pref="."
	else if(chnl=="department")
		pref=":h"
	else if(chnl == "left intercom")
		pref=":l"
	else if(chnl == "center intercom")
		pref=":c"
	else if(chnl == "right intercom")
		pref=":r"
	else
		for(var/testpref in department_radio_keys)
			if(department_radio_keys[testpref]==chnl)
				pref=testpref
				break
	txt = "[pref][txt]"
	var/list/params = list()
	params["text"] = txt
	winset(src, "input", list2params(params))
