//
/obj/structure/new_year_tree
	name = "The fir"
	desc = "This is a fir. Real fir on dammit spess station. You smell pine-needles."
	icon = 'icons/NYObj.dmi'
	icon_state = "Little tree"
	anchored = 1
	opacity = 0
	density = 1
	layer = 5

/obj/item/weapon/bengpacket
	name = "bengal fires packet"
	desc = "The most popular brand of Space Cigarettes, sponsors of the Space Olympics."
	icon = 'icons/NYObj.dmi'
	icon_state = "BengalPack"
	item_state = "cigpacket"
	w_class = 1
	throwforce = 2
	flags = TABLEPASS
	slot_flags = SLOT_BELT
	var/bengcount = 4

	update_icon()
		if(bengcount==initial(bengcount))
			icon_state = "BengalPack"
		else
			icon_state = "BengalPackO"
		desc = "There are [bengcount] fire\s left!"
		return

	attack_hand(mob/user as mob)
		if(user.r_hand == src || user.l_hand == src)
			if(bengcount == 0)
				user << "<span class='notice'>Empty</span>"
				return
			else
				var/obj/item/weapon/bengfire/W = new(user)
				user.put_in_active_hand(W)
				bengcount--
		else
			return ..()
		update_icon()
		return

/obj/item/weapon/bengfire
	name = "bengal Fire"
	desc = "A stick for partyes."
	icon = 'icons/NYObj.dmi'
	item_icon = 'icons/NYObj.dmi'
	icon_state = "Bengal"
	throw_speed = 0.5
	item_state = "Bengal"
	w_class = 1
	body_parts_covered = null
	attack_verb = list("burnt", "singed")
	var/lit = 0
	var/icon_on = "BengalF"  //Note - these are in masks.dmi not in cigarette.dmi
	var/icon_off = "Bengal"
	var/butt_icon = "Bengal0"
	var/smoketime = 80

	attackby(obj/item/weapon/W as obj, mob/user as mob)
		..()
		if(istype(W, /obj/item/weapon/weldingtool))
			var/obj/item/weapon/weldingtool/WT = W
			if(WT.isOn())//Badasses dont get blinded while lighting their cig with a welding tool
				light("<span class='notice'>[user] casually lights the [name] with [W], what a badass.</span>")

		else if(istype(W, /obj/item/weapon/lighter/zippo))
			var/obj/item/weapon/lighter/zippo/Z = W
			if(Z.light_on)
				light("<span class='rose'>With a single flick of their wrist, [user] smoothly lights their [name] with their [W]. Damn they're cool.</span>")

		else if(istype(W, /obj/item/weapon/lighter))
			var/obj/item/weapon/lighter/L = W
			if(L.light_on)
				light("<span class='notice'>After some fiddling, [user] manages to light their [name] with [W].</span>")

		else if(istype(W, /obj/item/weapon/match))
			var/obj/item/weapon/match/M = W
			if(M.lit)
				light("<span class='notice'>[user] lights their [name] with their [W].</span>")

		else if(istype(W, /obj/item/weapon/bengfire))
			var/obj/item/weapon/bengfire/M = W
			if(M.lit)
				light("<span class='notice'>[user] lights their [name] with their [W].</span>")

		else if(istype(W, /obj/item/weapon/melee/energy/sword))
			var/obj/item/weapon/melee/energy/sword/S = W
			if(S.active)
				light("<span class='warning'>[user] swings their [W], barely missing their nose. They light their [name] in the process.</span>")

		else if(istype(W, /obj/item/device/assembly/igniter))
			light("<span class='notice'>[user] fiddles with [W], and manages to light their [name].</span>")

		//can't think of any other way to update the overlays :<
		user.update_inv_l_hand(0)
		user.update_inv_r_hand(1)
		return

	process()
		var/turf/location = get_turf(src)
		smoketime--
		if(smoketime < 1)
			put_out()
			if(ismob(loc))
				var/mob/living/M = loc
				M << "<span class='notice'>Your [name] goes out.</span>"
			return
		if(lit == 1)
			if(location && prob(15))
				new/obj/effect/effect/sparks(location)
		return

/obj/item/weapon/bengfire/proc/light(var/flavor_text = "[usr] lights the [name].")
	if(!src.lit)
		src.lit = 1
		damtype = "fire"
		icon_state = icon_on
		item_state = icon_on
		var/turf/T = get_turf(src)
		T.visible_message(flavor_text)
		processing_objects.Add(src)

/obj/item/weapon/bengfire/proc/put_out()
	if(src.lit == 1)
		src.lit = -1
		icon_state = src.butt_icon
		item_state = "Bengal"
		desc = "burnt [src] stick."
		name = "[src] stick"
		attack_verb = list("poked")
		processing_objects.Remove(src)
		if (usr)
			usr.update_inv_l_hand(0)
			usr.update_inv_r_hand(1)

/datum/recipe/berrypie
	reagents = list("flour" = 10, "milk" = 5, "sugar" = 5)
	items = list(
		/obj/item/weapon/reagent_containers/food/snacks/egg,
		/obj/item/weapon/reagent_containers/food/snacks/egg,
		/obj/item/weapon/reagent_containers/food/snacks/grown/berries,
	)
	result = /obj/item/weapon/reagent_containers/food/snacks/sliceable/berrypie

/obj/item/weapon/reagent_containers/food/snacks/sliceable/berrypie
	name = "Berry Pie"
	desc = "Special recipe."
	icon = 'icons/NYObj.dmi'
	icon_state = "BerryNYpie"
	slice_path = /obj/item/weapon/reagent_containers/food/snacks/berrypieslice
	slices_num = 5
	New()
		..()
		reagents.add_reagent("nutriment", 10)
		bitesize = 10

/obj/item/weapon/reagent_containers/food/snacks/berrypieslice
	name = "Berry Pie"
	desc = "Special recipe."
	icon = 'icons/NYObj.dmi'
	icon_state = "BerryNYpieSlice"
	New()
		..()
		bitesize = 2

/datum/recipe/suficake
	reagents = list("flour" = 5, "sugar" = 15, "enzyme" = 5)
	items = list(
		/obj/item/weapon/reagent_containers/food/snacks/egg,
	)
	result = /obj/item/weapon/reagent_containers/food/snacks/suficake

/obj/item/weapon/reagent_containers/food/snacks/suficake
	name = "Sufi Cake"
	desc = "Special recipe."
	icon = 'icons/NYObj.dmi'
	icon_state = "NYSulfiCake"
	New()
		..()
		reagents.add_reagent("nutriment", 6)
		bitesize = 4

/datum/recipe/nycookie
	reagents = list("flour" = 5, "sugar" = 10, "enzyme" = 5)
	result = /obj/item/weapon/reagent_containers/food/snacks/nycookie

/obj/item/weapon/reagent_containers/food/snacks/nycookie
	name = "New Year cookie"
	desc = "Special recipe."
	icon = 'icons/NYObj.dmi'
	icon_state = "HNYcookie"
	New()
		..()
		reagents.add_reagent("nutriment", 4)
		bitesize = 2
