//This file was auto-corrected by findeclaration.exe on 25.5.2012 20:42:33

#define ACTION_LOCK "lock"
#define ACTION_TOGGLE "toggle"
#define ACTION_ACTIVATE "activate"
#define ACTION_DEACTIVATE "deactivate"
#define ACTION_OVERCHARGE "overcharge"
#define ACTION_MOAR "more"
#define ACTION_LESS "less"
#define ACTION_FAKE1 "fake1"
#define ACTION_FAKE2 "fake2"

/obj/machinery/particle_accelerator/control_box
	name = "Particle Accelerator Control Computer"
	desc = "This controls the density of the particles."
	icon = 'icons/obj/machines/particle_accelerator.dmi'
	icon_state = "control_box"
	reference = "control_box"
	anchored = 0
	density = 1
	use_power = 0
	idle_power_usage = 500
	active_power_usage = 10000
	construction_state = 0
	active = 0
	dir = 1
	var/list/wires = list()
	var/list/wires_signalers = list()
	var/list/wires_actions = list()
	var/input_locked = 0
	var/list/obj/structure/particle_accelerator/connected_parts
	var/assembled = 0
	var/parts = null

/obj/machinery/particle_accelerator/control_box/New()
	connected_parts = list()
	var/list/colors = list("Light Red","Dark Red","Blue","Green","Yellow","Black","White","Gray","Orange","Pink")
	var/list/actions = list(ACTION_LOCK,ACTION_TOGGLE,ACTION_ACTIVATE,ACTION_DEACTIVATE,ACTION_MOAR,ACTION_LESS,ACTION_OVERCHARGE,ACTION_FAKE1,ACTION_FAKE2)
	while(actions.len)
		var/act = pick(actions)
		var/color = pick(colors)
		actions -= act
		colors -= color
		wires+=color
		wires_actions[color]=act
		wires_signalers[color] = null
	..()

/obj/machinery/particle_accelerator/control_box/attack_hand(mob/user as mob)
	if(construction_state >= 2)
		interact(user)

/obj/machinery/particle_accelerator/control_box/update_state()
	if(construction_state < 3)
		use_power = 0
		assembled = 0
		active = 0
		for(var/obj/structure/particle_accelerator/part in connected_parts)
			part.strength = null
			part.powered = 0
			part.update_icon()
		connected_parts = list()
		return
	if(!part_scan())
		use_power = 1
		active = 0
		connected_parts = list()

	return

/obj/machinery/particle_accelerator/control_box/update_icon()
	if(active)
		if(input_locked)
			icon_state = "[reference]p1l"
		else
			icon_state = "[reference]p1"
	else
		if(use_power && !(stat&NOPOWER))
			if(assembled)
				icon_state = "[reference]p"
				if(input_locked)
					icon_state = "[reference]pl"
			else
				icon_state = "u[reference]p"
				if(input_locked)
					icon_state = "u[reference]pl"
		else
			switch(construction_state)
				if(0)
					icon_state = "[reference]"
				if(1)
					icon_state = "[reference]"
				if(2)
					icon_state = "[reference]w"
				else
					if(input_locked || (stat&NOPOWER))
						icon_state = "[reference]cl"
					else
						icon_state = "[reference]c"
		overlays.Cut()
		if(construction_state==2)
			overlays += image('icons/obj/doors/Doorint.dmi', "panel_open")
	return

/obj/machinery/particle_accelerator/control_box/Topic(href, href_list)
	..()
	if(href_list["wireact"]) WireTopic(href, href_list)

	if(input_locked) return

	//Ignore input if we are broken, !silicon guy cant touch us, or nonai controlling from super far away
	if(stat & (BROKEN|NOPOWER) || (get_dist(src, usr) > 1 && !istype(usr, /mob/living/silicon)) || (get_dist(src, usr) > 8 && !istype(usr, /mob/living/silicon/ai)))
		usr.machine = null
		usr << browse(null, "window=pacontrol")
		return

	if( href_list["close"] )
		usr << browse(null, "window=pacontrol")
		usr.machine = null
		return
	if(href_list["togglep"])
		src.toggle_power()
		investigate_log("turned [active?"<font color='red'>ON</font>":"<font color='green'>OFF</font>"] by [usr.key]","singulo")
		if (active)
			message_admins("PA Control Computer turned ON by [key_name(usr, usr.client)](<A HREF='?src=%holder_ref%;adminmoreinfo=\ref[usr]'>?</A>) in ([x],[y],[z] - <A HREF='?src=%holder_ref%;adminplayerobservecoodjump=1;X=[x];Y=[y];Z=[z]'>JMP</a>)",0,1)
			log_game("PA Control Computer turned ON by [usr.ckey]([usr]) in ([x],[y],[z])")
	else if(href_list["scan"])
		src.part_scan()
	else if(href_list["strengthup"])
		strength++
		if(strength > 2)
			strength = 2
		else
			message_admins("PA Control Computer increased to [strength] by [key_name(usr, usr.client)](<A HREF='?src=%holder_ref%;adminmoreinfo=\ref[usr]'>?</A>) in ([x],[y],[z] - <A HREF='?src=%holder_ref%;adminplayerobservecoodjump=1;X=[x];Y=[y];Z=[z]'>JMP</a>)",0,1)
			log_game("PA Control Computer increased to [strength] by [usr.ckey]([usr]) in ([x],[y],[z])")
			investigate_log("increased to <font color='red'>[strength]</font> by [usr.key]","singulo")
		for(var/obj/structure/particle_accelerator/part in connected_parts)
			part.strength = strength
			part.update_icon()

	else if(href_list["strengthdown"])
		strength--
		if(strength < 0)
			strength = 0
		else
			investigate_log("decreased to <font color='green'>[strength]</font> by [usr.key]","singulo")
		for(var/obj/structure/particle_accelerator/part in connected_parts)
			part.strength = strength
			part.update_icon()
	src.updateDialog()
	src.update_icon()
	return


/obj/machinery/particle_accelerator/control_box/power_change()
	..()
	if(stat & NOPOWER)
		active = 0
		use_power = 0
	else if(!stat && construction_state == 3)
		use_power = 1
	return


/obj/machinery/particle_accelerator/control_box/process()
	if(src.active)
		//a part is missing!
		if( length(connected_parts) < 6 )
			investigate_log("lost a connected part; It <font color='red'>powered down</font>.","singulo")
			src.toggle_power()
			return
		//emit some particles
		for(var/obj/structure/particle_accelerator/particle_emitter/PE in connected_parts)
			if(PE)
				PE.emit_particle(src.strength)
	return


/obj/machinery/particle_accelerator/control_box/proc/part_scan()
	for(var/obj/structure/particle_accelerator/fuel_chamber/F in orange(1,src))
		src.dir = F.dir
	connected_parts = list()
	var/tally = 0
	var/ldir = turn(dir,-90)
	var/rdir = turn(dir,90)
	var/odir = turn(dir,180)
	var/turf/T = src.loc
	T = get_step(T,rdir)
	if(check_part(T,/obj/structure/particle_accelerator/fuel_chamber))
		tally++
	T = get_step(T,odir)
	if(check_part(T,/obj/structure/particle_accelerator/end_cap))
		tally++
	T = get_step(T,dir)
	T = get_step(T,dir)
	if(check_part(T,/obj/structure/particle_accelerator/power_box))
		tally++
	T = get_step(T,dir)
	if(check_part(T,/obj/structure/particle_accelerator/particle_emitter/center))
		tally++
	T = get_step(T,ldir)
	if(check_part(T,/obj/structure/particle_accelerator/particle_emitter/left))
		tally++
	T = get_step(T,rdir)
	T = get_step(T,rdir)
	if(check_part(T,/obj/structure/particle_accelerator/particle_emitter/right))
		tally++
	if(tally >= 6)
		assembled = 1
		return 1
	else
		assembled = 0
		return 0


/obj/machinery/particle_accelerator/control_box/proc/check_part(var/turf/T, var/type)
	if(!(T)||!(type))
		return 0
	var/obj/structure/particle_accelerator/PA = locate(/obj/structure/particle_accelerator) in T
	if(istype(PA, type))
		if(PA.connect_master(src))
			if(PA.report_ready(src))
				src.connected_parts.Add(PA)
				return 1
	return 0


/obj/machinery/particle_accelerator/control_box/proc/toggle_power(var/force = -1)
	src.active = !src.active
	if(force>=0)
		active = (force>0)
	if(force>1)
		strength = 2
		input_locked = 1
		update_icon()
	if(src.active)
		src.use_power = 2
		for(var/obj/structure/particle_accelerator/part in connected_parts)
			part.strength = src.strength
			part.powered = 1
			part.update_icon()
	else
		src.use_power = 1
		for(var/obj/structure/particle_accelerator/part in connected_parts)
			part.strength = null
			part.powered = 0
			part.update_icon()
	return 1


/obj/machinery/particle_accelerator/control_box/proc/interact(mob/user)
	if(construction_state==2)
		interact_wires(user)
	else
		interact_real(user)

/obj/machinery/particle_accelerator/control_box/proc/pulsed(var/obj/item/device/assembly/signaler/Sig)
	for(var/color in wires_signalers)
		if(wires_signalers[color]==Sig)
			pulse_act(wires_actions[color])
			return

/obj/machinery/particle_accelerator/control_box/proc/pulse_act(var/act)
	if(!act) return
	var/old_active = active
	switch(act)
		if(ACTION_LOCK)
			input_locked =! input_locked
			update_icon()
		if(ACTION_ACTIVATE)
			src.toggle_power(1)
		if(ACTION_DEACTIVATE)
			src.toggle_power(0)
		if(ACTION_TOGGLE)
			if(!input_locked)
				src.toggle_power()
		if(ACTION_OVERCHARGE)
			src.toggle_power(2)
		if(ACTION_MOAR)
			if(strength<2)
				strength++
		if(ACTION_LESS)
			if(strength>0)
				strength--
	if(active!=old_active)
		investigate_log("turned [active?"<font color='red'>ON</font>":"<font color='green'>OFF</font>"] by [usr?"[usr.key]":"Unknown (Radio?)"]","singulo")
		if (active && usr)
			message_admins("PA Control Computer turned ON by [key_name(usr, usr.client)](<A HREF='?src=%holder_ref%;adminmoreinfo=\ref[usr]'>?</A>) in ([x],[y],[z] - <A HREF='?src=%holder_ref%;adminplayerobservecoodjump=1;X=[x];Y=[y];Z=[z]'>JMP</a>)",0,1)
			log_game("PA Control Computer turned ON by [usr.ckey]([usr]) in ([x],[y],[z])")

/obj/machinery/particle_accelerator/control_box/proc/WireTopic(href, href_list)
	if(get_dist(src, usr) > 1 || construction_state!=2)
		usr << browse(null, "window=pacontrol")
		return
	var/id = text2num(href_list["id"])
	if(id<1 || id>wires.len) return
	var/act = wires_actions[wires[id]]
	switch(href_list["wireact"])
		if("pulse")
			if(!istype(usr.get_active_hand(), /obj/item/device/multitool))
				usr << "You need a multitool!"
				return
			pulse_act(act)
		if("signalera")
			if(wires_signalers[wires[id]]) return
			if(!istype(usr.get_active_hand(), /obj/item/device/assembly/signaler))
				usr << "You need a signaller!"
				return
			var/obj/item/device/assembly/signaler/S = usr.get_active_hand()
			if(S.secured)
				usr << "This radio can't be attached!"
				return
			var/mob/M = usr
			M.drop_item()
			S.loc = src
			wires_signalers[wires[id]] = S
		if("signalerd")
			if(!wires_signalers[wires[id]]) return
			var/obj/item/device/assembly/signaler/S = wires_signalers[wires[id]]
			wires_signalers[wires[id]] = null
			S.loc=usr.loc
			usr.put_in_hands(S)
	interact_wires(usr)

/obj/machinery/particle_accelerator/control_box/proc/interact_wires(mob/user)
	if(construction_state!=2) return
	if(get_dist(src, user) > 1)
		user.machine = null
		user << browse(null, "window=pacontrol")
		return
	user.machine = src

	var/dat = ""
	dat += "Particle Accelerator Control Wires<BR><BR>"
	for(var/i = 1, i<=wires.len, i++)
		dat += "[wires[i]] wire <a href='?src=\ref[src];wireact=pulse;id=[i]'>Pulse</a> <a href='?src=\ref[src];wireact=signaler[(wires_signalers[wires[i]]?"d":"a")];id=[i]'>[(wires_signalers[wires[i]]?"Detach":"Attach")] signaler</a><br>"
	dat+="<hr>"
	dat+="Check light is [assembled?"on":"off"]<br>"
	dat+="Power light is [active?"on":"off"]<br>"
	dat+="Power indicator is [strength==0?"green":(strength==1?"yellow":"red")]<br>"
	user << browse(dat, "window=pacontrol;size=420x500")
	onclose(user, "pacontrol")

/obj/machinery/particle_accelerator/control_box/proc/interact_real(mob/user)
	if((get_dist(src, user) > 1) || (stat & (BROKEN|NOPOWER)) || input_locked)
		if(!istype(user, /mob/living/silicon) || input_locked)
			user.machine = null
			user << browse(null, "window=pacontrol")
			return
	user.machine = src

	var/dat = ""
	dat += "Particle Accelerator Control Panel<BR>"
	dat += "<A href='?src=\ref[src];close=1'>Close</A><BR><BR>"
	dat += "Status:<BR>"
	if(!assembled)
		dat += "Unable to detect all parts!<BR>"
		dat += "<A href='?src=\ref[src];scan=1'>Run Scan</A><BR><BR>"
	else
		dat += "All parts in place.<BR><BR>"
		dat += "Power:"
		if(active)
			dat += "On<BR>"
		else
			dat += "Off <BR>"
		dat += "<A href='?src=\ref[src];togglep=1'>Toggle Power</A><BR><BR>"
		dat += "Particle Strength: [src.strength+1] "
		dat += "<A href='?src=\ref[src];strengthdown=1'>--</A>|<A href='?src=\ref[src];strengthup=1'>++</A><BR><BR>"

	user << browse(dat, "window=pacontrol;size=420x500")
	onclose(user, "pacontrol")
	return

#undef ACTION_LOCK
#undef ACTION_TOGGLE
#undef ACTION_ACTIVATE
#undef ACTION_DEACTIVATE
#undef ACTION_OVERCHARGE
#undef ACTION_MOAR
#undef ACTION_LESS
#undef ACTION_FAKE1
#undef ACTION_FAKE2
