// the SMES
// stores power

#define SMESMAXCHARGELEVEL 200000
#define SMESMAXOUTPUT 200000

/obj/machinery/power/smes
	name = "power storage unit"
	desc = "A high-capacity superconducting magnetic energy storage (SMES) unit."
	icon_state = "smes"
	density = 1
	anchored = 1
	var/output = 50000
	var/lastout = 0
	var/lastin = 0
	var/loaddemand = 0
	var/capacity = 5e6
	var/charge = 1e6
	var/charging = 0
	var/chargemode = 0
	var/chargecount = 0
	var/chargelevel = 50000
	var/online = 1
	var/n_tag = null
	var/obj/machinery/power/terminal/terminal = null


/obj/machinery/power/smes/New()
	..()
	req_access = list(access_engine)
	component_parts = list()
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
	component_parts += new /obj/item/weapon/stock_parts/manipulator(src)
	component_parts += new /obj/item/weapon/stock_parts/micro_laser(src)
	RefreshParts()
	spawn(5)
		dir_loop:
			for(var/d in cardinal)
				var/turf/T = get_step(src, d)
				for(var/obj/machinery/power/terminal/term in T)
					if(term && term.dir == turn(d, 180) && !term.master)
						terminal = term
						break dir_loop
		if(!terminal)
			if(!istype(usr,/mob))
				stat |= BROKEN
				return
			else
				terminal = new
				terminal.master = src
				if(get_dir(loc,usr.loc) in cardinal)
					terminal.loc = get_turf(usr)
				else
					terminal.loc = get_step(loc,pick(cardinal))
				terminal.hide(0)
				stat &= ~BROKEN
		terminal.master = src
		updateicon()
	return

/obj/machinery/power/smes/RefreshParts()
	var/tmp_power_rate = 0
	var/mult = 0
	for(var/obj/item/weapon/stock_parts/SP in component_parts)
		if(istype(SP, /obj/item/weapon/stock_parts/capacitor))
			tmp_power_rate += SP.rating
		if(istype(SP, /obj/item/weapon/stock_parts/micro_laser))
			mult = max(sqrt(SP.rating),mult)
	if(tmp_power_rate>10)
		capacity=sqrt(tmp_power_rate/10)*initial(capacity)*mult
	else
		capacity=tmp_power_rate*initial(capacity)*mult/10

/obj/machinery/power/smes/Del()
	if(terminal)
		del(terminal)
	..()

/obj/machinery/power/smes/proc/updateicon()
	overlays = null
	if(stat & BROKEN)	return

	overlays += image('icons/obj/power.dmi', "smes-op[online]")

	if(charging)
		overlays += image('icons/obj/power.dmi', "smes-oc1")
	else
		if(chargemode)
			overlays += image('icons/obj/power.dmi', "smes-oc0")

	var/clevel = chargedisplay()
	if(clevel>0)
		overlays += image('icons/obj/power.dmi', "smes-og[clevel]")
	return


/obj/machinery/power/smes/proc/chargedisplay()
	return round(5.5*charge/(capacity ? capacity : 5e6))

/obj/machinery/power/smes/attackby(obj/item/weapon/W, mob/user)
	if(istype(W,/obj/item/weapon/wrench) && get_dir(loc,user.loc) in cardinal)
		if(istype(src,/obj/machinery/power/smes/magical)) return
		playsound(src.loc, 'sound/items/Ratchet.ogg', 75, 1)
		anchored = !anchored
		user << "You [anchored?"fastened":"unfastened"] \the [src] [anchored?"to":"from"] the floor"
		if(!anchored && terminal)
			disconnect_from_network()
			terminal.disconnect_from_network()
			terminal.loc = src
			terminal.hide(1)
			charging = 0
			chargemode = 0
		else if(anchored)
			if(!terminal)
				terminal = new
				terminal.master = src
				stat &= ~BROKEN
			terminal.loc = get_turf(user)
			terminal.dir = get_dir(user.loc,loc)
			terminal.connect_to_network()
			terminal.hide(0)
			connect_to_network()
		updateicon()
	else if(!anchored && istype(W, /obj/item/weapon/crowbar))
		var/obj/machinery/constructable_frame/machine_frame/new_frame = new /obj/machinery/constructable_frame/machine_frame(src.loc)
		for(var/obj/item/I in component_parts)
			if(I.reliability < 100 && !prob(I.reliability))
				I.crit_fail = 1
			I.loc = src.loc
		new/obj/item/weapon/circuitboard/smes (src.loc)
		new_frame.state = 2
		new_frame.icon_state = "box_1"
		del(src)

#define SMESRATE 0.025			// rate of internal charge to external power

/obj/machinery/power/smes/process()
	if(!anchored) return

	if(stat & BROKEN)	return

	//store machine state to see if we need to update the icon overlays
	var/last_disp = chargedisplay()
	var/last_chrg = charging
	var/last_onln = online

	if(terminal)
		var/excess = terminal.newsurplus()

		if(charging)
			if(excess > 0)		// if there's power available, try to charge

				lastin = min((capacity-charge)/SMESRATE, min(chargelevel,excess))		// charge at set rate, limited to spare capacity

				charge += lastin * SMESRATE	// increase the charge

				add_load(lastin)		// add the load to the terminal side network

			else					// if not enough capcity
				charging = 0		// stop charging
				chargecount  = 0
				lastin = 0

		else
			lastin = 0
			if(chargemode)
				if(chargecount > rand(3,6))
					charging = 1
					chargecount = 0

				if(excess >= chargelevel)
					chargecount++
				else
					chargecount = 0
			else
				chargecount = 0

	if(online)		// if outputting
		lastout = min( charge/SMESRATE, output)		//limit output to that stored

		charge -= lastout*SMESRATE		// reduce the storage (may be recovered in /restore() if excessive)

		add_avail(lastout)				// add output to powernet (smes side)
	else
		lastout = 0

//		if(charge < 0.0001)
//			online = 0					// stop output if charge falls to zero

	// only update icon if state changed
	if(last_disp != chargedisplay() || last_chrg != charging || last_onln != online)
		updateicon()

	for(var/mob/M in viewers(1, src))
		if ((M.client && M.machine == src))
			src.interact(M)
	AutoUpdateAI(src)
	return

// called after all power processes are finished
// restores charge level to smes if there was excess this ptick


/obj/machinery/power/smes/proc/restore()
	if(stat & BROKEN)
		return

	if(!online || !powernet)
		loaddemand = 0
		return

	var/excess = max(0, powernet.netexcess)		// this was how much wasn't used on the network last ptick, minus any removed by other SMESes

	excess = min(lastout, excess)				// clamp it to how much was actually output by this SMES last ptick

	excess = min((capacity-charge)/SMESRATE, excess)	// for safety, also limit recharge by space capacity of SMES (shouldn't happen)

	// now recharge this amount

	var/clev = chargedisplay()

	charge += excess * SMESRATE
	powernet.netexcess -= excess		// remove the excess from the powernet, so later SMESes don't try to use it

	loaddemand = lastout-excess

	if(clev != chargedisplay() )
		updateicon()
	return


/obj/machinery/power/smes/add_load(var/amount)
	if(terminal && terminal.powernet)
		terminal.powernet.newload += amount
		terminal.powernet.smes_taken["[name] \ref[src]"] = amount


/obj/machinery/power/smes/attack_ai(mob/user)
	add_fingerprint(user)
	if(stat & BROKEN) return
	interact(user)


/obj/machinery/power/smes/attack_hand(mob/user)
	add_fingerprint(user)
	if(stat & BROKEN) return

	if(ishuman(user))
		if(istype(user:gloves, /obj/item/clothing/gloves/space_ninja)&&user:gloves:candrain&&!user:gloves:draining)
			call(/obj/item/clothing/gloves/space_ninja/proc/drain)("SMES",src,user:wear_suit)
			return
	interact(user)


/obj/machinery/power/smes/proc/interact(mob/user)
	if(get_dist(src, user) > 1 && !istype(user, /mob/living/silicon/ai) && !isadminobserver(user))
		user.machine = null
		user << browse(null, "window=smes")
		return

	user.machine = src

	var/t = "<TT><B>SMES Power Storage Unit</B> [n_tag? "([n_tag])" : null]<HR><PRE>"

	t += "Stored capacity : [round(100.0*charge/capacity, 0.1)]%<BR><BR>"

	t += "Input: [charging ? "Charging" : "Not Charging"]    [chargemode ? "<B>Auto</B> <A href = '?src=\ref[src];cmode=1'>Off</A>" : "<A href = '?src=\ref[src];cmode=1'>Auto</A> <B>Off</B> "]<BR>"


	t += "Input level:  <A href = '?src=\ref[src];input=-4'>M</A> <A href = '?src=\ref[src];input=-3'>-</A> <A href = '?src=\ref[src];input=-2'>-</A> <A href = '?src=\ref[src];input=-1'>-</A> [add_lspace(chargelevel,5)] <A href = '?src=\ref[src];input=1'>+</A> <A href = '?src=\ref[src];input=2'>+</A> <A href = '?src=\ref[src];input=3'>+</A> <A href = '?src=\ref[src];input=4'>M</A><BR>"

	t += "<BR><BR>"

	t += "Output: [online ? "<B>Online</B> <A href = '?src=\ref[src];online=1'>Offline</A>" : "<A href = '?src=\ref[src];online=1'>Online</A> <B>Offline</B> "]<BR>"

	t += "Output level: <A href = '?src=\ref[src];output=-4'>M</A> <A href = '?src=\ref[src];output=-3'>-</A> <A href = '?src=\ref[src];output=-2'>-</A> <A href = '?src=\ref[src];output=-1'>-</A> [add_lspace(output,5)] <A href = '?src=\ref[src];output=1'>+</A> <A href = '?src=\ref[src];output=2'>+</A> <A href = '?src=\ref[src];output=3'>+</A> <A href = '?src=\ref[src];output=4'>M</A><BR>"

	t += "Output load: [round(loaddemand)] W<BR>"

	t += "<BR></PRE><HR><A href='?src=\ref[src];close=1'>Close</A>"

	t += "</TT>"
	user << browse(t, "window=smes;size=460x300")
	onclose(user, "smes")
	return


/obj/machinery/power/smes/Topic(href, href_list)
	..()

	if(!allowed(usr))
		usr<<"You don't know how to operate this"
		return

	if (usr.stat || usr.restrained() )
		return
	if (!(istype(usr, /mob/living/carbon/human) || ticker) && ticker.mode.name != "monkey")
		if(!istype(usr, /mob/living/silicon/ai) && !isadminobserver(usr))
			usr << "\red You don't have the dexterity to do this!"
			return

//world << "[href] ; [href_list[href]]"

	if (( usr.machine==src && ((get_dist(src, usr) <= 1) && istype(src.loc, /turf))) || (istype(usr, /mob/living/silicon/ai)))


		if( href_list["close"] )
			usr << browse(null, "window=smes")
			usr.machine = null
			return

		else if( href_list["cmode"] )
			chargemode = !chargemode
			if(!chargemode)
				charging = 0
			updateicon()

		else if( href_list["online"] )
			online = !online
			updateicon()
		else if( href_list["input"] )

			var/i = text2num(href_list["input"])

			var/d = 0
			switch(i)
				if(-4)
					chargelevel = 0
				if(4)
					chargelevel = SMESMAXCHARGELEVEL		//30000

				if(1)
					d = 100
				if(-1)
					d = -100
				if(2)
					d = 1000
				if(-2)
					d = -1000
				if(3)
					d = 10000
				if(-3)
					d = -10000

			chargelevel += d
			chargelevel = max(0, min(SMESMAXCHARGELEVEL, chargelevel))	// clamp to range

		else if( href_list["output"] )

			var/i = text2num(href_list["output"])

			var/d = 0
			switch(i)
				if(-4)
					output = 0
				if(4)
					output = SMESMAXOUTPUT		//30000

				if(1)
					d = 100
				if(-1)
					d = -100
				if(2)
					d = 1000
				if(-2)
					d = -1000
				if(3)
					d = 10000
				if(-3)
					d = -10000

			output += d
			output = max(0, min(SMESMAXOUTPUT, output))	// clamp to range

		investigate_log("input/output; [chargelevel>output?"<font color='green'>":"<font color='red'>"][chargelevel]/[output]</font> | Output-mode: [online?"<font color='green'>on</font>":"<font color='red'>off</font>"] | Input-mode: [chargemode?"<font color='green'>auto</font>":"<font color='red'>off</font>"] by [usr.key]","singulo")
		src.updateUsrDialog()

	else
		usr << browse(null, "window=smes")
		usr.machine = null
	return


/obj/machinery/power/smes/proc/ion_act()
	if(src.z == 1)
		if(prob(1)) //explosion
			world << "\red SMES explosion in [src.loc.loc]"
			for(var/mob/M in viewers(src))
				M.show_message("\red The [src.name] is making strange noises!", 3, "\red You hear sizzling electronics.", 2)
			sleep(10*pick(4,5,6,7,10,14))
			var/datum/effect/effect/system/harmless_smoke_spread/smoke = new /datum/effect/effect/system/harmless_smoke_spread()
			smoke.set_up(3, 0, src.loc)
			smoke.attach(src)
			smoke.start()
			explosion(src.loc, -1, 0, 1, 3, 0)
			del(src)
			return
		if(prob(15)) //Power drain
			world << "\red SMES power drain in [src.loc.loc]"
			var/datum/effect/effect/system/spark_spread/s = new /datum/effect/effect/system/spark_spread
			s.set_up(3, 1, src)
			s.start()
			if(prob(50))
				emp_act(1)
			else
				emp_act(2)
		if(prob(5)) //smoke only
			world << "\red SMES smoke in [src.loc.loc]"
			var/datum/effect/effect/system/harmless_smoke_spread/smoke = new /datum/effect/effect/system/harmless_smoke_spread()
			smoke.set_up(3, 0, src.loc)
			smoke.attach(src)
			smoke.start()


/obj/machinery/power/smes/emp_act(severity)
	online = 0
	charging = 0
	output = 0
	charge -= 1e6/severity
	if (charge < 0)
		charge = 0
	spawn(100)
		output = initial(output)
		charging = initial(charging)
		online = initial(online)
	..()


/obj/machinery/power/smes/backup
	name = "UPS"
	charge=50000
	AI_UPS
		name = "AI - UPS"
		New()
			..()
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			RefreshParts()
			charge = capacity

	Engi
		name = "Engineering - UPS"
		New()
			..()
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
			RefreshParts()
			charge = capacity

	New()
		..()
		req_access = list(access_engine)
		component_parts = list()
		component_parts += new /obj/item/weapon/cable_coil(src)
		component_parts += new /obj/item/weapon/cable_coil(src)
		component_parts += new /obj/item/weapon/stock_parts/capacitor(src)
		component_parts += new /obj/item/weapon/stock_parts/manipulator(src)
		component_parts += new /obj/item/weapon/stock_parts/micro_laser(src)
		RefreshParts()
		charge = capacity

	process()
		if(!anchored) return
		if(stat & BROKEN)	return

		//store machine state to see if we need to update the icon overlays
		var/last_disp = chargedisplay()
		var/last_chrg = charging
		var/last_onln = online

		if(terminal && terminal.powernet)
			var/avail = terminal.powernet.to_ups

			if(charging)
				if(avail >= 0)		// if there's power available, try to charge
					lastin = min((capacity-charge)/SMESRATE, min(chargelevel,round(avail*chargelevel/terminal.powernet.smes_load)))
					charge += lastin * SMESRATE	// increase the charge
					add_load(lastin)		// add the load to the terminal side network
				else					// if not enough capcity
					charging = 0		// stop charging
					chargecount  = 0
					lastin = 0
			else
				lastin = 0
				if(chargemode)
					charging = 1
		if(online)		// if outputting
			lastout = min( charge/SMESRATE, output)		//limit output to that stored
			charge -= lastout*SMESRATE		// reduce the storage (may be recovered in /restore() if excessive)
			add_avail(lastout)				// add output to powernet (smes side)
		if(last_disp != chargedisplay() || last_chrg != charging || last_onln != online)
			updateicon()

		for(var/mob/M in viewers(1, src))
			if ((M.client && M.machine == src))
				src.interact(M)
		AutoUpdateAI(src)
		return

/obj/machinery/power/smes/magical
	name = "magical power storage unit"
	desc = "A high-capacity superconducting magnetic energy storage (SMES) unit. Magically produces power."
	process()
		capacity = INFINITY
		charge = INFINITY
		..()



/proc/rate_control(var/S, var/V, var/C, var/Min=1, var/Max=5, var/Limit=null)
	var/href = "<A href='?src=\ref[S];rate control=1;[V]"
	var/rate = "[href]=-[Max]'>-</A>[href]=-[Min]'>-</A> [(C?C : 0)] [href]=[Min]'>+</A>[href]=[Max]'>+</A>"
	if(Limit) return "[href]=-[Limit]'>-</A>"+rate+"[href]=[Limit]'>+</A>"
	return rate


