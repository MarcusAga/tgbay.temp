//
/obj/item/clothing/head/powered/ironman
	name = "Ironman head"
	item_icon = 'icons/obj/clothing/ironman_suit.dmi'
	icon = 'icons/obj/clothing/ironman_suit.dmi'
	icon_state = "head"
	item_state = "head"
	var/closed_iconstate = "head"
	var/opened_iconstate = "head-open"
	flags = FPRINT | TABLEPASS | HEADCOVERSEYES | HEADCOVERSMOUTH | BLOCKHAIR

	atmotoggle()
		set category = "Object"
		set name = "Toggle helmet seals"

		var/mob/living/carbon/human/user = usr

		if(!istype(user))
			user << "\red This helmet is not rated for nonhuman use."
			return

		if(user.head != src)
			user << "\red Can't engage the seals without wearing the helmet."
			return

		if(!user.wear_suit || !istype(user.wear_suit,/obj/item/clothing/suit/powered))
			user << "\red This helmet can only couple with powered armor."
			return

		var/obj/item/clothing/suit/powered/armor = user.wear_suit

		if(!armor.atmoseal)
			user << "\red This armor's atmospheric seals are missing or incompatible."
			return

		armor.atmoseal:toggle_head()

/obj/item/clothing/gloves/powered/ironman
	name = "Ironman gloves"
	item_icon = 'icons/obj/clothing/ironman_suit.dmi'
	item_state = "gloves"
	icon = 'icons/obj/clothing/ironman_suit.dmi'
	icon_state = "gloves"

/obj/item/clothing/shoes/powered/ironman
	name = "Ironman shoes"
	item_icon = 'icons/obj/clothing/ironman_suit.dmi'
	item_state = "shoes"
	icon = 'icons/obj/clothing/ironman_suit.dmi'
	icon_state = "shoes"

/obj/item/clothing/suit/powered/spawnable/ironman
	name = "Ironman armor"
	desc = "Red and shiny."
	icon = 'icons/obj/clothing/ironman_suit.dmi'
	icon_state = "suit"
	item_icon = 'icons/obj/clothing/ironman_suit.dmi'
	item_state = "suit"

	verb/toggle_jetpack_stabile()
		set name = "Toggle Suit Jetpack Stabilization"
		set category = "Object"
		if(jetpack)
			jetpack.toggle_rockets()

	New()
		servos = new /obj/item/powerarmor/servos(src)
		servos.parent = src
		reactive = new /obj/item/powerarmor/reactive/centcomm(src)
		reactive.parent = src
		atmoseal = new /obj/item/powerarmor/atmoseal/ironman(src)
		atmoseal.parent = src
		power = new /obj/item/powerarmor/power(src)
		power.parent = src
		jetpack = new /obj/item/powerarmor/jetpack(src)
		jetpack.parent = src

		verbs += /obj/item/clothing/suit/powered/proc/poweron

		var/obj/item/clothing/head/powered/helm = new /obj/item/clothing/head/powered/ironman(src.loc)
		helm.verbs += /obj/item/clothing/head/powered/ironman/atmotoggle
		new /obj/item/clothing/gloves/powered/ironman(src.loc)
		new /obj/item/clothing/shoes/powered/ironman(src.loc)
