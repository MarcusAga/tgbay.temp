//
/obj/item/clothing/suit/powered/spawnable/spacemarine
	name = "Spacemarine Armor"
	desc = "For the Emperor!"
	icon = 'icons/obj/clothing/spacemarine.dmi'
	item_icon = 'icons/obj/clothing/spacemarine.dmi'
	icon_state = "imperium_s"
	item_state = "swat"
	body_parts_covered = UPPER_TORSO|LOWER_TORSO|LEGS|ARMS|FEET|HANDS
	helmrequired = 0

	New()
		servos = new /obj/item/powerarmor/servos(src)
		servos.parent = src
		servos.toggleslowdown = 8
		reactive = new /obj/item/powerarmor/reactive/spacemarine(src)
		reactive.parent = src
		atmoseal = new /obj/item/powerarmor/atmoseal/adminbus(src)
		atmoseal.parent = src
		power = new /obj/item/powerarmor/power(src)
		power.parent = src

		verbs += /obj/item/clothing/suit/powered/proc/poweron

		new /obj/item/clothing/head/powered/spacemarine(src.loc)
		new /obj/item/weapon/tank/jetpack/spacemarine(src.loc)

/obj/item/clothing/head/powered/spacemarine
	name = "Spacemarine helmet"
	icon = 'icons/obj/clothing/spacemarine.dmi'
	item_icon = 'icons/obj/clothing/spacemarine.dmi'
	icon_state = "imperium_h"
	item_state = "head"
	flags = FPRINT | TABLEPASS | HEADCOVERSEYES | HEADCOVERSMOUTH | BLOCKHAIR | MASKINTERNALS

/obj/item/weapon/tank/jetpack/spacemarine
	name = "Jetpack"
	desc = "A backpack of spacemarine exosuit."
	icon = 'icons/obj/clothing/spacemarine.dmi'
	item_icon = 'icons/obj/clothing/spacemarine.dmi'
	icon_state = "imperium_b"
	item_state = "jetpack"
	w_class = 4.0
	icon_action_button = "action_jetpack"
	distribute_pressure = ONE_ATMOSPHERE*O2STANDARD
	volume = 5

	New()
		..()
		src.air_contents.oxygen = (6*ONE_ATMOSPHERE)*volume/(R_IDEAL_GAS_EQUATION*T20C)
		src.air_contents.update_values()
		return

	remove_air(amount)
		var/datum/gas_mixture/G = new
		G.copy_from(air_contents)
		return G

	return_air()
		return air_contents

	assume_air(datum/gas_mixture/giver)
		return 1

	remove_air_volume(volume_to_return)
		if(!air_contents)
			return null

		var/tank_pressure = air_contents.return_pressure()
		if(tank_pressure < distribute_pressure)
			distribute_pressure = tank_pressure

		var/moles_needed = distribute_pressure*volume_to_return/(R_IDEAL_GAS_EQUATION*air_contents.temperature)
		moles_needed = max(moles_needed,0)

		var/datum/gas_mixture/G = remove_air(moles_needed)
		G.volume = volume_to_return
		return G

	toggle()
		set name = "Toggle Jetpack"
		set category = "Object"
		on = !on
		if(on)
//			icon_state = "[icon_state]-on"
//			item_state = "[item_state]-on"
			ion_trail.start()
		else
//			icon_state = initial(icon_state)
//			item_state = initial(item_state)
			ion_trail.stop()
		return


	allow_thrust(num, mob/living/user as mob)
		return (src.on)

	ui_action_click()
		toggle()

/obj/item/clothing/suit/powered/spawnable/spacemarine/chaos
	name = "Chaos Spacemarine Armor"
	desc = "For the Chaos Gods!"
	icon_state = "chaos_s"
	New()
		servos = new /obj/item/powerarmor/servos(src)
		servos.parent = src
		servos.toggleslowdown = 8
		reactive = new /obj/item/powerarmor/reactive/spacemarine(src)
		reactive.parent = src
		atmoseal = new /obj/item/powerarmor/atmoseal/adminbus(src)
		atmoseal.parent = src
		power = new /obj/item/powerarmor/power(src)
		power.parent = src

		verbs += /obj/item/clothing/suit/powered/proc/poweron

		new /obj/item/clothing/head/powered/spacemarine/chaos(src.loc)
		new /obj/item/weapon/tank/jetpack/spacemarine/chaos(src.loc)
/obj/item/clothing/head/powered/spacemarine/chaos
	name = "Chaos Spacemarine helmet"
	icon_state = "chaos_h"
/obj/item/weapon/tank/jetpack/spacemarine/chaos
	desc = "A backpack of chaos space marine exosuit."
	icon_state = "chaos_b"
