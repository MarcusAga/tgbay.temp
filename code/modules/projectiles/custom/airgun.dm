/obj/item/weapon/gun/airgun
	name = "Airgun"
	desc = "Pneumatic, compressed gas driven cannon"
	icon = 'icons/obj/airgun.dmi'
	item_icon = 'icons/obj/airgun.dmi'
	icon_state = "airgun"
	item_state = "airgun"
	force = 6.0
	var/obj/item/stuff = null
	var/obj/item/weapon/tank/emergency_oxygen/tank = null
	fire_sound = 'sound/effects/extinguish.ogg'
	origin_tech = "combat=2;engineering=3"
	m_amt = 5000
	g_amt = 1000

	examine()
		..()
		if(tank)
			usr << "It has [tank] attached, pressure display reads [tank.air_contents.return_pressure()]"
		else
			usr << "It has no airtank attached"
		if(stuff)
			usr << "It is loaded with [stuff]"
		else
			usr << "It is not loaded"

	update_icon()
		if(tank)
			if(istype(tank,/obj/item/weapon/tank/emergency_oxygen/engi) || istype(tank,/obj/item/weapon/tank/emergency_oxygen/double))
				icon_state = "airgun_y"
			else
				icon_state = "airgun_b"
		else
			icon_state = "airgun"

	attack_self(mob/living/user as mob)
		if(stuff)
			stuff.loc=user.loc
			user.put_in_hands(stuff)
			stuff=null
			return
		if(tank)
			tank.loc=user.loc
			user.put_in_hands(tank)
			tank=null
			update_icon()
			return
	
	attackby(var/obj/item/A as obj, mob/user as mob)
		if(istype(A,/obj/item/weapon/tank/emergency_oxygen))
			if(!tank)
				user.u_equip(A)
				tank=A
				A.loc=src
				update_icon()
		else if(istype(A))
			if(!stuff && A.w_class<=2)
				user.u_equip(A)
				stuff=A
				A.loc=src

	load_into_chamber()
		if(in_chamber)
			return 1

		var/airforce = 0
		if(tank)
			var/datum/gas_mixture/G = tank.remove_air_volume(3)
			airforce = max(0,G.return_pressure())
			if(airforce>0) 
				airforce = round(log(2,G.return_pressure()))
				in_chamber = new /obj/item/projectile/air_gun_dummy
				in_chamber:air = G
				in_chamber.kill_count = airforce

		if(!in_chamber) return 0

		if(!stuff)
			return 1

		in_chamber:payload = stuff
		stuff.loc = in_chamber
		if(istype(stuff,/obj/item/weapon/grenade))
			var/obj/item/weapon/grenade/G = stuff
			G.activate()
		in_chamber.icon = stuff.icon
		in_chamber.icon_state = stuff.icon_state
//		in_chamber.damage = stuff.force+airforce
		in_chamber.name = stuff.name
		stuff = null

		return 1

/obj/item/projectile/air_gun_dummy
	name = "air"
	desc = ""
	icon = 'icons/effects/effects.dmi'
	icon_state = "extinguish"
	flags = 0
	pass_flags = PASSTABLE
	kill_count = 50
	nodamage = 1
	damage = 0

	var/datum/gas_mixture/air
	var/obj/item/payload

	Del()
		if(payload)
			payload.loc = src.loc
		if(air)
			var/turf/T = get_turf(src)
			T.assume_air(air)
		..()

	process()
		if(kill_count < 1)
			del(src)
		kill_count--
		spawn while(src)
			if((!( current ) || loc == current))
				current = locate(min(max(x + xo, 1), world.maxx), min(max(y + yo, 1), world.maxy), z)
				if(current==loc)
					del(src)
					return
			if((x == 1 || x == world.maxx || y == 1 || y == world.maxy))
				del(src)
				return
			step_towards(src, current)
			if(air)
				var/datum/gas_mixture/tmp = air.remove_ratio(0.5)
				var/turf/T = get_turf(src)
				T.assume_air(tmp)
			if(kill_count < 1)
				del(src)
			kill_count--
			if(!bumped)
				if(loc == original)
					for(var/mob/living/M in original)
						if(!(M in permutated))
							Bump(M)
							sleep(3)
				else
					for(var/obj/item/clothing/mask/facehugger/F in loc)
						Bump(F) //Hit facehuggers
			sleep(3)
		return

	on_hit(var/atom/target, var/blocked = 0)
		var/airforce = 0
		if(air)
			airforce = 0.5*(log(1.75,air.return_pressure())**2)
		if(!isliving(target))	return 0
		var/mob/living/L = target
		if(payload)
			if(blocked >= 2)		return 0//Full block
			L.apply_damage(airforce+payload.w_class, payload.damtype, def_zone, blocked, payload.sharp, payload.name)
		else if(airforce && L.weakened<15)
			if(airforce>6)
				L.Weaken(max(0,min(airforce,15-L.weakened)))
				L << "Sudden rush of air knocks you over"
				airforce -= 7
				L.throw_at(current, airforce, 1)
			else if(prob(20*airforce))
				L.Weaken(max(0,min(airforce,15-L.weakened)))
				L << "Sudden rush of air knocks you over"
		del(src)
		return 1

datum/design/airgun
	name = "Airgun"
	desc = "Pneumatic, compressed gas driven cannon"
	id = "airgun"
	req_tech = list("combat" = 2, "engineering" = 3)
	build_type = 2 //PROTOLATHE
	materials = list("$metal" = 5000, "$glass" = 1000)
	build_path = "/obj/item/weapon/gun/airgun"
