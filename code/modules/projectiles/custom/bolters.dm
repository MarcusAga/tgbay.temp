/obj/item/weapon/gun/projectile/bolter
	name = "Imperial Bolter"
	desc = "An imperial bolter"
	icon = 'icons/obj/Bolter.dmi'
	item_icon = 'icons/obj/Bolter.dmi'
	icon_state = "bolter"
	item_state = "bolter"
	force = 14.0
	max_shells = 1
	caliber = "3.0"
	ammo_type ="/obj/item/ammo_casing/bolter"
	load_method = 2

	New()
		..()
		update_icon()
		return


	afterattack(atom/target as mob|obj|turf|area, mob/living/user as mob|obj, flag)
		..()
		if(!loaded.len && empty_mag)
//			empty_mag.loc = get_turf(src.loc)
//			empty_mag = null
			playsound(user, 'sound/weapons/smg_empty_alarm.ogg', 40, 1)
			update_icon()
		return

	load_into_chamber()
		if(!loaded.len)
			return 0
		var/obj/item/ammo_casing/AC = new ammo_type //load next casing.
		AC.name = "Bolter ammo casing"
		AC.desc = "Spent bolter ammo casing."
		if(isnull(AC) || !istype(AC))
			return 0
		if(AC.BB)
			in_chamber = AC.BB //Load projectile into chamber.
			AC.BB.loc = src //Set projectile loc to gun.
			return 1
		return 0

/obj/item/weapon/gun/projectile/bolter/chaos
	name = "Chaos Bolter"
	desc = "An chaos bolter"
	icon_state = "chaosbolter"
	item_state = "chaosbolter"

/obj/item/ammo_casing/bolter
	desc = "A 3.0 bolt casing."
	caliber = "3.0"
	projectile_type = "/obj/item/projectile/bullet/bolter_bullet"

/obj/item/projectile/bullet/bolter_bullet
	damage = 50
	icon_state = "bolter"
