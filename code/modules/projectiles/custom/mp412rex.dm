//
/obj/item/weapon/gun/projectile/mp412rex
	name = "MP-412 Rex"
	desc = "Russian revolver"
	icon = 'icons/obj/mp412rex.dmi'
	item_icon = 'icons/obj/mp412rex.dmi'
	icon_state = "mp412rex"
	item_state = "mp412rex"
	caliber = "357"
	ammo_type = "/obj/item/ammo_casing/a357"
	max_shells = 6
