//
/obj/item/weapon/gun
	name = "gun"
	desc = "Its a gun. It's pretty terrible, though."
	icon = 'icons/obj/gun.dmi'
	icon_state = "detective"
	item_state = "gun"
	flags =  FPRINT | TABLEPASS | CONDUCT |  USEDELAY
	slot_flags = SLOT_BELT
	m_amt = 2000
	w_class = 3.0
	throwforce = 5
	throw_speed = 4
	throw_range = 5
	force = 5.0
	origin_tech = "combat=1"
	attack_verb = list("struck", "hit", "bashed")

	var/fire_sound = 'sound/weapons/Gunshot.ogg'
	var/obj/item/projectile/in_chamber = null
	var/caliber = ""
	var/silenced = 0
	var/recoil = 0
	var/ejectshell = 1
	var/clumsy_check = 1


	proc/load_into_chamber()
		return

	proc/special_check(var/mob/M)
		return


	load_into_chamber()
		return 0


	special_check(var/mob/M) //Placeholder for any special checks, like detective's revolver.
		return 1


	emp_act(severity)
		for(var/obj/O in contents)
			O.emp_act(severity)


	afterattack(atom/target as mob|obj|turf|area, mob/living/user as mob|obj, flag, params)//TODO: go over this
		if(flag)	return //we're placing gun on a table or in backpack
		if(istype(target, /obj/machinery/recharger) && istype(src, /obj/item/weapon/gun/energy))	return//Shouldnt flag take care of this?

		//Exclude lasertag guns from the CLUMSY check.
		if(src.clumsy_check)
			if(istype(user, /mob/living))
				var/mob/living/M = user
				if ((CLUMSY in M.mutations) && prob(50))
					M << "\red The [src.name] blows up in your face."
					M.take_organ_damage(0,20)
					M.drop_item()
					del(src)
					return

		if (!user.IsAdvancedToolUser())
			user << "\red You don't have the dexterity to do this!"
			return
		if(istype(user, /mob/living))
			var/mob/living/M = user
			if (HULK in M.mutations)
				M << "\red Your meaty finger is much too large for the trigger guard!"
				return

		add_fingerprint(user)

		var/skill = ishuman(user)?(user:getSkill("weapons")):(issilicon(user)?3:0)
		var/distance = get_dist(user,target)
		var/unprecision = max(0,10-skill*3.8)*(distance+1)/8
		if(round(unprecision)!=unprecision)
			unprecision = round(unprecision)+prob((unprecision-round(unprecision))*100)?1:0

		var/turf/curloc = user.loc
		var/turf/targloc = get_turf(target)
		if(skill<=0.2 && prob(15))
			target = user
			targloc = get_turf(target)
		else if(unprecision>0 && targloc!=curloc)
			target = pick(range(unprecision,targloc))
			targloc = get_turf(target)

		if (!istype(targloc) || !istype(curloc))
			return

		if(!special_check(user))
			return
		if(!load_into_chamber())
			user << "\red *click*";
			return

		if(!in_chamber)
			return

		in_chamber.firer = user
		in_chamber.def_zone = user.zone_sel.selecting
		in_chamber.silenced = silenced
		in_chamber.original = targloc
		in_chamber.loc = get_turf(user)
		in_chamber.starting = get_turf(user)
		in_chamber.current = curloc

		if(recoil)
			spawn()
				shake_camera(user, recoil + 1, recoil)

		if(silenced)
			playsound(user, fire_sound, 10, 1)
		else
			playsound(user, fire_sound, 50, 1)
			var/shot_sound = "pneumatic shot"
			if(istype(in_chamber, /obj/item/projectile))
				shot_sound = "gunshot"
			else if(istype(in_chamber, /obj/item/projectile/beam))
				shot_sound = "laser blast"
			user.visible_message("\red %knownface:1% fires the [src.name]!", "\red You fire the [src.name]!", "\blue You hear a [shot_sound]!", actors=list(user))

		if(target == user)
			var/datum/organ/external/affecting = user.zone_sel.selecting
			if(affecting in list("head","mouth","eyes"))
				user.visible_message("\red %knownface:1% fires the [src.name] at his [affecting]!", "\red You fire the [src.name] at your [affecting]!", "\blue You hear a [istype(in_chamber, /obj/item/projectile/beam) ? "laser blast" : "gunshot"]!",actors=list(user))
				if(!in_chamber.nodamage)
					user.attack_log += text("\[[time_stamp()]\] <b>[user]/[user.ckey]</b> suicided by a <b>[src.type]</b> to the [affecting].")
					target:apply_damage(100*in_chamber.damage, in_chamber.damage_type, affecting, 0) // You are dead, dead, dead.
				user.bullet_act(in_chamber,affecting)
				var/inchamber = in_chamber
				in_chamber=null
				spawn(5) del(inchamber)
				update_icon()
				return
		if(targloc == curloc)
			target.bullet_act(in_chamber,user.zone_sel.selecting)
			var/inchamber = in_chamber
			in_chamber=null
			spawn(5) del(inchamber)
			update_icon()
			return

		user.next_move = world.time + 4
		in_chamber.yo = targloc.y - curloc.y
		in_chamber.xo = targloc.x - curloc.x

		if(params)
			var/list/mouse_control = params2list(params)
			if(mouse_control["icon-x"])
				in_chamber.p_x = text2num(mouse_control["icon-x"])
			if(mouse_control["icon-y"])
				in_chamber.p_y = text2num(mouse_control["icon-y"])

		spawn()
			if(in_chamber)
				in_chamber.process()
		sleep(1)
		in_chamber = null

		update_icon()
		return


/obj/item/weapon/gun/proc/isHandgun()
	return 1
