//
/obj/item/projectile/beam/jedi_lightning
	name = "jedi lightning"
	icon = 'icons/effects/jedi_lightning.dmi'
	icon_state = "con"
	start_icon_state = "end"
	end_icon_state = "start"
	weaken = 10
	stutter = 10
	damage = 15
