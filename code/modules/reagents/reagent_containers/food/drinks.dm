////////////////////////////////////////////////////////////////////////////////
/// Drinks.
////////////////////////////////////////////////////////////////////////////////
/obj/item/weapon/reagent_containers/food/drinks
	name = "drink"
	desc = "yummy"
	icon = 'icons/obj/drinks.dmi'
	icon_state = null
	flags = FPRINT | TABLEPASS | OPENCONTAINER
	var/gulp_size = 5 //This is now officially broken ... need to think of a nice way to fix it.
	possible_transfer_amounts = list(5,10,25,50)
	volume = 50

	on_reagent_change()
		if (gulp_size < 5) gulp_size = 5
		else gulp_size = max(round(reagents.total_volume / 5), 5)

	attack_self(mob/user as mob)
		return

	attack(mob/M as mob, mob/user as mob, def_zone)
		var/datum/reagents/R = src.reagents
		var/fillevel = gulp_size

		if(!R.total_volume || !R)
			user << "\red None of [src] left, oh no!"
			return 0

		if(ishuman(M))
			var/obj/item/cover = M:checkcoverage("head","mouth")
			if(istype(cover))
				user << "[src] fails to get through the [cover]"
				return 0

		if(M == user)
			if(amount_per_transfer_from_this <= 5)
				user.visible_message("\blue %knownface:1% swallows a gulp of [src].","\blue You swallow a gulp of [src].","You hear a gulp.", actors=list(user))
			else if(amount_per_transfer_from_this>=reagents.total_volume)
				user.visible_message("\blue %knownface:1% drains the [src].","\blue You drain \the [src].","You hear a big gulp.", actors=list(user))
			else
				user.visible_message("\blue %knownface:1% makes a big gulp of [src].","\blue You make a big gulp of [src].","You hear a big gulp.", actors=list(user))
			if(reagents.total_volume)
				reagents.reaction(M, INGEST)
				spawn(5)
					reagents.trans_to(M, amount_per_transfer_from_this)

			playsound(M.loc,'sound/items/drink.ogg', rand(10,50), 1)
			return 1
		else if(ishuman(M) || ismonkey(M))

			for(var/mob/O in viewers(world.view, user))
				O.show_message("\red [user] attempts to feed [M] [src].", 1)
			if(!do_mob(user, M)) return
			for(var/mob/O in viewers(world.view, user))
				O.show_message("\red [user] feeds [M] [src].", 1)

			M.attack_log += text("\[[time_stamp()]\] <font color='orange'>Has been fed [src.name] by [user.name] ([user.ckey]) Reagents: [reagentlist(src)]</font>")
			user.attack_log += text("\[[time_stamp()]\] <font color='red'>Fed [M.name] by [M.name] ([M.ckey]) Reagents: [reagentlist(src)]</font>")

			log_attack("<font color='red'>[user.name] ([user.ckey]) fed [M.name] ([M.ckey]) with [src.name] (INTENT: [uppertext(user.a_intent)])</font>")

			log_admin("ATTACK: [user.name] ([user.ckey]) fed [M.name] ([M.ckey]) with [src.name] (INTENT: [uppertext(user.a_intent)])")
			msg_admin_attack("ATTACK: [user.name] ([user.ckey]) fed [M.name] ([M.ckey]) with [src.name] (INTENT: [uppertext(user.a_intent)])") //BS12 EDIT ALG

			if(reagents.total_volume)
				reagents.reaction(M, INGEST)
				spawn(5)
					reagents.trans_to(M, gulp_size)

			if(isrobot(user)) //Cyborg modules that include drinks automatically refill themselves, but drain the borg's cell
				var/mob/living/silicon/robot/bro = user
				bro.cell.use(30)
				var/refill = R.get_master_reagent_id()
				spawn(600)
					R.add_reagent(refill, fillevel)

			playsound(M.loc,'sound/items/drink.ogg', rand(10,50), 1)
			return 1

		return 0


	afterattack(obj/target, mob/user , flag)

		if(istype(target, /obj/structure/reagent_dispensers)) //A dispenser. Transfer FROM it TO us.

			if(!target.reagents.total_volume)
				user << "\red [target] is empty."
				return

			if(reagents.total_volume >= reagents.maximum_volume)
				user << "\red [src] is full."
				return

			var/trans = target.reagents.trans_to(src, target:amount_per_transfer_from_this)
			user << "\blue You fill [src] with [trans] units of the contents of [target]."

		else if(target.is_open_container()) //Something like a glass. Player probably wants to transfer TO it.
			if(!reagents.total_volume)
				user << "\red [src] is empty."
				return

			if(target.reagents.total_volume >= target.reagents.maximum_volume)
				user << "\red [target] is full."
				return



			var/datum/reagent/refill
			var/datum/reagent/refillName
			if(isrobot(user))
				refill = reagents.get_master_reagent_id()
				refillName = reagents.get_master_reagent_name()

			var/trans = src.reagents.trans_to(target, amount_per_transfer_from_this)
			user << "\blue You transfer [trans] units of the solution to [target]."

			if(isrobot(user)) //Cyborg modules that include drinks automatically refill themselves, but drain the borg's cell
				var/mob/living/silicon/robot/bro = user
				var/chargeAmount = max(30,4*trans)
				bro.cell.use(chargeAmount)
				user << "Now synthesizing [trans] units of [refillName]..."


				spawn(300)
					reagents.add_reagent(refill, trans)
					user << "Cyborg [src] refilled."

		return

	examine()
		set src in view()
		..()
		if (!(usr in range(0)) && usr!=src.loc) return
		if(!reagents || reagents.total_volume==0)
			usr << "\blue \The [src] is empty!"
		else if (reagents.total_volume<src.volume/4)
			usr << "\blue \The [src] is almost empty!"
		else if (reagents.total_volume<src.volume/2)
			usr << "\blue \The [src] is half full!"
		else if (reagents.total_volume<src.volume/0.90)
			usr << "\blue \The [src] is almost full!"
		else
			usr << "\blue \The [src] is full!"


/obj/item/weapon/reagent_containers/food/drinks/cup
	name = "Empty cup"
	desc = "Empty clean cup."
	icon = 'icons/obj/mugs_and_cans.dmi'
	icon_state = "mug_clear"
	item_state = "coffee"
	New()
		..()
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)
	on_reagent_change()
		if (reagents.reagent_list.len > 0)
			switch(reagents.get_master_reagent_id())
				if("water")
					icon_state = "mug_clear"
					name = "Cup of water"
					desc = "The father of all refreshments."
				if("water")
					icon_state = "mug_clear"
					name = "Cup of water"
					desc = "The father of all refreshments."
				if("tomatojuice")
					icon_state = "mug_red"
					name = "Cup of Tomato juice"
					desc = "Are you sure this is tomato juice?"
				if("blood")
					icon_state = "mug_red"
					name = "Cup of Tomato juice"
					desc = "Are you sure this is tomato juice?"
				if("milk")
					icon_state = "mug_white"
					name = "Cup of milk"
					desc = "White and nutritious goodness!"
				if("soymilk")
					icon_state = "mug_white"
					name = "Cup of soy milk"
					desc = "White and nutritious soy goodness!"
				if("cream")
					icon_state  = "mug_white"
					name = "Cup of cream"
					desc = "Ewwww..."
				if("coffee")
					icon_state = "mug_brown"
					name = "Cup of coffee"
					desc = "Careful, the beverage you're about to enjoy is extremely hot."
				if("tea")
					icon_state = "Duke_Purple_Tea"
					name = "Duke Purple Tea"
					desc = "An insult to Duke Purple is an insult to the Space Queen! Any proper gentleman will fight you, if you sully this tea."
				if("hot_coco")
					icon_state = "Dutch_Hot_Coco"
					name = "Dutch Hot Coco"
					desc = "Made in Space South America."
				if("spacemountainwind")
					icon_state = "mug_Space_mountain_wind"
					name = "Space Mountain Wind"
					desc = "Blows right through you like a space wind."
				if("thirteenloko")
					icon_state = "mug_thirteen_loko"
					name = "Thirteen Loko"
					desc = "The CMO has advised crew members that consumption of Thirteen Loko may result in seizures, blindness, drunkeness, or even death. Please Drink Responsably."
				if("dr_gibb")
					icon_state = "mug_dr_gibb"
					name = "Dr. Gibb"
					desc = "A delicious mixture of 42 different flavors."
				if("space_up")
					icon_state = "mug_space-up"
					name = "Space-Up"
					desc = "Tastes like a hull breach in your mouth."
				if("berryjuice")
					icon_state = "mug_berryjuice"
					name = "Cup of berry juice"
					desc = "Berry juice. Or maybe its jam. Who cares?"
				if("banana")
					icon_state = "mug_banana"
					name = "Cup of banana juice"
					desc = "The raw essence of a banana. HONK"
				if("carrotjuice")
					icon_state = "mug_carrotjuice"
					name = "Cup of carrot juice"
					desc = "It is just like a carrot but without crunching."
				if("chocolate")
					icon_state  = "mug_chocolate"
					name = "Cup of chocolate"
					desc = "Tasty"
				if("lemon")
					icon_state  = "mug_lemon"
					name = "Cup of lemon"
					desc = "Sour..."
				if("poisonberryjuice")
					icon_state = "mug_poisonberryjuice"
					name = "Cup of poison berry juice"
					desc = "A glass of deadly juice."
				if("ice")
					icon_state = "mug_ice"
					name = "Cup of ice"
					desc = "Generally, you're supposed to put something else in there too..."
				if("icecoffee")
					icon_state = "mug_icedcoffee"
					name = "Iced Coffee"
					desc = "A drink to perk you up and refresh you!"
				if("cola")
					icon_state  = "mug_brown"
					name = "Cup of Space Cola"
					desc = "A cup of refreshing Space Cola"
				if("orangejuice")
					icon_state = "mug_orange"
					name = "Cup of Orange juice"
					desc = "Vitamins! Yay!"
				if("limejuice")
					icon_state = "mug_green"
					name = "Cup of Lime juice"
					desc = "A cup of sweet-sour lime juice."
				else
					icon_state ="mug_brown"
					name = "Cup of ..what?"
					desc = "You can't really tell what this is."
		else
			icon_state = "mug_empty"
			name = "Cup"
			desc = "Your standard drinking cup"

////////////////////////////////////////////////////////////////////////////////
/// Drinks. END
////////////////////////////////////////////////////////////////////////////////

/obj/item/weapon/reagent_containers/food/drinks/golden_cup
	desc = "A golden cup"
	name = "golden cup"
	icon_state = "golden_cup"
	item_state = "" //nope :(
	w_class = 4
	force = 14
	throwforce = 10
	amount_per_transfer_from_this = 20
	possible_transfer_amounts = null
	volume = 150
	flags = FPRINT | CONDUCT | TABLEPASS | OPENCONTAINER

/obj/item/weapon/reagent_containers/food/drinks/golden_cup/tournament_26_06_2011
	desc = "A golden cup. It will be presented to a winner of tournament 26 june and name of the winner will be graved on it."


///////////////////////////////////////////////Drinks
//Notes by Darem: Drinks are simply containers that start preloaded. Unlike condiments, the contents can be ingested directly
//	rather then having to add it to something else first. They should only contain liquids. They have a default container size of 50.
//	Formatting is the same as food.

/obj/item/weapon/reagent_containers/food/drinks/milk
	name = "Space Milk"
	desc = "It's milk. White and nutritious goodness!"
	icon_state = "milk"
	item_state = "carton"
	New()
		..()
		reagents.add_reagent("milk", 50)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/flour
	name = "flour sack"
	desc = "A big bag of flour. Good for baking!"
	icon = 'icons/obj/food.dmi'
	icon_state = "flour"
	item_state = "flour"
	New()
		..()
		reagents.add_reagent("flour", 30)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/soymilk
	name = "SoyMilk"
	desc = "It's soy milk. White and nutritious goodness!"
	icon_state = "soymilk"
	item_state = "carton"
	New()
		..()
		reagents.add_reagent("soymilk", 50)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/cup/coffee
	name = "Robust Coffee"
	desc = "Careful, the beverage you're about to enjoy is extremely hot."
	New()
		..()
		reagents.add_reagent("coffee", 30)

/obj/item/weapon/reagent_containers/food/drinks/cup/tea
	name = "Duke Purple Tea"
	desc = "An insult to Duke Purple is an insult to the Space Queen! Any proper gentleman will fight you, if you sully this tea."
	New()
		..()
		reagents.add_reagent("tea", 30)

/obj/item/weapon/reagent_containers/food/drinks/ice
	name = "Ice Cup"
	desc = "Careful, cold ice, do not chew."
	icon_state = "coffee"
	New()
		..()
		reagents.add_reagent("ice", 30)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/cup/h_chocolate
	name = "Dutch Hot Coco"
	desc = "Made in Space South America."
	New()
		..()
		reagents.add_reagent("hot_coco", 30)

/obj/item/weapon/reagent_containers/food/drinks/dry_ramen
	name = "Cup Ramen"
	desc = "Just add 10ml water, self heats! A taste that reminds you of your school years."
	icon_state = "ramen"
	New()
		..()
		reagents.add_reagent("dry_ramen", 30)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/cola
	name = "Space Cola"
	desc = "Cola. in space."
	icon_state = "cola"
	New()
		..()
		reagents.add_reagent("cola", 30)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/beer
	name = "Space Beer"
	desc = "Beer. In space."
	icon_state = "beer"
	New()
		..()
		reagents.add_reagent("beer", 30)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/ale
	name = "Magm-Ale"
	desc = "A true dorf's drink of choice."
	icon_state = "alebottle"
	item_state = "beer"
	New()
		..()
		reagents.add_reagent("ale", 30)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/space_mountain_wind
	name = "Space Mountain Wind"
	desc = "Blows right through you like a space wind."
	icon_state = "space_mountain_wind"
	New()
		..()
		reagents.add_reagent("spacemountainwind", 30)

/obj/item/weapon/reagent_containers/food/drinks/thirteenloko
	name = "Thirteen Loko"
	desc = "The CMO has advised crew members that consumption of Thirteen Loko may result in seizures, blindness, drunkeness, or even death. Please Drink Responsably."
	icon_state = "thirteen_loko"
	New()
		..()
		reagents.add_reagent("thirteenloko", 30)

/obj/item/weapon/reagent_containers/food/drinks/dr_gibb
	name = "Dr. Gibb"
	desc = "A delicious mixture of 42 different flavors."
	icon_state = "dr_gibb"
	New()
		..()
		reagents.add_reagent("dr_gibb", 30)

/obj/item/weapon/reagent_containers/food/drinks/starkist
	name = "Star-kist"
	desc = "The taste of a star in liquid form. And, a bit of tuna...?"
	icon_state = "starkist"
	New()
		..()
		reagents.add_reagent("cola", 15)
		reagents.add_reagent("orangejuice", 15)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)

/obj/item/weapon/reagent_containers/food/drinks/space_up
	name = "Space-Up"
	desc = "Tastes like a hull breach in your mouth."
	icon_state = "space-up"
	New()
		..()
		reagents.add_reagent("space_up", 30)

/obj/item/weapon/reagent_containers/food/drinks/lemon_lime
	name = "Lemon-Lime"
	desc = "You wanted ORANGE. It gave you Lemon Lime."
	icon_state = "lemon-lime"
	New()
		..()
		reagents.add_reagent("lemon_lime", 30)
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)


/obj/item/weapon/reagent_containers/food/drinks/sillycup
	name = "Paper Cup"
	desc = "A paper water cup."
	icon_state = "water_cup_e"
	possible_transfer_amounts = null
	volume = 10
	New()
		..()
		src.pixel_x = rand(-20, 20)
		src.pixel_y = rand(-20, 20)
	on_reagent_change()
		if(reagents.total_volume)
			icon_state = "water_cup"
		else
			icon_state = "water_cup_e"

/obj/item/weapon/reagent_containers/food/drinks/tonic
	name = "T-Borg's Tonic Water"
	desc = "Quinine tastes funny, but at least it'll keep that Space Malaria away."
	icon_state = "tonic"
	New()
		..()
		reagents.add_reagent("tonic", 50)

/obj/item/weapon/reagent_containers/food/drinks/sodawater
	name = "Soda Water"
	desc = "A can of soda water. Why not make a scotch and soda?"
	icon_state = "sodawater"
	New()
		..()
		reagents.add_reagent("sodawater", 50)
//////////////////////////drinkingglass and shaker//
//Note by Darem: This code handles the mixing of drinks. New drinks go in three places: In Chemistry-Reagents.dm (for the drink
//	itself), in Chemistry-Recipes.dm (for the reaction that changes the components into the drink), and here (for the drinking glass
//	icon states.

/obj/item/weapon/reagent_containers/food/drinks/shaker
	name = "Shaker"
	desc = "A metal shaker to mix drinks in."
	icon_state = "shaker"
	amount_per_transfer_from_this = 10
	volume = 100

/obj/item/weapon/reagent_containers/food/drinks/flask
	name = "Captain's Flask"
	desc = "A metal flask belonging to the captain"
	icon_state = "flask"
	volume = 60

/obj/item/weapon/reagent_containers/food/drinks/britcup
	name = "cup"
	desc = "A cup with the british flag emblazoned on it."
	icon_state = "britcup"
	volume = 30