//

/obj/item/weapon/gun/syringe
	name = "syringe gun"
	desc = "A spring loaded rifle designed to fit syringes, designed to incapacitate unruly patients from a distance."
	icon = 'icons/obj/gun.dmi'
	icon_state = "syringegun"
	item_state = "syringegun"
	w_class = 3.0
	throw_speed = 2
	throw_range = 10
	force = 4.0
	var/list/syringes = new/list()
	var/max_syringes = 1
	m_amt = 2000
	fire_sound = 'sound/items/syringeproj.ogg'

	examine()
		set src in view()
		..()
		if (!(usr in view(2)) && usr!=src.loc) return
		usr << "\blue [syringes.len] / [max_syringes] syringes."

	attackby(obj/item/I as obj, mob/user as mob)
		if(istype(I, /obj/item/weapon/reagent_containers/syringe) ||\
		   istype(I, /obj/item/weapon/dnainjector) ||\
		   istype(I, /obj/item/weapon/screwdriver) ||\
		   istype(I, /obj/item/weapon/scalpel) ||\
		   istype(I, /obj/item/weapon/pen))
			if(syringes.len < max_syringes)
				user.drop_item()
				I.loc = src
				syringes += I
				user << "\blue You put the syringe in [src]."
				user << "\blue [syringes.len] / [max_syringes] syringes."
			else
				usr << "\red [src] cannot hold more syringes."

	load_into_chamber()
		if(in_chamber)
			return 1
		if(!syringes.len) return 0

		in_chamber = new /obj/item/projectile/syringe_gun_dummy
		var/obj/item/S = syringes[1]
		if(!S)
			return 0
		syringes -= S
		in_chamber:payload = S
		S.loc = in_chamber

		if(istype(S, /obj/item/weapon/reagent_containers/syringe) || istype(S, /obj/item/weapon/dnainjector))
			in_chamber.icon_state = "syringeproj"
			in_chamber.damage = rand(2,5)
		else
			in_chamber.icon = S.icon
			in_chamber.icon_state = S.icon_state
			in_chamber.damage = S.force
		in_chamber.name = S.name

		return 1

/obj/item/weapon/gun/syringe/rapidsyringe
	name = "rapid syringe gun"
	desc = "A modification of the syringe gun design, using a rotating cylinder to store up to four syringes."
	icon_state = "rapidsyringegun"
	max_syringes = 4

/obj/item/projectile/syringe_gun_dummy
	name = "air-thrown object"
	desc = ""
	icon = 'icons/obj/chemical.dmi'
	icon_state = "null"
	flags = 0
	pass_flags = PASSTABLE
	anchored = 1
	kill_count = 6
	nodamage = 1
	damage = 0

	var/obj/item/payload

	Del()
		if(payload)
			payload.loc = src.loc
		..()

	process()
		if(kill_count < 1)
			del(src)
		kill_count--
		spawn while(src)
			if((!( current ) || loc == current))
				current = locate(min(max(x + xo, 1), world.maxx), min(max(y + yo, 1), world.maxy), z)
				if(current==loc)
					del(src)
					return
			if((x == 1 || x == world.maxx || y == 1 || y == world.maxy))
				del(src)
				return
			step_towards(src, current)
			if(kill_count < 1)
				del(src)
			kill_count--
			if(!bumped)
				if(loc == original)
					for(var/mob/living/M in original)
						if(!(M in permutated))
							Bump(M)
							sleep(1)
				else
					for(var/obj/item/clothing/mask/facehugger/F in loc)
						Bump(F) //Hit facehuggers
			sleep(1)
		return

	on_hit(var/atom/target, var/blocked = 0)
		if(blocked >= 2)		return 0//Full block
		if(!isliving(target))	return 0
		if(isanimal(target))	return 0
		var/mob/living/L = target
		if(def_zone=="eyes")
			eyestab(L,firer)
		if(istype(payload,/obj/item/weapon/reagent_containers/syringe))
			if(payload.reagents)
				payload.reagents.reaction(target, INGEST)
				payload.reagents.trans_to(target, payload.reagents.total_volume)
		else if(istype(payload,/obj/item/weapon/dnainjector))
			if(payload:dnatype == "se")
				if (isblockon(getblock(payload:dna, STRUCDNASIZE,3),STRUCDNASIZE) && istype(L, /mob/living/carbon/human))
					if(prob(60))
						spawn(1) del(src)
						return 1
					message_admins("[key_name_admin(firer)] shot [key_name_admin(L)] with the [name] \red(MONKEY)")
					log_game("[key_name(firer)] injected [key_name(L)] with the [name] (MONKEY)")
				else
					log_game("[key_name(firer)] injected [key_name(L)] with the [name]")
			else
				log_game("[key_name(firer)] injected [key_name(L)] with the [name]")
			payload:inject(L)//Now we actually do the heavy lifting.
		else
			L.apply_damage(payload.force, payload.damtype, def_zone, blocked, payload.sharp, payload.name)
		del(src)
		return 1
