//

/datum/artifact_effect
	var/artifact_id = ""       // Display ID of the spawning artifact
	var/trigger = "touch"      // What activates it?
	var/triggerX = "none"      // Used for more varied triggers
	var/effecttype = "healing" // What does it do?
	var/effectmode = "aura"    // How does it carry out the effect?
	var/aurarange = 4          // How far the artifact will extend an aura effect.
	var/list/created_field
	var/forcefield_on = 0
	var/archived_loc

/datum/artifact_effect/New()
	//
	created_field = new()

/datum/artifact_effect/proc/GetOriginString(var/origin)

/datum/artifact_effect/proc/GetEffectString(var/effect)

/datum/artifact_effect/proc/GetTriggerString(var/trigger)

/datum/artifact_effect/proc/GetRangeString(var/range)
	switch(effectmode)
		if("aura") return "Constant Short-Range Energy Field"
		if("pulse")
			if(aurarange > 7) return "Long Range Energy Pulses"
			else return "Medium Range Energy Pulses"
		if("worldpulse") return "Extreme Range Energy Pulses"
		if("contact") return "Requires contact with subject"
		else return "Unknown Range"

/datum/artifact_effect/proc/HaltEffect()
	for(var/obj/effect/energy_field/F in created_field)
		created_field.Remove(F)
		del F
	forcefield_on = 0

/datum/artifact_effect/proc/UpdateEffect(var/atom/originator)
	/*for(var/obj/effect/energy_field/F in created_field)
		created_field.Remove(F)
		del F*/
	if(originator.loc != archived_loc)
		archived_loc = originator.loc
		update_move(originator)

	for(var/obj/effect/energy_field/E in created_field)
		if(E.strength < 5)
			E.Strengthen(0.2)

/datum/artifact_effect/proc/DoEffect(var/atom/originator)
	. = 0
	archived_loc = get_turf(originator)
	if (src.effectmode == "contact")
		var/mob/living/user = originator
		if(!user)
			return 0
		switch(src.effecttype)
			if("hallucination")
				effect_hallucination(user,25)
				return 1
			if("drugs")
				if(effect_drugs(user,15))
					user << "\blue You feel a tonic energy filling your head."
				return 1
			if("helping")
				if(effect_helping(user,10))
					user << "\blue You feel a soothing energy invigorate you."
				else
					user << "Nothing happens."
				return 1
			if("healing")
				//caeltodo
				if(effect_healing(user,25))
					user << "\blue You feel a soothing energy invigorate you."
				else
					user << "Nothing happens."
				return 1
			if("injure")
				if(effect_injure(user,25))
					user << "\red A painful discharge of energy strikes you!"
				else
					user << "Nothing happens."
				return 1
			if("stun")
				if(effect_stun(user,12))
					user << "\red A powerful force overwhelms your consciousness."
				else
					user << "Nothing happens."
				return 1
			if("roboheal")
				if(effect_roboheal(user,30))
					user << "\blue Your systems report damaged components mending by themselves!"
				else
					user << "Nothing happens."
				return 1
			if("robohurt")
				if(effect_robohurt(user,40))
					user << "\red Your systems report severe damage has been inflicted!"
				else
					user << "Nothing happens."
				return 1
			if("forcefield")
				effect_forcefield(user)
				return 1
			if("teleport")
				effect_teleport(user,50)
				return 1
	else if (src.effectmode == "aura")
		switch(src.effecttype)
			if("hallucination")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && (istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly) || M:isCyborg()))
						continue
					if(effect_hallucination(M,5))
						if(prob(5)) M << "\blue You feel a tonic energy radiating from something nearby."
				return 1
			if("drugs")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && (istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly) || M:isCyborg()))
						continue
					if(effect_drugs(M,1))
						if(prob(5)) M << "\blue You feel a tonic energy filling your head."
				return 1
			if("helping")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && (istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly) || M:isCyborg()))
						continue
					if(effect_helping(M,1))
						if(prob(5)) M << "\blue You feel a soothing energy radiating from something nearby."
				return 1
			if("healing")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && (istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly) || M:isCyborg()))
						continue
					if(effect_healing(M,1))
						if(prob(5)) M << "\blue You feel a soothing energy radiating from something nearby."
				return 1
			if("injure")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && (istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly) || M:isCyborg()))
						continue
					if(effect_injure(M,1))
						if(prob(5)) M << "\red You feel a painful force radiating from something nearby."
				return 1
			if("stun")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(prob(5)) M << "\red Energy radiating from the [originator] is making you feel numb."
					if(prob(20))
						effect_stun(M,1)
						M << "\red Your body goes numb for a moment."
				return 1
			if("roboheal")
				var/list/ranged = range(src.aurarange,archived_loc)
				for (var/mob/living/silicon/robot/M in ranged)
					if(effect_roboheal(M,1))
						if(prob(5))
							M << "\blue SYSTEM ALERT: Beneficial energy field detected!"
				for (var/mob/living/carbon/human/M in ranged)
					if(!M.isCyborg())
						continue
					if(effect_healing(M,1))
						if(!M.stat && prob(5))
							M << "\blue SYSTEM ALERT: Beneficial energy field detected!"
				return 1
			if("robohurt")
				var/list/ranged = range(src.aurarange,archived_loc)
				for (var/mob/living/silicon/robot/M in ranged)
					if(effect_robohurt(M,1))
						if(prob(5))
							M << "\red SYSTEM ALERT: Harmful energy field detected!"
				for (var/mob/living/carbon/human/M in ranged)
					if(!M.isCyborg())
						continue
					if(effect_injure(M,1))
						if(!M.stat && prob(5))
							M << "\red SYSTEM ALERT: Harmful energy field detected!"
				return 1
			if("cellcharge")
				effect_cellcharge(range(src.aurarange,archived_loc),10)
				return 1
			if("celldrain")
				effect_celldrain(range(src.aurarange,archived_loc),15)
				return 1
			if("planthelper")
				effect_planthelper(range(src.aurarange,archived_loc),1)
				return 1
			if("gravpull")
				effect_gravpull(originator,src.aurarange,1)
				return 1
	else if (src.effectmode == "pulse")
		for(var/mob/O in viewers(originator, null))
			O.show_message(text("<b>[]</b> emits a pulse of energy!", originator), 1)
		switch(src.effecttype)
			if("hallucination")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					effect_hallucination(M,15)
				return 1
			if("drugs")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_drugs(M,10))
						M << "\blue You feel a tonic energy filling your head."
				return 1
			if("helping")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_helping(M,5))
						M << "\blue A wave of energy invigorates you."
				return 1
			if("healing")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_healing(M,5))
						M << "\blue A wave of energy invigorates you."
				return 1
			if("injure")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_injure(M,5))
						M << "\red A wave of energy causes you great pain!"
				return 1
			if("stun")
				for (var/mob/living/carbon/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_stun(M,2))
						M << "\red A wave of energy overwhelms your senses!"
				return 1
			if("roboheal")
				for (var/mob/living/silicon/robot/M in range(src.aurarange,archived_loc))
					if(effect_roboheal(M,10))
						M << "\blue SYSTEM ALERT: Structural damage has been repaired by energy pulse!"
				return 1
			if("robohurt")
				for (var/mob/living/silicon/robot/M in range(src.aurarange,archived_loc))
					if(effect_robohurt(M,10))
						M << "\red SYSTEM ALERT: Structural damage inflicted by energy pulse!"
				return 1
			if("cellcharge")
				effect_cellcharge(range(src.aurarange,archived_loc),250)
				return 1
			if("celldrain")
				effect_celldrain(range(src.aurarange,archived_loc),250)
				return 1
			if("planthelper")
				effect_planthelper(range(src.aurarange,archived_loc),15)
				return 1
			if("teleport")
				for (var/mob/living/M in range(src.aurarange,archived_loc))
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					effect_teleport(M,30)
				return 1
			if("gravpull")
				effect_gravpull(originator,src.aurarange,2)
				return 1
	else if (src.effectmode == "worldpulse")
		for(var/mob/O in viewers(originator, null))
			O.show_message(text("<b>[]</b> emits a powerful burst of energy!", originator), 1)
		switch(src.effecttype)
			if("hallucination")
				for (var/mob/living/carbon/M in world)
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					effect_hallucination(M,20)
				return 1
			if("drugs")
				for (var/mob/living/carbon/M in world)
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					effect_drugs(M,5)
				return 1
			if("helping")
				for (var/mob/living/carbon/M in world)
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_helping(M,3))
						M << "\blue A wave of energy invigorates you."
				return 1
			if("healing")
				for (var/mob/living/carbon/M in world)
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_healing(M,3))
						M << "\blue Waves of soothing energy wash over you."
				return 1
			if("injure")
				for (var/mob/living/carbon/human/M in world)
					if(effect_injure(M,3))
						M << "\red A wave of painful energy strikes you!"
				return 1
			if("stun")
				for (var/mob/living/carbon/M in world)
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					if(effect_stun(M,3))
						M << "\red A powerful force causes you to black out momentarily."
				return 1
			if("roboheal")
				for (var/mob/living/silicon/robot/M in world)
					if(effect_roboheal(M,5))
						M << "\blue SYSTEM ALERT: Structural damage has been repaired by energy pulse!"
				return 1
			if("robohurt")
				for (var/mob/living/silicon/robot/M in world)
					if(effect_robohurt(M,5))
						M << "\red SYSTEM ALERT: Structural damage inflicted by energy pulse!"
				return 1
			if("cellcharge")
				effect_cellcharge(range(src.aurarange,archived_loc),125)
				return 1
			if("celldrain")
				effect_celldrain(range(src.aurarange,archived_loc),125)
				return 1
			if("teleport")
				for (var/mob/living/M in world)
					if(ishuman(M) && istype(M:wear_suit,/obj/item/clothing/suit/bio_suit/anomaly) && istype(M:head,/obj/item/clothing/head/bio_hood/anomaly))
						continue
					effect_teleport(M,15)
				return 1

//initially for the force field artifact
/datum/artifact_effect/proc/update_move(var/atom/originator)
	switch(effecttype)
		if("forcefield")
			if(!forcefield_on) return
			while(created_field.len < 16)
				//for now, just instantly respawn the fields when they get destroyed
				var/obj/effect/energy_field/E = new (locate(originator.x,originator.y,originator))
				created_field.Add(E)
				E.strength = 1
				E.density = 1
				E.invisibility = 0

			var/obj/effect/energy_field/E = created_field[1]
			E.loc = locate(originator.x + 2,originator.y + 2,originator.z)
			E = created_field[2]
			E.loc = locate(originator.x + 2,originator.y + 1,originator.z)
			E = created_field[3]
			E.loc = locate(originator.x + 2,originator.y,originator.z)
			E = created_field[4]
			E.loc = locate(originator.x + 2,originator.y - 1,originator.z)
			E = created_field[5]
			E.loc = locate(originator.x + 2,originator.y - 2,originator.z)
			E = created_field[6]
			E.loc = locate(originator.x + 1,originator.y + 2,originator.z)
			E = created_field[7]
			E.loc = locate(originator.x + 1,originator.y - 2,originator.z)
			E = created_field[8]
			E.loc = locate(originator.x,originator.y + 2,originator.z)
			E = created_field[9]
			E.loc = locate(originator.x,originator.y - 2,originator.z)
			E = created_field[10]
			E.loc = locate(originator.x - 1,originator.y + 2,originator.z)
			E = created_field[11]
			E.loc = locate(originator.x - 1,originator.y - 2,originator.z)
			E = created_field[12]
			E.loc = locate(originator.x - 2,originator.y + 2,originator.z)
			E = created_field[13]
			E.loc = locate(originator.x - 2,originator.y + 1,originator.z)
			E = created_field[14]
			E.loc = locate(originator.x - 2,originator.y,originator.z)
			E = created_field[15]
			E.loc = locate(originator.x - 2,originator.y - 1,originator.z)
			E = created_field[16]
			E.loc = locate(originator.x - 2,originator.y - 2,originator.z)

datum/artifact_effect/proc/effect_hallucination(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/carbon/))
		M.hallucination = min(M.hallucination+3*power,500)
		M.stuttering += power*0.4
		return 1
	return 0

datum/artifact_effect/proc/effect_drugs(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/carbon/))
		if(ishuman(M))
			M:iamhigh = max(M:iamhigh, power)
		if(M.reagents.get_reagent_amount("tramadol")<25)
			M.reagents.add_reagent("tramadol", power*REAGENTS_METABOLISM)
		if(M.reagents.get_reagent_amount("inaprovaline")<25)
			M.reagents.add_reagent("inaprovaline", power*REAGENTS_METABOLISM)
		M.hallucination = max(0, M.hallucination - 5*power)
		M.stunned = max(0,M.stunned-power)
		M.stuttering = max(0,M.stuttering-power)
		return 1
	return 0

datum/artifact_effect/proc/effect_helping(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/carbon/))
		var/mob/living/carbon/C = M
		C.drowsyness = max(M.drowsyness-2*power, 0)
		if(C.reagents.has_reagent("toxin"))
			C.reagents.remove_reagent("toxin", 2*power)
		if(C.reagents.has_reagent("stoxin"))
			C.reagents.remove_reagent("stoxin", 2*power)
		if(C.reagents.has_reagent("plasma"))
			C.reagents.remove_reagent("plasma", 1*power)
		if(C.reagents.has_reagent("sacid"))
			C.reagents.remove_reagent("sacid", 1*power)
		if(C.reagents.has_reagent("cyanide"))
			C.reagents.remove_reagent("cyanide", 1*power)
		if(C.reagents.has_reagent("amatoxin"))
			C.reagents.remove_reagent("amatoxin", 2*power)
		if(C.reagents.has_reagent("chloralhydrate"))
			C.reagents.remove_reagent("chloralhydrate", 5*power)
		if(C.reagents.has_reagent("carpotoxin"))
			C.reagents.remove_reagent("carpotoxin", 1*power)
		if(C.reagents.has_reagent("zombiepowder"))
			C.reagents.remove_reagent("zombiepowder", 0.5*power)
		if(C.reagents.has_reagent("mindbreaker"))
			C.reagents.remove_reagent("mindbreaker", 2*power)
		C.adjustToxLoss(-power)
		C.paralysis = max(0,C.paralysis-power)
		C.weakened = max(0,C.weakened-power)
		C.radiation -= min(C.radiation, power)
		return 1
	return 0

datum/artifact_effect/proc/effect_healing(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/carbon/human/))
		var/mob/living/carbon/human/H = M
		if(power>=25)
			for(var/datum/organ/external/affecting in H.organs)
				if(!affecting)    continue
				if(!istype(affecting, /datum/organ/external))    continue
				affecting.heal_damage(power, power)
		M.adjustOxyLoss(-power)
		M.adjustToxLoss(-power)
		M.adjustBruteLoss(-power)
		M.adjustFireLoss(-power)
		M.adjustBrainLoss(-power)
		M.radiation -= min(M.radiation, power)
		if(!M:isCyborg())
			M.nutrition += power
		H.bodytemperature = initial(H.bodytemperature)
		//
		H.vessel.add_reagent("blood",2*power)
		spawn(1)
			H.fixblood()
		H.regenerate_icons()
		return 1
	//
	if (istype(M, /mob/living/carbon/monkey/))
		M.adjustOxyLoss(-power)
		M.adjustToxLoss(-power)
		M.adjustBruteLoss(-power)
		M.adjustFireLoss(-power)
		M.adjustBrainLoss(-power)
		return 1
	return 0

datum/artifact_effect/proc/effect_injure(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/carbon/))
		M.adjustOxyLoss(power)
		M.adjustToxLoss(power)
		M.adjustBruteLoss(power)
		M.adjustFireLoss(power)
		M.adjustBrainLoss(power)
		M.radiation += power
		if(ishuman(M) && !M:isCyborg())
			M.nutrition -= min(power*2, M.nutrition)
		M.stunned += sqrt(power)
		M.weakened += sqrt(power)
		return 1
	return 0

datum/artifact_effect/proc/effect_stun(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/carbon/))
		if(power>1) M.paralysis += 3*power
		M.stunned += 4*power
		M.weakened += 4*power
		M.stuttering += 4*power
		return 1
	return 0

datum/artifact_effect/proc/effect_roboheal(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/silicon/robot))
		M.adjustBruteLoss(-power)
		M.adjustFireLoss(-power)
		return 1
	return 0

datum/artifact_effect/proc/effect_robohurt(var/mob/living/M,var/power = 1)
	if (istype(M, /mob/living/silicon/robot))
		M.adjustBruteLoss(power)
		M.adjustFireLoss(power)
		return 1
	return 0

datum/artifact_effect/proc/effect_forcefield(var/mob/living/M,var/power = 1)
	while(created_field.len < 16)
		var/obj/effect/energy_field/E = new (locate(M.x,M.y,M.z))
		created_field.Add(E)
		E.strength = 1
		E.density = 1
		E.invisibility = 0
	forcefield_on = 1
	return 1

datum/artifact_effect/proc/effect_teleport(var/mob/M,var/range = 1)
	var/turf/Mturf = get_turf(M)
	if(!Mturf || Mturf.z==2) return
	var/list/randomturfs = new/list()
	for(var/turf/T in orange(M, range))
		if(istype(T, /turf/space) || T.density)
			continue
		var/dense = 0
		for(var/obj/S in T)
			if(T.density)
				dense = 1
				break
		if(!dense)
			randomturfs.Add(T)
	if(randomturfs.len > 0)
		M << "\red You are suddenly zapped away elsewhere!"
		if (M.buckled)
			M.buckled.unbuckle()
		var/obj/effect/effect/sparks/S = new(get_turf(M))
		spawn(15) del(S)
		M.loc = pick(randomturfs)
		var/datum/effect/effect/system/spark_spread/sparks = new /datum/effect/effect/system/spark_spread()
		sparks.set_up(3, 0, get_turf(M)) //no idea what the 0 is
		sparks.start()
	return 1

datum/artifact_effect/proc/effect_cellcharge(var/list/ranged,var/power = 1)
	for (var/obj/machinery/power/apc/C in ranged)
		for (var/obj/item/weapon/cell/B in C.contents)
			B.adjust_charge(power)
	for (var/obj/machinery/power/smes/S in ranged)
		S.charge = min(S.capacity,S.charge+2*power)
	for (var/obj/item/weapon/cell/C in ranged)
		C.adjust_charge(power)
	for (var/mob/living/silicon/robot/M in ranged)
		for (var/obj/item/weapon/cell/D in M.contents)
			D.adjust_charge(power)
			if(prob(10)) M << "\blue SYSTEM ALERT: Energy boosting field detected!"
	for (var/mob/living/carbon/human/M in ranged)
		if(M.isCyborg())
			if(!M.stat && prob(10)) M << "\blue SYSTEM ALERT: Energy boosting field detected!"
			M.nutrition += power
	return 1

datum/artifact_effect/proc/effect_celldrain(var/list/ranged,var/power = 1)
	for (var/obj/machinery/power/apc/C in ranged)
		for (var/obj/item/weapon/cell/B in C.contents)
			B.adjust_charge(-power)
	for (var/obj/machinery/power/smes/S in ranged)
		S.charge = max(S.charge-2*power,0)
	for (var/obj/item/weapon/cell/C in ranged)
		C.adjust_charge(-power)
	for (var/mob/living/silicon/robot/M in ranged)
		for (var/obj/item/weapon/cell/D in M.contents)
			D.adjust_charge(-power)
			if(prob(10)) M << "\red SYSTEM ALERT: Energy draining field detected!"
	for (var/mob/living/carbon/human/M in ranged)
		if(M.isCyborg())
			if(!M.stat && prob(10)) M << "\red SYSTEM ALERT: Energy draining field detected!"
			M.nutrition -= min(power*2, M.nutrition)
	return 1

datum/artifact_effect/proc/effect_planthelper(var/list/ranged,var/power = 1)
	for (var/obj/machinery/hydroponics/H in ranged)
		//makes weeds and shrooms and stuff more potent too
		if(H.planted)
			H.waterlevel = min(200,H.waterlevel+2*power)
			H.nutrilevel = min(200,H.nutrilevel+2*power)
			if(H.toxic > 0)
				H.toxic = max(0,H.toxic-power)
			H.health = min(100,H.health+power)
			if(H.pestlevel > 0)
				H.pestlevel = max(0,H.pestlevel-power)
			if(H.weedlevel > 0)
				H.weedlevel = max(0,H.weedlevel-power)
			H.lastcycle = max(H.lastcycle-4*power,0)
	return 1

datum/artifact_effect/proc/effect_gravpull(var/origin,var/range,var/power)
	var/location = get_turf(origin)
	if(power==1 && prob(75)) return
	for(var/obj/M in orange(range,location))
		var/gp = 1
		if(M.anchored || M == origin)
			gp = 0
			continue
		if(M.flags & CONDUCT || prob(20))
			gp++
		if(power == 2)
			gp++
		if(gp)
			var/i
			var/iter = rand(1,gp)
			for(i=0,i<iter,i++)
				spawn(i*5) step_towards(M,location)
