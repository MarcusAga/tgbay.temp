//
/obj/item/weapon/anobattery
	name = "Anomaly power battery"
	icon = 'anomaly.dmi'
	icon_state = "anobattery0"
	item_state = "cell"
	var/datum/artifact_effect/battery_effect
	var/capacity = 200
	var/stored_charge = 0

/obj/item/weapon/anobattery/New()
	battery_effect = new()

/obj/item/weapon/anobattery/proc/UpdateSprite()
	var/p = (stored_charge/capacity)*100
	icon_state = "anobattery[round(p,25)]"

/obj/item/device/assembly/anodevice
	name = "Anomaly power utilizer"
	icon = 'anomaly.dmi'
	icon_state = "anodev"
//	var/cooldown = 0
	var/activated = 0
	var/time = 50
	var/last_time = 50
	var/last_icostate = ""
	var/obj/item/weapon/anobattery/inserted_battery

	New()
		spawn(10)
			effect_pulse()

	update_icon()
		if(!inserted_battery)
			icon_state = "anodev"
		else
			var/p = (inserted_battery.stored_charge/inserted_battery.capacity)*100
			var/s = min(max(round(p,25),0),100)
			icon_state = "anodev[s]"
		if(icon_state!=last_icostate && holder)
			holder.update_icon()
		last_icostate = icon_state

	interact(var/mob/user)
		user.machine = src
		var/dat = "<b>Anomalous Materials Energy Utiliser</b><br>"
		if(activated)
			dat += "Device active, stand by.<BR>"
		else if(cooldown)
			dat += "Cooldown in progress, please wait.<BR>"
		else
			if(!inserted_battery)
				dat += "Please insert battery<BR>"
			else
				dat += "[inserted_battery] inserted, anomaly ID: [inserted_battery.battery_effect.artifact_id == "" ? "???" : "[inserted_battery.battery_effect.artifact_id]"]<BR>"
				dat += "<b>Total Power:</b> [inserted_battery.stored_charge]/[inserted_battery.capacity]<BR><BR>"
				dat += "<b>Timed activation:</b> <A href='?src=\ref[src];neg_changetime_max=-100'>--</a> <A href='?src=\ref[src];neg_changetime=-10'>-</a> [time >= 1000 ? "[time/10]" : time >= 100 ? " [time/10]" : "  [time/10]" ] <A href='?src=\ref[src];changetime=10'>+</a> <A href='?src=\ref[src];changetime_max=100'>++</a><BR>"
				if(cooldown && !activated)
					dat += "<font color=red>Cooldown in progress.</font><BR>"
				else if(activated)
					dat += "<A href='?src=\ref[src];stoptimer=1'>Stop timer</a><BR>"
				else
					dat += "<A href='?src=\ref[src];starttimer=1'>Start timer</a><BR>"
				dat += "<A href='?src=\ref[src];ejectbattery=1'>Eject battery</a><BR>"
		dat += "<A href='?src=\ref[src];refresh=1'>Refresh</a><BR>"
		dat += "<A href='?src=\ref[src];close=1'>Close</a><BR>"

		user << browse(dat, "window=anodevice;size=400x500")
		onclose(user, "anodevice")
		return

	attackby(var/obj/I as obj, var/mob/user as mob)
		if(istype(I, /obj/item/weapon/anobattery))
			if(!inserted_battery)
				user << "\blue You insert the battery."
				user.drop_item()
				I.loc = src
				inserted_battery = I
				update_icon()
		else
			return ..()

//	attack_ai(var/mob/user as mob)
//		return src.interact(user)

//	attack_paw(var/mob/user as mob)
//		return src.interact(user)

	attack_self(var/mob/user as mob)
		return src.interact(user)

//	attack_hand(var/mob/user as mob)
//		return src.interact(user)

	proc/effect_pulse()
		if(activated && inserted_battery)
			var/poweruse = 10 + rand(-1,1)
			if(inserted_battery && inserted_battery.battery_effect)
				if(inserted_battery.battery_effect.effectmode != "contact")
					poweruse += inserted_battery.battery_effect.aurarange
				else
					poweruse += 15
			if(time <= 0 || !inserted_battery)
				time = last_time
				activated = 0
				var/turf/T = get_turf(src)
				T.visible_message("\icon[src]\blue The utiliser device buzzes.", "\icon[src]\blue You hear something buzz.")
				if(inserted_battery && inserted_battery.battery_effect) inserted_battery.battery_effect.HaltEffect()
			else
				time -= 10
				if(inserted_battery.stored_charge>=poweruse && inserted_battery.battery_effect.DoEffect(src))
					inserted_battery.stored_charge -= poweruse
					if(inserted_battery.battery_effect.effectmode != "contact")
						cooldown += 10
						update_icon()
		else if(cooldown > 0)
			cooldown -= 10
			if(cooldown <= 0)
				cooldown = 0
				var/turf/T = get_turf(src)
				T.visible_message("\icon[src]\blue The utiliser device chimes.", "\icon[src]\blue You hear something chime.")

		spawn(10)
			effect_pulse()

	Topic(href, href_list)
		if(href_list["neg_changetime_max"])
			time += -100
			if(time > inserted_battery.capacity)
				time = inserted_battery.capacity
			else if (time < 0)
				time = 0
		if(href_list["neg_changetime"])
			time += -10
			if(time > inserted_battery.capacity)
				time = inserted_battery.capacity
			else if (time < 0)
				time = 0
		if(href_list["changetime"])
			time += 10
			if(time > inserted_battery.capacity)
				time = inserted_battery.capacity
			else if (time < 0)
				time = 0
		if(href_list["changetime_max"])
			time += 100
			if(time > inserted_battery.capacity)
				time = inserted_battery.capacity
			else if (time < 0)
				time = 0

		if(href_list["stoptimer"])
			activated = 0

		if(href_list["starttimer"])
			activate()
			if(activated)
				last_time = time

		if(href_list["ejectbattery"])
			inserted_battery.UpdateSprite()
			inserted_battery.loc = get_turf(src)
			usr.put_in_hands(inserted_battery)
			if(inserted_battery && inserted_battery.battery_effect) inserted_battery.battery_effect.HaltEffect()
			inserted_battery = null
			update_icon()

		if(href_list["close"])
			usr << browse(null, "window=anodevice")
			usr.machine = null
		else if(usr)
			src.interact(usr)

	activate()
		if(activated || !secured || (cooldown > 0))	return 0
		if(!inserted_battery) return 0
		if(!inserted_battery.battery_effect) return 0
		if(inserted_battery.battery_effect.effectmode == "contact" && inserted_battery.battery_effect.effecttype!="forcefield") return 0
		activated = 1
		last_time = time

	afterattack(atom/target as mob|obj|turf|area, mob/living/user as mob|obj, inrange, params)
		if(!inserted_battery || (inserted_battery.battery_effect.effectmode != "contact" || inserted_battery.battery_effect.effecttype=="forcefield")) return ..()
		if(cooldown > 0 || !istype(target,/mob)) return ..()
		var/poweruse = 10 + rand(-1,1)
		if(inserted_battery.stored_charge>=poweruse && inserted_battery.battery_effect.DoEffect(target))
			inserted_battery.stored_charge -= poweruse
			cooldown += 10
			update_icon()
		return ..()
