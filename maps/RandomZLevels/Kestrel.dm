//

/obj/effect/fakesingulo
	name = "Shield generator core"
	icon = 'icons/effects/falsegularity.dmi'
	icon_state = "singularity_s1"
	pixel_x=-8
	pixel_y=-8

/area/shuttle/Kestrel
	cabin
		name = "Cabin"
	cameras
		name = "Cameras"
	doors
		name = "Doors"
	shields
		name = "Shields"
	medbay
		name = "Med-bay"
	weapons
		name = "Weapons"
	engine
		name = "Engine"
	oxygen
		name = "Oxygen"
	teleport
		name = "Teleport"
	doorsl
		name = "Doors"
	doorst
		name = "Doors"
	doorsb
		name = "Doors"
	empty1
		name = "Empty"
	empty2
		name = "Empty"
	empty3
		name = "Empty"
	empty4
		name = "Empty"
	empty5
		name = "Empty"
